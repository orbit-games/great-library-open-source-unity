# The Great Library - Unity

This is the Unity/Game front-end of the Great Library game based learning environment.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

You need to have Unity 2019.2+ installed.

### Installing

1. Checkout the repository
2. Install [Image Warp](https://assetstore.unity.com/packages/tools/gui/image-warp-84984) in the `Assets/Extensions` folder.
3. Install a slightly modified version of [Paroxe PDF Renderer](https://assetstore.unity.com/packages/tools/gui/pdf-renderer-32815) in the `Assets/Extensions` folder.
   To modify the original plugin, add the following method to `Paroxe\PDFRenderer\API\PDFViewer.cs`:
   ```csharp
        public void GoToPagePoint(int pageIndex, Vector2 pointPercent)
        {
            if (pageIndex < 0)
            {
                pageIndex = 0;
            }
            else if (pageIndex > m_PageCount - 1)
            {
                pageIndex = m_PageCount - 1;
            }

            m_Internal.m_PageInputField.text = (pageIndex + 1).ToString();
            m_Internal.m_PageContainer.anchoredPosition = new Vector2(m_Internal.m_PageContainer.anchoredPosition.x,
                m_PageOffsets[pageIndex]
                - m_PageSizes[pageIndex].y * 0.5f
                + Mathf.Max(0f, m_PageSizes[pageIndex].y * pointPercent.y - 200f));
            m_Internal.m_PageContainer.anchoredPosition -= m_VerticalMarginBetweenPages * Vector2.up;

            SetPageCountLabel(pageIndex, m_PageCount);

            EnsureValidPageContainerPosition();
        }
   ```
   And, in the same file, replace all instances of:
   ```
   StartCoroutine(
   ```
    by 
    ```
    GLRun.Coroutine(
    ```
4. Copy the `Assets\Utilities\Cryptography\GLSecretKeysExample.cs` to `Assets\Utilities\Cryptography\GLSecretKeys.cs`.
For Orbit Games, the secret keys used in production can be found in the shared Google Drive under:  `TU Delft - The Great Library\Phase 2 Pilot\Dependencies\GLSecretKeys.zip`.
5. Open the `Game.unity` scene in Unity and press the play button.

After completing these steps, you should be able to run the game, using the back-end running under `https://greatlibrary.orbitgames.nl`. 

## Settings

Configuration of the Great Library.

### API settings

You can configure which URL/back-end to use in the API System singleton (`GLApi.cs`). In the `Game.unity` scene hierarchy, navigate to `Systems/Api System`. Here you can find a "Debug" and a "Live" path. The "Debug" path will be be used in the Editor or when using a debug/develop build. The "Live" path will be used for production builds. You can force the use of the live path by checking the `Force Use Live` checkbox.

## Maintenance

Help/tips for jobs that may be needed as part of maintenance

### Updating the API Spec

The Backend API is specified using [OpenAPI 3.0](https://swagger.io/specification/). The specifications are part of [Great Library Backend repository](https://bitbucket.org/orbit-games/your-great-library-backend/src/master/resources/your-great-library-api-spec.yml). A [seperate repository](https://bitbucket.org/orbit-games/your-great-library-csharp-client-generated) is used to track changes in the generated C# client.

The following steps should be taken to upgrade the generated Client API:
1. For historic reasons, the C# Client is generated using [version 2.x of the Swagger Codegen](https://github.com/swagger-api/swagger-codegen). In order to use this version, the OpenAPI 3.0 spec needs to be converted to a Swagger 2.0 step first. To do this, one may use the [Apimatic online converter](https://www.apimatic.io/), which is free after you login. Select the transformer, and choose output format `OpenAPI/Swagger v2.0 (YAML)`.
2. There are two ways to generate the C# client from the Swagger v2.0 Spec:
   - *Online*: Copy the Swagger v2.0 spec into the [Swagger editor](http://editor.swagger.io/). In the top menu bar, select `Generate Client > csharp`. 
   - *Offline*: Download the latest version of the `swagger-codegen-cli.jar` 2.x, e.g. from [Maven Central](https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.8/swagger-codegen-cli-2.4.8.jar). Then run the following command:
   ```sh
   java -jar swagger-codegen-cli-2.4.8.jar generate -i your-great-library-api-spec.yml-SwaggerYaml.yaml -l csharp -o csharp-client-generated/
   ```
3. Checkout the [C# Generated Client repository](https://bitbucket.org/orbit-games/your-great-library-csharp-client-generated), replace the working directory contents with the output of the client generation, and check was has changed. If all seems okay, create a new commit on this repository.
4. Copy the contents of the generated `your-great-library-csharp-client-generated/src/IO.Swagger/Model` to `Assets/API/Model`. Note that this will cause compilation errors on enums in Unity, as some of them are (unfortunately) generated wrongly. To fix this, replace the  `VALUE = VALUE` syntax by just `VALUE`, e.g. in the `QuestionType.cs` enum, replace: `SELECTMULTIPLE = SELECT_MULTIPLE` by `SELECT_MULTIPLE`.
5. Copy the contents of the generated `your-great-library-csharp-client-generated/src/IO.Swagger/Client` to `Assets/API/Client`. After copying, replace the `ApiClient.CallApiAsync()` method contents with the following (note the added `GLApi.Log...` calls): 
    ```cs
    var request = PrepareRequest(
                    path, method, queryParams, postBody, headerParams, formParams, fileParams,
                    pathParams, contentType);

    GLApi.LogCallRawApi(path, method, queryParams, postBody, headerParams, formParams, fileParams, pathParams, contentType);

    InterceptRequest(request);
    var response = await RestClient.ExecuteTaskAsync(request);
    InterceptResponse(request, response);

    GLApi.LogResponseCallRawApi(response, path, method, queryParams, postBody, headerParams, formParams, fileParams, pathParams, contentType);

    return (Object)response;
    ```
   You will probably also get a compiler error in the `ApiClient.PrepareRequest()` method. This can be fixed by replacing:
    ```cs
    request.AddFile(param.Value.Name, param.Value.Writer, param.Value.FileName, param.Value.ContentType);
    ```
    By (adding the content length parameter)
    ```
    request.AddFile(param.Value.Name, param.Value.Writer, param.Value.FileName, param.Value.ContentLength, param.Value.ContentType);
    ```

    Finally, you will probably have a change in the `Configuration.DefaultExceptionFactory`, where for statuscode 400, it returns:
    ```cs
    return new ApiException(status,
                    string.Format("Error calling {0}: {1}", methodName, response.Content),
                    response.Content);
            }
    ```
    Replace this with:
    ```cs
    return new ApiException(status,
                    string.Format("Error calling {0}: {1}", methodName, response.Content),
                    response);
            }
    ```
    Probably you will be left with no changes in the `Client` folder of the generated API. If there are changes, make sure to test the affected areas.

6. Replace `Assets/API/Api/GLRawGameApi.cs` by `your-great-library-csharp-client-generated/src/IO.Swagger/Api/GLRawGameApi.cs`.
   
   The generated client has an error in correctly adding file parameters (such as the submitted document in the "submission" review step). To fix this, replace all method arguments like `List<byte[]> file` by `List<System.IO.Stream> file`. And then replace:
   ```cs
   if (file != null) localVarFormParams.Add("file", this.Configuration.ApiClient.ParameterToString(file)); // form parameter
   ```
   By
   ```cs
   if (file != null) localVarFileParams.Add("file", Configuration.ApiClient.ParameterToFile("file", file[0])); // form parameter
   ```

7. You should now have the updated API available. You can do a quick smoke check by:

   1. Checking if you get a nice error message when entering an invalid username/password combination
   2. Logging in and submitting a document
   3. Opening/viewing the submitted document



## Authors

* **Olivier Hokke** - *Main developer*
* **Bas Dado** - *API/backend development and integration*

## License

This project is licensed under the the 2-Clause BSD License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [TU Delft](https://www.tudelft.nl/)
* [SURF](https://www.surf.nl/)