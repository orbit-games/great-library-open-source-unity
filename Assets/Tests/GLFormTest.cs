﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLFormTest : MonoBehaviour {

    [Buttons("test1","test1","test2","test2","test3","test3")]
    public ButtonsContainer tests;
    public GLForm form;

    public void test1()
    {
        GLForm.Setup setup = new GLForm.Setup();
        setup.AddElement(new GLForm.TitleElement()
        {
            labelText = ("Form test 1")
        });
        setup.AddElement(new GLForm.TextAreaElement()
        {
            labelText = ("This is a text area"),
            hintText = ("Bliep bloep"),
            characterLimit = 100,
            contentType = TMPro.TMP_InputField.ContentType.Standard,
            preferredHeight = 50,
            preferredWidth = 1000,
            variableName = "bliep"
        });
        setup.AddElement(new GLForm.PlainTextElement()
        {
            labelText = ("hello, I am a grumpy old piece of text wanting to be read.")
        });

        var elementsGroup = setup.AddElement(new GLForm.ElementsGroup() { });

        elementsGroup.AddElement(new GLForm.ButtonElement()
        {
            buttonType = GLForm.ButtonType.Submit,
            labelText = ("submit"),
            onPress = () => { Debug.Log("Pressed the submit button :D"); }
        });

        elementsGroup.AddElement(new GLForm.ButtonElement()
        {
            buttonType = GLForm.ButtonType.Reset,
            labelText = ("reset"),
            onPress = () => { Debug.Log("Pressed the reset button :D"); }
        });

        setup.onReset = () =>
        {
            Debug.Log("YES A RESET");
        };

        setup.onSubmit = (GLForm.Data data) =>
        {
            Debug.Log("You submitted the following data: " + data);
        };

        form.SetupForm(setup);
    }

    public void test2()
    {
        GLForm.Setup setup = new GLForm.Setup();
        setup.AddElement(new GLForm.TitleElement()
        {
            labelText = ("Form test 2")
        });
        setup.AddElement(new GLForm.TextAreaElement()
        {
            labelText = ("Another test area"),
            hintText = ("Bwaaaaaaaap"),
            characterLimit = 40,
            contentType = TMPro.TMP_InputField.ContentType.Standard,
            preferredHeight = 150,
            preferredWidth = 150,
            variableName = "bliep"
        });
        setup.AddElement(new GLForm.PlainTextElement()
        {
            labelText = ("The more text you read, the more your brain has read text. It's as simple as that.")
        });

        var radiobuttons = setup.AddElement(new GLForm.RadioButtonsElement()
        {
            variableName = "radio",
            defaultValue = "radioA",
            labelText = ("Well let's just pick something here, will ya?")
        });
        radiobuttons.AddElement(new GLForm.RadioButtonOptionElement()
        {
            labelText = ("This is radio A"),
            optionValue = "radioA",
            optionPoints = 100
        });
        radiobuttons.AddElement(new GLForm.RadioButtonOptionElement()
        {
            labelText = ("This is radio B"),
            optionValue = "radioB",
            optionPoints = 200
        });
        radiobuttons.AddElement(new GLForm.RadioButtonOptionElement()
        {
            labelText = ("This is radio C"),
            optionValue = "radioC",
            optionPoints = 300
        });

        var dropdown = setup.AddElement(new GLForm.DropdownElement()
        {
            variableName = "dropdown",
            defaultValue = "dropdown30",
            labelText = ("Open this fellow for options so mellow")
        });
        for (int i = 0; i < 100; i++)
        {
            dropdown.AddElement(new GLForm.DropdownOptionElement()
            {
                labelText = ("Option #" + i),
                optionValue = "dropdown" + i,
                optionPoints = 10
            });
        }

        var checkboxes = setup.AddElement(new GLForm.CheckboxesElement()
        {
            labelText = ("Checkboxes are always nice to tick")
        });
        checkboxes.AddElement(new GLForm.CheckboxOptionElement()
        {
            labelText = ("This is checkbox A"),
            variableName = "checkboxA",
            defaultValue = bool.FalseString,
            optionPoints = 1
        });
        checkboxes.AddElement(new GLForm.CheckboxOptionElement()
        {
            labelText = ("This is checkbox B"),
            variableName = "checkboxB",
            defaultValue = bool.FalseString,
            optionPoints = 2
        });
        checkboxes.AddElement(new GLForm.CheckboxOptionElement()
        {
            labelText = ("This is checkbox C"),
            variableName = "checkboxC",
            defaultValue = bool.TrueString,
            optionPoints = 3
        });
        checkboxes.AddElement(new GLForm.CheckboxOptionElement()
        {
            labelText = ("This is checkbox D"),
            variableName = "checkboxD",
            defaultValue = bool.FalseString,
            optionPoints = 4
        });

        var elementsGroup = setup.AddElement(new GLForm.ElementsGroup() { });

        elementsGroup.AddElement(new GLForm.ButtonElement()
        {
            buttonType = GLForm.ButtonType.Submit,
            labelText = ("submit"),
            onPress = () => { Debug.Log("Pressed the submit button :D"); }
        });

        elementsGroup.AddElement(new GLForm.ButtonElement()
        {
            buttonType = GLForm.ButtonType.Reset,
            labelText = ("reset"),
            onPress = () => { Debug.Log("Pressed the reset button :D"); }
        });

        setup.onReset = () =>
        {
            Debug.Log("YES A RESET");
        };

        setup.onSubmit = (GLForm.Data data) =>
        {
            Debug.Log("You submitted the following data: " + data);
        };

        form.SetupForm(setup);
    }
}
