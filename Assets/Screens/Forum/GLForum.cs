﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ForumTab
{
    Direct, Topics, Group, Special, All
}

public class GLForum : GLSingletonBehaviour<GLForum>
{
    protected override void OnSingletonInitialize()
    {
        GLApi.RegisterCourseSelectedListener(OnCourseSelected);

        forumTabs.Add(new GLTab.Settings()
        {
            id = (int)ForumTab.Direct,
            title = Label_DirectTab,
            onClick = () => { OpenTab(ForumTab.Direct); }
        });
        forumTabs.Add(new GLTab.Settings()
        {
            id = (int)ForumTab.Topics,
            title = Label_TopicsTab,
            onClick = () => { OpenTab(ForumTab.Topics); }
        });
        forumTabs.Add(new GLTab.Settings()
        {
            id = (int)ForumTab.Special,
            title = Label_SpecialTab,
            onClick = () => { OpenTab(ForumTab.Special); }
        });
        forumTabs.Add(new GLTab.Settings()
        {
            id = (int)ForumTab.All,
            title = Label_AllTab,
            onClick = () => { OpenTab(ForumTab.All); }
        });
    }

    private List<GLTab.Settings> forumTabs = new List<GLTab.Settings>();
    private ForumTab selectedTab = ForumTab.All;

    [Header("Localization")]
    public LocalizedText Error_ConversationNotFound;
    public LocalizedText Label_DirectTab;
    public LocalizedText Label_TopicsTab;
    public LocalizedText Label_GroupsTab;
    public LocalizedText Label_SpecialTab;
    public LocalizedText Label_AllTab;

    [Header("References")]
    public GLPoolManagedList conversationItemList;
    public GLPoolManagedList tabsItemList;
    public GLConversation conversation;
    
    private string activeConversation;
    public bool IsActiveConversation(string conversationRef)
    {
        return activeConversation == conversationRef;
    }

    private void OnCourseSelected(Course course)
    {
        conversationItemList.Clear();
        conversation.Clear();
    }

    public void OnEnable()
    {
        GLConversations.I.OnConversationShown();
        GLApi.RegisterKnowledgeListener<Conversations>(OnConversationsUpdate);
        GLApi.RegisterKnowledgeListener<Conversation>(OnConversationUpdate);

        if (activeConversation == null)
        {
            var allConversationsInsight = GLInsights.GetAllConversationsInsight();
            if (allConversationsInsight.conversationInsights.Count > 0)
            {
                OpenConversation(allConversationsInsight.conversationInsights[0].conversation.Ref);
            }
        }

        BuildForumConversationsList();
    }

    private void OnDisable()
    {
        GLConversations.I?.OnConversationHidden();
        GLApi.RemoveKnowledgeListener<Conversations>(OnConversationsUpdate);
        GLApi.RemoveKnowledgeListener<Conversation>(OnConversationUpdate);
    }

    public void OpenTab(ForumTab tab)
    {
        selectedTab = tab;
        BuildForumConversationsList();
    }

    private void OnConversationsUpdate(Conversations conversations)
    {
        BuildForumConversationsList();
    }
    private void OnConversationUpdate(Conversation conversation)
    {
        BuildForumConversationsList();
    }

    public void BuildForumConversationsList()
    {
        if (!GLApi.IsLoggedInAndCourseSelected()) return;

        var allConversationsInsight = GLInsights.GetAllConversationsInsight();

        tabsItemList.SyncFully<GLTab.Settings, GLTab>(forumTabs,
            t => t.id, (settings, tab) => {
                tab.Setup(settings, settings.id == (int)selectedTab, allConversationsInsight.tabUnreadMessagesCount[(ForumTab)settings.id]);
            });

        var filteredConversations = allConversationsInsight.conversationInsights.AsEnumerable();
        if (selectedTab != ForumTab.All)
        {
            filteredConversations = allConversationsInsight.conversationInsights.Where(insight => insight.forumTab == selectedTab);
        }

        conversationItemList.SyncNewAndRemovedOnly<ConversationInsight, GLConversationItem>(
            filteredConversations, i => i.conversation.Ref, (item, instance) => instance.Setup(item.conversation.Ref));
    }

    public void OpenConversationWithUser(string userRef)
    {
        OpenTab(ForumTab.Direct);
        var allConversationsInsight = GLInsights.GetAllConversationsInsight();
        var conversationInsight = allConversationsInsight.userToDirectConversationInsightMapping.GetOrDefault(userRef);
        if (conversationInsight != null)
        {
            OpenConversation(conversationInsight.conversation.Ref);
        }
        else
        {
            GLPopup.MakeErrorPopup(Error_ConversationNotFound).Show();
        }
    }

    public void OpenConversation(string conversationRef)
    {
        activeConversation = conversationRef;
        conversation.OpenConversation(conversationRef);
    }

}
