﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TMPro;
using UnityEngine;

public class GLChatDateHeader : MonoBehaviour {

    public GameObject dateContainer;
    public TMP_Text textField;
    private IEnumerable<MonoBehaviour> dateItems;

    public void SetDateItemsList(IEnumerable<MonoBehaviour> dateItems)
    {
        this.dateItems = dateItems;
    }

    private void LateUpdate()
    {
        if (dateItems == null)
        {
            dateContainer.SetActive(false);
            return;
        }

        float closestAboveY = float.MaxValue;
        MonoBehaviour closestAboveItem = null;
        var thisY = transform.position.y;
        foreach (var dateItem in dateItems)
        {
            var thatY = dateItem.transform.position.y;
            if (thisY < thatY && thatY < closestAboveY)
            {
                closestAboveY = thatY;
                closestAboveItem = dateItem;
            }
        }

        if (closestAboveItem != null)
        {
            var infoItem = closestAboveItem.GetComponent<GLChatInfoItem>();
            if (infoItem != null)
            {
                textField.text = infoItem.messageBody.text;
                dateContainer.SetActive(true);
            }
            else
            {
                dateContainer.SetActive(false);
            }
        }
        else
        {
            dateContainer.SetActive(false);
        }
    }
}
