﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLTab : MonoBehaviour
{
    [System.Serializable]
    public class Settings
    {
        public int id;
        public GLFormattedLocalizedText title;
        public Action onClick;
    }

    [Header("References")]
    public LocalizedTextBehaviour label;
    public GameObject unreadCountContainer;
    public LocalizedTextBehaviour unreadCountLabel;

    [Header("Settings")]
    public float tabSelectedHeight;
    public float tabUnselectedHeight;

    private Settings settings;

    public void Setup(Settings settings, bool selected, int unreadCount)
    {
        this.settings = settings;
        GLColorState.BroadcastColorState(this, selected);
        var sizeDelta = (transform as RectTransform).sizeDelta;
        sizeDelta.y = selected ? tabSelectedHeight : tabUnselectedHeight;
        (transform as RectTransform).sizeDelta = sizeDelta;

        unreadCountContainer.SetActive(unreadCount != 0);
        unreadCountLabel.FormattedAsset = unreadCount.ToString();

        label.FormattedAsset = settings.title;
    }

    public void OnClick()
    {
        settings?.onClick?.Invoke();
    }
}
