﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLTinyOwlDummy : MonoBehaviour {
    
    private void OnEnable()
    {
        GLTinyOwl.I.owlContainer.position = transform.position;
    }
}
