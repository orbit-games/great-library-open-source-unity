﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLTinyOwl : GLSingletonBehaviour<GLTinyOwl> {

    [Header("Settings")]
    public string animationTriggerParameter = "unreadCountChanged";
    public string animationUnreadParameter = "unreadCount";
    public string animationInstantHideParameter = "instantHide";

    [Buttons("In", "DebugIn", "Out", "DebugOut", "HideInstant", "InstantHide")]
    public ButtonsContainer buttons;

    [Header("References")]
    public Transform owlContainer;
    public GameObject button;
    public TMP_Text unreadCount;
    public Animator animator;

    public GameObject tempObject;
    public TMP_Text tempUnreadCount;

    private int unreadMessagesCount;

    public void DebugIn()
    {
        SetNewUnreadCount(1 + Mathf.RoundToInt(Mathf.Pow(Random.value * 10f, 3f)));
    }

    public void DebugOut()
    {
        SetNewUnreadCount(0);
    }

    public void HideInstant()
    {
        OnLogout();
    }

    protected override void OnSingletonInitialize()
    {
        OnLogout();
        GLApi.RegisterKnowledgeListener<Conversations>(OnConversationsUpdated);
        GLApi.RegisterKnowledgeListener<Conversation>(OnConversationUpdated);
        GLApi.RegisterLogoutListener(OnLogout);
    }

    private void OnLogout()
    {
        unreadMessagesCount = 0;
        animator.SetInteger(animationUnreadParameter, unreadMessagesCount);
        animator.SetTrigger(animationInstantHideParameter);

        tempUnreadCount.text = unreadMessagesCount.ToString();
        tempObject.SetActive(unreadMessagesCount > 0);

        button.SetActive(false);
    }

    private void OnConversationUpdated(Conversation conversation)
    {
        if (!GLApi.IsLoggedInAndCourseSelected()) return;

        SetNewUnreadCount(DetermineUnreadCountForUnopenedConversations());
    }

    private void OnConversationsUpdated(Conversations conversations)
    {
        if (!GLApi.IsLoggedInAndCourseSelected()) return;

        SetNewUnreadCount(DetermineUnreadCountForUnopenedConversations());
    }

    private int DetermineUnreadCountForUnopenedConversations()
    {
        var allConversationsInsight = GLInsights.GetAllConversationsInsight();
        int unreadCount = allConversationsInsight.unreadMessagesCount;
        foreach (var conversationInsight in allConversationsInsight.unreadConversationInsights)
        {
            if (GLConversation.IsCurrentlyOpened(conversationInsight.conversation.Ref))
            {
                unreadCount -= conversationInsight.unreadMessagesCount;
            }
        }
        return unreadCount;
    }

    public void SetNewUnreadCount(int count)
    {
        if (count == unreadMessagesCount) return;

        unreadMessagesCount = count;
        animator.SetInteger(animationUnreadParameter, unreadMessagesCount);
        animator.SetTrigger(animationTriggerParameter);

        tempUnreadCount.text = unreadMessagesCount.ToString();
        tempObject.SetActive(unreadMessagesCount > 0);

        button.SetActive(unreadMessagesCount != 0);
    }

    public void DisplayNextUnreadCount()
    {
        unreadCount.text = unreadMessagesCount.ToString();
    }

    public void OpenUnreadConversation()
    {
        var allConversationsInsight = GLInsights.GetAllConversationsInsight();
        if (allConversationsInsight.unreadConversationInsights.Count > 0)
        {
            GLGameFlow.I.ShowChatWithConversation(allConversationsInsight.unreadConversationInsights[0].conversation.Ref);
        }
        else
        {
            GLGameFlow.I.ShowChat();
        }
    }

}
