﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLChatMessageItem : MonoBehaviour {

    [Header("Settings")]
    public int sidePadding = 75;

    [Header("References")]
    public TMP_Text messageBody;
    public TMP_Text messageTime;
    public GameObject messageTimeContainer;
    public TMP_Text messageSender;
    public LayoutGroup layoutGroup;
    public RectTransform[] movableRectTransforms;

    private string messageRef;

    public void Setup(string conversationMessageRef, bool showName = true, bool showTime = true)
    {
        var message = GLApi.GetConversationMessageKnowledge(conversationMessageRef);

        messageBody.text = message.Message;
        messageTime.text = message.Sent.Value.ToShortTimeString();
        messageSender.gameObject.SetActive(showName);
        messageTimeContainer.gameObject.SetActive(showTime);

        var course = GLApi.GetActiveCourse();
        var courseProgressSummary = GLApi.GetCourseProgressSummaryKnowledge(message.Sender, course.Ref);
        if (courseProgressSummary != null)
        {
            messageSender.text = courseProgressSummary.DisplayName;
        }
        else
        {
            Debug.LogWarning("Handle by reloading course progress?");
            messageSender.text = "";
        }

        var user = GLApi.GetActiveUser();
        var padding = layoutGroup.padding;
        padding.left = message.Sender == user.Ref ? sidePadding : 0;
        padding.right = message.Sender != user.Ref ? sidePadding : 0;
        layoutGroup.childAlignment = message.Sender == user.Ref ? TextAnchor.UpperRight : TextAnchor.UpperLeft;
        layoutGroup.padding = padding;

        if (message.Sender == user.Ref)
        {
            foreach (var rect in movableRectTransforms)
            {
                rect.pivot = new Vector2(1, rect.pivot.y);
                rect.anchorMin = new Vector2(1, rect.anchorMin.y);
                rect.anchorMax = new Vector2(1, rect.anchorMax.y);
                rect.anchoredPosition = new Vector2(-Mathf.Abs(rect.anchoredPosition.x), rect.anchoredPosition.y);
            }
        }
        else
        {
            foreach (var rect in movableRectTransforms)
            {
                rect.pivot = new Vector2(0, rect.pivot.y);
                rect.anchorMin = new Vector2(0, rect.anchorMin.y);
                rect.anchorMax = new Vector2(0, rect.anchorMax.y);
                rect.anchoredPosition = new Vector2(Mathf.Abs(rect.anchoredPosition.x), rect.anchoredPosition.y);
            }
        }
    }
}
