﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLConversation : GLSingletonBehaviour<GLConversation> {

    private static string openedConversationRef;
    public static bool IsCurrentlyOpened(string conversationRef)
    {
        return conversationRef == openedConversationRef;
    }
    public static bool IsCurrentlyChatting()
    {
        var eventSystem = UnityEngine.EventSystems.EventSystem.current;
        return eventSystem.currentSelectedGameObject == I.textInput.gameObject;
    }

    protected override void OnSingletonInitialize()
    {
        dynamicList.AddPrefab<ConversationMessage, GLChatMessageItem>(
            "message", messageItemPrefab, state => state.CurrentItem.Ref,
            (instance, state) => instance.Setup(state.CurrentItem.Ref, isGroupConversation && state.CurrentItem.Sender != currentUserRef));

        dynamicList.AddPrefab<ConversationMessage, GLChatInfoItem>(
            "date", infoItemPrefab, state => state.CurrentItem.Ref,
            (instance, state) => instance.Setup(state.CurrentItem.Sent.Value.ToShortDateString()));

        dynamicList.SetPrefabPlacer<ConversationMessage>(builder =>
        {
            if (builder.PreviousItem == null)
            {
                builder.PlacePrefab("date");
            }
            else
            {
                var prevSent = builder.PreviousItem.Sent.Value;
                var nowSent = builder.CurrentItem.Sent.Value;
                if (prevSent.DayOfYear != nowSent.DayOfYear || prevSent.Year != nowSent.Year)
                {
                    builder.PlacePrefab("date");
                }
            }

            builder.PlacePrefab("message");
        });

        dateHeader.SetDateItemsList(dynamicList.GetReadOnlyInstancesCollection("date"));
    }

    [Header("References")]
    public ScrollRect messagesScrollRect;
    public RectTransform conversationRect;
    public RectTransform contentContainer;
    public RectTransform messagesContainer;
    public TMP_InputField textInput;
    public GLConversationItem conversationHeader;
    //public GLPoolManagedList messagesList;
    public GLPoolDynamicList dynamicList;
    public GLChatDateHeader dateHeader;

    public GameObject inputContainer;
    public GameObject noWritePermissionsContainer;

    public GameObject retrievingMoreMessagesContainer;
    public GLLoading retrievingMoreMessagesLoader;

    public GameObject sendingMessageContainer;
    public GLLoading sendingMessageLoader;

    [Header("References")]
    public GLChatMessageItem messageItemPrefab;
    public GLChatInfoItem infoItemPrefab;

    private string conversationRef;
    private bool retrievingMoreMessages;
    private bool scrolledToTop;
    private int sendingMessages = 0;

    private bool isGroupConversation;
    private string currentUserRef;

    private void Awake()
    {
        textInput.onSubmit.AddListener((string text) => {
            SendMessage();
        });

        retrievingMoreMessagesContainer.SetActive(false);
        sendingMessageContainer.SetActive(false);
    }

    private void OnEnable()
    {
        GLConversations.I.OnConversationShown();
        GLApi.RegisterKnowledgeListener<Conversation>(OnConversationUpdate);
        openedConversationRef = conversationRef;
        MarkAsReadIfNeeded();
    }

    private void OnDisable()
    {
        GLConversations.I?.OnConversationHidden();
        GLApi.RemoveKnowledgeListener<Conversation>(OnConversationUpdate);
        openedConversationRef = null;
    }

    public void Clear()
    {
        dynamicList.Clear();
    }

    private void OnConversationUpdate(Conversation conversation)
    {
        if (conversation.Ref == conversationRef)
        {
            OpenConversation(conversation.Ref);
        }
    }

    public void OpenConversation(string conversationRef)
    {   
        if (conversationRef != this.conversationRef)
        {
            messagesContainer.anchoredPosition = new Vector2(messagesContainer.anchoredPosition.x, 0);
        }

        openedConversationRef = conversationRef;
        this.conversationRef = conversationRef;
        
        RebuildConversation();

        SelectInputFieldWhenNotSelecting();

        MarkAsReadIfNeeded();
    }

    public void MarkAsReadIfNeeded()
    {
        var conversation = GLApi.GetConversationKnowledge(conversationRef);
        if (conversation == null) return;

        var conversationInsight = conversation.GetInsight();
        if (conversationInsight.unreadMessagesCount > 0)
        {
            var course = GLApi.GetActiveCourse();
            var lastMessage = conversation.Messages[conversation.Messages.Count - 1];
            GLApi.MarkConversationAsRead(course.Ref, conversationRef, lastMessage.Ind.Value);
        }
    }

    public void RebuildConversation()
    {
        conversationHeader.Setup(conversationRef);

        // manage messages list
        var conversation = GLApi.GetConversationKnowledge(conversationRef);
        var conversationInsight = conversation.GetInsight();
        var userRef = GLApi.GetActiveUser().Ref;

        isGroupConversation = conversation.Type != ConversationType.DIRECT;
        currentUserRef = userRef;

        dynamicList.Sync<ConversationMessage, GLChatMessageItem>(conversation.Messages);

        // determining if it is allowed to send messages
        inputContainer.SetActive(conversationInsight.canLoggedInUserWriteMessages);
        noWritePermissionsContainer.SetActive(!conversationInsight.canLoggedInUserWriteMessages);
    }

    public void Update()
    {
        if (messagesScrollRect.verticalNormalizedPosition > 0.999f)
        {
            scrolledToTop = true;
        }
        else
        {
            scrolledToTop = false;
        }

        if (conversationRef.IsNullOrEmpty()) return;
        if (retrievingMoreMessages) return;
        if (!HasIncompleteMessagesList()) return;

        if (scrolledToTop)
        {
            GetMoreMessagesIfNeeded();
        }
        else if (contentContainer.rect.height < conversationRect.rect.height)
        {
            GetMoreMessagesIfNeeded();
        }
    }

    private bool HasIncompleteMessagesList()
    {
        var conversation = GLApi.GetConversationKnowledge(conversationRef);
        return conversation.MessageCount.Value != conversation.Messages.Count;
    }

    private void GetMoreMessagesIfNeeded()
    {
        var conversation = GLApi.GetConversationKnowledge(conversationRef);
        if (HasIncompleteMessagesList())
        {
            retrievingMoreMessages = true;

            long skip = 0;
            if (conversation.Messages.Count > 0)
            {
                skip = conversation.MessageCount.Value - conversation.Messages[0].Ind.Value;
            }

            var course = GLApi.GetActiveCourse();
            retrievingMoreMessagesContainer.SetActive(true);
            retrievingMoreMessagesLoader.AddTask(
                    GLApi.FetchConversation(course.Ref, conversationRef, null, 50, (int)skip),
                    OnRetrieveMoreMessagesReply
                );
        }
    }

    private void OnRetrieveMoreMessagesReply(GLTaskResult result)
    {
        retrievingMoreMessages = false;
        retrievingMoreMessagesContainer.SetActive(false);
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            GLForum.I.BuildForumConversationsList();
            RebuildConversation();
        }
    }

    public void SendMessage()
    {
        var text = textInput?.text?.Trim();
        if (text.IsNullOrEmpty())
        {
            SelectInputFieldWhenNotSelecting();
            return;
        }

        var course = GLApi.GetActiveCourse();
        textInput.text = "";

        sendingMessages++;
        sendingMessageContainer.SetActive(true);

        var task = GLApi.SendChatMessage(course.Ref, conversationRef, 
            new IO.Swagger.Model.PostConversationMessageRequest(text)).AddOnCompletedListener(OnSendMessageReply);

        sendingMessageLoader.AddTask(task, null);
        SelectInputFieldWhenNotSelecting();
    }

    public void OnSendMessageReply(GLTaskResult result)
    {
        sendingMessages--;
        sendingMessageContainer.SetActive(sendingMessages != 0);
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            OpenConversation(conversationRef);
            SelectInputFieldWhenNotSelecting();
        }
    }

    private void SelectInputFieldWhenNotSelecting()
    {
        var eventSystem = UnityEngine.EventSystems.EventSystem.current;
        if (!eventSystem.alreadySelecting)
        {
            textInput.ActivateInputField();
            textInput.Select();
        }
    }
}
