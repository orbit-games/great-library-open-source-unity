﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLChatInfoItem : MonoBehaviour {
    
    [Header("References")]
    public TMP_Text messageBody;

    private string messageRef;

    public void Setup(string infoMessage)
    {
        messageBody.text = infoMessage;
    }
}
