﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLDirectChatButton : MonoBehaviour {

    private string chatWithUserRef;

    public void Setup(string chatWithUserRef)
    {
        var user = GLApi.GetActiveUser();

        if (chatWithUserRef != null && user?.Ref != chatWithUserRef)
        {
            gameObject.SetActive(true);
            this.chatWithUserRef = chatWithUserRef;
        }
        else
        {
            gameObject.SetActive(false);
            this.chatWithUserRef = null;
        }
    }

    public void OnPress()
    {
        GLGameFlow.I.ShowChatWithUser(chatWithUserRef);
    }
}
