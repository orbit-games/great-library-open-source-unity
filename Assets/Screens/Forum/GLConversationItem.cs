﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class GLConversationItem : MonoBehaviour, IPoolEventsListener {

    [Header("Settings")]
    public bool showTime = true;

    [Header("Localization")]
    public LocalizedText Label_EmptyConversation;
    public LocalizedText Label_UntitledGroup;
    public LocalizedText Label_UnidentifiedPerson;
    public LocalizedText Label_NoTopic;

    [Space(10f)]
    public LocalizedText Label_YouAndDevelopers;
    public LocalizedText Label_YouAndTeachers;
    public LocalizedText Description_Announcements;
    public LocalizedText Description_YouAndDevelopers;
    public LocalizedText Description_YouAndTeachers;

    [Header("References")]
    public TMP_Text conversationTitle;
    public TMP_Text userTopic;
    public TMP_Text conversationSubtitle;
    public TMP_Text conversationMessage;
    public GameObject messageTimeContainer;
    public TMP_Text messageTime;
    public GameObject lastMessageWasSent;
    public GameObject lastMessageWasReceived;
    public GLBusteBuilder busteBuilder;
    public GameObject busteContainer;
    public GameObject unreadMessagesCountContainer;
    public TMP_Text unreadMessagesCount;

    [Space(10f)]
    public GameObject activeConversationIndicator;

    private string conversationRef;
    private string otherUserRef;

    public void OnPlacedFromPool() { }
    public void OnCreatedForPool() { }
    public void OnRemovedToPool()
    {
        conversationRef = null;
    }

    private void Awake()
    {
        GLApi.RegisterKnowledgeListener<Conversation>(OnConversationUpdate);
    }

    private void Update()
    {
        if (showTime)
        {
            activeConversationIndicator.SetActive(GLForum.I.IsActiveConversation(conversationRef));
        }
    }

    private void OnEnable()
    {
        GLConversations.I.OnConversationShown();
    }

    private void OnDisable()
    {
        GLConversations.I?.OnConversationHidden();
    }

    private void OnConversationUpdate(Conversation conversation)
    {
        if (conversation.Ref == conversationRef)
        {
            Setup(conversation.Ref);
        }
    }

    public void Setup(string conversationRef)
    {
        this.conversationRef = conversationRef;

        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var conversation = GLApi.GetConversationKnowledge(conversationRef);
        var conversationInsight = conversation.GetInsight();
        
        unreadMessagesCountContainer.SetActive(conversationInsight.unreadMessagesCount > 0);
        unreadMessagesCount.text = conversationInsight.unreadMessagesCount.ToString();

        activeConversationIndicator.SetActive(showTime && GLForum.I.IsActiveConversation(conversationRef));

        if (GLConversation.IsCurrentlyOpened(conversationRef))
        {
            unreadMessagesCountContainer.SetActive(false);
        }

        if (conversation.Type == ConversationType.DIRECT)
        {
            otherUserRef = conversationInsight.otherMemberRefsThanLoggedInUser[0];
            var otherUserCPS = GLApi.GetCourseProgressSummaryKnowledge(otherUserRef, course.Ref);
            conversationTitle.text = otherUserCPS == null || otherUserCPS.DisplayName.IsNullOrEmpty() ? Label_UnidentifiedPerson.Value : otherUserCPS.DisplayName;

            if (conversationSubtitle != null) conversationSubtitle.gameObject.SetActive(false);
            if (userTopic!= null)
            {
                userTopic.text = (otherUserCPS == null || otherUserCPS.Topic == null) ? Label_NoTopic : otherUserCPS.Topic;
                userTopic.gameObject.SetActive(true);
            }

            busteContainer.SetActive(true);
            busteBuilder.BuildComposition(GLBuste.I.GetComposition(otherUserRef, course.Ref));
        }
        else if (conversation.Type == ConversationType.ANNOUNCEMENTS)
        {
            if (userTopic != null) userTopic.gameObject.SetActive(false);
            if (conversationSubtitle != null)
            {
                conversationSubtitle.text = Description_Announcements;
                conversationSubtitle.gameObject.SetActive(true);
            }
            busteContainer.SetActive(false);
            conversationTitle.text = conversation.Title.IsNullOrEmpty() ? Label_UntitledGroup.Value : conversation.Title;
        }
        else if (conversation.Type == ConversationType.EDUCATIVESUPPORT)
        {
            if (userTopic != null) userTopic.gameObject.SetActive(false);
            if (conversationSubtitle != null)
            {
                conversationSubtitle.text = Description_YouAndTeachers;
                conversationSubtitle.gameObject.SetActive(true);
            }
            busteContainer.SetActive(false);
            conversationTitle.text = Label_YouAndTeachers;
        }
        else if (conversation.Type == ConversationType.TECHNICALSUPPORT)
        {
            if (userTopic != null) userTopic.gameObject.SetActive(false);
            if (conversationSubtitle != null)
            {
                conversationSubtitle.text = Description_YouAndDevelopers;
                conversationSubtitle.gameObject.SetActive(true);
            }
            busteContainer.SetActive(false);
            conversationTitle.text = Label_YouAndDevelopers;
        }
        else
        {
            if (userTopic != null) userTopic.gameObject.SetActive(false);
            if (conversationSubtitle != null) conversationSubtitle.gameObject.SetActive(false);

            busteContainer.SetActive(false);
            conversationTitle.text = conversation.Title.IsNullOrEmpty() ? Label_UntitledGroup.Value : conversation.Title;
        }

        if (!conversationInsight.isEmptyConversation)
        {
            if (conversationMessage != null)
                conversationMessage.text = conversationInsight.lastMessageLine;

            messageTimeContainer.gameObject.SetActive(showTime);
            messageTime.text = GLDate.GetPrettyTrueDateTime(conversationInsight.lastMessageReceivedTime);

            if (lastMessageWasReceived != null)
                lastMessageWasReceived.SetActive(!conversationInsight.lastMessageIsFromLoggedInUser);
            if (lastMessageWasSent != null)
                lastMessageWasSent.SetActive(conversationInsight.lastMessageIsFromLoggedInUser);
        }
        else
        {
            messageTimeContainer.gameObject.SetActive(false);

            if (conversationMessage != null)
                conversationMessage.text = Label_EmptyConversation.Value;
        }
    }

    public void OnPress()
    {
        GLForum.I.OpenConversation(conversationRef);
    }

}
