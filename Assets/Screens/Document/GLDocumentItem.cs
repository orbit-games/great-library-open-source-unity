﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GLDocumentItem : MonoBehaviour {

    public Paroxe.PdfRenderer.PDFViewer pdfViewer;
    private Action onClose;
    private string documentUrl;
    private string savePath;

    public int page;
    public Vector2 point;
    [Buttons("Goto")]
    public ButtonsContainer btn;

    public void Setup(string url, string savePath = null)
    {
        documentUrl = url;
        pdfViewer.LoadDocumentFromWeb(url, null);
        GLTransition.Disable(this);

        if (savePath.IsNullOrEmpty())
        {
            this.savePath = "";
        }
    }

    public void ViewDocument(Action onClose = null)
    {
        GLTransition.In(this);
        this.onClose = onClose;
    }

    public void Goto()
    {
        pdfViewer.GoToPagePoint(page, point);
    }

    public void Refresh()
    {
        pdfViewer.LoadDocumentFromWeb(documentUrl, null);
    }

    public void SaveDocument()
    {
        if (documentUrl.IsNullOrEmpty())
        {
            Debug.LogError("Trying to save a document with an empty source url");
        }

        var lastSlashIndex = documentUrl.LastIndexOf("/");
        var filename = documentUrl.Substring(lastSlashIndex + 1);
        filename = filename.Split('?')[0];

        GLDocument.I.PickSaveDocumentLocation(savePath, filename, documentPath =>
        {
            if (!documentPath.IsNullOrEmpty())
            {
                documentPath = Path.GetFullPath(documentPath);

                if (Path.GetExtension(documentPath) != ".pdf")
                {
                    documentPath = documentPath + ".pdf";
                }

                if (File.Exists(documentPath))
                {
                    GLDocument.I.ShowOverwriteConfirmPopup(documentPath, () =>
                    {
                        SaveDocument(documentPath);
                    });
                }
                else
                {
                    SaveDocument(documentPath);
                }
            }
        });
    }

    private void SaveDocument(string documentPath)
    {
        if (!documentPath.IsNullOrEmpty())
        {
            documentPath = Path.GetFullPath(documentPath);

            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                ViewDocumentOnline();
            }
            else
            {
                if (pdfViewer.SaveDocumentAsFile(documentPath))
                {
                    if (File.Exists(documentPath))
                    {
                        GLSystem.OpenURL(Path.GetDirectoryName(documentPath));
                    }
                    else
                    {
                        GLSystem.OpenURL(documentPath);
                    }
                }
                else
                {
                    GLDocument.I.ShowFailedSavingPopup(documentPath);
                }
            }
        }
    }

    public void ViewDocumentOnline()
    {
        GLSystem.OpenURL(documentUrl);
    }
    
    public void CloseDocument()
    {
        GLTransition.Out(this);
        GLDocument.I.OnDocumentClose();
        onClose?.Invoke();
        onClose = null;
    }
}
