﻿using GameToolkit.Localization;
using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GLDocument : GLSingletonBehaviour<GLDocument>
{
    protected override void OnSingletonInitialize()
    {
        GLTransition.Disable(this);

        var detaultPath = GetDefaultSavePath();
        EnsureDirectoryExists(detaultPath);
        SimpleFileBrowser.FileBrowser.AddQuickLink(Label_SavedDocumentsFolder, GLFiles.SavedDocumentsFolder);
    }

    private static bool filePickerOpened = false;
    public static bool IsFilePickerActive()
    {
        return filePickerOpened;
    }

    [Header("Preferences")]
    public GLStringPreference lastOpenLocation;
    public GLStringPreference lastSaveLocation;

    [Header("Localization")]
    public LocalizedText Title_SaveDocument;
    public LocalizedText Title_PickDocument;
    public LocalizedText Label_SaveDocument;
    public LocalizedText Label_PickDocument;
    public LocalizedText Label_PDFDocument;
    [Space(10f)]
    public LocalizedText Error_FileSizeExceeded;
    public LocalizedText Error_FileDoesntExist;
    public LocalizedText Error_CouldNotOpen;
    public LocalizedText Error_CouldNotSave;
    public LocalizedText Confirm_OverwriteFile;
    [Space(10f)]
    public LocalizedText Label_SavedDocumentsFolder;

    [Header("References")]
    public Transform documentItemContainer;
    public Transform pickFileContainer;
    public Canvas canvas;

    [Header("Prefab")]
    public GLDocumentItem documentItemPrefab;

    private Dictionary<string, GLDocumentItem> documentItems = new Dictionary<string, GLDocumentItem>();
    private Action onClose;

    public void PreloadDocument(string url)
    {
        if (!documentItems.ContainsKey(url))
        {
            var documentItem = GLPool.placeCopy(documentItemPrefab, documentItemContainer);
            documentItems.Add(url, documentItem);
            documentItem.Setup(url);

            var rect = documentItem.GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.one;
            rect.pivot = Vector2.one * 0.5f;
            rect.sizeDelta = Vector2.zero;
        }
    }

    private string GetExistingDirectoryPathEnding(string path, string fallback)
    {
        if (path.IsNullOrEmpty())
        {
            return fallback;
        }

        path = Path.GetFullPath(path);
        if (File.Exists(path))
        {
            path = Path.GetDirectoryName(path);
        }

        if (Directory.Exists(path))
        {
            return path;
        }
        else
        {
            var slash = "" + Path.DirectorySeparatorChar;
            if (path.Contains(slash))
            {
                return GetExistingDirectoryPathEnding(Path.GetDirectoryName(path), fallback);
            }
            else
            {
                return fallback;
            }
        }
    }

    private void EnsureDirectoryExists(string directory)
    {
        try
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
        catch
        {
            Debug.LogError("Could not create folder " + directory);
        }
    }

    private string GetMemorizedOpenPath()
    {
        var lastOpen = lastOpenLocation.Value;
        var fallback = GetDefaultOpenPath();
        if (lastOpen.IsNullOrEmpty())
        {
            return fallback;
        }
        else
        {
            return GetExistingDirectoryPathEnding(lastOpen, fallback);
        }
    }

    private string GetMemorizedSavePath()
    {
        var lastSave = lastSaveLocation.Value;
        var fallback = GetDefaultSavePath();
        if (lastSave.IsNullOrEmpty())
        {
            return fallback;
        } else
        {
            return GetExistingDirectoryPathEnding(lastSave, fallback);
        }
    }

    private string GetDefaultOpenPath()
    {
        return GLFiles.GreatLibraryFolder;
    }

    private string GetDefaultSavePath()
    {
        return GLFiles.SavedDocumentsFolder;
    }

    public void ShowFileSizeExceededPopup(string path, long fileSize, long maxSize)
    {
        var maxMB = (maxSize / 1024f / 1024f).toNumberString(2);
        var fileMB = (fileSize / 1024f / 1024f).toNumberString(2);
        GLPopup.MakeErrorPopup(Error_FileSizeExceeded.Format(path, fileMB, maxMB)).Show();
    }

    public void ShowFailedSavingPopup(string path)
    {
        GLPopup.MakeErrorPopup(Error_CouldNotSave.Format(path)).Show();
    }

    public void ShowFileDoesntExistPopup(string path)
    {
        GLPopup.MakeErrorPopup(Error_FileDoesntExist.Format(path)).Show();
    }

    public void ShowFailedReadingPopup(string path)
    {
        GLPopup.MakeErrorPopup(Error_CouldNotOpen.Format(path)).Show();
    }

    public void ShowOverwriteConfirmPopup(string path, Action onConfirm)
    {
        GLPopup.MakeConfirmPopup(Confirm_OverwriteFile.Format(path), onConfirm).Show();
    }

    public void PickSaveDocumentLocation(string subPath, string documentFilename, Action<string> onPathPicked)
    {
        var initialPath = GetMemorizedSavePath();

        if (!subPath.IsNullOrEmpty())
        {
            initialPath = Path.Combine(initialPath, subPath);
            EnsureDirectoryExists(initialPath);
        }

        filePickerOpened = true;
        GLTransition.In(pickFileContainer);
        SimpleFileBrowser.FileBrowser.SetFilters(false, new SimpleFileBrowser.FileBrowser.Filter(Label_PDFDocument, ".pdf"));
        SimpleFileBrowser.FileBrowser.ShowSaveDialog(url =>
        {
            lastSaveLocation.Value = Path.GetDirectoryName(url);
            GLTransition.Out(pickFileContainer);
            onPathPicked?.Invoke(url);
            filePickerOpened = false;
        },
        () => {
            GLTransition.Out(pickFileContainer);
            onPathPicked?.Invoke(null);
            filePickerOpened = false;
        }, false, initialPath, Title_SaveDocument, Label_SaveDocument);
        SimpleFileBrowser.FileBrowser.SetFilename(documentFilename);
    }
    
    public void PickDocumentToOpen(Action<string> pickPdfListener)
    {
        var initialPath = GetMemorizedOpenPath();

        filePickerOpened = true;
        GLTransition.In(pickFileContainer);
        SimpleFileBrowser.FileBrowser.SetFilters(false, new SimpleFileBrowser.FileBrowser.Filter(Label_PDFDocument, ".pdf"));
        SimpleFileBrowser.FileBrowser.ShowLoadDialog(url =>
        {
            lastOpenLocation.Value = Path.GetDirectoryName(url);
            GLTransition.Out(pickFileContainer);
            pickPdfListener?.Invoke(url);
            filePickerOpened = false;
        },
        () => {
            GLTransition.Out(pickFileContainer);
            pickPdfListener?.Invoke(null);
            filePickerOpened = false;
        }, false, initialPath, Title_PickDocument, Label_PickDocument);
    }

    public int GetSortingOrder()
    {
        return canvas.sortingOrder;
    }

    public void ViewDocument(string url, Action onClose = null)
    {
        PreloadDocument(url);
        documentItems[url].ViewDocument(onClose);
    }

    public void OnDocumentClose()
    {
        // nothing to do
    }
}
 