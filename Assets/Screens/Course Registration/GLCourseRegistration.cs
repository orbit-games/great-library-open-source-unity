﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLCourseRegistration : GLSingletonBehaviour<GLCourseRegistration>
{
    protected override void OnSingletonInitialize() { }

    [Header("Localization")]
    public LocalizedText Label_DeadlineUnknown;
    public LocalizedText Title_CourseRegistration;
    public LocalizedText Description_CourseRegistration;

    [Space(10f)]
    public LocalizedText Title_DisplayName;
    public LocalizedText Description_DisplayName;
    public LocalizedText Hint_DisplayName;

    [Space(10f)]
    public LocalizedText Title_Avatar;
    public LocalizedText Description_Avatar;
    public LocalizedText Label_Generate;

    [Space(10f)]
    public LocalizedText Description_Register;
    public LocalizedText Label_Register;

    [Space(10f)]
    public LocalizedText Title_Confirm;
    public LocalizedText Description_LimitedOptionsForChange;

    [Space(10f)]
    public LocalizedText Label_SkipForNow;
    public LocalizedText Label_LogOut;

    [Header("Preferences")]
    public GLSerializablePreference avatarPreference;
    public GLSerializablePreference namePreference;

    [Header("References")]
    public GLForm form;
    public TMP_Text displayName;
    public GLBusteBuilder busteBuilder;

    private GLNameComposition currentName;
    private GLBusteComposition currentBuste;

    public void OnEnable()
    {
        var user = GLApi.GetActiveUser();
        if (user == null) return;
        var course = GLApi.GetActiveCourse();
        if (course == null) return;
        var courseProgress = GLApi.GetCourseProgressKnowledge(user.Ref, course.Ref);

        var courseInsight = course.GetInsight();
        var deadlineText = Label_DeadlineUnknown.Value;
        if (courseInsight.timelineInsights.Count > 0)
        {
            var deadline = courseInsight.timelineInsights[0].deadline;
            deadlineText = deadline.ToShortDateString() + " " + deadline.ToShortTimeString();
        }

        var formHelper = GLForm.GenerateBasicForm(Title_CourseRegistration, Description_CourseRegistration.Format(deadlineText), Label_Register, OnRegister);
        
        formHelper.AddHeader(Title_DisplayName);
        formHelper.AddPlainText(Description_DisplayName);
        // formHelper.AddCustomButton(Label_SkipForNow, Skip);

        // formHelper.AddCustomButton(Label_LogOut, GLGameFlow.I.Logout);

        //formHelper.AddTextField(null, "name", Hint_DisplayName);
        var generateNameButtons = formHelper.fields.AddElement(new GLForm.ElementsGroup
        {
            direction = GLForm.ElementsGroup.Direction.Horizontal
        });
        generateNameButtons.AddElement(new GLForm.ButtonElement
        {
            buttonType = GLForm.ButtonType.Custom,
            labelText = Label_Generate,
            onPress = GenerateName
        });

        formHelper.AddHeader(Title_Avatar);
        formHelper.AddPlainText(Description_Avatar);

        var generateButtons = formHelper.fields.AddElement(new GLForm.ElementsGroup
        {
            direction = GLForm.ElementsGroup.Direction.Horizontal
        });
        generateButtons.AddElement(new GLForm.ButtonElement
        {
            buttonType = GLForm.ButtonType.Custom,
            labelText = Label_Generate,
            onPress = GenerateBuste
        });

        formHelper.AddPlainText(Description_Register);
        form.SetupForm(formHelper);

        // setting the start values
        currentName = (GLNameComposition)namePreference.Value;
        if (currentName == null && !courseProgress.DisplayName.IsNullOrEmpty())
        {
            currentName = new GLNameComposition();
            currentName.AddPart(-1, courseProgress.DisplayName, false);
        }

        var prefName = currentName?.ToString();
        if (prefName.IsNullOrEmpty())
        {
            GenerateName();
        }
        else
        {
            displayName.text = prefName.ToString();
        }

        var prefBuste = avatarPreference.Value;
        if (prefBuste == null)
        {
            GenerateBuste();
        }
        else
        {
            currentBuste = ((GLBusteComposition)prefBuste);
            busteBuilder.BuildComposition(currentBuste);
        }
    }
    
    private void GenerateName()
    {
        currentName = GLName.I.Generate();
        displayName.text = currentName != null ? currentName.ToString() : "";
    }

    private void GenerateBuste()
    {
        currentBuste = busteBuilder.GenerateRandomComposition();
    }

    private void Skip()
    {
        GLGameFlow.I.TryEnteringPlayerRoom(skipRegistrationForNow: true);
    }

    private void OnRegister(GLForm.Data data)
    {
        //GLPopup.MakeConfirmPopup(Title_Confirm, Description_LimitedOptionsForChange, DoRegister).Show();
        DoRegister();
    }

    private void DoRegister()
    {
        if (currentBuste == null || currentName == null)
        {
            return;
        }

        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var courseProgressSummary = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref);

        courseProgressSummary.DisplayName = currentName.ToString();
        avatarPreference.Value = currentBuste;
        namePreference.Value = currentName;

        GLLoadingOverlay.AddOnLoadingCompletedListener(OnUpdateCourseProgressSummary);
        GLLoadingOverlay.ShowLoading(avatarPreference.SyncTasks);
        GLLoadingOverlay.ShowLoading(GLApi.UpdateCourseProgressSummary(user.Ref, course.Ref, courseProgressSummary));
    }

    void OnUpdateCourseProgressSummary(List<GLTaskResult> results)
    {
        foreach (var result in results)
        {
            if (GLApi.DiplayErrorPopupIfFailed(result))
            {
                return;
            }
        }

        GLGameFlow.I.TryEnteringPlayerRoom();
    }
}
