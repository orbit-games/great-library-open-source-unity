﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLSettings : GLSingletonBehaviour<GLSettings>
{
    protected override void OnSingletonInitialize()
    {
        SetRenderSpeed(RenderSpeedSetting.Value);
    }

    public static Resolution CurrentWindowResolution
    {
        get
        {
            return new Resolution()
            {
                height = Screen.height,
                width = Screen.width,
                refreshRate = Screen.currentResolution.refreshRate
            };
        }
    }

    [Header("Localization")]
    public LocalizedText Title_Settings;
    public LocalizedText Description_Settings;

    public LocalizedText Title_Resolution;
    public LocalizedText Description_Resolution;

    public LocalizedText Label_CustomResolution;
    public LocalizedText Label_MaxResolution;
    public LocalizedText Label_LargeResolution;
    public LocalizedText Label_MediumResolution;

    public LocalizedText Title_RenderSpeed;
    public LocalizedText Description_RenderSpeed;

    public LocalizedText Label_MaxRenderSpeed;
    public LocalizedText Label_AdaptiveRenderSpeed;
    public LocalizedText Label_LowAdaptiveRenderSpeed;

    public LocalizedText Label_Apply;
    public LocalizedText Label_Cancel;
    public LocalizedText Label_Back;

    [Header("Preferences")]
    public GLIntPreference RenderSpeedSetting;

    public void ShowSettings()
    {
        GLPerformance.I.InitializeSuggestedResolutions();

        var prompt = GLPopup.MakeNotificationPopup(Title_Settings, Description_Settings);

        prompt.AddClosingButton(Title_Resolution, () => ShowResolutionSettings(ShowSettings));
        prompt.AddClosingButton(Title_RenderSpeed, () => ShowRenderSettings(ShowSettings));
        
        prompt.Show();
    }

    public void ShowRenderSettings(Action onClose = null)
    {
        GLPerformance.I.InitializeSuggestedResolutions();

        var prompt = GLPopup.MakePromptPopup(Title_RenderSpeed, Description_RenderSpeed,
            data => { ApplySettings(data); onClose?.Invoke(); }, onClose);
        
        var renderSpeeds = new List<Tuple<string, LocalizedText>>();
        renderSpeeds.Add(Tuple.Create("0", Label_MaxRenderSpeed));
        renderSpeeds.Add(Tuple.Create("1", Label_AdaptiveRenderSpeed));
        renderSpeeds.Add(Tuple.Create("2", Label_LowAdaptiveRenderSpeed));
        
        prompt.formHelper.AddRadioButtons(null, "renderSpeed", renderSpeeds, t => t.Item1, t => t.Item2.Value);

        prompt.OverrideCloseButtonText(Label_Back);
        prompt.OverrideSubmitButtonText(Label_Apply);

        prompt.Show();
        
        if (GLPerformance.I.GetRenderSpeed() == GLRenderSpeed.MAXIMUM)
        {
            prompt.SetData("renderSpeed", "0");
        }
        else if (GLPerformance.I.GetRenderSpeed() == GLRenderSpeed.LOW_ADAPTIVE)
        {
            prompt.SetData("renderSpeed", "1");
        }
        else if (GLPerformance.I.GetRenderSpeed() == GLRenderSpeed.TERRIBLE_ADAPTIVE)
        {
            prompt.SetData("renderSpeed", "2");
        }
    }

    public void ShowResolutionSettings(Action onClose = null)
    {
        GLPerformance.I.InitializeSuggestedResolutions();

        var prompt = GLPopup.MakePromptPopup(Title_Resolution, Description_Resolution, 
            data => { ApplySettings(data); onClose?.Invoke(); }, onClose);

        var resolutions = new List<Tuple<string, GLFormattedLocalizedText>>();

        if (GLPerformance.I.IsCurrentResolutionSetToCustom())
        {
            resolutions.Add(Tuple.Create("-1", Label_CustomResolution.Format(
                Screen.width.ToString(),
                Screen.height.ToString())));
        }

        resolutions.Add(Tuple.Create("0", Label_MaxResolution.Format(
            GLPerformance.I.suggestedResolutionMaxWidth.ToString(),
            GLPerformance.I.suggestedResolutionMaxHeight.ToString())));

        if (!GLPerformance.I.LargeSuggestedResolutionIsMaxResolution())
        {
            resolutions.Add(Tuple.Create("1", Label_LargeResolution.Format(
                GLPerformance.I.suggestedResolutionLargeWidth.ToString(),
                GLPerformance.I.suggestedResolutionLargeHeight.ToString())));
        }

        resolutions.Add(Tuple.Create("2", Label_MediumResolution.Format(
            GLPerformance.I.suggestedResolutionMediumWidth.ToString(),
            GLPerformance.I.suggestedResolutionMediumHeight.ToString())));
        
        prompt.formHelper.AddRadioButtons(null, "resolution", resolutions, t => t.Item1, t => t.Item2.ToString());
        
        prompt.OverrideCloseButtonText(Label_Back);
        prompt.OverrideSubmitButtonText(Label_Apply);

        prompt.Show();

        if (GLPerformance.I.IsMaxResolutionCurrent())
        {
            prompt.SetData("resolution", "0");
        }
        else if (GLPerformance.I.IsLargeResolutionCurrent())
        {
            prompt.SetData("resolution", "1");
        }
        else if (GLPerformance.I.IsMediumResolutionCurrent())
        {
            prompt.SetData("resolution", "2");
        }
        else if (GLPerformance.I.IsCurrentResolutionSetToCustom())
        {
            prompt.SetData("resolution", "-1");
        }
    }

    private void ApplySettings(GLForm.Data data)
    {
        SetResolution(data["resolution"]);
        SetRenderSpeed(data["renderSpeed"]);
    }

    private void SetResolution(string resolution)
    {
        if (resolution == null) return;
        try
        {
            SetResolution(int.Parse(resolution));
        }
        catch { }
    }

    private void SetResolution(int resolution)
    {
        switch (resolution)
        {
            case 0:
                GLPerformance.I.SetMaximumResolution();
                break;
            case 1:
                GLPerformance.I.SetLargeSuggestedResolution();
                break;
            case 2:
                GLPerformance.I.SetMediumSuggestedResolution();
                break;
            default:
                break;
        }
    }

    private void SetRenderSpeed(string speed)
    {
        if (speed == null) return;
        try
        {
            SetRenderSpeed(int.Parse(speed));
        }
        catch { }
    }

    public void SetRenderSpeed(int speed)
    {
        SetRenderSpeed((GLRenderSpeed)speed);
    }

    public void SetRenderSpeed(GLRenderSpeed speed)
    {
        RenderSpeedSetting.Value = (int)speed;
        GLPerformance.I.SetRenderSpeed(speed);
    }
}
