﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLGameMenu : GLSingletonBehaviour<GLGameMenu> {

    protected override void OnSingletonInitialize() { }

    [Header("References")]
    public GameObject mainMenu;
    public GameObject backMenu;
    public GameObject noMenu;
    public GLBusteBuilder busteBuilder;
    public TMP_Text displayName;

    [Header("Preferences")]
    public GLSerializablePreference busteComposition;

    public void ShowMainMenu()
    {
        //var comp = (GLBusteComposition)busteComposition.Value;
        //busteBuilder.BuildComposition(comp);

        //var user = GLApi.GetActiveUser();
        //var course = GLApi.GetActiveCourse();
        //var courseProgressSummary = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref);
        //displayName.text = courseProgressSummary.DisplayName;

        GLTransition.Out(backMenu);
        GLTransition.In(mainMenu);
        GLTransition.Out(noMenu);
    }

    public void ShowBackMenu()
    {
        GLTransition.Out(mainMenu);
        GLTransition.In(backMenu);
        GLTransition.Out(noMenu);
    }

    public void ShowNoMenu()
    {
        GLTransition.Out(mainMenu);
        GLTransition.Out(backMenu);
        GLTransition.In(noMenu);
    }
}
