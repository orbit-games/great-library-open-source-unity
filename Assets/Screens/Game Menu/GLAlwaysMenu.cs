﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAlwaysMenu : MonoBehaviour
{

    public void ExitGame()
    {
        GLGameFlow.I.ExitGame();
    }

    public void ShowReporter()
    {
        GLDebug.I.ShowProblemReporter();
    }

    public void FullscreenToggle()
    {
        GLGameFlow.I.ToggleFullscreen();
    }

    public void ShowSettings()
    {
        GLSettings.I.ShowSettings();
    }
}
