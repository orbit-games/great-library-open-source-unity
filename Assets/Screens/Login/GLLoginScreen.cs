﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLLoginScreen : GLSingletonBehaviour<GLLoginScreen> {

    [Header("References")]
    public GLLoading loader;
    public GLForm form;
    public LocalizedTextBehaviour infoMessage;

    [Header("State")]
    [ReadOnly]
    public string email;

    [Header("Preferences")]
    public GLStringPreference previousUser;
    public GLEncryptedStringPreference rememberedPassword;
    public GLBoolPreference rememberMe;

    [Header("Localization")]
    public LocalizedText Description_ForgotPassword;
    public LocalizedText Description_ResetPassword;
    public LocalizedText Description_LogIn;
    public LocalizedText Hint_Email;
    public LocalizedText Hint_Name;
    public LocalizedText Hint_ResetToken;
    public LocalizedText Label_Cancel;
    public LocalizedText Label_Reset;
    public LocalizedText Label_HelloWorld;
    public LocalizedText Label_DisplayName;
    public LocalizedText Label_Username;
    public LocalizedText Label_Email;
    public LocalizedText Label_ForgotPassword;
    public LocalizedText Label_LogIn;
    public LocalizedText Label_LogOut;
    public LocalizedText Label_NewPassword;
    public LocalizedText Label_Password;
    public LocalizedText Label_ReceivedAToken;
    public LocalizedText Label_Register;
    public LocalizedText Label_ResetPassword;
    public LocalizedText Label_RepeatPassword;
    public LocalizedText Label_SendToken;
    public LocalizedText Label_Token;
    public LocalizedText Label_RememberMe;
    public LocalizedText Error_PasswordMismatch;
    public LocalizedText Notify_LoggedIn;
    public LocalizedText Notify_LoggedOut;
    public LocalizedText Notify_LoggedOutInactivity;
    public LocalizedText Notify_PasswordReset;
    public LocalizedText Notify_Registered;
    public LocalizedText Notify_SentToken;

    protected override void OnSingletonInitialize() { }

    private bool allowAutoLoginOnce = false;
    public void AllowAutoLoginOnce()
    {
        allowAutoLoginOnce = true;
    }

    public void OnLogout()
    {
        rememberMe.Value = false;
        rememberedPassword.Value = "";
    }

    private void OnEnable()
    {
        if (allowAutoLoginOnce && rememberMe.Value)
        {
            Login(previousUser.Value, rememberedPassword.Value);
        }
        else
        {
            ShowLoginForm();
        }
        allowAutoLoginOnce = false;
    }
    
    private void ShowLoginForm()
    {
        var helper = GLForm.GenerateBasicForm(Label_LogIn, null, Label_LogIn, (data) =>
        {
            previousUser.Value = data["email"];
            rememberMe.Value = data["remember"] == bool.TrueString;
            rememberedPassword.Value = rememberMe.Value ? data["password"] : "";

            Login(data["email"], data["password"]);
        });
        helper.AddPlainText(Description_LogIn);
        helper.AddTextField(Label_Email, "email", Hint_Email);
        helper.AddPasswordField(Label_Password, "password");
        helper.AddCheckbox(Label_RememberMe, "remember");
        helper.AddCustomButton(Label_Register, ShowRegisterForm);
        helper.AddCustomButton(Label_ForgotPassword, ShowForgotPasswordForm);
        form.SetupForm(helper);

        form.SetValue("email", previousUser.Value);
        form.SetValue("password", rememberMe.Value ? rememberedPassword.Value : "");
        form.SetValue("remember", rememberMe.Value ? bool.TrueString : bool.FalseString);
    }

    private void Login(string email, string password)
    {
        GLLoadingOverlay.ShowLoading(GLApi.LoginUser(
            new IO.Swagger.Model.UserLoginRequest(email, password)),
            (result) =>
            {
                if (!result.Failed)
                {
                    GLAgreementManager.I.HandleLogin(() =>
                    {
                        GLGameFlow.I.OnLogin();
                    }, GLGameFlow.I.Logout);
                }
                else
                {
                    ShowLoginForm();
                    GLApi.DiplayErrorPopupIfFailed(result);
                }
            }
        );
    }

    private void ShowForgotPasswordForm()
    {
        email = form.GetData()["email"];
        var helper = GLForm.GenerateBasicForm(Label_ForgotPassword, Description_ForgotPassword, Label_SendToken, (data) =>
        {
            GLLoadingOverlay.ShowLoading(GLApi.RequestPasswordResetToken(
                new IO.Swagger.Model.UserForgotPasswordRequest(data["email"])),
                (result) =>
                {
                    if (!GLApi.DiplayErrorPopupIfFailed(result))
                        GLPopup.MakeNotificationPopup(Notify_SentToken.Format(data["email"]), ShowResetPasswordForm).Show();
                }
            );
        });
        helper.AddTextField(Label_Email, "email", Hint_Email);
        helper.AddCustomButton(Label_Cancel, ShowLoginForm);
        helper.AddCustomButton(Label_ReceivedAToken, ShowResetPasswordForm);
        form.SetupForm(helper);
        form.SetValue("email", email);
    }

    private void ShowRegisterForm()
    {
        email = form.GetData()["email"];
        var helper = GLForm.GenerateBasicForm(Label_Register, null, Label_Register, (data) =>
        {
            if (data["password"] != data["password_check"])
            {
                GLPopup.MakeErrorPopup(Error_PasswordMismatch).Show();
                return;
            }

            GLAgreementManager.I.ShowRegistrationAgreement(() =>
            {
                var agreement = GLApi.GetLatestAgreementKnowledge();
                GLLoadingOverlay.ShowLoading(GLApi.Register(
                   new IO.Swagger.Model.UserRegistrationRequest(data["email"], data["email"], data["password"], agreement.Ref, data["name"])),
                   (result) =>
                   {
                       if (!GLApi.DiplayErrorPopupIfFailed(result))
                           GLPopup.MakeNotificationPopup(Notify_Registered.Format(data["name"], data["email"]), ShowLoginForm).Show();
                   }
               );
            }, null);
        });
        helper.AddTextField(Label_DisplayName, "name", Hint_Name);
        helper.AddTextField(Label_Email, "email", Hint_Email);
        helper.AddPasswordField(Label_NewPassword, "password");
        helper.AddPasswordField(Label_RepeatPassword, "password_check");
        helper.AddCustomButton(Label_Cancel, ShowLoginForm);
        form.SetupForm(helper);
        form.SetValue("email", email);
    }

    private void ShowResetPasswordForm()
    {
        email = form.GetData()["email"];
        var helper = GLForm.GenerateBasicForm(Label_ResetPassword, Description_ResetPassword, Label_Reset, (data) =>
        {
            if (data["password"] != data["password_check"])
            {
                GLPopup.MakeErrorPopup(Error_PasswordMismatch).Show();
                return;
            }

            GLLoadingOverlay.ShowLoading(GLApi.ResetPassword(
                new IO.Swagger.Model.UserResetPasswordRequest(data["email"], data["token"], data["password"])),
                (result) =>
                {
                    if (!GLApi.DiplayErrorPopupIfFailed(result))
                        GLPopup.MakeNotificationPopup(Notify_PasswordReset, ShowLoginForm).Show();
                }
            );
        });
        helper.AddTextField(Label_Token, "token", Hint_ResetToken);
        helper.AddTextField(Label_Email, "email", Hint_Email);
        helper.AddPasswordField(Label_NewPassword, "password");
        helper.AddPasswordField(Label_RepeatPassword, "password_check");
        helper.AddCustomButton(Label_Cancel, ShowLoginForm);
        form.SetupForm(helper);
        form.SetValue("email", email);
    }

}
