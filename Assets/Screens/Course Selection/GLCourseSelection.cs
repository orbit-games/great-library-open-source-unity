﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLCourseSelection : GLSingletonBehaviour<GLCourseSelection>
{

    protected override void OnSingletonInitialize() { }

    [Header("References")]
    public GLForm form;
    public GLLoading loader;

    [Header("Preferences")]
    public GLBoolPreference autoOpenCourse;
    public GLStringPreference lastSelectedCourse;

    [Header("Localization")]
    public LocalizedText Description_EnterCodeToJoin;
    public LocalizedText Description_YourJoinedCourses;
    public LocalizedText Hint_CourseCode;
    public LocalizedText Label_AutoOpen;
    public LocalizedText Label_CourseSelection;
    public LocalizedText Label_CourseCode;
    public LocalizedText Label_Join;
    public LocalizedText Label_JoinCourse;
    public LocalizedText Label_NoCourse;
    public LocalizedText Label_Open;
    public LocalizedText Label_LogOut;
    public LocalizedText Label_Cancel;
    public LocalizedText Notify_JoinedCourse;
    public LocalizedText Notify_JoiningFailed;

    private bool allowAutoOpeningOnce = false;
    public void AllowAutoOpeningOnce()
    {
        allowAutoOpeningOnce = true;
    }

    private void OnEnable()
    {
        var user = GLApi.GetActiveUser();
        if (!user.Courses.IsNullOrEmpty())
        {
            var lastSelected = lastSelectedCourse.Value;
            if (allowAutoOpeningOnce && UserHasCourse(lastSelected) && autoOpenCourse.Value)
            {
                allowAutoOpeningOnce = false;
                Debug.Log("Loading course");
                LoadCourse(lastSelected);
            }
            else
            {
                ShowCourseSelectionScreen();
            }
        }
        else
        {
            ShowJoinCourseScreen();
        }
    }

    bool UserHasCourse(string courseRef)
    {
        if (courseRef == null || courseRef == "") return false;
        var user = GLApi.GetActiveUser();
        var courseExists = false;
        try
        {
            courseExists = user.Courses.FindIndex(summary => summary.Ref == courseRef) >= 0;
        }
        catch { }
        return courseExists;
    }

    void LoadCourse(string courseRef)
    {
        GLLoadingOverlay.ShowLoading(GLApi.OpenCourse(courseRef), OnCourseLoadResponse);
    }

    void OnCourseLoadResponse(GLTaskResult openCourseResponse)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(openCourseResponse))
        {
            GLProgressLoader.I.LoadCourseProgress(() =>
            {
                GLGameFlow.I.TryEnteringPlayerRoom();
            });
        }
    }
    
    /// <summary>
    /// Show the course selection screen in order for a user to login to a course
    /// </summary>
    void ShowCourseSelectionScreen()
    {
        var helper = GLForm.GenerateBasicForm(Label_CourseSelection, Description_YourJoinedCourses, Label_Open, (data) =>
        {
            var courseRef = data["course"];

            if (courseRef != null)
            {
                autoOpenCourse.Value = data["auto_open"] == bool.TrueString;
                lastSelectedCourse.Value = courseRef;
                LoadCourse(courseRef);
            }
        });

        var radioButtons = helper.fields.AddElement(new GLForm.RadioButtonsElement()
        {
            variableName = "course",
            labelText = null
        });

        var user = GLApi.GetActiveUser();
        foreach (var course in user.Courses)
        {
            string courseName = course.Name + " Q" + course.Quarter + " " + course.Year;
            radioButtons.AddElement(new GLForm.RadioButtonOptionElement()
            {
                labelText = (courseName),
                optionValue = course.Ref
            });
        }
        helper.AddCheckbox(Label_AutoOpen, "auto_open", true);
        helper.AddCustomButton(Label_JoinCourse, ShowJoinCourseScreen);
        helper.AddCustomButton(Label_LogOut, GLGameFlow.I.Logout);
        form.SetupForm(helper);

        var select = lastSelectedCourse.Value;
        if (select == null || select == "")
            select = user.Courses[user.Courses.Count - 1].Ref;

        form.SetValue("course", select);
        form.SetValue("auto_open", autoOpenCourse.Value ? bool.TrueString : bool.FalseString);
    }

    /// <summary>
    /// Show the screen to allow a user to join a course using a code
    /// </summary>
    void ShowJoinCourseScreen()
    {
        var helper = GLForm.GenerateBasicForm(Label_JoinCourse, Description_EnterCodeToJoin, Label_Join, (data) =>
        {
            var code = data["course_code"];
            if (code != null)
            {
                GLLoadingOverlay.ShowLoading(GLApi.JoinCourse(code), (result) =>
                {
                    if (result.Failed)
                    {
                        var error = GLApi.GetEnumFromErrorString(result.ErrorMessage);
                        if (error == GLApiError.EntityNotFound)
                        {
                            GLPopup.MakeErrorPopup(Notify_JoiningFailed).Show();
                        }
                        else
                        {
                            GLApi.DiplayErrorPopupIfFailed(result);
                        }
                    }
                    else
                    {
                        ShowCourseSelectionScreen();
                        GLPopup.MakeNotificationPopup(Notify_JoinedCourse).Show();
                    }
                });
            }
        });

        helper.AddTextField(Label_CourseCode, "course_code", Hint_CourseCode);

        var user = GLApi.GetActiveUser();
        if (user.Courses.Count > 0)
        {
            helper.AddCustomButton(Label_Cancel, ShowCourseSelectionScreen);
        }
        form.SetupForm(helper);
    }
}
