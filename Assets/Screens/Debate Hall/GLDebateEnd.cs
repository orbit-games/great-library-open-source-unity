﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLDebateEnd : MonoBehaviour
{
    [Header("Localization")]
    public LocalizedText Title_TrackDone;
    public LocalizedText Title_ChapterDone;
    public LocalizedText Title_CourseDone;
    public LocalizedText Description_TrackDone;
    public LocalizedText Description_ChapterDone;
    public LocalizedText Description_CourseDone;

    [Header("References")]
    public LocalizedTextBehaviour title;
    public LocalizedTextBehaviour description;
    public GameObject trackDone;
    public GameObject chapterDone;
    public GameObject courseDone;

    public void Setup(string chapterRef, string trackRef)
    {
        // get all basic insight information
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var courseProgressInsight = course.GetProgressInsight(user.Ref);
        var trackProgressInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var chapterInsight = GLInsights.GetChapterInsight(trackProgressInsight.chapterRef);
        var chapterProgressInsight = chapterInsight.GetProgressInsight(user.Ref);
        var submitter = GLApi.GetCourseProgressSummaryKnowledge(trackProgressInsight.track.Submitter, course.Ref);

        trackDone.SetActive(!chapterProgressInsight.isLoggedInUserDone);
        chapterDone.SetActive(chapterProgressInsight.isLoggedInUserDone);
        courseDone.SetActive(courseProgressInsight.isLoggedInUserDone);

        if (courseProgressInsight.isLoggedInUserDone)
        {
            title.LocalizedAsset = Title_CourseDone;
            description.LocalizedAsset = Description_CourseDone;
        }
        else if (chapterProgressInsight.isLoggedInUserDone)
        {
            title.LocalizedAsset = Title_ChapterDone;
            description.LocalizedAsset = Description_ChapterDone;
        }
        else
        {
            title.LocalizedAsset = Title_TrackDone;
            description.LocalizedAsset = Description_TrackDone;
        }
    }
}
