﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using Paroxe.PdfRenderer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class GLDebateSubmission : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Label_Prefix_FileUploaded;
    public LocalizedText Label_UploadDocument;
    public LocalizedText Label_UploadAnotherDocument;
    public LocalizedText Label_CurrentDocument;
    public LocalizedText Label_ClickToPickDocument;

    [Header("References")]
    public GameObject uploadContainer;
    public GameObject leftImage;
    public GameObject rightImage;
    public LocalizedTextBehaviour documentTitle;
    public LocalizedTextBehaviour dodumentInfo;
    public GameObject uploadIcon;
    public GameObject pdfIcon;

    [Space(10f)]
    public GameObject documentViewerContainer;
    public PDFViewer documentViewer;

    private string trackRef;
    private string stepRef;

    public void Setup(string trackRef, string stepRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        rightImage.SetActive(stepInsight.step.PeerType == PeerType.REVIEWER);
        leftImage.SetActive(stepInsight.step.PeerType == PeerType.SUBMITTER);

        var hasSubmission = stepProgressInsight.hasSubmissionData;
        var shouldSubmit = stepProgressInsight.isSubmissionTodo && stepProgressInsight.isActiveStep;

        uploadContainer.SetActive(true);
        // uploadContainer.SetActive(!stepProgressInsight.isClosed);
        //documentViewerContainer.SetActive(hasSubmission);

        pdfIcon.SetActive(!stepProgressInsight.isActiveStep);
        uploadIcon.SetActive(stepProgressInsight.isActiveStep);

        if (!shouldSubmit && hasSubmission)
        {
            var fileData = stepProgressInsight.result.FileUploadMetadata;
            GLDocument.I.PreloadDocument(fileData.DownloadUrl);

            documentTitle.FormattedAsset = GLLocalization.Create(fileData.Filename);
            dodumentInfo.FormattedAsset = Label_Prefix_FileUploaded.Format(fileData.Created.ToString(), (fileData.Size / 1024) + " kB");
        }
        else if (hasSubmission)
        {
            var fileData = stepProgressInsight.result.FileUploadMetadata;
            GLDocument.I.PreloadDocument(fileData.DownloadUrl);

            documentTitle.FormattedAsset = Label_UploadAnotherDocument;
            dodumentInfo.FormattedAsset = Label_CurrentDocument.Format(fileData.Filename);
        }
        else if (shouldSubmit)
        {
            documentTitle.FormattedAsset = Label_UploadDocument;
            dodumentInfo.FormattedAsset = Label_ClickToPickDocument;
        }
        else
        {
            documentTitle.FormattedAsset = "";
            dodumentInfo.FormattedAsset = "";
        }
    }

    public void OnPress()
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];

        if (!stepProgressInsight.isClosed)
        {
            GLDebateHall.I.PickPDF(UploadPDF);
        }
        else
        {
            GLDocument.I.ViewDocument(stepProgressInsight.result.FileUploadMetadata.DownloadUrl);
        }
    }

    FileStream uploadStream;
    private void UploadPDF(string url)
    {
        if (url == null) return;
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var chapterInsight = GLInsights.GetChapterInsight(trackInsight.chapterRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        if (!File.Exists(url))
        {
            Debug.LogError("Failed opening PDF document because it doesn't exist: " + url);
            GLDocument.I.ShowFailedReadingPopup(url);
            return;
        }

        var serverInfo = GLApi.GetServerInfo();
        var fileInfo = new FileInfo(url);
        var maxSize = serverInfo.ReviewStepUploadMaxSize.Value;

        if (fileInfo.Length > maxSize)
        {
            Debug.LogError("File size " + (fileInfo.Length / 1024f).toNumberString(2) + " KB PDF exceeded maximum: " + url);
            GLDocument.I.ShowFileSizeExceededPopup(url, fileInfo.Length, maxSize);
            return;
        }

        try
        {
            uploadStream = new FileStream(url, FileMode.Open);
            GLLoadingOverlay.ShowLoading(
                GLApi.UploadFileForReviewStep(user.Ref, course.Ref, trackInsight.chapterRef, trackInsight.track.Ref, stepRef, uploadStream),
                OnUploadPDFResponse
            );
        }
        catch (System.Exception e)
        {
            Debug.LogError("Failed opening PDF document " + url);
            Debug.LogException(e);
            GLDocument.I.ShowFailedReadingPopup(url);
        }
    }

    private void OnUploadPDFResponse(GLTaskResult result)
    {
        if (uploadStream != null)
        {
            uploadStream.Close();
            uploadStream = null;
        }

        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            GLDebateHall.I.RebuildDebateHall();
            //Setup(trackRef, stepRef);
        }
    }
}
