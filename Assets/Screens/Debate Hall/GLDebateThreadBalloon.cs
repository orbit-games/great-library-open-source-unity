﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLDebateThreadBalloon : MonoBehaviour {

    [Header("Settings")]
    public int selectedY = -5;

    [Header("References")]
    public RectTransform rectTransform;
    public GameObject rightMessage;
    public GameObject leftMessage;
    public Transform container;
    public GLAnimateWiggly wiggly;

    public GameObject selectedIcon;
    public GameObject hasMessageIcon;
    public GameObject todoIcon;
    public GameObject plusIcon;
    public TMP_Text threadNumber;

    private string trackRef;
    private string stepRef;
    private string threadRef;
    private string forcedArgumentTypeRef;
    private GLDebateDiscussion discussionModule;

    public void Setup(GLDebateDiscussion discussionModule, string trackRef, string stepRef, string threadRef, string forcedArgumentTypeRef = null)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;
        this.threadRef = threadRef;
        this.forcedArgumentTypeRef = forcedArgumentTypeRef;
        this.discussionModule = discussionModule;

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);
        var isSubmitter = stepInsight.step.PeerType == PeerType.SUBMITTER;
        var threadIndex = trackInsight.discussionThreadInsights.FindIndex(threadInsight => threadInsight.threadRef == threadRef);

        rightMessage.SetActive(!isSubmitter);
        leftMessage.SetActive(isSubmitter);

        hasMessageIcon.SetActive(false);
        todoIcon.SetActive(false);
        plusIcon.SetActive(false);
        threadNumber.gameObject.SetActive(false);

        if (threadRef != null)
        {
            var threadInsight = GLInsights.GetArgumentThreadInsight(threadRef);
            if (!stepProgressInsight.isClosed)
            {
                var hasMessage = threadInsight.argumentPerStepInsights.ContainsKey(stepRef);
                if (hasMessage)
                {
                    // hasMessageIcon.SetActive(true);
                    threadNumber.gameObject.SetActive(true);
                    threadNumber.text = (threadIndex + 1) + "";
                }
                else
                {
                    todoIcon.SetActive(true);
                }
            }
            else
            {
                threadNumber.gameObject.SetActive(true);
                threadNumber.text = (threadIndex + 1) + "";
            }
        }
        else
        {
            if (forcedArgumentTypeRef != null)
            {
                todoIcon.SetActive(true);
            }
            else
            {
                plusIcon.SetActive(true);
            }
        }
    }

    public void ShowSelectedIfThread(string activeThreadRef, string forcedArgumentTypeRef)
    {
        var selected = false;
        if (threadRef == activeThreadRef)
        {
            selected = threadRef == null ? (this.forcedArgumentTypeRef == forcedArgumentTypeRef) : true;
        }

        selectedIcon.SetActive(selected);
        container.localPosition = new Vector3(0, selected ? selectedY : 0, 0);
        wiggly.ResetStartPosition();
        wiggly.enabled = selected;
    }

    public void OnPress()
    {
        if (discussionModule.IsSelected(threadRef, forcedArgumentTypeRef))
        {
            return;
        }
        if (threadRef == null)
        {
            discussionModule.AddThread(forcedArgumentTypeRef);
        }
        else
        {
            GLDebateHall.I.RememberScrollAndExecute(rectTransform, () =>
            {
                GLDebateHall.I.SetThreadRef(threadRef);
            });
        }
    }

}