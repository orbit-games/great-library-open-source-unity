﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLDebateLibrarySection : MonoBehaviour {

    public TMP_Text relationDescription;
    public TMP_Text sectionTitle;
    
    private string chapterRef;
    private string sectionRef;

    public void Setup(string chapterRef, string sectionRef)
    {
        this.chapterRef = chapterRef;
        this.sectionRef = sectionRef;

        var chapterInsight = GLInsights.GetChapterInsight(chapterRef);
        var relationInsight = chapterInsight.referencedSectionInsights.Find(item => item.sectionInsight.section.Ref == sectionRef);
        if (relationInsight != null)
        {
            relationDescription.text = relationInsight.relation.Description;
            sectionTitle.text = relationInsight.sectionInsight.section.Name;
        }
    }

    public void OpenLibrary()
    {
        GLLibrary.I.OpenSection(sectionRef);
        GLGameFlow.I.ShowLibrary();
    }
}
