﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateAddArgument : MonoBehaviour, IUnsavedChanges
{
    [Header("Settings")]
    public int sidePadding = 45;

    [Header("Localization")]
    public LocalizedText Description_NewArgument;
    public LocalizedText Label_Argument;
    public LocalizedText Label_ReplyArgument;
    public LocalizedText Label_Message;
    public LocalizedText Label_ArgumentType;
    public LocalizedText Label_Send;
    public LocalizedText Label_Update;
    public LocalizedText Label_Delete;

    public LocalizedText Label_ArgumentValue;
    public LocalizedText Label_MajorIssue;
    public LocalizedText Label_MinorIssue;
    public LocalizedText Label_Recommendation;
    public LocalizedText Label_Strength;

    public LocalizedText Description_ReplySent;
    public LocalizedText Description_ArgumentSent;

    public LocalizedText Title_AddArgument;
    public LocalizedText Title_AddReply;
    public LocalizedText Title_ModifyArgument;
    public LocalizedText Title_ModifyReply;

    [Header("References")]
    public GameObject leftImage;
    public GameObject rightImage;
    public GameObject shadow;
    public RectTransform positionDummy;
    public GLForm form;

    public VerticalLayoutGroup verticalLayoutGroup;

    private string trackRef;
    private string stepRef;
    private bool hasDiscussion;
    private string threadRef;
    private string forcedArgumentTypeRef;

    private static List<Tuple<string, LocalizedText>> _argumentValues = new List<Tuple<string, LocalizedText>>();
    private List<Tuple<string, LocalizedText>> ArgumentValues
    {
        get
        {
            if (_argumentValues.Count == 0)
            {
                _argumentValues.Add(Tuple.Create("Major Issue", Label_MajorIssue));
                _argumentValues.Add(Tuple.Create("Minor Issue", Label_MinorIssue));
                _argumentValues.Add(Tuple.Create("Recommendation", Label_Recommendation));
                _argumentValues.Add(Tuple.Create("Strength", Label_Strength));
            }
            return _argumentValues;
        }
    }

    void Awake()
    {
        shadow.SetActive(false);
    }

    public void Setup(string trackRef, string stepRef, string threadRef, string forcedArgumentTypeRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;
        this.threadRef = threadRef;
        this.forcedArgumentTypeRef = forcedArgumentTypeRef;

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepRef);
        var isSubmitter = stepInsight.step.PeerType == PeerType.SUBMITTER;
        var isReply = stepInsight.step.DiscussionType == DiscussionType.REPLY;

        rightImage.SetActive(!isSubmitter);
        leftImage.SetActive(isSubmitter);

        var padding = verticalLayoutGroup.padding;
        padding.left = !isSubmitter ? sidePadding : -sidePadding;
        padding.right = isSubmitter ? sidePadding : -sidePadding;
        
        if (threadRef == null)
        {
            // we should add
            var helper = GLForm.GenerateBasicForm(isReply ? Title_AddReply : Title_AddArgument, null, Label_Send, (data) =>
            {
                SendArgument(data["message"], null, data["argumentType"]);
            });
            helper.AddPlainText(Description_NewArgument);
            FinalizeArgumentForm(helper);
            form.SetupForm(helper, false);
            form.SetValue("argumentType", forcedArgumentTypeRef);
        }
        else
        {
            var threadInsight = GLInsights.GetArgumentThreadInsight(threadRef);

            // should we edit or add?
            if (!threadInsight.argumentPerStepInsights.ContainsKey(stepRef))
            {
                var previousDiscussionStepRef = stepInsight.previousDiscussionStepInsight.step.Ref;
                var previousArgument = threadInsight.argumentPerStepInsights[previousDiscussionStepRef];

                // we should add
                var helper = GLForm.GenerateBasicForm(isReply ? Title_AddReply : Title_AddArgument, null, Label_Send, (data) =>
                {
                    SendArgument(data["message"], previousArgument.argument.Ref, null);
                });
                FinalizeArgumentForm(helper);
                form.SetupForm(helper, false);
            }
            else
            {
                // we should edit
                var argumentInsight = threadInsight.argumentPerStepInsights[stepRef];
                var argumentRef = argumentInsight.argument.Ref;

                var helper = GLForm.GenerateBasicForm(isReply ? Title_ModifyReply : Title_ModifyArgument, null, Label_Update, (data) =>
                {
                    UpdateArgument(argumentRef, data["message"], argumentInsight.argument.ReplyToArgumentRef, data["argumentType"]);
                });

                helper.AddPlainText(isReply ? Description_ReplySent : Description_ArgumentSent);

                helper.AddCustomButton(Label_Delete, () =>
                {
                    GLPopup.MakeConfirmDeletePopup(() =>
                    {
                        RemoveArgument(argumentRef);
                    }).Show();
                });

                FinalizeArgumentForm(helper);
                form.SetupForm(helper, false);

                form.SetValue("message", argumentInsight.argument.Message);
                form.SetValue("argumentType", argumentInsight.argument.ArgumentTypeRef);
            }
        }
    }

    public void OnViewDocument(bool state)
    {
        shadow.SetActive(state);
    }

    public bool HasUnsavedChanges()
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];

        var data = form.GetData();
        var unsavedChanges = false;
        if (data["message"] != null)
        {
            if (threadRef == null)
            {
                unsavedChanges = unsavedChanges || data["message"].Length > 0;
            }
            else
            {
                var threadInsight = GLInsights.GetArgumentThreadInsight(threadRef);
                if (threadInsight.argumentPerStepInsights.ContainsKey(stepRef))
                {
                    unsavedChanges = unsavedChanges || !data["message"].Equals(threadInsight.argumentPerStepInsights[stepRef].argument.Message);
                }
                else
                {
                    unsavedChanges = unsavedChanges || data["message"].Length > 0;
                }
            }
        }
        if (data["argumentType"] != null)
        {
            if (threadRef != null)
            {
                var threadInsight = GLInsights.GetArgumentThreadInsight(threadRef);
                unsavedChanges = unsavedChanges || !data["argumentType"].Equals(threadInsight.argumentPerStepInsights[stepRef].argument.ArgumentTypeRef);
            }
        }
        return unsavedChanges;
    }

    private void FinalizeArgumentForm(GLForm.SetupHelper helper)
    {
        var stepInsight = GLInsights.GetReviewStepInsight(stepRef);

        if (stepInsight.useArgumentTypes)
        {
            var picker = helper.AddAdvancedPicker(Label_ArgumentType, "argumentType",
                stepInsight.step.AllowedArguments,
                argument => argument.ParentRef,
                argument => argument.Ref,
                argument => argument.Name,
                argument => argument.Description,
                null);

            picker.disabled = forcedArgumentTypeRef != null;
        }

        //helper.AddDropdown(Label_ArgumentValue, "argumentValue",
        //    ArgumentValues, tuple => tuple.Item1, tuple => tuple.Item2.Value, null);

        helper.AddRichTextArea(Label_Message, "message", null, 200, 500);
    }

    private void UpdateArgument(string argumentRef, string message, string replyToRef, string argumentTypeRef)
    {
        var course = GLApi.GetActiveCourse();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepInsight = GLInsights.GetStepProgressInsight(trackRef, stepRef);

        if (message.IsNullOrEmpty()) return;

        GLLoadingOverlay.ShowLoading(GLApi.UpdateArgument(stepInsight.userRef, course.Ref, trackInsight.chapterRef, trackRef, stepRef, argumentRef, new Argument
        {
            Message = message,
            ReplyToArgumentRef = replyToRef,
            ArgumentTypeRef = argumentTypeRef
        }), OnUpdateArgumentReply);
    }

    private void OnUpdateArgumentReply(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            GLDebateHall.I.RememberScrollAndExecute(positionDummy, () =>
            {
                GLDebateHall.I.UpdateThread();
                GLDebateHall.I.UpdateClosability();
            });
        }
    }

    private void RemoveArgument(string argumentRef)
    {
        var course = GLApi.GetActiveCourse();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepInsight = GLInsights.GetStepProgressInsight(trackRef, stepRef);

        GLLoadingOverlay.ShowLoading(
            GLApi.DeleteArgument(stepInsight.userRef, course.Ref, trackInsight.chapterRef, trackRef, stepRef, argumentRef),
            OnRemoveArgumentReply);
    }

    private void OnRemoveArgumentReply(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            Setup(trackRef, stepRef, threadRef, forcedArgumentTypeRef);
            GLDebateHall.I.RememberScrollAndExecute(positionDummy, () =>
            {
                GLDebateHall.I.UpdateThread();
                GLDebateHall.I.UpdateClosability();
            });
        }
    }

    private void SendArgument(string message, string replyToRef, string argumentTypeRef)
    {
        var course = GLApi.GetActiveCourse();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var chapterInsight = GLInsights.GetChapterInsight(trackInsight.chapterRef);
        var stepInsight = GLInsights.GetStepProgressInsight(trackRef, stepRef);

        if (message.IsNullOrEmpty()) return;

        GLLoadingOverlay.ShowLoading(GLApi.SendArgument(stepInsight.userRef, course.Ref, trackInsight.chapterRef, trackRef, stepRef, new Argument
        {
            Message = message,
            ReplyToArgumentRef = replyToRef,
            ArgumentTypeRef = argumentTypeRef
        }), OnSendMessageReply);
    }

    private void OnSendMessageReply(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            var reviewStepResult = (ReviewStepResult)result.Result;
            var argument = reviewStepResult.Arguments.Last();
            var argumentInsight = argument.GetInsight();
            threadRef = argumentInsight.threadInsight.threadRef;

            GLDebateHall.I.RememberScrollAndExecute(positionDummy, () =>
            {
                GLDebateHall.I.SetThreadRef(threadRef);
                GLDebateHall.I.UpdateClosability();
            });
        }
    }
}
