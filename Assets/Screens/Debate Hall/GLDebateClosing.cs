﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateClosing : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Title_HandIn;
    public LocalizedText Title_ThanksForSubmission;
    public LocalizedText Title_DeliverablesIncomplete;
    public LocalizedText Description_HandIn_BeforeDeadline;
    public LocalizedText Description_HandIn_AfterDeadline;
    public LocalizedText Description_ThanksForSubmission_BeforeDeadline;
    public LocalizedText Description_ThanksForSubmission_AfterDeadline;
    public LocalizedText Description_DeliverablesIncomplete_BeforeDeadline;
    public LocalizedText Description_DeliverablesIncomplete_AfterDeadline;

    [Header("References")]
    public LocalizedTextBehaviour handInTitle;
    public LocalizedTextBehaviour handInDescription;
    public GameObject handinButtonContainer;
    public Button handinButton;
    public GameObject tourStillTodo;

    private string trackRef;
    private string stepRef;

    public void Setup(string trackRef, string stepRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];

        var tourCompleted = GLGameFlow.I.IsTourCompleted();
        handinButton.interactable = tourCompleted;
        tourStillTodo.SetActive(!tourCompleted);

        if (!stepProgressInsight.isReadyForClose)
        {
            handInTitle.LocalizedAsset = Title_DeliverablesIncomplete;
            handinButtonContainer.SetActive(false);

            if (stepProgressInsight.isDeadlinePassed)
            {
                handInDescription.LocalizedAsset = Description_DeliverablesIncomplete_AfterDeadline;
            }
            else
            {
                handInDescription.LocalizedAsset = Description_DeliverablesIncomplete_BeforeDeadline;
            }
        }
        else if (stepProgressInsight.isMarkedClosed)
        {
            handInTitle.LocalizedAsset = Title_ThanksForSubmission;
            handinButtonContainer.SetActive(false);
            tourStillTodo.SetActive(false);

            if (stepProgressInsight.isDeadlinePassed)
            {
                handInDescription.LocalizedAsset = Description_ThanksForSubmission_AfterDeadline;
            }
            else
            {
                handInDescription.LocalizedAsset = Description_ThanksForSubmission_BeforeDeadline;
            }
        }
        else
        {
            handinButtonContainer.SetActive(true);
            handInTitle.LocalizedAsset = Title_HandIn;
            if (stepProgressInsight.isDeadlinePassed)
            {
                handInDescription.LocalizedAsset = Description_HandIn_AfterDeadline;
            }
            else
            {
                handInDescription.LocalizedAsset = Description_HandIn_BeforeDeadline;
            }
        }
    }

    public void OnPress()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var chapterInsight = GLInsights.GetChapterInsight(trackInsight.chapterRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        GLLoadingOverlay.ShowLoading(
            GLApi.CloseReviewStep(user.Ref, course.Ref, trackInsight.chapterRef, trackInsight.track.Ref, stepRef), 
            OnHandInResponse
        );
    }

    private void OnHandInResponse(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            GLDebateHall.I.RebuildDebateHall();
            //Setup(trackRef, stepRef);
        }
    }
}
