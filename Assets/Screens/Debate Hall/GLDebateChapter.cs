﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLDebateChapter : MonoBehaviour
{
    [Header("Localization")]
    public LocalizedText Title_Chapter;

    [Header("References")]
    public LocalizedTextBehaviour chapterNumber;
    public GameObject librarySectionsContainer;
    public TMP_Text chapterTitle;
    public TMP_Text chapterDescription;

    [Header("Prefabs")]
    public GLDebateLibrarySection librarySectionPrefab;

    public void Setup(string chapterRef)
    {
        GLPool.removeAllPoolableChildren(librarySectionsContainer);

        var course = GLApi.GetActiveCourse();
        var chapterInsight = GLInsights.GetChapterInsight(chapterRef);

        chapterNumber.FormattedAsset = Title_Chapter.Format(chapterInsight.chapterNumber);

        chapterTitle.text = chapterInsight.chapter.Name;
        chapterDescription.text = chapterInsight.chapter.Description;

        if (!chapterInsight.referencedSectionInsights.IsNullOrEmpty())
        {
            librarySectionsContainer.SetActive(true);

            foreach (var relationInsight in chapterInsight.referencedSectionInsights)
            {
                var libSectionObject = GLPool.placeCopy(librarySectionPrefab, librarySectionsContainer);
                libSectionObject.Setup(relationInsight.chapterInsight.chapter.Ref, relationInsight.sectionInsight.section.Ref);
            }
        }
        else
        {
            librarySectionsContainer.SetActive(false);
        }
    }
}
