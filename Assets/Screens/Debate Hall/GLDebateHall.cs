﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateHall : GLSingletonBehaviour<GLDebateHall>, IUnsavedChanges
{
    protected override void OnSingletonInitialize() { }
    
    [Header("Preferences")]
    public GLSerializablePreference busteSettings;

    [Header("References")]
    public GLDebateChapter chapter;
    public GLDebateEnd ending;

    [Space(10f)]
    public GameObject submitterContainer;
    public GLBusteBuilder submitterBuste;
    public TMP_Text submitterName;
    public GameObject submitterHighlight;
    public List<GameObject> reviewerContainers;
    public List<GLBusteBuilder> reviewerBustes;
    public List<TMP_Text> reviewerNames;
    public List<GameObject> reviewerHighlights;

    [Space(10f)]
    public GLDirectChatButton submitterChatButton;
    public GLDirectChatButton reviewerChatButton;

    [Space(10f)]
    public GameObject reviewerSteps;
    public GameObject submitterSteps;
    public GameObject submittedDocumentViewer;

    [Space(10f)]
    public RectTransform stepYDummy;
    public RectTransform viewport;
    public RectTransform trackContainer;

    [Space(10f)]
    public GLPoolManagedList debateStepList;
    public GLPoolManagedList reviewerStepsList;
    public GLPoolManagedList submitterStepsList;

    [Header("Prefabs")]
    public GLDebateStepIndicator debateStepIndicator;
    public GLDebateStep debateStep;

    [Header("State")]
    [ReadOnly]
    public int standingReviewerID;
    [ReadOnly]
    public string trackRef;
    [ReadOnly]
    public string documentUrl;
    [ReadOnly]
    public string todoStepRef;
    // trackref to active threadref
    public Dictionary<string, string> threadRefs = new Dictionary<string, string>();
    [ReadOnly]
    string threadRef;
    [ReadOnly]
    public int threadIndex;
    [ReadOnly]
    public string highlightStepRef;

    private List<GLDebateStep> debateSteps = new List<GLDebateStep>();

    public void SetDebateTrack(string trackRef, string stepRef = null)
    {
        this.trackRef = trackRef;
        highlightStepRef = stepRef;

        threadRef = threadRefs.GetOrDefault(trackRef);

        if (threadRef == null)
        {
            threadIndex = 0;
            threadRef = GetDefaultThread();
        }
    }

    private void OnEnable()
    {
        if (trackRef.IsNullOrEmpty())
        {
            return;
        }
        
        CloseFilePicker();
        RebuildDebateHall();

        if (highlightStepRef == null)
        {
            ScrollToActiveStep();
        }
        else
        {
            var stepInsight = GLInsights.GetReviewStepInsight(highlightStepRef);
            GLRun.NextFrame(() =>
            {
                ScrollToStep(stepInsight.stepIndex);
            });
        }

        highlightStepRef = null;
    }

    public void RebuildDebateHall()
    {
        // get all basic insight information
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var trackProgressInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var chapterInsight = GLInsights.GetChapterInsight(trackProgressInsight.chapterRef);
        var submitter = GLApi.GetCourseProgressSummaryKnowledge(trackProgressInsight.track.Submitter, course.Ref);
        var chapterRef = chapterInsight.chapter.Ref;

        // setup chats
        submitterChatButton.Setup(trackProgressInsight.track.Submitter);
        reviewerChatButton.Setup(trackProgressInsight.track.Reviewer);

        // setting up chapter information
        chapter.Setup(chapterRef);

        // place the step indicators
        submitterStepsList.SyncFully<ReviewStep, GLDebateStepIndicator>(
            chapterInsight.chapter.ReviewSteps, i => i.Ref,
            (step, instance) => instance.Setup(trackProgressInsight, step.GetInsight(), PeerType.SUBMITTER));

        reviewerStepsList.SyncFully<ReviewStep, GLDebateStepIndicator>(
            chapterInsight.chapter.ReviewSteps, i => i.Ref,
            (step, instance) => instance.Setup(trackProgressInsight, step.GetInsight(), PeerType.REVIEWER));

        // reset user display
        foreach (var reviewerContainer in reviewerContainers)
        {
            reviewerContainer.SetActive(false);
        }

        // update this track users display
        SetUserDisplay(submitterContainer, submitterBuste, submitterName, submitterHighlight, trackProgressInsight.track.Submitter);
        SetUserDisplay(reviewerContainers[0], reviewerBustes[0], reviewerNames[0], reviewerHighlights[0], trackProgressInsight.track.Reviewer);
        
        // figure out the current step thats todo
        todoStepRef = null;
        todoStepRef = trackProgressInsight.activeStepInsight?.reviewStepRef;

        // disable the submission flag for now
        submittedDocumentViewer.SetActive(false);
        documentUrl = "";

        foreach (var step in chapterInsight.chapter.ReviewSteps)
        {
            var stepInsight = step.GetInsight();
            var stepProgressInsight = stepInsight.GetProgressInsight(trackRef);

            if (stepInsight.isSubmitterStep && stepProgressInsight.hasSubmissionData)
            {
                documentUrl = stepProgressInsight.result.FileUploadMetadata.DownloadUrl;
                submittedDocumentViewer.SetActive(true);
            }

            if (stepProgressInsight.isAvailable)
            {
                var stepProgress = stepInsight.GetProgressInsight(trackProgressInsight.track);
                if (!stepProgress.isClosed && stepProgress.userRef == user.Ref)
                {
                    todoStepRef = step.Ref;
                }
            }
        }

        // building all the steps in the track!
        debateSteps.Clear();
        debateStepList.SyncFully<ReviewStep, GLDebateStep>(
            chapterInsight.chapter.ReviewSteps, i => i.Ref,
            (step, instance) =>
            {
                instance.Setup(trackRef, step.Ref, threadRef);
                instance.gameObject.SetActive(true);
                debateSteps.Add(instance);
            });

        // ensuring the ending is the last sinling and activated when track is ended
        if (trackProgressInsight.isCompleted)
        {
            ending.gameObject.SetActive(true);
            ending.transform.SetAsLastSibling();
            ending.Setup(chapterRef, trackRef);
        }
        else
        {
            ending.gameObject.SetActive(false);
        }
    }

    private void SetUserDisplay(GameObject container, GLBusteBuilder buste, TMP_Text name, GameObject highlight, string displayUserRef)
    {
        var activeUser = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);

        if (!displayUserRef.IsNullOrEmpty())
        {
            container.SetActive(true);

            highlight.SetActive(activeUser.Ref == displayUserRef);

            var displayingUser = GLApi.GetCourseProgressSummaryKnowledge(displayUserRef, course.Ref);
            var userProgressSummary = GLApi.GetCourseProgressSummaryKnowledge(displayingUser.UserRef, course.Ref);
            name.text = userProgressSummary.DisplayName;

            var busteComposition = (GLBusteComposition)busteSettings[displayingUser.UserRef, displayingUser.CourseRef];
            buste.BuildComposition(busteComposition);
        }
        else
        {
            container.SetActive(false);
            highlight.SetActive(false);
        }
    }

    public void OpenDocument()
    {
        if (!documentUrl.IsNullOrEmpty())
        {
            OnViewDocument(true);
            GLDocument.I.ViewDocument(documentUrl, () =>
            {
                OnViewDocument(false);
            });
        }
    }

    private void OnViewDocument(bool state)
    {
        foreach (var step in debateSteps)
        {
            step.OnViewDocument(state);
        }
    }

    private void Update()
    {
        if (GLInput.OnNext())
        {
            //NextThread();
        }
        else if (GLInput.OnPrevious())
        {
            //PreviousThread();
        }
        var number = GLInput.GetPressedNumericKey() - 1;
        if (number >= 0 && !GLInput.IsNavigationBlocked())
        {
            if (number < debateSteps.Count)
            {
                ScrollToStep(number);
            }
        }
    }

    public void UpdateClosability()
    {
        foreach (var step in debateSteps)
        {
            step.UpdateClosability();
        }
    }

    public bool HasUnsavedChanges()
    {
        var unsavedChanges = false;
        foreach (var step in debateSteps)
        {
            unsavedChanges = unsavedChanges || step.HasUnsavedChanges();
        }
        return unsavedChanges;
    }

    #region user navigation
    //public void PutReviewerToStand(int index)
    //{
    //    // get all basic insight information
    //    var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
    //    var course = GLApi.GetActiveCourse();
    //    var submitter = GLApi.GetCourseProgressSummaryKnowledge(trackInsight.track.Submitter, course.Ref);
    //    var activeUser = GLApi.GetActiveUser();

    //    if (submitter.UserRef == activeUser.Ref)
    //    {
    //        var tracks = GLInsights.GetSubmitterTracks(trackInsight.chapterRef, submitter.UserRef);
    //        trackRef = tracks[index].Ref;
    //        RebuildDebateHall();
    //    }
    //}

    public void OpenLibrarySection()
    {
        GLGameFlow.I.ShowLibrary();
        GLLibrary.I.OpenSection("");
    }

    #endregion
    #region scrolling

    public void ScrollToActiveStep()
    {
        GLRun.NextFrame(() =>
        {
            // scroll to appropriate step
            var trackProgressInsight = GLInsights.GetTrackProgressInsight(trackRef);
            var activeStep = trackProgressInsight.activeStepInsight;
            if (activeStep != null)
            {
                ScrollToStep(activeStep.reviewStepIndex);
            }
            else
            {
                if (trackProgressInsight.isCompleted)
                {
                    ScrollToStep(debateSteps.Count - 1);
                }
                else
                {
                    ScrollToStep(0);
                }
            }
        });
    }

    private float GetStepY(int step)
    {
        var maxY = 0;
        var minY = -trackContainer.rect.height + viewport.rect.height;
        var pos = trackContainer.anchoredPosition;
        var toStep = debateSteps.GetClamped(step);

        var fromY = toStep.stepYDummy.LocalPositionWithin(this).y;
        var toY = stepYDummy.LocalPositionWithin(this).y;
        var changeY = toY - fromY;
        
        return Mathf.Clamp(pos.y + changeY, minY, maxY);
    }

    public void ScrollToStep(int step)
    {
        if (debateSteps.Count == 0) return;

        var minY = 0;
        var maxY = -trackContainer.rect.height + viewport.rect.height;
        var pos = trackContainer.anchoredPosition;

        var trackProgressInsight = GLInsights.GetTrackProgressInsight(trackRef);
        
        if (step == 0 && !trackProgressInsight.stepProgressInsights[0].hasData)
        {
            trackContainer.anchoredPosition = new Vector2(pos.x, GetStepY(debateSteps.Count - 1));
            ScrollToYAnimated(maxY);
        }
        else if (step == debateSteps.Count - 1)
        {
            ScrollToYAnimated(minY);
        }
        else
        {
            ScrollToYAnimated(GetStepY(step));
        }
    }

    private Coroutine scrollRoutine;

    public void StopScrollAnimation()
    {
        if (scrollRoutine != null)
            StopCoroutine(scrollRoutine);
    }

    private void ScrollToYAnimated(float toY)
    {
        StopScrollAnimation();
        scrollRoutine = StartCoroutine(ScrollToYAnimatedCoroutine(toY));
    }

    IEnumerator ScrollToYAnimatedCoroutine(float toY)
    {
        var current = trackContainer.anchoredPosition;
        while (Mathf.Abs(toY - current.y) > 0.5f) {
            yield return null;
            current += new Vector2(0, toY - current.y) * Mathf.Min(1f, Time.deltaTime * 3f);
            trackContainer.anchoredPosition = current;
        }
        trackContainer.anchoredPosition = new Vector2(current.x, toY);
    }

    public void ScrollInstantRelative(float dy)
    {
        trackContainer.position += new Vector3(0, dy, 0);
    }

    public void ScrollInstantAbsolute(float dy)
    {
        var pos = trackContainer.position;
        trackContainer.position = new Vector3(pos.x, dy, pos.z);
    }

    public void RememberScrollAndExecute(RectTransform forTransform, Action todo)
    {
        var canvas = GetComponentInParent<Canvas>();
        Vector2 mouseInTransformPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(forTransform, Input.mousePosition, canvas.worldCamera, out mouseInTransformPoint);

        var mouseInWorldPoint = forTransform.TransformPoint(mouseInTransformPoint);

        todo?.Invoke();
        Canvas.ForceUpdateCanvases();

        var newMouseInWorldPoint = forTransform.TransformPoint(mouseInTransformPoint);
        ScrollInstantRelative(mouseInWorldPoint.y - newMouseInWorldPoint.y);
    }

    #endregion
    #region threads

    public void UpdateThread()
    {
        SetThreadIndex(threadIndex);
    }

    private void SetThreadIndex(int index)
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);

        if (trackInsight.discussionThreadInsights.IsNullOrEmpty())
        {
            threadIndex = -1;
            threadRef = null;
        }
        else
        {
            threadIndex = Mathf.Clamp(index, 0, trackInsight.discussionThreadInsights.Count - 1);
            threadRef = trackInsight.discussionThreadInsights[threadIndex].threadRef;
        }

        threadRefs.SetOrAdd(trackRef, threadRef);

        foreach (var step in debateSteps)
        {
            step.SetThreadRef(threadRef);
        }

        Canvas.ForceUpdateCanvases();
    }

    public void SetThreadRef(string newThreadRef)
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        threadRef = newThreadRef;
        threadIndex = trackInsight.discussionThreadInsights.FindIndex(thread => thread.threadRef == newThreadRef);
        threadRefs.SetOrAdd(trackRef, threadRef);

        foreach (var step in debateSteps)
        {
            step.SetThreadRef(threadRef);
        }

        Canvas.ForceUpdateCanvases();
    }

    public void PreviousThread()
    {
        SetThreadIndex(threadIndex - 1);
    }

    public void NextThread()
    {
        SetThreadIndex(threadIndex + 1);
    }

    public void ResetThread()
    {
        SetThreadRef(GetDefaultThread());
    }

    private string GetDefaultThread()
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        if (trackInsight.discussionThreadInsights.IsNullOrEmpty())
        {
            return null;
        }
        else
        {
            return trackInsight.discussionThreadInsights[0].threadRef;
        }
    }

    private void HandleThread()
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        if (threadRef.IsNullOrEmpty())
        {
            ResetThread();
        }
        else
        {
            var threadInsight = trackInsight.discussionThreadInsights.Find(t => t.threadRef == threadRef);
            if (threadInsight == null)
            {
                ResetThread();
            }
            else
            {
                SetThreadRef(threadRef);
            }
        }
    }

    #endregion
    #region document management
    public void PickPDF(Action<string> pickPdfListener)
    {
        GLDocument.I.PickDocumentToOpen(path=>
        {
            pickPdfListener?.Invoke(path);
        });
    }

    public void CloseFilePicker()
    {
        SimpleFileBrowser.FileBrowser.HideDialog(true);
    }
    #endregion
}
