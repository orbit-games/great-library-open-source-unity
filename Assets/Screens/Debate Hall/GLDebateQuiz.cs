﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateQuiz : MonoBehaviour, IUnsavedChanges
{

    [Header("Localization")]
    public LocalizedText Label_RetakeDebateQuiz;

    [Header("Settings")]
    public int sidePadding = 100;

    [Header("References")]
    public GameObject ratingContainer;
    public TMP_Text ratingText;
    public RectTransform positionDummy;

    public GLQuiz quiz;
    public GLForm form;

    public LayoutGroup layoutGroup;

    private string trackRef;
    private string stepRef;

    public void Setup(string trackRef, string stepRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;

        var user = GLApi.GetActiveUser();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        var isSubmitter = stepInsight.step.PeerType == PeerType.SUBMITTER;

        var padding = layoutGroup.padding;
        padding.left = !isSubmitter ? sidePadding : 0;
        padding.right = isSubmitter ? sidePadding : 0;
        layoutGroup.padding = padding;

        if (stepProgressInsight.hasQuizData)
        {
            var result = stepProgressInsight.result.QuizResult;
            var totalScore = result.TotalScore.Value;
            var maxScore = stepInsight.step.Quiz.MaxScore.Value;

            ratingText.text = maxScore > 4
                ? (10f * totalScore / (float)maxScore).toNumberString(1)
                : (totalScore > 0 ? "+" : "") + totalScore;

            if (stepProgressInsight.isClosed)
            {
                quiz.SetupDisplayResults(stepInsight.step.Quiz, result);
                ratingContainer.SetActive(true);
                ratingContainer.transform.SetAsLastSibling();
            }
            else
            {
                ratingContainer.SetActive(false);
                quiz.SetupModifyableResults(stepInsight.step.Quiz, result, Label_RetakeDebateQuiz, SendQuizResult);
            }
        }
        else
        {
            quiz.SetupTakeQuiz(stepInsight.step.Quiz, SendQuizResult);
            ratingContainer.SetActive(false);
        }
    }

    public bool HasUnsavedChanges()
    {
        return quiz.IsTakingQuiz();
    }

    private void SendQuizResult(QuizResult result)
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);


        GLLoadingOverlay.ShowLoading(
            GLApi.SendReviewQuizResults(user.Ref, course.Ref, trackInsight.chapterRef, trackRef, stepRef, result),
            OnSendQuizResultResponse
        );
    }

    private void OnSendQuizResultResponse(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            // Setup(trackRef, stepRef);
            GLDebateHall.I.RebuildDebateHall();
        }
    }
}
