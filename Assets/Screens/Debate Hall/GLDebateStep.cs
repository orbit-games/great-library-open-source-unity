﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLDebateStep : MonoBehaviour, IUnsavedChanges
{
    [Header("References")]
    public GameObject reviewerActiveIndicator;
    public GameObject submitterActiveIndicator;
    public RectTransform stepYDummy;
    public RectTransform myRectTransform;

    public GLDebateStepInfo infoModule;
    public GLDebateSubmission submissionModule;
    public GLDebateDiscussion discussionModule;
    public GLDebateQuiz quizModule;
    public GLDebateClosing closingModule;

    private string trackRef;
    private string stepRef;

    public void Setup(string trackRef, string stepRef, string currentThreadRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;

        var user = GLApi.GetActiveUser();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        infoModule.Setup(trackRef, stepRef);
        
        submissionModule.gameObject.SetActive(false);
        discussionModule.gameObject.SetActive(false);
        quizModule.gameObject.SetActive(false);
        closingModule.gameObject.SetActive(false);

        reviewerActiveIndicator.SetActive(false);
        submitterActiveIndicator.SetActive(false);

        if (stepProgressInsight.isContentVisible)
        {
            if (stepProgressInsight.isActiveStep)
            {
                reviewerActiveIndicator.SetActive(trackInsight.isLoggedInUserReviewer);
                submitterActiveIndicator.SetActive(trackInsight.isLoggedInUserSubmitter);
            }

            if (stepInsight.requiresSubmission)
            {
                submissionModule.gameObject.SetActive(true);
                submissionModule.Setup(trackRef, stepRef);
            }

            if (stepInsight.requiresDiscussion)
            {
                discussionModule.gameObject.SetActive(true);
                discussionModule.Setup(trackRef, stepRef, currentThreadRef);
            }

            if (stepInsight.requiresQuiz)
            {
                quizModule.gameObject.SetActive(true);
                quizModule.Setup(trackRef, stepRef);
            }
        }

        UpdateClosability();
    }

    public void UpdateClosability()
    {
        var user = GLApi.GetActiveUser();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        closingModule.gameObject.SetActive(false);

        if (stepProgressInsight.isLoggedInUserStep)
        {
            //var isNotClosedButClosable = stepProgressInsight.isReadyForClose && !stepProgressInsight.isClosed;
            var isClosedAndWaiting = stepProgressInsight.isClosed && stepProgressInsight.nextStepInsight != null && stepProgressInsight.nextStepInsight.isActiveStep == true;

            if (stepProgressInsight.isActiveStep || isClosedAndWaiting)
            {
                closingModule.gameObject.SetActive(true);
                closingModule.Setup(trackRef, stepRef);
            }
        }
    }

    public bool HasUnsavedChanges()
    {
        var unsavedChanges = false;
        if (discussionModule.gameObject.activeSelf)
        {
            unsavedChanges = unsavedChanges || discussionModule.HasUnsavedChanges();
        }
        if (quizModule.gameObject.activeSelf)
        {
            unsavedChanges = unsavedChanges || quizModule.HasUnsavedChanges();
        }
        return unsavedChanges;
    }

    public void OnViewDocument(bool state)
    {
        if (discussionModule.gameObject.activeSelf)
        {
            discussionModule.OnViewDocument(state);
        }
    }

    public void SetThreadRef(string threadRef)
    {
        if (discussionModule.gameObject.activeSelf)
        {
            discussionModule.SetThread(threadRef);
        }
    }
}
