﻿using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateDiscussion : MonoBehaviour, IUnsavedChanges
{
    
    [Header("References")]
    public GLDebateArgument previousArgument;
    public GLDebateArgument currentArgument;
    public GLDebateAddArgument addArgument;
    public Transform threadBalloonsContainer;
    public Canvas onViewDocumentCanvas;
    public VerticalLayoutGroup onViewDocumentLayout;

    [Header("Prefabs")]
    public GLDebateThreadBalloon threadBalloonPrefab;
    private List<GLDebateThreadBalloon> balloons = new List<GLDebateThreadBalloon>();

    private string currentThreadRef;
    private string currentForcedArgumentTypeRef;
    private string trackRef;
    private string stepRef;
    private string prevStepRef;

    public void Setup(string trackRef, string stepRef, string currentThreadRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;
        this.currentThreadRef = currentThreadRef;

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        // if we have to add an argument, then lets see if there is a required one and set it as current
        if (currentThreadRef == null && stepProgressInsight.requiredArgumentTypesTodo.Count > 0)
        {
            currentForcedArgumentTypeRef = stepProgressInsight.requiredArgumentTypesTodo[0];
        }

        prevStepRef = stepInsight.previousDiscussionStepInsight?.step.Ref;

        SetupArguments();
    }

    private void OnDisable()
    {
        onViewDocumentCanvas.overrideSorting = false;
    }

    public void OnViewDocument(bool state)
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        addArgument.OnViewDocument(state);

        if (state && stepProgressInsight.isActiveStep)
        {
            onViewDocumentCanvas.overrideSorting = true;
            onViewDocumentCanvas.sortingOrder = GLDocument.I.GetSortingOrder() + 1;
        }
        else
        {
            onViewDocumentCanvas.overrideSorting = false; 
        }
        
    }

    private void SetupArguments()
    {
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        GLPool.removeAllPoolableChildren(threadBalloonsContainer);
        balloons.Clear();

        // build balloons for existing threads
        foreach (var threadInsight in trackInsight.discussionThreadInsights)
        {
            var balloon = GLPool.placeCopy(threadBalloonPrefab, threadBalloonsContainer);
            balloons.Add(balloon);
            balloon.Setup(this, trackRef, stepRef, threadInsight.threadRef);
            balloon.ShowSelectedIfThread(currentThreadRef, currentForcedArgumentTypeRef);
        }

        // build plus balloon and balloons for required argument types
        if (stepInsight.requiresDiscussion && stepProgressInsight.isActiveStep)
        {
            foreach (var requiredArgumentRef in stepProgressInsight.requiredArgumentTypesTodo)
            {
                var balloon = GLPool.placeCopy(threadBalloonPrefab, threadBalloonsContainer);
                balloons.Add(balloon);
                balloon.Setup(this, trackRef, stepRef, null, requiredArgumentRef);
                balloon.ShowSelectedIfThread(currentThreadRef, currentForcedArgumentTypeRef);
            }
            if (stepInsight.canCreateThreads)
            {
                var balloon = GLPool.placeCopy(threadBalloonPrefab, threadBalloonsContainer);
                balloons.Add(balloon);
                balloon.Setup(this, trackRef, stepRef, null, null);
                balloon.ShowSelectedIfThread(currentThreadRef, currentForcedArgumentTypeRef);
            }
        }

        // handle selection
        foreach (var balloon in balloons)
        {
            balloon.ShowSelectedIfThread(currentThreadRef, currentForcedArgumentTypeRef);
        }

        SetupPreviousArgument();

        if (!currentThreadRef.IsNullOrEmpty())
        {
            var threadInsight = GLInsights.GetArgumentThreadInsight(currentThreadRef);
            var message = threadInsight.argumentPerStepInsights.GetOrDefault(stepRef);
            if (message != null)
            {
                // just show the message, doesn't matter if the step is active
                SetupAddArgument(shouldAddArgument: stepProgressInsight.isActiveStep);
            }
            else
            {
                // thread is not null, but we don't have a current message.
                // it must mean that there was a message in a previous step, just not this one
                SetupAddArgument(shouldAddArgument: stepProgressInsight.isActiveStep);
            }
        }
        else
        {
            previousArgument.gameObject.SetActive(false);
            SetupAddArgument(shouldAddArgument: stepProgressInsight.isActiveStep);
        }
    }

    private void SetupPreviousArgument()
    {
        if (!currentThreadRef.IsNullOrEmpty() && prevStepRef != null)
        {
            previousArgument.gameObject.SetActive(true);
            previousArgument.Setup(trackRef, prevStepRef, currentThreadRef);
        }
        else
        {
            previousArgument.gameObject.SetActive(false);
        }
    }

    private void SetupAddArgument(bool shouldAddArgument)
    {
        if (shouldAddArgument)
        {
            // we may add one
            currentArgument.gameObject.SetActive(false);
            addArgument.gameObject.SetActive(true);
            addArgument.Setup(trackRef, stepRef, currentThreadRef, currentForcedArgumentTypeRef);
        }
        else
        {
            // we should just display that we have nothing to show
            addArgument.gameObject.SetActive(false);
            currentArgument.gameObject.SetActive(true);
            currentArgument.Setup(trackRef, stepRef, currentThreadRef);
        }
    }

    public bool HasUnsavedChanges()
    {
        if (addArgument.gameObject.activeSelf)
        {
            return addArgument.HasUnsavedChanges();
        }
        return false;
    }

    private void OnThreadSwitchAllowed(string threadRef, Action doThreadSwitch)
    {
        if (HasUnsavedChanges())
        {
            GLPopup.MakeConfirmDiscardChangesPopup(doThreadSwitch).Show();
        }
        else
        {
            doThreadSwitch?.Invoke();
        }
    }

    public bool IsSelected(string threadRef, string forcedArgumentTypeRef)
    {
        if (threadRef == currentThreadRef)
        {
            return currentThreadRef != null ? true : forcedArgumentTypeRef == currentForcedArgumentTypeRef;
        }
        return false;
    }

    public string GetThread()
    {
        return currentThreadRef;
    }

    public string GetForcedArgumentTypeRef()
    {
        return currentForcedArgumentTypeRef;
    }

    public void SetThread(string newThreadRef, string newForcedArgumentTypeRef = null)
    {
        OnThreadSwitchAllowed(newThreadRef, () =>
        {
            currentForcedArgumentTypeRef = newForcedArgumentTypeRef;
            currentThreadRef = newThreadRef;
            foreach (var balloon in balloons)
            {
                balloon.ShowSelectedIfThread(newThreadRef, newForcedArgumentTypeRef);
            }

            SetupArguments();
        });
    }

    public void AddThread(string forcedArgumentTypeRef = null)
    {
        SetThread(null, forcedArgumentTypeRef);
    }

}
