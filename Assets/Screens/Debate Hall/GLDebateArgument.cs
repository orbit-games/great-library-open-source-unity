﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateArgument : MonoBehaviour {

    [Header("Settings")]
    public int sidePadding = 45;

    [Header("Localization")]
    public LocalizedText Label_NoMessageForThread;

    [Header("References")]
    public GameObject leftImage;
    public GameObject rightImage;
    public TMP_Text argumentTypeText;
    public TMP_Text argumentText;

    public VerticalLayoutGroup verticalLayoutGroup;

    private string trackRef;
    private string stepRef;
    private bool hasDiscussion;
    private string threadRef;

    public void Setup(string trackRef, string stepRef, string threadRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;
        this.threadRef = threadRef;

        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);
        var isSubmitter = stepInsight.step.PeerType == PeerType.SUBMITTER;
        
        rightImage.SetActive(!isSubmitter);
        leftImage.SetActive(isSubmitter);

        var padding = verticalLayoutGroup.padding;
        padding.left = !isSubmitter ? sidePadding : -sidePadding;
        padding.right = isSubmitter ? sidePadding : -sidePadding;
        verticalLayoutGroup.padding = padding;

        verticalLayoutGroup.childAlignment = isSubmitter ? TextAnchor.UpperLeft : TextAnchor.UpperRight;

        hasDiscussion = stepProgressInsight.hasDiscussionData;
        if (hasDiscussion)
        {
            argumentText.fontStyle = FontStyles.Normal;

            if (threadRef.IsNullOrEmpty())
            {
                DisplayNoMessageForThread();
            }
            else
            {
                var threadInsight = GLInsights.GetArgumentThreadInsight(threadRef);
                var stepArgumentInsight = threadInsight.argumentPerStepInsights.GetOrDefault(stepRef);
                if (stepArgumentInsight == null)
                {
                    DisplayNoMessageForThread();
                }
                else
                {
                    DisplayArgument(stepArgumentInsight.argument);
                }
            }
        }
        else
        {
            DisplayNoMessageForThread();
        }
    }

    private void DisplayArgument(Argument argument)
    {
        if (!argument.ArgumentTypeRef.IsNullOrEmpty())
        {
            argumentTypeText.gameObject.SetActive(true);
            var argumentTypeInsight = GLInsights.GetArgumentTypeInsight(argument.ArgumentTypeRef);
            argumentTypeText.text = argumentTypeInsight.GetFullName();
        }
        else
        {
            argumentTypeText.gameObject.SetActive(false);
        }
        argumentText.fontStyle = FontStyles.Normal;
        argumentText.text = argument.Message;
    }

    private void DisplayNoMessageForThread()
    {
        argumentTypeText.gameObject.SetActive(false);
        argumentText.text = Label_NoMessageForThread.Value;
        argumentText.fontStyle = FontStyles.Italic;
    }
}
