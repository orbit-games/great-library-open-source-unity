﻿using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDebateStepIndicator : MonoBehaviour {

    [Header("Settings")]
    public int opponentStepPadding = 5;
    public float opponentStepAlpha = 0.3f;

    [Header("References")]
    public Transform numberFrameTransform;
    public Transform titleTransform;
    public TMP_Text stepNumber;
    public TMP_Text titleText;
    public GameObject completedIndicator;
    public CanvasGroup canvasGroup;
    public HorizontalLayoutGroup horizontalLayoutGroup;
    
    public GameObject reviewerStepAvailable;
    public GameObject submitterStepAvailable;

    private int stepIndex;

    public void Reset()
    {
        titleText.text = "Untitled";
        numberFrameTransform.SetAsFirstSibling();
        completedIndicator.SetActive(false);
        reviewerStepAvailable.SetActive(false);
        submitterStepAvailable.SetActive(false);
        SetIsOpponentStep(false);
    }

    public void Setup(TrackProgressInsight trackInsight, ReviewStepInsight reviewStepInsight, PeerType role)
    {
        Reset();

        stepIndex = reviewStepInsight.stepIndex;
        titleText.text = reviewStepInsight.step.Name;
        stepNumber.text = (stepIndex + 1) + "";

        var stopProgressInsight = GLInsights.GetStepProgressInsight(trackInsight.track.Ref, reviewStepInsight.step.Ref);
        SetStatus(stopProgressInsight.status, role);

        if (role == PeerType.REVIEWER)
        {
            titleTransform.SetAsFirstSibling();
        }

        SetIsOpponentStep(role != reviewStepInsight.step.PeerType);
    }

    public void OnPress()
    {
        GLDebateHall.I.ScrollToStep(stepIndex);
    }

    private void SetIsOpponentStep(bool isOpponentStep)
    {
        if (isOpponentStep)
        {
            SetPadding(opponentStepPadding);
            canvasGroup.alpha = opponentStepAlpha;
        }
        else
        {
            SetPadding(0);
            canvasGroup.alpha = 1;
        }
    }

    private void SetPadding(int paddingOffset)
    {
        var padding = horizontalLayoutGroup.padding;
        padding.left = paddingOffset;
        padding.right = paddingOffset;
        horizontalLayoutGroup.padding = padding;
    }

    private void SetStatus(StepStatus status, PeerType role)
    {
        completedIndicator.SetActive(false);
        reviewerStepAvailable.SetActive(false);
        submitterStepAvailable.SetActive(false);
        switch (status)
        {
            case StepStatus.UNAVAILABLE:
                break;
            case StepStatus.AVAILABLE:
                reviewerStepAvailable.SetActive(role == PeerType.REVIEWER);
                submitterStepAvailable.SetActive(role == PeerType.SUBMITTER);
                break;
            case StepStatus.DANGER:
                reviewerStepAvailable.SetActive(role == PeerType.REVIEWER);
                submitterStepAvailable.SetActive(role == PeerType.SUBMITTER);
                break;
            case StepStatus.LATE:
                reviewerStepAvailable.SetActive(role == PeerType.REVIEWER);
                submitterStepAvailable.SetActive(role == PeerType.SUBMITTER);
                break;
            case StepStatus.CLOSED:
                completedIndicator.SetActive(true);
                break;
            default:
                break;
        }
    }
}
