﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLDebateStepInfo : MonoBehaviour
{
    [Header("Localization")]
    public LocalizedText Label_Prefix_Deadline;
    public LocalizedText Label_StepFuture;
    public LocalizedText Label_WaitingForReviewer;
    public LocalizedText Label_WaitingForSubmitter;

    [Header("References")]
    public Transform stepNumberContainer;
    public GameObject noticeContainer;
    public GameObject skillsContainer;
    public TMP_Text skillsHeader;
    public TMP_Text skillsList;
    public TMP_Text stepNumber;
    public TMP_Text stepTitle;
    public TMP_Text stepSubtitle;
    public TMP_Text stepDescription;
    public TMP_Text notice;

    private string trackRef;
    private string stepRef;

    public void Setup(string trackRef, string stepRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;

        var user = GLApi.GetActiveUser();
        var trackInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var stepProgressInsight = trackInsight.stepInsightsMapping[stepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);

        var isReviewer = trackInsight.track.Reviewer == user.Ref;
        var isReviewerStep = stepInsight.step.PeerType == PeerType.REVIEWER;

        stepTitle.alignment = stepInsight.isReviewerStep ? TextAlignmentOptions.TopRight : TextAlignmentOptions.TopLeft;
        stepSubtitle.alignment = stepTitle.alignment;
        stepDescription.alignment = stepTitle.alignment;
        skillsHeader.alignment = stepTitle.alignment;
        skillsList.alignment = stepTitle.alignment;

        if (stepInsight.isReviewerStep) stepNumberContainer.SetAsLastSibling();
        else stepNumberContainer.SetAsFirstSibling();

        stepNumber.text = (stepInsight.stepIndex + 1) + "";
        stepTitle.text = stepInsight.step.Name;
        stepSubtitle.text = Label_Prefix_Deadline.Format(stepInsight.step.Deadline.Value.ToString()).ToString();

        if (stepInsight.step.Description.IsNullOrEmpty())  stepDescription.text = "";
        else stepDescription.text = stepInsight.step.Description;

        // determine skills
        skillsContainer.SetActive(!stepInsight.step.Skills.IsNullOrEmpty());
        if (!stepInsight.step.Skills.IsNullOrEmpty())
        {
            skillsList.text = "";
            foreach (var skillRef in stepInsight.step.Skills)
            {
                var skillInsight = GLInsights.GetSkillInsight(skillRef);
                if (skillsList.text != "")
                {
                    skillsList.text += "\n";
                }
                skillsList.text += skillInsight.skill.Name;
            }
        }

        noticeContainer.gameObject.SetActive(false);

        if (!stepProgressInsight.isClosed)
        {
            if (stepProgressInsight.status == StepStatus.UNAVAILABLE)
            {
                noticeContainer.gameObject.SetActive(true);
                notice.text = Label_StepFuture.Value;
            }
            else if (!stepProgressInsight.isLoggedInUserStep)
            {
                noticeContainer.gameObject.SetActive(true);
                if (stepInsight.isSubmitterStep) notice.text = Label_WaitingForSubmitter.Value;
                if (stepInsight.isReviewerStep) notice.text = Label_WaitingForReviewer.Value;
            }
        }
    }
}