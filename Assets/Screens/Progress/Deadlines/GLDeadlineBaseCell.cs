﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLDeadlineBaseCell : MonoBehaviour
{
    public LocalizedTextBehaviour textField;

    public virtual void Setup(GLFormattedLocalizedText text)
    {
        textField.FormattedAsset = text;
    }
}
