﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GLDeadlineColumn : MonoBehaviour
{
    public GLDeadlineBaseCell headerCell;
    public GLPoolManagedList stepsList;

    public enum DeadlineState
    {
        Passed, Next, Future
    }

    public void Setup(string chapterRef)
    {
        var chapterInsight = GLInsights.GetChapterInsight(chapterRef);

        // for future use if we want to incorporate the progress of te player into the table
        //var userTracks = GLInsights.GetTracks(chapterRef, GLApi.GetActiveUser().Ref);
        //userTracks.ForEach(track =>
        //{
        //    track.isCompleted
        //});
        
        var firstFound = false;
        stepsList.SyncFully<ReviewStep, GLDeadlineCell>(chapterInsight.chapter.ReviewSteps,
            step => step.Ref, (step, cell) => 
            {
                bool passed = step.Deadline.Value < GLDate.Now;

                if (!passed && !firstFound)
                {
                    firstFound = true;
                    cell.Setup(step.Deadline.Value, DeadlineState.Next);
                }
                else
                {
                    cell.Setup(step.Deadline.Value, passed ? DeadlineState.Passed : DeadlineState.Future);
                }
            });

        headerCell.Setup(chapterInsight.chapter.Name);
    }
}
