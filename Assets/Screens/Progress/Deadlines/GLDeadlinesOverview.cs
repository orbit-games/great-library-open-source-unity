﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLDeadlinesOverview : MonoBehaviour
{
    public GLPoolManagedList reviewStepsColumn;
    public GLPoolManagedList chapterColumns;

    public void OnEnable()
    {
        var course = GLApi.GetActiveCourse();

        if (!GLApi.IsLoggedInAndCourseSelected()) return;
        if (course.Chapters.Count == 0)
        {
            reviewStepsColumn.Clear();
            chapterColumns.Clear();
            return;
        }

        // place the step names
        var reviewSteps = course.Chapters[0].ReviewSteps;
        reviewStepsColumn.SyncFully<ReviewStep, GLDeadlineReviewStepHeader>(reviewSteps, step => step.Ref,
            (step, header) => header.Setup(step.Name));

        // place the chapters
        var chapters = course.Chapters;
        chapterColumns.SyncFully<Chapter, GLDeadlineColumn>(chapters, s => s.Ref,
            (chapter, column) => column.Setup(chapter.Ref));
    }
}
