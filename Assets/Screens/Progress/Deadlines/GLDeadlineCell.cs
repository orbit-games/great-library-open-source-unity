﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLDeadlineCell : GLDeadlineBaseCell
{
    public Image cellPanel;
    public TMP_Text cellTextField;

    public bool passedDeadlinePanelEnabled;
    public Color passedDeadlineTextColor;
    public Color passedDeadlinePanelColor;

    public bool nextDeadlinePanelEnabled;
    public Color nextDeadlineTextColor;
    public Color nextDeadlinePanelColor;

    public bool futureDeadlinePanelEnabled;
    public Color futureDeadlineTextColor;
    public Color futureDeadlinePanelColor;

    public void Setup(DateTime dateTime, GLDeadlineColumn.DeadlineState deadlineState)
    {
        base.Setup(dateTime.ToString("g"));

        switch (deadlineState)
        {
            case GLDeadlineColumn.DeadlineState.Passed:
                cellPanel.enabled = passedDeadlinePanelEnabled;
                cellPanel.color = passedDeadlinePanelColor;
                cellTextField.color = passedDeadlineTextColor;
                break;
            case GLDeadlineColumn.DeadlineState.Next:
                cellPanel.enabled = nextDeadlinePanelEnabled;
                cellPanel.color = nextDeadlinePanelColor;
                cellTextField.color = nextDeadlineTextColor;
                break;
            case GLDeadlineColumn.DeadlineState.Future:
                cellPanel.enabled = futureDeadlinePanelEnabled;
                cellPanel.color = futureDeadlinePanelColor;
                cellTextField.color = futureDeadlineTextColor;
                break;
            default:
                break;
        }
    }
}
