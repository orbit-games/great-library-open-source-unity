﻿using Fenderrio.ImageWarp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLProgressTreeLine : MonoBehaviour {

    public RawImageWarp rawImageWarp;
    public GLConnectingWarpedLine warpedLine;
    public GLColorState colorState;
    public Texture2D otherUserTexture;
    public Texture2D currentUserTexture;
    public CanvasGroup canvasGroup;

    public void Setup(bool currentUser, Transform from, Transform to)
    {
        rawImageWarp.texture = currentUser ? currentUserTexture : otherUserTexture;
        colorState.SetColorState(currentUser);

        warpedLine.from = from;
        warpedLine.to = to;

        warpedLine.imageOffset = Random.value;
        warpedLine.beginWarp = (RandomWarp + RandomWarp) * 0.5f;
        warpedLine.endWarp = (RandomWarp + RandomWarp) * 0.5f;
    }

    private float RandomWarp
    {
        get
        {
            return Random.Range(-2.5f, 2.5f);
        }
    }
}
