﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLProgressTreeTrackStep : MonoBehaviour {

    public GLCircleLayout circleLayout;
    public GLAnimateWiggly animateWiggly;
    public Transform submissionIcon;
    public Transform quizIcon;
    public Transform discussionIcon;
    public GameObject highlighter;
    public CanvasGroup canvasGroup;

    string reviewStepRef;
    string trackRef;
    string userRef;

    public void Setup(string reviewStepRef, string trackRef, string userRef)
    {
        this.reviewStepRef = reviewStepRef;
        this.trackRef = trackRef;
        this.userRef = userRef;

        var stepInsight = GLInsights.GetReviewStepInsight(reviewStepRef);
        var stepProgressInsight = stepInsight.GetProgressInsight(trackRef);

        submissionIcon.gameObject.SetActive(stepProgressInsight.isMarkedClosed && stepProgressInsight.hasSubmissionData);
        quizIcon.gameObject.SetActive(stepProgressInsight.isMarkedClosed && stepProgressInsight.hasQuizData);
        discussionIcon.gameObject.SetActive(stepProgressInsight.isMarkedClosed && stepProgressInsight.hasDiscussionData);

        circleLayout.startPhase = Random.Range(-10f, 10f) + (Random.value > 0.5f ? 180f : 0f);

        submissionIcon.localEulerAngles = Vector3.forward * Random.Range(-10f, 10f);
        quizIcon.localEulerAngles = Vector3.forward * Random.Range(-10f, 10f);
        discussionIcon.localEulerAngles = Vector3.forward * Random.Range(-10f, 10f);

        highlighter.SetActive(stepProgressInsight.isActiveStep && stepProgressInsight.isLoggedInUserStep);

        animateWiggly.Reinitialize();
        animateWiggly.enabled = highlighter.activeSelf;

        canvasGroup.alpha = stepProgressInsight.isAvailable ? 1f : 0.25f;
        GLColorState.BroadcastColorState(this, stepProgressInsight.userRef == userRef);
    }

    public void OnPress()
    {
        GLGameFlow.I.ShowDebate(trackRef, reviewStepRef);
    }
}
