﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLProgressTree : MonoBehaviour {

    [Header("Settings")]
    public float chapterWidth = 200f;

    [Header("References")]
    public Transform chapterContainer;
    public Transform linesContainer;
    public Transform treeSource;
    public TMP_Text displayName;

    [Header("Prefabs")]
    public GLProgressTreeChapter chapterPrefab;
    public GLProgressTreeLine linePrefab;

    public void OnEnable()
    {
        RebuildTree();
    }

    private void RebuildTree()
    {
        GLPool.removeAllPoolableChildren(chapterContainer);
        GLPool.removeAllPoolableChildren(linesContainer);
        
        var course = GLApi.GetActiveCourse();
        var user = GLApi.GetActiveUser();

        if (course == null || user == null) return;

        var totalWidth = chapterWidth * (course.Chapters.Count * 2 - 1) * 0.5f;
        var currentUnitCircleX = -0.65f;
        var unitCircleXStep = 2f * Mathf.Abs(currentUnitCircleX) / (course.Chapters.Count * 2 - 1);
        var startUnitCircleY = Mathf.Sin(Mathf.Acos(currentUnitCircleX)) - 0.2f;

        // correction on total width based on start X on unit circle;
        totalWidth = totalWidth / Mathf.Abs(currentUnitCircleX);
        
        displayName.text = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref).DisplayName;

        for (int chapterIndex = 0; chapterIndex < course.Chapters.Count; chapterIndex++)
        {
            var chapter = course.Chapters[chapterIndex];
            var chapterObject = GLPool.placeCopy(chapterPrefab, chapterContainer);
            var unitCircleY = Mathf.Sin(Mathf.Acos(currentUnitCircleX));
            chapterObject.transform.localPosition = new Vector2(currentUnitCircleX, unitCircleY - startUnitCircleY) * totalWidth;
            chapterObject.Setup(chapter.Ref, user.Ref, PeerType.SUBMITTER);
            currentUnitCircleX += unitCircleXStep;

            var chapterProgress = chapter.GetProgressInsight(user.Ref);
            BuildLine(chapterObject, chapterProgress.isSubmissionStepAvailable);
        }

        for (int chapterIndex = course.Chapters.Count - 1; chapterIndex >= 0; chapterIndex--)
        {
            var chapter = course.Chapters[chapterIndex];
            var chapterObject = GLPool.placeCopy(chapterPrefab, chapterContainer);
            var unitCircleY = Mathf.Sin(Mathf.Acos(currentUnitCircleX));
            chapterObject.transform.localPosition = new Vector2(currentUnitCircleX, unitCircleY - startUnitCircleY) * totalWidth;
            chapterObject.Setup(chapter.Ref, user.Ref, PeerType.REVIEWER);
            currentUnitCircleX += unitCircleXStep;

            var chapterProgress = chapter.GetProgressInsight(user.Ref);
            BuildLine(chapterObject, chapterProgress.isReviewStepAvailable);
        }
    }

    private void BuildLine(GLProgressTreeChapter chapterObject, bool animate)
    {
        var from = chapterObject.chapterName.transform;
        var line = GLPool.placeCopy(linePrefab, linesContainer);
        line.Setup(true, from, treeSource);
        var diff = (from.position - treeSource.position).normalized;
        line.warpedLine.beginWarp = diff.x * -6f;
        line.warpedLine.endWarp = diff.x * 6f;
        line.warpedLine.animate = animate;
        line.warpedLine.startWidth = animate ? 2f : 0.8f;
        line.warpedLine.endWidth = animate ? 0.8f : 0.35f;
        line.canvasGroup.alpha = animate ? 1f : 0.3f;
        Debug.Log(line.warpedLine.beginWarp);
    }
}
