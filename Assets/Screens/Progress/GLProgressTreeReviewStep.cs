﻿using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLProgressTreeReviewStep : MonoBehaviour {

    [Header("Settings")]
    public float userStepsWidth = 100f;
    public float otherStepsWidth = 150f;

    public float submissionStepScale = 1f;
    public float userStepScale = 0.75f;
    public float otherStepScale = 0.5f;

    [Header("References")]
    public TMP_Text stepName;
    public TMP_Text stepDeadline;
    public Transform trackStepContainer;

    [Header("Prefabs")]
    public GLProgressTreeTrackStep trackStepPrefab;
    
    string reviewStepRef;

    public List<Tuple<Transform, bool>> Setup(string reviewStepRef, string userRef, PeerType role)
    {
        this.reviewStepRef = reviewStepRef;

        var stepInsight = GLInsights.GetReviewStepInsight(reviewStepRef);
        var chapterInsight = stepInsight.chapterInsight;
        var trackInsights = GLInsights.GetTracks(chapterInsight.chapter.Ref, userRef, role);

        var isUserStep = stepInsight.step.PeerType == role;

        stepName.text = stepInsight.step.Name;
        stepDeadline.text = stepInsight.step.Deadline.Value.ToLongDateString();

        GLColorState.BroadcastColorState(stepName, isUserStep);
        GLColorState.BroadcastColorState(stepDeadline, isUserStep);

        GLPool.removeAllPoolableChildren(trackStepContainer);

        var branchables = new List<Tuple<Transform, bool>>();

        if (!stepInsight.requiresDiscussion && !stepInsight.requiresQuiz && isUserStep)
        {
            // can only mean that given user only requires a submission, so just show 1 step
            var trackStep = GLPool.placeCopy(trackStepPrefab, trackStepContainer);
            trackStep.Setup(reviewStepRef, trackInsights[0].track.Ref, userRef);
            trackStep.transform.localScale = Vector3.one * submissionStepScale;

            var offset = Vector3.up * UnityEngine.Random.Range(-10f, 10f)
                        + Vector3.right * UnityEngine.Random.Range(-10f, 10f);

            trackStep.transform.localPosition = offset;

            var progressInsight = trackInsights[0].stepInsightsMapping[reviewStepRef];

            for (int i = 0; i < trackInsights.Count; i++)
                branchables.Add(Tuple.Create(trackStep.transform, progressInsight.isAvailable));
        }
        else
        {
            var width = isUserStep ? userStepsWidth : otherStepsWidth;
            var stepDistance = (width / (trackInsights.Count - 1 + 2));
            width = stepDistance * (trackInsights.Count - 1);

            var current = Vector3.left * width * 0.5f;
            var scale = isUserStep ? userStepScale : otherStepScale;

            foreach (var trackInsight in trackInsights)
            {
                var trackStep = GLPool.placeCopy(trackStepPrefab, trackStepContainer);

                var offset = Vector3.up * UnityEngine.Random.Range(-10f, 10f)
                            + Vector3.right * UnityEngine.Random.Range(-10f, 10f);

                trackStep.transform.localScale = Vector3.one * scale;
                trackStep.transform.localPosition = current + offset;
                current += Vector3.right * stepDistance;

                trackStep.Setup(reviewStepRef, trackInsight.track.Ref, userRef);

                var progressInsight = trackInsight.stepInsightsMapping[reviewStepRef];
                branchables.Add(Tuple.Create(trackStep.transform, progressInsight.isAvailable));
            }
        }

        return branchables;
    }
}
