﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLProgressTreeChapter : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Label_ChapterPrefix;
    public LocalizedText Label_SubmitterTracks;
    public LocalizedText Label_ReviewerTracks;
    public LocalizedText Label_UnknownReviewer;
    public LocalizedText Label_UnknownSubmitter;

    [Header("Settings")]
    public float otherUsersWidth = 150f;
    public float totalHeight = 1500f;
    public float minimalHeightPerStep = 128f;

    [Header("References")]
    public LocalizedTextBehaviour chapterNumber;
    public LocalizedTextBehaviour chapterBranches;
    public CanvasGroup canvasGroup;
    public GLAnimateWiggly chapterRing;
    public TMP_Text chapterName;

    public Transform otherUsersContainer;
    public Transform stepsContainer;
    public Transform branchesContainer;

    public Transform userBranchStart;

    [Header("Prefabs")]
    public GLProgressTreeLine linePrefab;
    public GLProgressTreeOtherUser otherUserPrefab;
    public GLProgressTreeReviewStep stepPrefab;

    public void Setup(string chapterRef, string userRef, PeerType role)
    {
        // reset
        GLPool.removeAllPoolableChildren(otherUsersContainer);
        GLPool.removeAllPoolableChildren(stepsContainer);
        GLPool.removeAllPoolableChildren(branchesContainer);

        // initialize basic info
        var isSubmitterTracks = role == PeerType.SUBMITTER;
        var chapterInsight = GLInsights.GetChapterInsight(chapterRef);
        var chapterProgressInsight = chapterInsight.GetProgressInsight(userRef);
        var chapter = chapterInsight.chapter;
        var courseInsight = chapterInsight.courseInsight;
        var course = courseInsight.course;

        // defining the chapter
        chapterNumber.FormattedAsset = Label_ChapterPrefix.Format(chapterInsight.chapterNumber);
        chapterBranches.LocalizedAsset = isSubmitterTracks ? Label_SubmitterTracks : Label_ReviewerTracks;
        chapterName.text = chapterInsight.chapter.Name;
        chapterName.transform.localEulerAngles = Vector3.forward * UnityEngine.Random.Range(-7f, 7f);

        // handle display when (in)active
        var trackInsights = GLInsights.GetTracks(chapterRef, userRef, role);
        var available = chapterProgressInsight.isAvailable && !trackInsights.IsNullOrEmpty();
        chapterRing.enabled = available;
        canvasGroup.alpha = available ? 1f : 0.5f;
        if (!available)
        {
            return;
        }

        // helpers for the branches
        List<List<Tuple<Transform, bool>>> otherBranches = new List<List<Tuple<Transform, bool>>>();
        List<List<Tuple<Transform, bool>>> myBranches = new List<List<Tuple<Transform, bool>>>();

        // creating the other users
        var stepDistance = (otherUsersWidth / (trackInsights.Count - 1 + 2));
        var width = stepDistance * (trackInsights.Count - 1);
        var current = Vector3.left * width * 0.5f;

        for (int i = 0; i < trackInsights.Count; i++)
        {
            var trackInsight = trackInsights[i];
            var otherUserObject = GLPool.placeCopy(otherUserPrefab, otherUsersContainer);

            var otherUserRef = isSubmitterTracks ? trackInsight.track.Reviewer : trackInsight.track.Submitter;
            if (otherUserRef != null)
            {
                otherUserObject.Setup(i, GLApi.GetCourseProgressSummaryKnowledge(otherUserRef, course.Ref).DisplayName, otherUserRef);
            }
            else
            {
                otherUserObject.Setup(i, isSubmitterTracks ? string.Format(Label_UnknownSubmitter, i + 1) : string.Format(Label_UnknownReviewer, i + 1), null);
            }
            
            otherUserObject.transform.localPosition = current;
            current += Vector3.right * stepDistance;

            // process the branch helpers
            var myTrackBranches = new List<Tuple<Transform, bool>>();
            myTrackBranches.Add(Tuple.Create(userBranchStart, true));
            myBranches.Add(myTrackBranches);

            var otherTrackBranches = new List<Tuple<Transform, bool>>();
            otherTrackBranches.Add(Tuple.Create(otherUserObject.transform, true));
            otherBranches.Add(otherTrackBranches);
        }

        // creating the review steps and setting positions
        var courseTimeSeconds = courseInsight.timeSpan.TotalSeconds;
        var heightPerSecond = totalHeight / courseTimeSeconds;
        var firstStep = chapterInsight.chapter.ReviewSteps[0];
        var currentHeight = 0f;

        //var startYposition = (float)((firstStep.Deadline.Value - courseInsight.startTime).TotalSeconds * heightPerSecond);
        //transform.localPosition = new Vector3(transform.localPosition.x, startYposition, transform.localPosition.y);
        
        foreach (var step in chapter.ReviewSteps)
        {
            //var stepTrueHeight = (float)((step.Deadline.Value - firstStep.Deadline.Value).TotalSeconds * heightPerSecond);
            //currentHeight = Mathf.Max(stepTrueHeight, currentHeight);

            var stepObject = GLPool.placeCopy(stepPrefab, stepsContainer);
            var branchables = stepObject.Setup(step.Ref, userRef, role);
            stepObject.transform.localPosition = Vector3.up * currentHeight;

            currentHeight = currentHeight + minimalHeightPerStep;

            // process the branch helpers
            for (int i = 0; i < branchables.Count; i++)
            {
                if (step.PeerType == role)
                {
                    myBranches[i].Add(branchables[i]);
                }
                else
                {
                    otherBranches[i].Add(branchables[i]);
                }
            }
        }

        // creating the branches
        foreach (var otherBranchTrack in otherBranches)
        {
            for (int i = 0; i < otherBranchTrack.Count - 1; i++)
            {
                MakeLine(false, otherBranchTrack, i);
            }
        }
        foreach (var myBranchTrack in myBranches)
        {
            for (int i = 0; i < myBranchTrack.Count - 1; i++)
            {
                MakeLine(true, myBranchTrack, i);
            }
        }
    }

    private void MakeLine(bool thisUser, List<Tuple<Transform, bool>> branchList, int i)
    {
        var animate = branchList[i + 1].Item2;
        var from = branchList[i + 1].Item1;
        var to = branchList[i].Item1;

        var line = GLPool.placeCopy(linePrefab, branchesContainer);
        line.Setup(thisUser, from, to);
        line.canvasGroup.alpha = animate ? 1f : 0.3f;
        line.warpedLine.animate = animate;
        line.warpedLine.startWidth = animate ? 1.2f : 0.7f;
        line.warpedLine.endWidth = animate ? 1.2f : 0.7f;
    }
}
