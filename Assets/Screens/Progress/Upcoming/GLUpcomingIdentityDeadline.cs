﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLUpcomingIdentityDeadline : MonoBehaviour
{
    [Header("References")]
    public TMP_Text deadlineNumber;
    public TMP_Text deadlineUnit;

    public GameObject todoContainer;
    public GameObject doneContainter;
    public GameObject warningContainer;

    public void DisplayIfNecessary()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var courseInsight = course.GetInsight();
        if (courseInsight.timelineInsights.Count > 0)
        {
            var timelineInsight = courseInsight.timelineInsights[0];
            var definedIdentity = GLInsights.HasFakeIdentityDefined(user.Ref, course.Ref);

            gameObject.SetActive(!(timelineInsight.isPassed && definedIdentity));
            todoContainer.SetActive(!timelineInsight.isPassed && !definedIdentity);
            doneContainter.SetActive(!timelineInsight.isPassed && definedIdentity);
            warningContainer.SetActive(timelineInsight.isPassed && !definedIdentity);
            
            var prettyDate = GLDate.GetPrettyDate(timelineInsight.deadline);
            deadlineNumber.text = prettyDate.number.ToString();
            deadlineUnit.text = prettyDate.unit.ToString();
        }
        else
        {
            gameObject.SetActive(false);
        }

    }

    public void OnPress()
    {
        GLGameFlow.I.ShowCourseRegistration();
    }
}
