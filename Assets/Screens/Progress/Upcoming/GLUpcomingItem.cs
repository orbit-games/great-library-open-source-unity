﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLUpcomingItem : MonoBehaviour
{
    [Header("Localization")]
    public LocalizedText chapterPrefix;
    public LocalizedText reviewerBranchNumber;
    public LocalizedText submitterBranchNumber;
    public LocalizedText submitterBranches;

    [Header("References")]
    public TMP_Text deadlineNumber;
    public TMP_Text deadlineUnit;

    public LocalizedTextBehaviour chapterTitle;
    public LocalizedTextBehaviour branchSubtitle;
    public TMP_Text stepTitle;

    public GameObject todoContainer;
    public GameObject doneContainter;
    public GameObject unavailableContainter;
    public GameObject warningContainer;
    public GameObject handInReady;

    string trackRef;
    string reviewStepRef;

    public void Setup(string trackRef, string reviewStepRef)
    {
        this.reviewStepRef = reviewStepRef;
        this.trackRef = trackRef;

        gameObject.SetActive(true);
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var trackProgressInsight = GLInsights.GetTrackProgressInsight(trackRef);
        var chapterInsight = GLInsights.GetChapterInsight(trackProgressInsight.chapterRef);
        var upcomingStepProgressInsights = trackProgressInsight.stepInsightsMapping[reviewStepRef];
        var stepInsight = GLInsights.GetReviewStepInsight(upcomingStepProgressInsights.reviewStepRef);

        var prettyDate = GLDate.GetPrettyDate(stepInsight.step.Deadline.Value);
        var submissionOnly = (!stepInsight.requiresDiscussion && !stepInsight.requiresQuiz);

        deadlineNumber.text = prettyDate.number.ToString();
        deadlineUnit.text = prettyDate.unit.ToString();

        chapterTitle.FormattedAsset = chapterPrefix.Format(chapterInsight.chapterNumber);

        stepTitle.text = stepInsight.step.Name;
        
        var index = 0;
        if (trackProgressInsight.isLoggedInUserReviewer)
        {
            index = trackProgressInsight.reviewerChapterProgressInsight.userReviewerTrackInsights.IndexOf(trackProgressInsight);
            //count = trackProgressInsight.reviewerChapterProgressInsight.userReviewerTrackInsights.Count;
            branchSubtitle.FormattedAsset = reviewerBranchNumber.Format("" + (index + 1));
        }
        else
        {
            index = trackProgressInsight.submitterChapterProgressInsight.userSubmitterTrackInsights.IndexOf(trackProgressInsight);
            //count = trackProgressInsight.submitterChapterProgressInsight.userSubmitterTrackInsights.Count;

            if (submissionOnly)
            {
                branchSubtitle.LocalizedAsset = submitterBranches;
            }
            else
            {
                branchSubtitle.FormattedAsset = submitterBranchNumber.Format("" + (index + 1));
            }
        }

        handInReady.SetActive(!upcomingStepProgressInsights.isMarkedClosed && upcomingStepProgressInsights.isReadyForClose);
        warningContainer.SetActive(!upcomingStepProgressInsights.isMarkedClosed && upcomingStepProgressInsights.isDeadlinePassed);
        todoContainer.SetActive(upcomingStepProgressInsights.isAvailable && !upcomingStepProgressInsights.isMarkedClosed);
        doneContainter.SetActive(upcomingStepProgressInsights.isAvailable && upcomingStepProgressInsights.isMarkedClosed);
        unavailableContainter.SetActive(!upcomingStepProgressInsights.isAvailable);
    }

    public void OnPress()
    {
        var trackProgressInsight = GLInsights.GetTrackProgressInsight(trackRef);
        GLGameFlow.I.ShowDebate(trackRef, reviewStepRef);
    }
}
