﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLUpcomingTourDeadline : MonoBehaviour
{
    [Header("Preferences")]
    public GLBoolPreference tourCompletion;

    [Header("References")]
    public TMP_Text deadlineNumber;
    public TMP_Text deadlineUnit;

    public GameObject todoContainer;
    public GameObject doneContainter;
    public GameObject warningContainer;
    
    public void DisplayIfNecessary()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var courseInsight = course.GetInsight();
        if (courseInsight.timelineInsights.Count > 0)
        {
            var timelineInsight = courseInsight.timelineInsights[0];
            var tourCompleted = tourCompletion.Value;

            gameObject.SetActive(!(timelineInsight.isPassed && tourCompleted));
            todoContainer.SetActive(!timelineInsight.isPassed && !tourCompleted);
            doneContainter.SetActive(!timelineInsight.isPassed && tourCompleted);
            warningContainer.SetActive(timelineInsight.isPassed && !tourCompleted);

            var prettyDate = GLDate.GetPrettyDate(timelineInsight.deadline);
            deadlineNumber.text = prettyDate.number.ToString();
            deadlineUnit.text = prettyDate.unit.ToString();
        }
        else
        {
            gameObject.SetActive(false);
        }

    }

    public void OnPress()
    {
        GLGameFlow.I.ShowTourOffice();
    }
}
