﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLUpcomingSteps : MonoBehaviour {
    
    [Header("References")]
    public Transform upcomingItemsContainer;
    public GLUpcomingIdentityDeadline identityDeadline;
    public GLUpcomingTourDeadline tourDeadline;

    [Header("Prefabs")]
    public GLUpcomingItem upcomingPrefab;

    private void OnEnable()
    {
        Rebuid();
    }

    public void Rebuid()
    {
        GLPool.removeAllPoolableChildren(upcomingItemsContainer);

        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        if (!GLApi.IsLoggedInAndCourseSelected()) return;

        identityDeadline.DisplayIfNecessary();
        tourDeadline.DisplayIfNecessary();

        var courseProgressInsight = GLInsights.GetCourseProgressInsight(course.Ref, user.Ref);

        courseProgressInsight.upcomingStepInsights.Sort((step1, step2) =>
        {
            var deadline1 = GLInsights.GetReviewStepInsight(step1.reviewStepRef).step.Deadline.Value;
            var deadline2 = GLInsights.GetReviewStepInsight(step2.reviewStepRef).step.Deadline.Value;
            return deadline1.CompareTo(deadline2);
        });

        foreach (var stepProgressInsight in courseProgressInsight.upcomingStepInsights)
        {
            // if it is a submission only step, then let's only place the frist banner
            var stepInsight = GLInsights.GetReviewStepInsight(stepProgressInsight.reviewStepRef);
            if (stepInsight.requiresSubmissionOnly 
                && stepInsight.isSubmitterStep 
                && stepProgressInsight.trackInsight.submitterTrackIndex > 0)
            {
                continue;
            }
            
            // place the banner
            var todo = GLPool.placeCopy(upcomingPrefab, upcomingItemsContainer);
            todo.Setup(stepProgressInsight.trackInsight.track.Ref, stepProgressInsight.reviewStepRef);
        }
    }
}
