﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLProgressTreeOtherUser : MonoBehaviour {

    public TMP_Text displayName;
    public GLDirectChatButton chatButton;

    public int trackIndex;

    public void Setup(int trackIndex, string name, string userRef)
    {
        this.trackIndex = trackIndex;
        displayName.text = name;
        chatButton.Setup(userRef);
    }
}
