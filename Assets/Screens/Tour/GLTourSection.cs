﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLTourSection : MonoBehaviour {
    public LocalizedText title;
    private void OnDrawGizmosSelected()
    {
        if (title == null) return;
        gameObject.name = title.name;
    }
}
