﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLTourScreen : MonoBehaviour {

    public LocalizedText caption;
    private void OnDrawGizmosSelected()
    {
        if (caption == null) return;
        gameObject.name = caption.name;
    }
}
