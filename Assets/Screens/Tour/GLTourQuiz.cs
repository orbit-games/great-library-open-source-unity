﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLTourQuiz : GLTourScreen
{
    [Serializable]
    public class TourQuizQuestion
    {
        public LocalizedText questionText;
        public List<TourQuizOption> answers;
    }

    [Serializable]
    public class TourQuizOption
    {
        public LocalizedText optionText;
        public bool correct;
    }

    public GLTourScreen onTestFail;
    public string uniquePrefix = "";
    public LocalizedText quizTitle;
    public LocalizedText quizDescription;
    public List<TourQuizQuestion> questions;
    
    public Quiz GenerateQuiz()
    {
        var id = 0;

        List<QuizElement> quizElements = new List<QuizElement>();
        foreach (var question in questions)
        {
            List<QuizElementOption> quizOptions = new List<QuizElementOption>();
            foreach (var answer in question.answers)
            {
                var option = new QuizElementOption(uniquePrefix + (id++), quizOptions.Count, answer.optionText.Value, answer.correct ? 1 : 0);
                quizOptions.Add(option);
            }

            var element = new QuizElement(uniquePrefix + (id++), quizElements.Count, question.questionText.Value, QuestionType.SELECT_ONE, quizOptions);
            quizElements.Add(element);
        }

        return new Quiz(uniquePrefix + (id++), quizTitle.Value, quizDescription.Value, questions.Count, quizElements);
    }
}
