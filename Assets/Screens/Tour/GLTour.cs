﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLTour : GLSingletonBehaviour<GLTour>, ICheatListener
{

    protected override void OnSingletonInitialize()
    {
        for (int i = 0; i < tourContainer.childCount; i++)
        {
            var section = tourContainer.GetChild(i).GetComponent<GLTourSection>();
            if (section != null)
            {
                section.gameObject.SetActive(false);
                sections.Add(section);
                var list = new List<GLTourScreen>();
                screens.Add(list);

                for (int w = 0; w < section.transform.childCount; w++)
                {
                    var screen = section.transform.GetChild(w).GetComponent<GLTourScreen>();
                    if (screen != null)
                    {
                        list.Add(screen);
                        screen.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    private List<GLTourSection> sections = new List<GLTourSection>();
    private List<List<GLTourScreen>> screens = new List<List<GLTourScreen>>();

    [Header("Localization")]
    public LocalizedText Title_TourFinished;
    public LocalizedText Description_TourFinished;

    public LocalizedText Title_TestFinished;
    public LocalizedText Description_TestFinished;

    public LocalizedText Title_TestFailed;
    public LocalizedText Description_TestFailed;

    [Header("Settings")]
    public float startWaitSeconds = 0.2f;
    public float characterSeconds = 0.05f;
    public float kommaSeconds = 0.1f;
    public float interpunctionSeconds = 0.2f;
    [Space(10f)]
    public float knownScreenTimeMultiplier = 0.5f;
    public int knownScreenCharacterSkips = 1;
    public float generalTimeMultiplier = 1f;
    public int generalCharacterSkips = 1;

    [Header("Preferences")]
    public GLBoolPreference tourCompleted;
    public GLIntPreference furthestTourSection;
    public GLIntPreference furthestTourScreen;

    [Header("References")]
    public Transform quizContainer;
    public Transform tourGuideContainer;
    public Transform tourContainer;

    public TMP_Text sectionText;
    public TMP_Text invisibleText;
    public TMP_Text displayText;

    public GameObject backButton;
    public GameObject forwardButton;
    public GameObject finishButton;
    public GameObject takeTestButton;
    public GameObject exitButton;

    public GLQuiz quiz;

    [Header("State")]
    [ReadOnly]
    public int sectionIndex;
    [ReadOnly]
    public int screenIndex;
    [ReadOnly]
    public GLTourSection sectionObject;
    [ReadOnly]
    public GLTourScreen screenObject;

    private void OnEnable()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        if (user == null || course == null) return;
        StartCoroutine(DisplayScreen(0, 0));
    }

    private bool IsQuizScreen()
    {
        return screenObject is GLTourQuiz;
    }

    private bool IsTourEnd()
    {
        return sectionIndex == sections.Count - 1 && screenIndex == screens[sectionIndex].Count - 1;
    }

    private bool IsTourBeginning()
    {
        return sectionIndex == 0 && screenIndex == 0;
    }

    private bool IsKnownScreen()
    {
        return sectionIndex < furthestTourSection.Value || 
            (sectionIndex == furthestTourSection.Value && screenIndex <= furthestTourScreen.Value);
    }

    private bool IsFurthestScreenYet()
    {
        return sectionIndex == furthestTourSection.Value && screenIndex == furthestTourScreen.Value;
    }

    private bool IsNewFurthestScreenYet()
    {
        if (sectionIndex < furthestTourSection.Value)
        {
            return false;
        }
        if (sectionIndex > furthestTourSection.Value)
        {
            return true;
        }
        if (screenIndex > furthestTourScreen.Value)
        {
            return true;
        }
        return false;
    }

    private void HandleScreenDisplay()
    {
        for (int testSectionIndex = 0; testSectionIndex < sections.Count; testSectionIndex++)
        {
            var section = sections[testSectionIndex];

            if (sectionIndex != testSectionIndex)
            {
                GLTransition.Out(section);
            }
            else
            {
                GLTransition.In(section);
            }

            for (int testScreenIndex = 0; testScreenIndex < screens[testSectionIndex].Count; testScreenIndex++)
            {
                var screen = screens[testSectionIndex][testScreenIndex];

                if (screenIndex != testScreenIndex)
                {
                    GLTransition.Out(screen);
                }
                else
                {
                    GLTransition.In(screen);
                }

            }
        }
    }

    //private bool fastForwardTour;
    public string OnCheat(string cheatUID)
    {
        if (cheatUID == "finishTour")
        {
            Finish();
            return "Finishing tour";
        }
        //else if (cheatUID == "fastForwardTour")
        //{
        //    fastForwardTour = !fastForwardTour;
        //    if (fastForwardTour) return "Tour fast forward activated";
        //    else return  "Tour fast forward disabled";
        //}
        return null;
    }

    private IEnumerator DisplayScreen(int sectionIndex, int screenIndex)
    {
        this.sectionIndex = sectionIndex;
        this.screenIndex = screenIndex;
        sectionObject = sections[sectionIndex];
        screenObject = screens[sectionIndex][screenIndex];

        HandleScreenDisplay();

        GLTransition.Out(backButton);
        GLTransition.Out(forwardButton);
        GLTransition.Out(finishButton);
        GLTransition.Out(takeTestButton);

        GLTransition.Out(quizContainer);
        GLTransition.In(tourGuideContainer);
        GLTransition.In(tourContainer);

        // GLTransition.In(exitButton);
        GLTransition.Out(exitButton); // we don't need the exit button anymore

        // get the text and process it
        var course = GLApi.GetActiveCourse();
        var finalText = screenObject.caption.Value;
        finalText = finalText.Replace("[COURSE_NAME]", course.Name);
        finalText = finalText.Replace("[CHAPTER_COUNT]", course.Chapters.Count.ToString());

        // set text start values
        sectionText.text = sectionObject.title;
        invisibleText.text = finalText + "<alpha=#00>" + "</alpha>";
        displayText.text = "";

        // determine a general time multiplier 
        var multiplier = generalTimeMultiplier;
        var skipCharacters = generalCharacterSkips;
        if (IsKnownScreen()) {
            skipCharacters = knownScreenCharacterSkips;
            multiplier *= knownScreenTimeMultiplier;
        }

        // just to display the hint in the log
        if (IsTourBeginning() && GLCheats.fastForward)
        {
            multiplier = 0f;
        }

        // build the text
        yield return new WaitForSeconds(startWaitSeconds * multiplier);

        var character = 0;
        var currentSkip = skipCharacters;
        while (character < finalText.Length)
        {

            // get next character, but if its a rich text tag, then skip until that one is closed
            var nextChar = finalText[character++];
            if (nextChar == '<')
            {
                while (nextChar != '>') nextChar = finalText[character++];
                nextChar = finalText[character++];
            }

            // skip using cheat?
            if (GLCheats.fastForward)
            {
                multiplier = 0f;
                character = finalText.Length;
                nextChar = finalText[character - 1];
            }

            // build text display accordingly
            displayText.text = finalText.Substring(0, character) + "<alpha=#00>" + finalText.Substring(character) + "</alpha>";

            // early break if finished, no need to check character
            if (character == finalText.Length)
                break;

            switch (nextChar)
            {
                case ',':
                    currentSkip = skipCharacters;
                    yield return new WaitForSeconds(kommaSeconds * multiplier);
                    break;
                case '.':
                case '?':
                case '!':
                    currentSkip = skipCharacters;
                    yield return new WaitForSeconds(interpunctionSeconds * multiplier);
                    break;
                default:
                    if (currentSkip > 0)
                    {
                        currentSkip--;
                    } else
                    {
                        currentSkip = skipCharacters;
                        yield return new WaitForSeconds(characterSeconds * multiplier);
                    }
                    break;
            }
        }

        // show take test button?
        if (IsQuizScreen())
        {
            GLTransition.In(takeTestButton);
        }

        // show back button?
        if (!IsTourBeginning())
        {
            GLTransition.In(backButton);
        }

        // show next/finish button?
        if (!IsQuizScreen() || IsKnownScreen())
        {
            if (!IsTourEnd())
            {
                GLTransition.In(forwardButton);
            }
            else
            {
                GLTransition.In(finishButton);
            }
        }

        // remember if screen is newest yet
        if (IsNewFurthestScreenYet())
        {
            furthestTourSection.Value = sectionIndex;
            furthestTourScreen.Value = screenIndex;
        }
    }

    public void OpenScreen(GLTourScreen targetScreenObject)
    {
        sectionObject = targetScreenObject.transform.parent.GetComponent<GLTourSection>();
        sectionIndex = sections.IndexOf(sectionObject);
        screenIndex = screens[sectionIndex].IndexOf(targetScreenObject);
        StartCoroutine(DisplayScreen(sectionIndex, screenIndex));
    }

    public void TakeTest()
    {
        GLTransition.In(quizContainer);
        GLTransition.Out(tourGuideContainer);
        GLTransition.Out(tourContainer);

        var tourQuiz = screenObject as GLTourQuiz;
        var generatedQuiz = tourQuiz.GenerateQuiz();
        quiz.SetupTakeQuiz(generatedQuiz, (result) =>
        {
            if (result.TotalScore >= generatedQuiz.MaxScore)
            {
                GLPopup.MakeNotificationPopup(Title_TestFinished, Description_TestFinished, () =>
                {
                    Next();
                }).Show();
            }
            else
            {
                GLPopup.MakeErrorPopup(Title_TestFailed, Description_TestFailed, () =>
                {
                    OpenScreen(tourQuiz.onTestFail);
                }).Show();
            }
        }, () =>
        {
            OpenScreen(tourQuiz.onTestFail);
        });
    }

    public void Finish()
    {
        tourCompleted.Value = true;
        GLPopup.MakeNotificationPopup(Title_TourFinished, Description_TourFinished, () =>
        {
            Exit();
        }).Show();
    }

    public void Next()
    {
        if (sectionIndex < sections.Count - 1 && screenIndex == screens[sectionIndex].Count - 1)
        {
            StartCoroutine(DisplayScreen(sectionIndex + 1, 0));
        }
        else if (sectionIndex < sections.Count - 1 && screenIndex < screens[sectionIndex].Count - 1)
        {
            StartCoroutine(DisplayScreen(sectionIndex, screenIndex + 1));
        }
        else
        {
            Finish();
        }
    }

    public void Back()
    {
        if (screenIndex > 0)
        {
            StartCoroutine(DisplayScreen(sectionIndex, screenIndex - 1));
            return;
        }
        else if (sectionIndex > 0)
        {
            StartCoroutine(DisplayScreen(sectionIndex - 1, screens[sectionIndex - 1].Count - 1));
            return;
        }
        else
        {
            Exit();
        }
    }

    public void Exit()
    {
        if (!tourCompleted.Value)
        {
            GLGameFlow.I.ShowCourseSelection();
        }
        else
        {
            GLGameFlow.I.TryEnteringPlayerRoom();
        }
    }

}
