﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLAchievedSkillItem : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Label_LvlPrefix;
    public LocalizedText Label_LvlMax;

    [Header("References")]
    public TMP_Text text;
    public GLColorState textColor;
    public GameObject levelContainer;
    public LocalizedTextBehaviour levelIndicator;
    public RectTransform progressIndicator;
    public GameObject achievedIndicator;

    public void Setup(string skillRef)
    {
        var skillInsight = GLInsights.GetSkillInsight(skillRef);
        var skillProgressInsight = GLInsights.GetSkillProgressInsight(skillRef);

        text.text = skillInsight.skill.Name;

        if (skillInsight.maxLevel == 0)
        {
            achievedIndicator.SetActive(false);
            levelContainer.SetActive(true);
            progressIndicator.anchorMax = new Vector2(0f, 1f);
            levelIndicator.FormattedAsset = GLLocalization.Create("?");
            GLColorState.BroadcastColorState(this, false);
            return;
        }

        GLColorState.BroadcastColorState(this, skillProgressInsight.achieved);
        achievedIndicator.SetActive(skillProgressInsight.achieved && skillInsight.maxLevel == skillProgressInsight.level);
        levelContainer.SetActive(skillInsight.maxLevel != skillProgressInsight.level);

        if (skillProgressInsight.level != skillInsight.maxLevel)
        {
            progressIndicator.anchorMax = new Vector2(skillProgressInsight.level / (float)skillInsight.maxLevel, 1f);
            levelIndicator.FormattedAsset = Label_LvlPrefix.Format(skillProgressInsight.level.ToString(), skillInsight.maxLevel.ToString());
        }
    }
}
