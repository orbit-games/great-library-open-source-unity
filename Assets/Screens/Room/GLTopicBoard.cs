﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLTopicBoard : GLSingletonBehaviour<GLTopicBoard>
{
    protected override void OnSingletonInitialize() { }

    [Header("Localization")]
    public LocalizedText titleSetTopic;
    public LocalizedText descriptionSetTopic;
    public LocalizedText labelSetTopic;
    public LocalizedText labelCancel;
    public LocalizedText labelTopic;

    [Header("References")]
    public TMP_Text topicField;

    private void OnEnable()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        if (course == null) return;

        var cps = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref);

        topicField.text = cps.Topic;
    }

    public void ShowSetTopicPopup()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var cps = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref);

        var setup = GLPopup.MakePromptPopup(titleSetTopic, descriptionSetTopic, OnSetTopic);
        setup.formHelper.AddTextArea(labelTopic, "topic", null, 80, 250);

        setup.OverrideSubmitButtonText(labelSetTopic);
        setup.OverrideCloseButtonText(labelCancel);

        setup.Show();
        setup.SetData("topic", cps.Topic);

    }

    private void OnSetTopic(GLForm.Data result)
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var cps = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref);

        if (result["topic"] == null || result["topic"] == cps.Topic)
            return;

        cps.Topic = result["topic"];

        GLLoadingOverlay.ShowFullcoverLoading(GLApi.UpdateCourseProgressSummary(user.Ref, course.Ref, cps), OnTopicSetResponse);
    }

    private void OnTopicSetResponse(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            OnEnable();
        }
    }

}
