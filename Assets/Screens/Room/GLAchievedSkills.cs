﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAchievedSkills : MonoBehaviour
{

    [Header("References")]
    public Transform achievedSkillsContainer;

    [Header("Prefabs")]
    public GLAchievedSkillItem achievedSkillPrefab;

    public void OnEnable()
    {
        GLPool.removeAllPoolableChildren(achievedSkillsContainer);

        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        if (user == null || course == null) return;

        var skillInsights = GLInsights.GetSkillInsights();
        foreach (var skillInsight in skillInsights)
        {
            if (skillInsight.enabled)
            {
                var item = GLPool.placeCopy(achievedSkillPrefab, achievedSkillsContainer);
                item.Setup(skillInsight.skill.Ref);
            }
        }
    }
}