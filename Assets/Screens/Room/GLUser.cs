﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLUser : MonoBehaviour {

    [Header("References")]
    public GLBusteBuilder busteBuilder;
    public TMP_Text displayName;

    public TMP_Text debugToday;

    [Header("Preferences")]
    public GLSerializablePreference busteComposition;

    private void OnEnable()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        if (course == null) return;

        var comp = (GLBusteComposition)busteComposition.Value;
        busteBuilder.BuildComposition(comp);

        var courseProgressSummary = GLApi.GetCourseProgressSummaryKnowledge(user.Ref, course.Ref);
        displayName.text = courseProgressSummary.DisplayName;

        debugToday.text = GLDate.Now.ToLongDateString();

    }

    public void NextDay()
    {
        GLDate.NextDay();
        GLInsights.UpdateProgressInsights();
        GLGameFlow.I.Refresh();
    }

    public void NextWeek()
    {
        GLDate.NextDay();
        GLDate.NextDay();
        GLDate.NextDay();
        GLDate.NextDay();
        GLDate.NextDay();
        GLDate.NextDay();
        GLDate.NextDay();
        GLInsights.UpdateProgressInsights();
        GLGameFlow.I.Refresh();
    }

    public void PreviousDay()
    {
        GLDate.PreviousDay();
        GLInsights.UpdateProgressInsights();
        GLGameFlow.I.Refresh();
    }

    public void PreviousWeek()
    {
        GLDate.PreviousDay();
        GLDate.PreviousDay();
        GLDate.PreviousDay();
        GLDate.PreviousDay();
        GLDate.PreviousDay();
        GLDate.PreviousDay();
        GLDate.PreviousDay();
        GLInsights.UpdateProgressInsights();
        GLGameFlow.I.Refresh();
    }
}
