﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLAbacusTrackBead : MonoBehaviour {

    public RectTransform rectTransform;
    private Image image;
    private Button button;

    [Header("State")]
    [ReadOnly]
    public StepStatus beadState;
    [ReadOnly]
    public string trackRef;
    [ReadOnly]
    public string stepRef;

    private void Awake()
    {
        image = GetComponent<Image>();
        button = GetComponent<Button>();
    }

    public void SetDebateSettings(string trackRef, string stepRef)
    {
        this.trackRef = trackRef;
        this.stepRef = stepRef;
    }

    public void SetBeadState(StepStatus state)
    {
        beadState = state;
        image.color = GLAbacusResources.I.GetBeadColor(state);
        button.enabled = beadState != StepStatus.UNAVAILABLE;
    }

    public void OnPress()
    {
        if (beadState != StepStatus.UNAVAILABLE)
        {
            GLGameFlow.I.ShowDebate(trackRef, stepRef);
        }
    }

    public void SetHeight(float height)
    {
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, height);
    }
}
