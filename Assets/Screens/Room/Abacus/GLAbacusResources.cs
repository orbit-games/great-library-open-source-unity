﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAbacusResources : GLSingletonBehaviour<GLAbacusResources> {

    protected override void OnSingletonInitialize() { }

    [Header("References")]
    public GLAbacusTrack abacusTrack;
    public GLAbacusTrackBead abacusBead;
    public float abacusBeadHeight = 8f;
    public GLAbacusTrackMultiBeads abacusMultiBeads;
    public float abacusMultiBeadsHeight = 8f;
    public GLAbacusTrackSpace abacusSpace;

    public Color beadUnavailableColor;
    public Color beadAvailableColor;
    public Color beadDangerColor;
    public Color beadLateColor;
    public Color beadCompletedColor;

    public Color GetBeadColor(StepStatus state)
    {
        switch (state)
        {
            case StepStatus.UNAVAILABLE:
                return beadUnavailableColor;
            case StepStatus.AVAILABLE:
                return beadAvailableColor;
            case StepStatus.DANGER:
                return beadDangerColor;
            case StepStatus.LATE:
                return beadLateColor;
            case StepStatus.CLOSED:
                return beadCompletedColor;
            default:
                return beadUnavailableColor;
        }
    }
}
