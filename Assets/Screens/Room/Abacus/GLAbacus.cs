﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAbacus : MonoBehaviour {

    [Header("References")]
    public GameObject tracksContainer;

    [Header("Settings")]
    public float allowedHeight;

    public void OnEnable()
    {
        RebuildAbacus();
    }

    private void RebuildAbacus()
    {
        GLPool.removeAllPoolableChildren(tracksContainer);

        var course = GLApi.GetActiveCourse();
        var user = GLApi.GetActiveUser();

        if (course == null || user == null) return;

        for (int chapterIndex = 0; chapterIndex < course.Chapters.Count; chapterIndex++)
        {
            var track = GLPool.placeCopy(GLAbacusResources.I.abacusTrack, tracksContainer);
            track.Setup(user.Ref, chapterIndex, PeerType.SUBMITTER, allowedHeight);
        }

        for (int chapterIndex = course.Chapters.Count - 1; chapterIndex >= 0; chapterIndex--)
        {
            var track = GLPool.placeCopy(GLAbacusResources.I.abacusTrack, tracksContainer);
            track.Setup(user.Ref, chapterIndex, PeerType.REVIEWER, allowedHeight);
        }
    }
}
