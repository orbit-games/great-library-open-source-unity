﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAbacusTrackSpace : MonoBehaviour {

    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void SetHeight(float height)
    {
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, height);
    }
}
