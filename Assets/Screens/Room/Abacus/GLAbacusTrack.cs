﻿using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GLAbacusTrack : MonoBehaviour {

    public GameObject container;
    public GLAbacusTrackBase trackBase;
    public GLAbacusTrackAward trackAward;
    
    private void Initialize()
    {
        trackBase.gameObject.SetActive(false);
        GLPool.removeAllPoolableChildren(container);
    }

    string userRef;
    int chapterIndex;
    PeerType role;

    Course course;
    CourseInsight courseInsight;
    CourseProgress courseProgress;
    Chapter chapter;
    ChapterInsight chapterInsight;
    ChapterProgress chapterProgress;
    List<TrackProgressInsight> tracks;

    public void Setup(string userRef, int chapterIndex, PeerType role, float trackHeight)
    {
        Initialize();

        // remember arguments
        this.userRef = userRef;
        this.chapterIndex = chapterIndex;
        this.role = role;

        // get all up to date objects
        course = GLApi.GetActiveCourse();
        courseInsight = course.GetInsight();
        courseProgress = GLApi.GetCourseProgressKnowledge(userRef, course.Ref);

        // determine course time range and height helper
        var courseTimeSeconds = courseInsight.timeSpan.TotalSeconds;
        var heightPerSecond = trackHeight / courseTimeSeconds;

        // get our chapter and its progress
        chapter = course.Chapters[chapterIndex];
        chapterInsight = chapter.GetInsight();
        chapterProgress = courseProgress.Chapters.Find(ch => ch.ChapterRef == chapter.Ref);

        // hacky check, shouldn't happen, but this class will be deleted soon anyway
        if (chapter.ReviewSteps.Count == 0)
        {
            return;
        }

        // get the tracks for this role type
        tracks = GLInsights.GetTracks(chapterProgress.ChapterRef, userRef, role);

        // ensure the track base as first sibling, 
        // every time we add a new child, we set it as first to ensure a proper ordering
        trackBase.transform.SetAsFirstSibling();

        // remember current height
        var currentHeight = 0f;

        // set chapter track base height
        var currentStepIndex = FindStepIndexForRoleFrom(0);
        var chapterStartStep = chapter.ReviewSteps[currentStepIndex];
        var chapterStartStepInsight = chapterStartStep.GetInsight();
        var chapterStartHeight = (chapterStartStepInsight.startTime - courseInsight.startTime).TotalSeconds * heightPerSecond;

        //if (chapterStartHeight > 4)
        {
            trackBase.gameObject.SetActive(true);
            trackBase.SetHeight((float)chapterStartHeight);
            currentHeight += (float)chapterStartHeight;
        }

        // now add beads and spaces
        while (currentStepIndex < chapter.ReviewSteps.Count)
        {
            // get the step
            var currentStep = chapter.ReviewSteps[currentStepIndex];
            // add beads
            currentHeight += AddStepBeads(currentStep);
            // find the index of the step that is meant for the current user
            var newStepIndex = FindStepIndexForRoleFrom(currentStepIndex + 1);
            // add space if we have one of more steps that are not for the user with the given role
            if (newStepIndex - currentStepIndex > 1)
            {
                // determine the desired height using the deadline of the step before the next
                var desiredHeight = (chapter.ReviewSteps[newStepIndex - 1].Deadline.Value - courseInsight.startTime).TotalSeconds * heightPerSecond;
                var heightToAdd = (float)desiredHeight - currentHeight;
                if (heightToAdd > 4)
                {
                    currentHeight += AddSpace(heightToAdd);
                }
            }
            // remember the new next step index
            currentStepIndex = newStepIndex;
        }

        // determine if this abacus track is completed and set the award
        if (tracks != null && tracks.Count > 0)
        {
            var completed = tracks.TrueForAll(track => track.isCompleted);
            trackAward.SetCompleted(completed);
        }
        else
        {
            trackAward.SetCompleted(false);
        }

        // now finally set the award as first sibling
        trackAward.transform.SetAsFirstSibling();
    }

    /// <summary>
    /// Find the next step that requires input from the user with the given role
    /// Startindex included
    /// </summary>
    /// <param name="startIndex"></param>
    /// <param name="chapter"></param>
    /// <param name="role"></param>
    /// <returns></returns>
    private int FindStepIndexForRoleFrom(int startIndex)
    {
        for (int i = startIndex; i < chapter.ReviewSteps.Count; i++)
        {
            if (chapter.ReviewSteps[i].PeerType == role)
            {
                return i;
            }
        }
        return chapter.ReviewSteps.Count;
    }

    /// <summary>
    /// Add all beads of the given step
    /// </summary>
    /// <param name="stepIndex"></param>
    /// <returns></returns>
    private float AddStepBeads(ReviewStep step)
    {
        var stepInsight = step.GetInsight();
        var height = 0f;

        var modules = 0;
        if (stepInsight.requiresQuiz) modules++;
        if (stepInsight.requiresDiscussion) modules++;
        if (stepInsight.requiresSubmission) modules++;

        var beadHeight = 0f;
        if (modules == 1)
        {
            beadHeight = 12f;
        }
        else if (modules == 2)
        {
            beadHeight = 8.5f;
        }
        else if (modules == 3)
        {
            beadHeight = 6f;
        }

        // create quiz beads with states
        if (stepInsight.requiresQuiz)
        {
            height = AddMultiBeads(stepInsight, beadHeight, stepProgressInsight => stepProgressInsight.hasQuizData);
        }

        // create discussion beads with states
        if (stepInsight.requiresDiscussion)
        {
            height = AddMultiBeads(stepInsight, beadHeight, stepProgressInsight => stepProgressInsight.hasCompletedDiscussion);
        }

        // create submission beads with states
        if (stepInsight.requiresSubmission)
        {
            // create multiple beads when the user is a reviewer, because he'd have to upload something per track
            if (role == PeerType.REVIEWER)
            {
                height = AddMultiBeads(stepInsight, beadHeight, stepProgressInsight => stepProgressInsight.hasSubmissionData);
            }
            else
            {
                height = AddSingleBead(stepInsight, beadHeight, stepProgressInsight => stepProgressInsight.hasSubmissionData);
            }
        }

        return beadHeight * modules;
    }

    /// <summary>
    /// Adds the multiple beads prefab to this track, according to given stepknowledge
    /// </summary>
    /// <param name="stepInsights"></param>
    /// <param name="resultChecker"></param>
    /// <returns></returns>
    private float AddMultiBeads(ReviewStepInsight stepInsights, float height, Func<StepProgressInsight, bool> resultChecker)
    {
        // add the corresponding beads
        var beads = GLPool.placeCopy(GLAbacusResources.I.abacusMultiBeads, container);
        beads.transform.SetAsFirstSibling();
        beads.SetHeight(height);

        // if we have tracks with progress, then determine proper beads states
        if (tracks != null && tracks.Count > 0)
        {
            // set the beads count
            beads.SetBeadsCount(tracks.Count, stepInsights.generalStatus);

            // loop over each track to set its individual state
            for (int trackIndex = 0; trackIndex < tracks.Count; trackIndex++)
            {
                var stepProgressInsight = stepInsights.GetProgressInsight(tracks[trackIndex]);
                beads.SetBeadState(trackIndex, stepProgressInsight.status);
                beads.SetDebateSettings(trackIndex, tracks[trackIndex].track.Ref, stepInsights.step.Ref);
                // if entire step is not completed, perhaps this substep?
                if (!stepProgressInsight.isReadyForClose && resultChecker(stepProgressInsight))
                {
                    beads.SetBeadState(trackIndex, StepStatus.CLOSED);
                }
            }
        }
        else
        {
            // set the beads count with their general status indicators
            beads.SetBeadsCount(chapter.ReviewerCount.GetValueOrDefault(2), stepInsights.generalStatus);
        }

        return height;
        //return GLAbacusResources.I.abacusMultiBeadsHeight;
    }

    /// <summary>
    /// Adds the single bead prefab to this track, according to given stepknowledge
    /// </summary>
    /// <param name="stepInsights"></param>
    /// <param name="resultChecker"></param>
    /// <returns></returns>
    private float AddSingleBead(ReviewStepInsight stepInsights, float height, Func<StepProgressInsight, bool> resultChecker)
    {
        // add the corresponding beads
        var beads = GLPool.placeCopy(GLAbacusResources.I.abacusBead, container);
        beads.transform.SetAsFirstSibling();
        beads.SetHeight(height);
        // if we have tracks with progress, then determine the proper bead state
        if (tracks != null && tracks.Count > 0)
        {
            beads.SetDebateSettings(tracks[0].track.Ref, stepInsights.step.Ref);
            var stepProgressInsight = stepInsights.GetProgressInsight(tracks[0]);
            beads.SetBeadState(stepProgressInsight.status);
            // if entire step is not completed, perhaps this substep?
            if (!stepProgressInsight.isReadyForClose && resultChecker(stepProgressInsight))
            {
                beads.SetBeadState(StepStatus.CLOSED);
            }
        }
        else
        {
            // set the beads count
            beads.SetBeadState(stepInsights.generalStatus);
        }
        return height;
        //return GLAbacusResources.I.abacusBeadHeight;
    }

    /// <summary>
    /// Adds space to the track with the given height
    /// </summary>
    /// <returns>the added height</returns>
    private float AddSpace(float addHeight)
    {
        // add the corresponding beads
        var space = GLPool.placeCopy(GLAbacusResources.I.abacusSpace, container);
        space.transform.SetAsFirstSibling();
        space.SetHeight(addHeight);
        return addHeight;
    }
}