﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLAbacusTrackMultiBeads : MonoBehaviour {

    public RectTransform rectTransform;
    public List<Image> beads;
    public List<Button> buttons;

    [Header("State")]
    [ReadOnly]
    public List<StepStatus> beadStates = new List<StepStatus>();
    [ReadOnly]
    public List<string> trackRefs;
    [ReadOnly]
    public List<string> stepRefs;

    private void Awake()
    {
        for (int i = 0; i < beads.Count; i++)
        {
            beadStates.Add(StepStatus.UNAVAILABLE);
            stepRefs.Add("");
            trackRefs.Add("");
        }
    }

    public void SetDebateSettings(int index, string trackRef, string stepRef)
    {
        trackRefs[index] = trackRef;
        stepRefs[index] = stepRef;
    }

    public void SetBeadsCount(int beadsCount, StepStatus startState)
    {
        for (int i = 0; i < beads.Count; i++)
        {
            beadStates[i] = startState;
            beads[i].gameObject.SetActive(i < beadsCount);
            SetBeadState(i, startState);
        }
    }

    public void SetBeadState(int index, StepStatus state)
    {
        beadStates[index] = state;
        beads[index].color = GLAbacusResources.I.GetBeadColor(state);
        buttons[index].enabled = state != StepStatus.UNAVAILABLE;
    }

    public void SetHeight(float height)
    {
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, height);
    }

    public void OnPress(int index)
    {
        if (beadStates[index] != StepStatus.UNAVAILABLE)
        {
            GLGameFlow.I.ShowDebate(trackRefs[index], stepRefs[index]);
        }
    }
}
