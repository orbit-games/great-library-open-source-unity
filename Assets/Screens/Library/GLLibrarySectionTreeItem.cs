﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLLibrarySectionTreeItem : MonoBehaviour, IPoolEventsListener {

    [Header("References")]
    public Button collapseButton;
    public GameObject sectionCollapsed;
    public GameObject sectionOpened;
    public GameObject selectedIndicator;
    public Transform subsectionContainer;

    public TMP_Text sectionTitle;
    //public TMP_Text sectionDescription;
    
    private GLLibrarySectionTreeItem sectionPrfab;

    private bool opened;
    private bool collapsable;
    private string sectionRef;

    private Dictionary<string, GLLibrarySectionTreeItem> refToSectionItem = new Dictionary<string, GLLibrarySectionTreeItem>();

    public void OnCreatedForPool() { Clear(); }
    public void OnPlacedFromPool() { Clear(); }
    public void OnRemovedToPool() { Clear(); }

    private void Update()
    {
        selectedIndicator.SetActive(GLLibrary.I.GetCurrentSectionRef() == sectionRef);
    }

    private void Clear()
    {
        GLPool.removeAllPoolableChildren(subsectionContainer);
        refToSectionItem.Clear();
        subsectionContainer.gameObject.SetActive(false);
        opened = false;

        sectionCollapsed.SetActive(!opened);
        sectionOpened.SetActive(opened);
        subsectionContainer.gameObject.SetActive(opened);
        collapseButton.interactable = true;
        collapsable = true;

        if (sectionPrfab == null)
        {
            sectionPrfab = GLPool.GetOriginal(gameObject).GetComponent<GLLibrarySectionTreeItem>();
        }
    }

    public void Setup(string sectionRef)
    {
        Clear();

        this.sectionRef = sectionRef;
        var sectionInsight = GLInsights.GetSectionInsight(sectionRef);
        sectionTitle.text = sectionInsight.section.Name;

        if (sectionInsight.childrenInsights.IsNullOrEmpty())
        {
            sectionCollapsed.SetActive(false);
            sectionOpened.SetActive(false);
            subsectionContainer.gameObject.SetActive(false);
            collapseButton.interactable = false;
            collapsable = false;
        }
    }

    public void Collapse()
    {
        if (opened)
        {
            Toggle();
        }
    }

    public void Expand()
    {
        if (!opened)
        {
            Toggle();
        }
    }

    public void ExpandSubsection(string subsectionRef)
    {
        if (refToSectionItem.ContainsKey(subsectionRef))
        {
            refToSectionItem[subsectionRef].Expand();
        }
    }

    public GLLibrarySectionTreeItem GetSubsectionItem(string subsectionRef)
    {
        return refToSectionItem.GetOrDefault(subsectionRef);
    }

    public void Toggle()
    {
        opened = !opened;
        if (collapsable)
        {
            sectionCollapsed.SetActive(!opened);
            sectionOpened.SetActive(opened);
            subsectionContainer.gameObject.SetActive(opened);
        }

        if (!opened)
        {
            GLPool.removeAllPoolableChildren(subsectionContainer);
            subsectionContainer.gameObject.SetActive(false);
            refToSectionItem.Clear();
        }
        else
        {
            GLPool.removeAllPoolableChildren(subsectionContainer);
            refToSectionItem.Clear();

            var sectionInsight = GLInsights.GetSectionInsight(sectionRef);
            foreach (var child in sectionInsight.childrenInsights)
            {
                var subsectionObject = GLPool.placeCopy(sectionPrfab, subsectionContainer);
                subsectionObject.Setup(child.section.Ref);
                refToSectionItem.Add(child.section.Ref, subsectionObject);
            }

            subsectionContainer.gameObject.SetActive(refToSectionItem.Count > 0);
        }
    }

    public void Open()
    {
        Toggle();

        // open the section
        GLLibrary.I.OpenSection(sectionRef, false);
    }
}
