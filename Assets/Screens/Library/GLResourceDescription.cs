﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLResourceDescription : MonoBehaviour {

    public TMP_Text descriptionField;

    public void Setup(string descriptionText)
    {
        descriptionField.text = descriptionText;
    }
}
