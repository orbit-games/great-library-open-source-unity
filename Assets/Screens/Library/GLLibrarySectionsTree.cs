﻿using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLLibrarySectionsTree : MonoBehaviour {
    
    [Header("References")]
    public RectTransform sectionContainer;

    [Header("Prefabs")]
    public GLLibrarySectionTreeItem sectionItemPrefab;

    private Dictionary<string, GLLibrarySectionTreeItem> rootSections = new Dictionary<string, GLLibrarySectionTreeItem>();
    private bool sectionsBuilt = false;
    private string expandedSection = null;

    public void OnCourseChange()
    {
        sectionsBuilt = false;
        expandedSection = null;
        BuildSections();
    }

    public void CollapseAll()
    {
        BuildSections();
        foreach (var item in rootSections.Values)
        {
            item.Collapse();
        }
    }

    public void ExpandToSection(string sectionRef)
    {
        BuildSections();
        CollapseAll();

        // if it is a root section, just expand and be done
        if (rootSections.ContainsKey(sectionRef))
        {
            rootSections[sectionRef].Expand();
            return;
        }

        // let's build the chain of references that we need to open up
        var sectionInsight = GLInsights.GetSectionInsight(sectionRef);
        Stack<string> toOpenChain = new Stack<string>();
        var current = sectionInsight;
        while (current != null)
        {
            toOpenChain.Push(current.section.Ref);
            current = current.parentInsight;
        }

        // let's process the chain top to bottom and keep expanding until we can't anymore
        var currentRef = toOpenChain.Pop();
        var activeItem = rootSections.GetOrDefault(currentRef);
        while (activeItem != null)
        {
            activeItem.Expand();

            if (toOpenChain.Count > 0)
            {
                currentRef = toOpenChain.Pop();
                activeItem = activeItem.GetSubsectionItem(currentRef);
            }
            else
            {
                return;
            }
        }
    }

    private void BuildSections()
    {
        if (sectionsBuilt) return;
        sectionsBuilt = true;

        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        if (course == null) return;
        var courseInsight = course.GetInsight();

        GLPool.removeAllPoolableChildren(sectionContainer);
        rootSections.Clear();

        foreach (var sectionInsight in courseInsight.rootSectionInsights)
        {
            var item = GLPool.placeCopy(sectionItemPrefab, sectionContainer);
            item.Setup(sectionInsight.section.Ref);
            rootSections.Add(sectionInsight.section.Ref, item);
        }
    }
}
