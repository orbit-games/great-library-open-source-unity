﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLResourceItem : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Title_FailedTest;
    public LocalizedText Description_FailedTest;

    [Header("References")]
    public TMP_Text title;
    public TMP_Text description;
    public TMP_Text skillGain;
    public TMP_Text skillAchieved;
    
    public GameObject videoIcon;
    public GameObject linkIcon;
    public GameObject pdfIcon;

    public GameObject openResourceButton;
    public GameObject takeQuizButton;

    public GameObject takeQuizContainer;
    public GameObject achievedContainer;

    private string resourceRef;

    public void Setup(CourseResource courseResource)
    {
        resourceRef = courseResource.Ref;
        title.text = courseResource.Name;
        description.text = courseResource.Description;

        var skillsText = "";
        foreach (var skillRef in courseResource.Skills)
        {
            var skillInsight = GLInsights.GetSkillInsight(skillRef);
            if (skillsText != "")
            {
                skillsText += "\n";
            }
            skillsText += skillInsight.skill.Name;
        }

        skillGain.text = skillsText;
        skillAchieved.text = skillsText;

        var user = GLApi.GetActiveUser();
        var resultInsight = courseResource.GetResultInsight(user.Ref);

        if (courseResource.Quiz == null)
        {
            takeQuizContainer.SetActive(false);
            achievedContainer.SetActive(false);
        }
        else if (resultInsight != null && resultInsight.isAchieved)
        {
            takeQuizContainer.SetActive(false);
            achievedContainer.SetActive(true);
        }
        else
        {
            takeQuizContainer.SetActive(true);
            achievedContainer.SetActive(false);
        }

        if (courseResource.Url.IsNullOrEmpty())
        {
            videoIcon.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            var lowerUrl = courseResource.Url.ToLower();
            videoIcon.transform.parent.gameObject.SetActive(true);

            videoIcon.SetActive(false);
            linkIcon.SetActive(false);
            pdfIcon.SetActive(false);

            if (lowerUrl.EndsWith(".pdf"))
            {
                pdfIcon.SetActive(true);
            }
            else if (lowerUrl.Contains("youtube.com/") 
                || lowerUrl.Contains("youtu.be/")
                || lowerUrl.Contains(".mp4")
                || lowerUrl.Contains(".mkv")
                || lowerUrl.Contains(".flv")
                || lowerUrl.Contains(".avi")
                || lowerUrl.Contains(".mov")
                || lowerUrl.Contains(".webm")
                || lowerUrl.Contains(".gif"))
            {
                videoIcon.SetActive(true);
            }
            else
            {
                linkIcon.SetActive(true);
            }
        }
    }

    public void OpenResource()
    {
        var resource = GLApi.GetResourceKnowledge(resourceRef);
        if (resource.Url.IsNullOrEmpty()) return;
        if (resource.Url.ToLower().EndsWith(".pdf"))
        {
            GLDocument.I.ViewDocument(resource.Url);
        }
        else
        {
            GLSystem.OpenURL(resource.Url);
        }
    }

    public void TakeQuiz()
    {
        var resource = GLApi.GetResourceKnowledge(resourceRef);
        var user = GLApi.GetActiveUser();
        var resultInsight = resource.GetResultInsight(user.Ref);
        GLLibrary.I.TakeQuiz(resource.Quiz, resultInsight?.resourceResult?.QuizResult, OnQuizResult);
    }

    private void OnQuizResult(QuizResult result)
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        GLLoadingOverlay.ShowLoading(
            GLApi.SendResourceQuizResult(user.Ref, course.Ref, resourceRef, result),
            OnQuizResultSent
        );
    }

    private void OnQuizResultSent(GLTaskResult result)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(result))
        {
            var resource = GLApi.GetResourceKnowledge(resourceRef);
            var user = GLApi.GetActiveUser();
            var resultInsight = resource.GetResultInsight(user.Ref);

            Setup(resource);

            if (!resultInsight.isAchieved)
            {
                GLPopup.MakeNotificationPopup(Title_FailedTest, Description_FailedTest).Show();
            }
        }
    }

}
