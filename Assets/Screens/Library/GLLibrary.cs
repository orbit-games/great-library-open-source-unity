﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLLibrary : GLSingletonBehaviour<GLLibrary>
{
    protected override void OnSingletonInitialize()
    {
        GLApi.RegisterCourseSelectedListener(OnCourseSelected);
    }

    private void OnCourseSelected(Course course)
    {
        sectionsTree.OnCourseChange();
        openedSection.OnCourseChange();
    }

    [Header("References")]
    public GLLibrarySectionsTree sectionsTree;
    public GLLibraryOpenedSection openedSection;
    public RectTransform resourcesContainer;
    public GLQuiz quizContainer;

    [Header("Prefabs")]
    public GLResourceItem resourseItemPrefab;

    private string currentSection = null;
    private bool sectionsBuilt = false;

    private void OnEnable()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        quizContainer.gameObject.SetActive(false);

        if (course == null) return;
    }

    public void OpenSection(string sectionRef, bool expandInTree = true)
    {
        currentSection = sectionRef;
        openedSection.OpenSection(sectionRef);

        if (expandInTree)
            sectionsTree.ExpandToSection(sectionRef);
    }

    public string GetCurrentSectionRef()
    {
        return currentSection;
    }

    public void TakeQuiz(Quiz quiz, QuizResult previousResult, Action<QuizResult> onResult)
    {
        if (quiz == null)
        {
            throw new Exception("Found a resource without a quiz");
        }

        quizContainer.gameObject.SetActive(true);
        quizContainer.SetupTakeQuiz(quiz, previousResult, result => {
            onResult?.Invoke(result);
            quizContainer.gameObject.SetActive(false);
        }, () =>
        {
            quizContainer.gameObject.SetActive(false);
        });
    }
}
