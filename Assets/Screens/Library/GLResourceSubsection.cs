﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLResourceSubsection : MonoBehaviour {

    public TMP_Text subsectionTitle;
    private string subsectionRef;

    public void Setup(string subsectionRef)
    {
        this.subsectionRef = subsectionRef;
        var subsectionInsight = GLInsights.GetSectionInsight(subsectionRef);
        subsectionTitle.text = subsectionInsight.section.Name;
    }

    public void OpenSection()
    {
        GLLibrary.I.OpenSection(subsectionRef);
    }
}
