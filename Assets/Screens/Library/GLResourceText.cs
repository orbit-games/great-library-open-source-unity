﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLResourceText : MonoBehaviour {

    [Header("References")]
    public TMP_Text title;
    public TMP_Text description;

    private string resourceRef;

    public void Setup(CourseResource courseResource)
    {
        resourceRef = courseResource.Ref;
        title.text = courseResource.Name;
        description.text = courseResource.Description;
    }

}
