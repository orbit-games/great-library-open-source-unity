﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLLibraryOpenedSection : MonoBehaviour {
    
    [Header("References")]
    public RectTransform resourcesContainer;
    public ScrollRect scrollRect;

    public TMP_Text sectionTitle;
    public TMP_Text sectionDescription;

    [Header("Prefabs")]
    public GLResourceItem resourseItemPrefab;
    public GLResourceText resourseTextPrefab;
    public GLResourceDescription resourseDescriptionPrefab;
    public GLResourceSubsection subsectionPrefab;

    private string currentSection = null;
    private bool sectionsBuilt = false;

    public void OnCourseChange()
    {
        sectionsBuilt = false;
        currentSection = null;
        RebuildOpenedSection();
    }

    public void OpenSection(string sectionRef)
    {
        currentSection = sectionRef;
        RebuildOpenedSection();
    }

    private void RebuildOpenedSection()
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        if (course == null) return;

        scrollRect.verticalNormalizedPosition = 1f;

        if (currentSection == null)
        {
            var courseInsight = course.GetInsight();
            if (courseInsight.rootSectionInsights.IsNullOrEmpty())
            {
                Debug.LogError("No root sections found");
                return;
            }

            currentSection = courseInsight.rootSectionInsights[0].section.Ref;
        }

        GLPool.removeAllPoolableChildren(resourcesContainer);

        var sectionInsight = GLInsights.GetSectionInsight(currentSection);
        
        sectionTitle.text = sectionInsight.section.Name ?? "";
        sectionDescription.text = sectionInsight.section.Description ?? "";

        foreach (var sectionResourceRelation in sectionInsight.referencingResources)
        {
            HandleRelationDescription(sectionResourceRelation.relation.Description);

            var recourseInsight = sectionResourceRelation.resourceInsight;
            if (recourseInsight.isText)
            {
                var item = GLPool.placeCopy(resourseTextPrefab, resourcesContainer);
                item.Setup(recourseInsight.resource);
            }
            else
            {
                var item = GLPool.placeCopy(resourseItemPrefab, resourcesContainer);
                item.Setup(recourseInsight.resource);
            }
        }

        foreach (var childSection in sectionInsight.childrenInsights)
        {
            HandleRelationDescription(childSection.section.ParentRelationDescription);
            
            var item = GLPool.placeCopy(subsectionPrefab, resourcesContainer);
            item.Setup(childSection.section.Ref);
        }
    }

    private void HandleRelationDescription(string descriptionText)
    {
        if (!descriptionText.IsNullOrEmpty())
        {
            var descriptionObject = GLPool.placeCopy(resourseDescriptionPrefab, resourcesContainer);
            descriptionObject.Setup(descriptionText);
        }
    }
}
