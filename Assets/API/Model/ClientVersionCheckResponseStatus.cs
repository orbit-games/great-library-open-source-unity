/* 
 * Your Great Library
 *
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// Defines ClientVersionCheckResponseStatus
    /// </summary>
    
    [JsonConverter(typeof(StringEnumConverter))]
    
    public enum ClientVersionCheckResponseStatus
    {
        
        /// <summary>
        /// Enum OK for value: OK
        /// </summary>
        [EnumMember(Value = "OK")]
        OK = 1,
        
        /// <summary>
        /// Enum UPDATEAVAILABLE for value: UPDATE_AVAILABLE
        /// </summary>
        [EnumMember(Value = "UPDATE_AVAILABLE")]
        UPDATEAVAILABLE = 2,
        
        /// <summary>
        /// Enum UPDATEREQUIRED for value: UPDATE_REQUIRED
        /// </summary>
        [EnumMember(Value = "UPDATE_REQUIRED")]
        UPDATEREQUIRED = 3,
        
        /// <summary>
        /// Enum ROLLBACK for value: ROLLBACK
        /// </summary>
        [EnumMember(Value = "ROLLBACK")]
        ROLLBACK = 4
    }

}
