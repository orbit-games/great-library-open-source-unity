/* 
 * Your Great Library
 *
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// Defines CourseProgressExportStepExecution
    /// </summary>
    
    [JsonConverter(typeof(StringEnumConverter))]
    
    public enum CourseProgressExportStepExecution
    {
        
        /// <summary>
        /// Enum GIVEN for value: GIVEN
        /// </summary>
        [EnumMember(Value = "GIVEN")]
        GIVEN = 1,
        
        /// <summary>
        /// Enum RECEIVED for value: RECEIVED
        /// </summary>
        [EnumMember(Value = "RECEIVED")]
        RECEIVED = 2
    }

}
