/* 
 * Your Great Library
 *
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// CourseProgressExportSpecification
    /// </summary>
    [DataContract]
    public partial class CourseProgressExportSpecification :  IEquatable<CourseProgressExportSpecification>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CourseProgressExportSpecification" /> class.
        /// </summary>
        /// <param name="columns">Collection of column specifications to include in the export.</param>
        public CourseProgressExportSpecification(List<CourseProgressExportColumnSpecification> columns = default(List<CourseProgressExportColumnSpecification>))
        {
            this.Columns = columns;
        }
        
        /// <summary>
        /// Collection of column specifications to include in the export
        /// </summary>
        /// <value>Collection of column specifications to include in the export</value>
        [DataMember(Name="columns", EmitDefaultValue=false)]
        public List<CourseProgressExportColumnSpecification> Columns { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CourseProgressExportSpecification {\n");
            sb.Append("  Columns: ").Append(Columns).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as CourseProgressExportSpecification);
        }

        /// <summary>
        /// Returns true if CourseProgressExportSpecification instances are equal
        /// </summary>
        /// <param name="input">Instance of CourseProgressExportSpecification to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CourseProgressExportSpecification input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Columns == input.Columns ||
                    this.Columns != null &&
                    this.Columns.SequenceEqual(input.Columns)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Columns != null)
                    hashCode = hashCode * 59 + this.Columns.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
