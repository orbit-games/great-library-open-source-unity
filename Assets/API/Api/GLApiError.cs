﻿

public enum GLApiError
{
    None,
    EntityExists,
    EntityNotFound,
    LoginInvalid,
    ErrorAuthorization,
    ErrorUnknown,
    ErrorConstraintViolation,
    ErrorCodeUnknown,
    JsonParseException,
    TokenInvalid,
    ApiCanceled,
    ApiUnknownError,
    ApiUnknownClientError,
    ApiClientError,
    ApiUnknownServerError,
    ApiServerError,
    ApiNotFound,
    ServerNotFound,
    EmptyResponse
}

public static class GLApiErrorExtension
{
    public static string ToErrorCode(this GLApiError error)
    {
        switch (error)
        {
            case GLApiError.None:
                return "";

            case GLApiError.EntityExists:
                return "entity.exists";
            case GLApiError.EntityNotFound:
                return "entity.not_found";
            case GLApiError.LoginInvalid:
                return "login.invalid";
            case GLApiError.ErrorAuthorization:
                return "error.authorization";
            case GLApiError.ErrorUnknown:
                return "error.unknown";
            case GLApiError.ErrorConstraintViolation:
                return "error.constraint_violation";

            case GLApiError.ErrorCodeUnknown:
                return "error_code.unknown";

            case GLApiError.TokenInvalid:
                return "login.token.invalid";

            case GLApiError.JsonParseException:
                return "json.parse.exception";
            case GLApiError.ApiUnknownError:
                return "api.exception";
            case GLApiError.ApiCanceled:
                return "api.canceled";
            case GLApiError.ApiUnknownClientError:
                return "api.unknown.client_error";
            case GLApiError.ApiClientError:
                return "api.client_error";
            case GLApiError.ApiUnknownServerError:
                return "api.unknown.server_error";
            case GLApiError.ApiServerError:
                return "api.server_error";
            case GLApiError.ApiNotFound:
                return "api.not_found";
            case GLApiError.ServerNotFound:
                return "server.not_found";
            case GLApiError.EmptyResponse:
                return "response.empty";
            default:
                return "entity.exists";
        }
    }
}