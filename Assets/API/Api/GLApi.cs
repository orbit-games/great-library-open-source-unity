﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;
using IO.Swagger.Api;
using GameToolkit.Localization;
using Newtonsoft.Json;

/// <summary>
/// Represents a collection of functions to interact with the API endpoints
/// 
/// ---------------------------- IMPORTANT -------------------------------
/// Modified the generated api by moving it out of its namespace, connect the singleton behaviour, added api live/debug path variables, modified the constructors to become setup methods
/// 
/// </summary>
public class GLApi : GLSingletonBehaviour<GLApi>
{
    #region knowledge
    private static Dictionary<string, User> users = new Dictionary<string, User>();
    private static Dictionary<string, string> displayNames = new Dictionary<string, string>();
    private static Dictionary<string, Course> courses = new Dictionary<string, Course>();
    private static Dictionary<string, CourseResource> resources = new Dictionary<string, CourseResource>();
    private static Dictionary<string, ArgumentType> argumentTypes = new Dictionary<string, ArgumentType>();
    private static Dictionary<string, CourseSummary> courseSummaries = new Dictionary<string, CourseSummary>();
    private static Dictionary<string, Conversation> conversations = new Dictionary<string, Conversation>();
    private static Dictionary<string, ConversationMessage> conversationMessages = new Dictionary<string, ConversationMessage>();
    private static GLUserCourseDictionary<CourseProgress> coursesProgress = new GLUserCourseDictionary<CourseProgress>();
    private static GLUserCourseDictionary<CourseProgressSummary> courseProgressSummaries = new GLUserCourseDictionary<CourseProgressSummary>();

    private static List<Announcement> announcements;
    private static ServerInfo serverInfo;
    private static UserAgreement latestAgreement;
    private static ClientVersion latestVersion;
    private static ClientVersion currentVersion;
    private static ClientVersionCheckResponseStatus versionStatus;

    private static Course activeCourse;
    private static User activeUser;
    private static string loginToken { get { return I.token; } set { I.token = value; } }
    public string token;

    public static List<Announcement> GetAnnouncementsKnowledge()
    {
        return announcements;
    }

    public static UserAgreement GetLatestAgreementKnowledge()
    {
        return latestAgreement;
    }

    public static ServerInfo GetServerInfo()
    {
        return serverInfo;
    }

    public static ClientVersion GetLatestVersionKnowledge()
    {
        return latestVersion;
    }

    public static ClientVersion GeCurrentVersionKnowledge()
    {
        return currentVersion;
    }

    public static ClientVersionCheckResponseStatus GetVersionStatusKnowledge()
    {
        return versionStatus;
    }

    public static User GetUserKnowledge(string itemRef)
    {
        return users.GetOrDefault(itemRef);
    }

    public static Course GetCourseKnowledge(string itemRef)
    {
        return courses.GetOrDefault(itemRef);
    }

    public static CourseResource GetResourceKnowledge(string itemRef)
    {
        return resources.GetOrDefault(itemRef);
    }

    public static CourseSummary GetCourseSummaryKnowledge(string itemRef)
    {
        return courseSummaries.GetOrDefault(itemRef);
    }

    public static Conversation GetConversationKnowledge(string itemRef)
    {
        return conversations.GetOrDefault(itemRef);
    }

    public static ConversationMessage GetConversationMessageKnowledge(string itemRef)
    {
        return conversationMessages.GetOrDefault(itemRef);
    }

    public static List<string> GetAllConversationKnowledgeRefs()
    {
        return new List<string>(conversations.Keys);
    }

    public static List<Conversation> GetAllConversationKnowledge()
    {
        return new List<Conversation>(conversations.Values);
    }

    public static CourseProgress GetCourseProgressKnowledge(string userRef, string courseRef)
    {
        return coursesProgress.GetOrDefault(Tuple.Create(userRef, courseRef));
    }

    public static ChapterProgress GetChapterProgressKnowledge(string userRef, string courseRef, string chapterRef)
    {
        if (!coursesProgress.ContainsKey(userRef, courseRef)) return null;
        var courseProgress = coursesProgress[userRef, courseRef];
        var chapterProgress = courseProgress.Chapters.Find(chapter => chapter.ChapterRef == chapterRef);
        return chapterProgress;
    }

    public static CourseProgressSummary GetCourseProgressSummaryKnowledge(string userRef, string courseRef)
    {
        return courseProgressSummaries.GetOrDefault(Tuple.Create(userRef, courseRef));
    }

    public static Course GetActiveCourse()
    {
        return activeCourse;
    }

    public static bool IsCourseSelected()
    {
        return activeCourse != null;
    }

    public static bool IsLoggedInAndCourseSelected()
    {
        return activeCourse != null;
    }

    public static bool IsLoggedIn()
    {
        return activeUser != null;
    }

    public static User GetActiveUser()
    {
        return activeUser;
    }

    public static void Logout()
    {
        activeUser = null;
        activeCourse = null;
        loginToken = null;
        logoutListeners?.Invoke();
    }

    #endregion
    #region listeners

    private interface IKnowledgeListener { }
    private class KnowledgeListener<T> : IKnowledgeListener
    {
        Action<T> listeners;

        public void Register(Action<T> listener)
        {
            listeners += listener;
        }

        public void Remove(Action<T> listener)
        {
            listeners -= listener;
        }

        public void Invoke(T data)
        {
            listeners?.Invoke(data);
        }
    }

    private static Action logoutListeners;
    private static Action<User> loginListeners;
    private static Action<Course> courseSelectedListeners;
    private static Dictionary<Type, IKnowledgeListener> listeners = new Dictionary<Type, IKnowledgeListener>();

    public static void RegisterLoginListener(Action<User> listener)
    {
        loginListeners += listener;
    }

    public static void RegisterCourseSelectedListener(Action<Course> listener)
    {
        courseSelectedListeners += listener;
    }

    public static void RegisterLogoutListener(Action listener)
    {
        logoutListeners += listener;
    }

    private static KnowledgeListener<T> GetKnowledgeListener<T>()
    {
        var type = typeof(T);
        if (!listeners.ContainsKey(type))
        {
            listeners.Add(type, new KnowledgeListener<T>());
        }
        return (KnowledgeListener<T>)listeners[type];
    }

    public static void RegisterKnowledgeListener<T>(Action<T> listener)
    {
        GetKnowledgeListener<T>().Register(listener);
    }

    public static void RemoveKnowledgeListener<T>(Action<T> listener)
    {
        GetKnowledgeListener<T>().Remove(listener);
    }

    private static void InvokeKnowledgeListeners<T>(T data)
    {
        GetKnowledgeListener<T>().Invoke(data);
    }

    #endregion
    #region knowledge updaters

    private static void ClearConversationKnowledge()
    {
        conversations.Clear();
        conversationMessages.Clear();
    }

    private static void UpdateKnowledge(GLTaskResult result)
    {
        if (!result.Failed && result.Result != null)
        {
            var data = result.Result;
            if (data is User)
            {
                UpdateUserKnowledge(data, true, true);
                InvokeKnowledgeListeners((User)data);
            }
            else if (data is ServerInfo)
            {
                serverInfo = (ServerInfo)data;
                InvokeKnowledgeListeners(serverInfo);
            }
            else if (data is List<Announcement>)
            {
                announcements = (List<Announcement>)data;
                InvokeKnowledgeListeners(announcements);
            }
            else if (data is UserAgreement)
            {
                UpdateUserAgreementKnowledge(data);
                InvokeKnowledgeListeners((UserAgreement)data);
            }
            else if (data is UserLoginResponse)
            {
                ClearConversationKnowledge();
                UpdateLoginKnowledge(data);
                loginListeners?.Invoke(((UserLoginResponse)data).User);
                InvokeKnowledgeListeners((UserLoginResponse)data);
            }
            else if (data is ClientVersionCheckResponse)
            {
                UpdateVersionKnowledge(data);
                InvokeKnowledgeListeners((ClientVersionCheckResponse)data);
            }
            else if (data is Property)
            {
                var userRef = result.RequestContext[ContextUser];
                var courseRef = result.RequestContext.GetOrDefault(ContextCourse, null);
                UpdatePropertyKnowledge(userRef, courseRef, data);
                InvokeKnowledgeListeners((Property)data);
            }
            else if (data is ConversationMessage)
            {
                var conversationRef = result.RequestContext[ContextConversation];
                var conversation = GetConversationKnowledge(conversationRef);
                UpdateConversationMessageKnowledge(conversationRef, data);

                GLInsights.UpdateConversationInsight(conversationRef);

                InvokeKnowledgeListeners((ConversationMessage)data);
                InvokeKnowledgeListeners(conversation);
            }
            else if (data is Conversation)
            {
                UpdateConversationKnowledge(data);
                GLInsights.UpdateConversationInsight(((Conversation)data).Ref);
                InvokeKnowledgeListeners((Conversation)data);
            }
            else if (data is Conversations)
            {
                var conversations = (Conversations)data;
                foreach (var conversation in conversations._Conversations)
                    UpdateConversationKnowledge(conversation);

                GLInsights.UpdateConversationsInsights(conversations);

                foreach (var conversation in conversations._Conversations)
                    InvokeKnowledgeListeners(conversation);
                InvokeKnowledgeListeners(conversations);
            }
            else if (data is Course)
            {
                // when loading a course, it becomes active and we should prepare!
                GLInsights.ClearInsights();
                ClearConversationKnowledge();
                activeCourse = data;

                UpdateCourseKnowledge(data);
                GLInsights.UpdateCourseInsights(data);
                courseSelectedListeners?.Invoke((Course)data);
                InvokeKnowledgeListeners((Course)data);
            }
            else if (data is CourseSummary)
            {
                if (result.RequestContext.ContainsKey(ContextUser))
                {
                    UpdateCourseSummaryKnowledgeForUser(data, result.RequestContext[ContextUser]);
                }
                UpdateCourseSummaryKnowledge(data, true, true);
                InvokeKnowledgeListeners((CourseSummary)data);
            }
            else if (data is CourseProgress)
            {
                UpdateCourseProgressKnowledge(data);
                GLInsights.UpdateCourseProgressInsights(data);
                InvokeKnowledgeListeners((CourseProgress)data);
            }
            else if (data is CourseResourceResult)
            {
                UpdateResourceResultKnowledge(data, result.RequestContext);
                GLInsights.UpdateResourceProgressInsight(data, result.RequestContext[ContextUser]);
                InvokeKnowledgeListeners((CourseResourceResult)data);
            }
            else if (data is ChapterProgress)
            {
                UpdateChapterProgressKnowledge(result.RequestContext[ContextUser], result.RequestContext[ContextCourse], data);
                GLInsights.UpdateChapterProgressInsights(result.RequestContext[ContextUser], data);
                InvokeKnowledgeListeners((ChapterProgress)data);
            }
            else if (data is ReviewStepResult)
            {
                UpdateReviewStepResultKnowledge(data, result.RequestContext);
                var chapterProgress = GetChapterProgressKnowledge(result.RequestContext[ContextUser], result.RequestContext[ContextCourse], result.RequestContext[ContextChapter]);
                GLInsights.UpdateChapterProgressInsights(result.RequestContext[ContextUser], chapterProgress);
                InvokeKnowledgeListeners((ReviewStepResult)data);
            }
            else if (data is CourseProgressSummary)
            {
                UpdateCourseProgressSummaryKnowledge(data, true, true);
                InvokeKnowledgeListeners((CourseProgressSummary)data);
            }
        }
    }

    /// <summary>
    /// updates the user and remembers the login
    /// </summary>
    /// <param name="login"></param>
    private static void UpdateLoginKnowledge(UserLoginResponse login)
    {
        activeUser = login.User;
        loginToken = login.SessionToken;
        UpdateUserKnowledge(login.User, updateCourseSummaries: true, updateCourseProgressSummaries: true);
    }

    /// <summary>
    /// Remember the latest user agreement
    /// </summary>
    private static void UpdateUserAgreementKnowledge(UserAgreement agreement)
    {
        latestAgreement = agreement;
    }

    /// <summary>
    /// remembers the latest version and the status of the current
    /// </summary>
    /// <param name="versionCheck"></param>
    private static void UpdateVersionKnowledge(ClientVersionCheckResponse versionCheck)
    {
        versionStatus = versionCheck.Status.Value;
        latestVersion = versionCheck.Latest;
        currentVersion = versionCheck.Current;
    }

    /// <summary>
    /// updates the knowledge on a course, but also updates across the knowledge base:
    /// - CourseSummary of itself
    /// - CourseProgressSummaries for its users
    /// </summary>
    /// <param name="course"></param>
    private static void UpdateCourseKnowledge(Course course)
    {
        courses.SetOrAdd(course.Ref, course);

        // remember all resources
        foreach (var resource in course.Resources)
        {
            resources.SetOrAdd(resource.Ref, resource);
        }

        // remember all argument types
        foreach (var chapter in course.Chapters)
        {
            foreach (var reviewStep in chapter.ReviewSteps)
            {
                foreach (var argumentType in reviewStep.AllowedArguments)
                {
                    argumentTypes.SetOrAdd(argumentType.Ref, argumentType);
                }
            }
        }

        // we should also update the course summary as everything that has this summary requires an updated version
        CourseSummary courseSummary = new CourseSummary(course.Ref, course.Name, course.Year, course.Quarter, course.AnonymousUsers, course.AnonymousReviews);
        UpdateCourseSummaryKnowledge(courseSummary, 
            updateCourse: false,  // no because that is the source of the update
            updateUsersWithSummaries: true
        );

        // we should also update all knowledge of users' course progress
        foreach (var courseProgressSummary in course.Users)
        {
            UpdateCourseProgressSummaryKnowledge(courseProgressSummary, 
                updateCourseProgress: true, 
                updateUserInCourse: false // no because that is the source of the update
            );
        }
    }

    /// <summary>
    /// If a user joins a course, the courseSummary can be added to its list of courses
    /// </summary>
    /// <param name="courseSummary"></param>
    /// <param name="userRef"></param>
    private static void UpdateCourseSummaryKnowledgeForUser(CourseSummary courseSummary, string userRef)
    {
        var courses = users[userRef].Courses;
        try
        {
            // if it already exists in the user, just update it
            var index = courses.FindIndex(item => item.Ref == courseSummary.Ref);
            courses[index] = courseSummary;
        }
        catch
        {
            // if not, add it
            courses.Add(courseSummary);
        }
    }

    /// <summary>
    /// Updates the course summary knowledge , but also updates across the knowledge base:
    /// - The course it belongs to, if desired
    /// - The coursesummaries that users have in their definitions, if desired
    /// </summary>
    /// <param name="courseSummary"></param>
    /// <param name="updateCourse"></param>
    /// <param name="updateUsersWithSummaries"></param>
    private static void UpdateCourseSummaryKnowledge(CourseSummary courseSummary, bool updateCourse, bool updateUsersWithSummaries)
    {
        courseSummaries.SetOrAdd(courseSummary.Ref, courseSummary);

        // we may have new knowledge on the courses themselves 
        if (updateCourse && courses.ContainsKey(courseSummary.Ref))
        {
            var course = courses[courseSummary.Ref];
            course.AnonymousReviews = courseSummary.AnonymousReviews;
            course.AnonymousUsers = courseSummary.AnonymousUsers;
            course.Quarter = courseSummary.Quarter;
            course.Year = courseSummary.Year;
            course.Name = courseSummary.Name;
        }

        // users contain references to coursesummaries, they should be updated as well
        if (updateUsersWithSummaries)
        {
            foreach (var kvp in users)
            {
                try
                {
                    var index = kvp.Value.Courses.FindIndex(item => item.Ref == courseSummary.Ref);
                    kvp.Value.Courses[index] = courseSummary;
                }
                catch { }
            }

        }
    }

    /// <summary>
    /// Updates our knowledge of a result for a review step
    /// </summary>
    /// <param name="newResult"></param>
    /// <param name="context"></param>
    private static void UpdateReviewStepResultKnowledge(ReviewStepResult newResult, Dictionary<string, string> context)
    {
        var userRef = context[ContextUser];
        var courseRef = context[ContextCourse];
        var chapterRef = context[ContextChapter];
        var trackRef = context[ContextReviewerRelation];
        var reviewStepRef = context[ContextReviewStep];

        var courseProgress = coursesProgress[Tuple.Create(userRef, courseRef)];
        var chapterProgress = courseProgress.Chapters.Find(chap => chap.ChapterRef == chapterRef);
        var reviewRelation = chapterProgress.ReviewerRelations.Find(track => track.Ref == trackRef);

        if (reviewRelation.ReviewStepResults == null)
        {
            reviewRelation.ReviewStepResults = new List<ReviewStepResult>();
        }

        var stepResultIndex = reviewRelation.ReviewStepResults.FindIndex(result => result.ReviewStepRef == reviewStepRef);
        if (stepResultIndex >= 0)
        {
            reviewRelation.ReviewStepResults[stepResultIndex] = newResult;
        }
        else
        {
            reviewRelation.ReviewStepResults.Add(newResult);
        }
    }

    /// <summary>
    /// Updates our knowledge of a result for a review step
    /// </summary>
    /// <param name="newResult"></param>
    /// <param name="context"></param>
    private static void UpdateResourceResultKnowledge(CourseResourceResult newResult, Dictionary<string, string> context)
    {
        var userRef = context[ContextUser];
        var courseRef = context[ContextCourse];
        var resourceRef = context[ContextResource];

        var courseProgress = coursesProgress[Tuple.Create(userRef, courseRef)];

        if (courseProgress.ResourceResults == null)
        {
            courseProgress.ResourceResults = new List<CourseResourceResult>();
        }

        var resourceResultIndex = courseProgress.ResourceResults.FindIndex(result => result.ResourceRef == resourceRef);
        if (resourceResultIndex >= 0)
        {
            courseProgress.ResourceResults[resourceResultIndex] = newResult;
        }
        else
        {
            courseProgress.ResourceResults.Add(newResult);
        }
    }

    /// <summary>
    /// Updates the course progress knowledge of a user and a course, but also updates across the knowledge base:
    /// - the CourseProgressSummary that can derived 
    /// </summary>
    /// <param name="courseProgress"></param>
    private static void UpdateCourseProgressKnowledge(CourseProgress courseProgress)
    {
        coursesProgress.SetOrAdd(Tuple.Create(courseProgress.UserRef, courseProgress.CourseRef), courseProgress);

        // displayname and properties are handled here, so no need to do this ourselves
        // the CourseUser object is basically a user's properties and its display name specific to a course
        var courseProgressSummaryOld = GetCourseProgressSummaryKnowledge(courseProgress.UserRef, courseProgress.CourseRef);
        var courseProgressSummary = new CourseProgressSummary(
            courseProgress.UserRef, 
            courseProgress.CourseRef, 
            courseProgress.DisplayName,
            courseProgressSummaryOld?.Active == true, // not sure if ?. would give false if obj is null, so lets check for true value
            courseProgressSummaryOld?.FullName,
            courseProgressSummaryOld?.Role, 
            courseProgress.Topic, 
            courseProgress.AchievedSkills, 
            courseProgress.UserCourseProperties);

        UpdateCourseProgressSummaryKnowledge(courseProgressSummary, 
            updateCourseProgress: false, // no because that is the source of the update
            updateUserInCourse: true
        );
    }

    /// <summary>
    /// Updates the chapter progress knowledge in the corresponding CourseProgress object
    /// </summary>
    /// <param name="userRef"></param>
    /// <param name="courseRef"></param>
    /// <param name="chapterProgress"></param>
    private static void UpdateChapterProgressKnowledge(string userRef, string courseRef, ChapterProgress chapterProgress)
    {
        // we are not maintaining a separate list for chapter progresses, because it exists within
        // a courseprogress. Therefore, we simply update that list by
        // replacing or adding the progress depending on if it exists in the list

        var courseProgress = GetCourseProgressKnowledge(userRef, courseRef);
        try
        {
            var index = courseProgress.Chapters.FindIndex(item => item.ChapterRef == chapterProgress.ChapterRef);
            courseProgress.Chapters[index] = chapterProgress;
        }
        catch
        {
            courseProgress.Chapters.Add(chapterProgress);
        }
    }

    /// <summary>
    /// Updates the properties of a user and its course (if applicable) by aookying its values to
    /// the corresponding preference objects
    /// </summary>
    /// <param name="userRef"></param>
    /// <param name="courseRef"></param>
    /// <param name="cloudProperties"></param>
    private static void UpdatePropertiesKnowledge(string userRef, string courseRef, Dictionary<string, string> cloudProperties)
    {
        foreach (var kvp in cloudProperties)
        {
            var pref = GLPrefs.Get(kvp.Key);

            // only process known preferences
            if (pref != null)
            {
                pref.SetCloudValue(userRef, courseRef, kvp.Value);
            }
        }
    }

    /// <summary>
    /// Updates the properties of a user and its course (if applicable) by aookying its values to
    /// the corresponding preference objects
    /// </summary>
    /// <param name="userRef"></param>
    /// <param name="courseRef"></param>
    /// <param name="cloudProperties"></param>
    private static void UpdatePropertyKnowledge(string userRef, string courseRef, Property property)
    {
        var pref = GLPrefs.Get(property.Key);
        pref.SetCloudValue(userRef, courseRef, property.Value);
    }

    /// <summary>
    /// Updates the CourseProgressSummary of a user and a course, but also updates across the knowledge base:
    /// - the displayname of a user
    /// - the user properties defined in the given object
    /// - the CourseProgress object that it corresponds to
    /// - the courses that has a list of CourseProgressSummaries
    /// </summary>
    /// <param name="courseProgressSummary"></param>
    /// <param name="updateCourseProgress"></param>
    /// <param name="updateUserInCourse"></param>
    private static void UpdateCourseProgressSummaryKnowledge(CourseProgressSummary courseProgressSummary, bool updateCourseProgress, bool updateUserInCourse)
    {
        var courseRef = courseProgressSummary.CourseRef;
        var userRef = courseProgressSummary.UserRef;
        var tuple = Tuple.Create(userRef, courseRef);

        // remember the summary
        courseProgressSummaries.SetOrAdd(tuple, courseProgressSummary);
        // update the display name in our knowledge base
        displayNames.SetOrAdd(userRef, courseProgressSummary.DisplayName);
        // update the properties of this user in our preference system
        UpdatePropertiesKnowledge(userRef, courseRef, courseProgressSummary.UserCourseProperties);
        // once received we don't manage them here, so empty them
        courseProgressSummary.UserCourseProperties.Clear();

        // we also need to update the courseprogress object where specific data in the CourseUser is also included
        if (updateCourseProgress)
        {
            var courseProgressTuple = Tuple.Create(userRef, courseRef);
            if (coursesProgress.ContainsKey(courseProgressTuple))
            {
                var courseProgress = coursesProgress[courseProgressTuple];
                courseProgress.DisplayName = courseProgressSummary.DisplayName;
                courseProgress.UserCourseProperties = courseProgressSummary.UserCourseProperties;
            }
        }

        // we also need to update the courseuser objects in the course object
        if (updateUserInCourse && courses.ContainsKey(courseRef))
        {
            var course = courses[courseRef];
            try
            {
                var index = course.Users.FindIndex(item => item.UserRef == userRef);
                course.Users[index] = courseProgressSummary;
            }
            catch
            {
                course.Users.Add(courseProgressSummary);
            }
        }
    }

    /// <summary>
    /// Updates the knowledge of a user across the knowledge base, but also:
    /// - the users' properties
    /// - the CourseSummaries that the user contains are updated throughout the knowledge base
    /// - the CourseProgressSummaries that the user contains are updated throughout the knowledge base
    /// </summary>
    /// <param name="user"></param>
    /// <param name="updateCourseSummaries"></param>
    /// <param name="updateCourseProgressSummaries"></param>
    private static void UpdateUserKnowledge(User user, bool updateCourseSummaries, bool updateCourseProgressSummaries)
    {
        // remember the user
        users.SetOrAdd(user.Ref, user);
        // update its properties
        UpdatePropertiesKnowledge(user.Ref, null, user.Properties);
        // once received we don't manage the properties here, so empty them
        user.Properties.Clear();

        // the summaries in the user of courses may contain new information
        if (updateCourseSummaries)
        {
            foreach (var courseSummary in user.Courses)
            {
                UpdateCourseSummaryKnowledge(
                    courseSummary, updateCourse: true, 
                    updateUsersWithSummaries: false // no because that is the source of the update
                );
            }
        }

        // the progress summaries may contain new information
        if (updateCourseProgressSummaries)
        {
            foreach (var courseProgressSummary in user.CourseProgressSummary)
            {
                UpdateCourseProgressSummaryKnowledge(courseProgressSummary, 
                    updateCourseProgress: true, 
                    updateUserInCourse: false // no because that is the source of the update
                );
            }
        }
    }

    /// <summary>
    /// Updates the knowledge of a message. This is only used when a conversation message has received Sent and Ref values
    /// but the message has not yet been included to the set of messages. Therefore, the message should be added/inserted to the list.
    /// </summary>
    /// <param name="conversationRef"></param>
    /// <param name="updatedMessage"></param>
    private static void UpdateConversationMessageKnowledge(string conversationRef, ConversationMessage updatedMessage)
    {
        var conversation = GetConversationKnowledge(conversationRef);
        if (conversation != null)
        {
            conversation.MessageCount++;
            conversationMessages.SetOrAdd(updatedMessage.Ref, updatedMessage);

            if (conversations[conversationRef].Messages == null)
            {
                conversations[conversationRef].Messages = new List<ConversationMessage>();
            }
            if (conversations[conversationRef].Messages.Count == 0)
            {
                conversations[conversationRef].Messages.Add(updatedMessage);
            }
            else
            {
                var messages = conversations[conversationRef].Messages;
                for (int i = messages.Count - 1; i >= 0; i++)
                {
                    if (messages[i].Sent < updatedMessage.Sent)
                    {
                        // we found a message that has a sent date BEFORE our new message, so we should add it after that
                        messages.Insert(i + 1, updatedMessage);
                        return;
                    }
                }
                UnityEngine.Debug.LogError("Couldn't find a place to insert the newly received message with ref " + updatedMessage.Ref);
            }
        }
        else
        {
            UnityEngine.Debug.LogError("We don't have a conversation available to add the given Conversation Message with ref " + updatedMessage.Ref + " to");
        }
    }

    /// <summary>
    /// Updates the knowledge on a conversation by replacing all plreviously known things and merging the list of messages
    /// </summary>
    /// <param name="conversationUpdate"></param>
    private static void UpdateConversationKnowledge(Conversation conversationUpdate)
    {

        // the received messages are guaranteed to be in order
        // but let's ensure they are in ascending order
        if (!conversationUpdate.Messages.IsNullOrEmpty() 
            && conversationUpdate.Messages.Count > 1
            && conversationUpdate.Messages[0].Ind.Value > conversationUpdate.Messages[1].Ind.Value)
        {
            conversationUpdate.Messages.Reverse();
        }
    
        // we are storing the conversation update, so we need to bring in the messages from the old conversation if we have it
        if (conversations.ContainsKey(conversationUpdate.Ref))
        {
            // merge the messages
            var originalConversation = conversations[conversationUpdate.Ref];
            var originalMessages = originalConversation.Messages;
            var newMessages = conversationUpdate.Messages;
            if (newMessages.Count > 0)
            {
                if (originalMessages.Count > 0)
                {
                    // we have to merge messages
                    var merged = MergeConversationMessages(originalMessages, newMessages);
                    conversationUpdate.Messages = merged;
                }
                else
                {
                    // nothing to do because the new messages are already set in the updated conversation
                }
            } 
            else
            {
                conversationUpdate.Messages = originalMessages;
            }

            // set the update
            conversations.SetOrAdd(conversationUpdate.Ref, conversationUpdate);
        }
        else
        {
            conversations.Add(conversationUpdate.Ref, conversationUpdate);
        }

        // update all knowledge on all messages
        foreach (var message in conversationUpdate.Messages)
        {
            conversationMessages.SetOrAdd(message.Ref, message);
        }
    }

    private static List<ConversationMessage> oldestMessages = new List<ConversationMessage>();
    private static List<ConversationMessage> newestMessages = new List<ConversationMessage>();
    private static Dictionary<long, ConversationMessage> tempMergedMessages = new Dictionary<long, ConversationMessage>();
    private static Dictionary<long, ConversationMessage> tempCleaningMessages = new Dictionary<long, ConversationMessage>();
    /// <summary>
    /// Whenever messages seem to be in wrong order or contain duplicates, this method will do hard cleanup
    /// </summary>
    /// <param name="messages"></param>
    private static void CleanupConversationList(List<ConversationMessage> messages)
    {
        tempCleaningMessages.Clear();
        foreach (var msg in messages)
        {
            tempCleaningMessages.SetOrAdd(msg.Ind.Value, msg);
        }
        messages.Clear();
        var sortedResult = tempCleaningMessages.Values.ToList();
        sortedResult.Sort((message1, message2) => message1.Ind.Value.CompareTo(message2.Ind.Value));
        messages.AddRange(sortedResult);
    }
    /// <summary>
    /// Merge a list of messages to ensure messages are unique, and messages are in order of sent date. This algorithm assumes that the provided lists are
    /// already ordered on sent date
    /// </summary>
    /// <param name="originalMessages"></param>
    /// <param name="updateMessages"></param>
    /// <returns></returns>
    private static List<ConversationMessage> MergeConversationMessages(List<ConversationMessage> originalMessages, List<ConversationMessage> updateMessages)
    {
        // let's check if there is anything to merge
        if (originalMessages.IsNullOrEmpty())
            if (updateMessages.IsNullOrEmpty()) return new List<ConversationMessage>();
            else return updateMessages;
        else if (updateMessages.IsNullOrEmpty()) return originalMessages;

        // let's check the sorting on the list of original messages
        var lastInd = long.MinValue;
        foreach (var msg in originalMessages)
        {
            if (msg.Ind.Value <= lastInd)
            {
                UnityEngine.Debug.LogError("List of KNOWLEDGE messages was not in correct order!! Cleaning up now, but this should not happen! Current Ind: " + msg.Ind.Value + ", but previous was: " + lastInd);
                originalMessages.SerializeToDebugLog();
                CleanupConversationList(originalMessages);
                originalMessages.SerializeToDebugLog();
                break;
            }
            lastInd = msg.Ind.Value;
        }

        // let's check the sorting on the list of new messages
        lastInd = long.MinValue;
        foreach (var msg in updateMessages)
        {
            if (msg.Ind.Value <= lastInd)
            {
                UnityEngine.Debug.LogError("List of NEW messages was not in correct order!! Cleaning up now, but this should not happen! Current Ind: " + msg.Ind.Value + ", but previous was: " + lastInd);
                updateMessages.SerializeToDebugLog();
                CleanupConversationList(updateMessages);
                updateMessages.SerializeToDebugLog();
                break;
            }
            lastInd = msg.Ind.Value;
        }

        oldestMessages.Clear();
        tempMergedMessages.Clear();
        newestMessages.Clear();

        var lastOriginalMessageInd = originalMessages[originalMessages.Count - 1].Ind.Value;
        var firstOriginalMessageInd = originalMessages[0].Ind.Value;
        var lastUpdateMessageInd = updateMessages[updateMessages.Count - 1].Ind.Value;
        var firstUpdateMessageInd = updateMessages[0].Ind.Value;

        var updateIndex = 0;
        var originalIndex = 0;

        //originalMessages.SerializeToDebugLog("originalMessages");
        //updateMessages.SerializeToDebugLog("updateMessages");

        // find oldest messages, here only at max 1 while loop will run
        while (originalIndex < originalMessages.Count && originalMessages[originalIndex].Ind.Value < firstUpdateMessageInd)
        {
            oldestMessages.Add(originalMessages[originalIndex++]);
        }
        while (updateIndex < updateMessages.Count && updateMessages[updateIndex].Ind.Value < firstOriginalMessageInd)
        {
            oldestMessages.Add(updateMessages[updateIndex++]);
        }
        //oldestMessages.SerializeToDebugLog("oldestMessages");

        // if lists overlap, this is where 2 while loops add all known messages to a dictionary, 
        // then we'll only have unique messages, all are sorted , and later added to the resultMessages list.
        while (originalIndex < originalMessages.Count && originalMessages[originalIndex].Ind.Value <= lastUpdateMessageInd)
        {
            var message = originalMessages[originalIndex++];
            tempMergedMessages.SetOrAdd(message.Ind.Value, message);
        }
        while (updateIndex < updateMessages.Count && updateMessages[updateIndex].Ind.Value <= lastOriginalMessageInd)
        {
            var message = updateMessages[updateIndex++];
            tempMergedMessages.SetOrAdd(message.Ind.Value, message);
        }
        var mergedMessages = tempMergedMessages.Values.ToList();
        mergedMessages.Sort((message1, message2) => message1.Ind.Value.CompareTo(message2.Ind.Value));
        //mergedMessages.SerializeToDebugLog("mergedMessages");

        // find newest messages, here only at max 1 while loop will run
        while (originalIndex < originalMessages.Count && lastUpdateMessageInd < originalMessages[originalIndex].Ind.Value)
        {
            newestMessages.Add(originalMessages[originalIndex++]);
        }
        while (updateIndex < updateMessages.Count && lastOriginalMessageInd < updateMessages[updateIndex].Ind.Value)
        {
            newestMessages.Add(updateMessages[updateIndex++]);
        }
        //newestMessages.SerializeToDebugLog("newestMessages");

        var resultMessages = new List<ConversationMessage>(newestMessages.Count + oldestMessages.Count + tempMergedMessages.Count);
        resultMessages.AddRange(oldestMessages);
        resultMessages.AddRange(mergedMessages);
        resultMessages.AddRange(newestMessages);
        return resultMessages;
    }

    private static void UpdateDictionaryKnowledge<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key, TValue data)
    {
        if (dictionary.ContainsKey(key))
        {
            dictionary[key] = data;
        }
        else
        {
            dictionary.Add(key, data);
        }
    }

    #endregion
    #region context helpers

    public static readonly string ContextUser = "userRef";
    public static readonly string ContextCourse = "courseRef";
    public static readonly string ContextChapter = "chapterRef";
    public static readonly string ContextResource = "resourceRef";
    public static readonly string ContextConversation = "conversationRef";
    public static readonly string ContextArgument = "argumentRef";
    public static readonly string ContextReviewStep = "reviewStepRef";
    public static readonly string ContextReviewerRelation = "reviewerRelationRef";

    private static Dictionary<string, string> GetEmptyContext()
    {
        return new Dictionary<string, string>();
    }

    private static Dictionary<string, string> GetContextForCourse(string courseRef)
    {
        return new Dictionary<string, string>() { { ContextCourse, courseRef } };
    }

    private static Dictionary<string, string> GetContextForUser(string userRef)
    {
        return new Dictionary<string, string>() { { ContextUser, userRef } };
    }

    private static Dictionary<string, string> GetContextForCourseProgress(string userRef, string courseRef)
    {
        return new Dictionary<string, string>() { { ContextUser, userRef }, { ContextCourse, courseRef } };
    }

    private static Dictionary<string, string> GetContextForResource(string userRef, string courseRef, string resourceRef)
    {
        return new Dictionary<string, string>() { { ContextUser, userRef }, { ContextCourse, courseRef }, { ContextResource, resourceRef } };
    }

    private static Dictionary<string, string> GetContextForConversation(string courseRef, string conversationRef)
    {
        return new Dictionary<string, string>() { { ContextCourse, courseRef }, { ContextConversation, conversationRef } };
    }

    private static Dictionary<string, string> GetContextForChapterProgress(string userRef, string courseRef, string chapterRef)
    {
        return new Dictionary<string, string>() { { ContextUser, userRef }, { ContextCourse, courseRef }, { ContextChapter, chapterRef } };
    }

    private static Dictionary<string, string> GetContextForReviewStepProgress(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef)
    {
        return new Dictionary<string, string>() { { ContextUser, userRef }, { ContextCourse, courseRef }, { ContextChapter, chapterRef }, { ContextReviewerRelation, reviewerRelationRef }, { ContextReviewStep, reviewStepRef } };
    }

    private static Dictionary<string, string> GetContextForArgumentProgress(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef, string argumentRef)
    {
        return new Dictionary<string, string>() { { ContextUser, userRef }, { ContextCourse, courseRef }, { ContextChapter, chapterRef }, { ContextReviewerRelation, reviewerRelationRef }, { ContextReviewStep, reviewStepRef }, { ContextArgument, argumentRef } };
    }

    #endregion
    #region setup
    [UnityEngine.Header("Setup")]
    public bool forceUseLive;
    public string apiDebugPath;
    public string apiDebugIP;
    public string apiLivePath;
    public string apiLiveIP;
    public bool logServerCommunication = true;
    public string bugReportPath = "/client/bug-report";
    public string communicationSentString = "      ------->>";
    public string communicationReceivedString = "<<-------      ";
    public bool useCache = true;

    public string ApiPath
    {
        get
        {
            if (GLDebug.IsDebugMode)
            {
                if (forceUseLive)
                {
                    return apiLivePath;
                }
                else
                {
                    return apiDebugPath;
                }
            }
            else
            {
                return apiLivePath;
            }
        }
    }

    public string ApiIP
    {
        get
        {
            if (GLDebug.IsDebugMode)
            {
                if (forceUseLive)
                {
                    return apiLiveIP;
                }
                else
                {
                    return apiDebugIP;
                }
            }
            else
            {
                return apiLiveIP;
            }
        }
    }

    [UnityEngine.Header("Preferences")]
    public GLSerializablePreference apiCache;

    [UnityEngine.Header("Error Localization")]
    public LocalizedText Error_None;
    public LocalizedText Error_Unknown;
    public LocalizedText Error_UnknownErrorCode;
    public LocalizedText Error_UnknownErrorEnum;
    public LocalizedText Error_NotFound;
    public LocalizedText Error_EntityExists;
    public LocalizedText Error_LoginInvalid;
    public LocalizedText Error_ConstrainViolation;
    public LocalizedText Error_Authorization;
    public LocalizedText Error_TokenInvalid;

    public LocalizedText Error_JsonParseException;

    public LocalizedText Error_ApiCanceled;
    public LocalizedText Error_ApiException;
    public LocalizedText Error_ApiUnknownClientError;
    public LocalizedText Error_ApiClientError;
    public LocalizedText Error_ApiUnknownServerError;
    public LocalizedText Error_ApiServerError;
    public LocalizedText Error_ApiNotFound;
    public LocalizedText Error_ServerNotFound;
    public LocalizedText Error_EmptyResponse;

    [UnityEngine.Header("Api Call Localizations")]
    public LocalizedText Call_UploadFileForReviewStep;
    public LocalizedText Call_UpdateUser;
    public LocalizedText Call_UpdateCourseProgressSummary;
    public LocalizedText Call_SendUserPreference;
    public LocalizedText Call_SendUserCoursePreference;
    public LocalizedText Call_SendReviewQuizResults;
    public LocalizedText Call_CloseReviewStep;
    public LocalizedText Call_OpenReviewStep;
    public LocalizedText Call_SendChatMessage;
    public LocalizedText Call_MarkAsRead;
    public LocalizedText Call_ResetPassword;
    public LocalizedText Call_RequestPasswordResetToken;
    public LocalizedText Call_Register;
    public LocalizedText Call_OpenCourse;
    public LocalizedText Call_LoginUser;
    public LocalizedText Call_JoinCourse;
    public LocalizedText Call_FetchUserPreference;
    public LocalizedText Call_FetchUserCoursePreference;
    public LocalizedText Call_FetchUserAgreement;
    public LocalizedText Call_FetchUser;
    public LocalizedText Call_FetchCourseProgressSummary;
    public LocalizedText Call_FetchCourseProgress;
    public LocalizedText Call_FetchConversation;
    public LocalizedText Call_FetchChatConversations;
    public LocalizedText Call_CreateChatConversations;
    public LocalizedText Call_FetchChapterProgress;
    public LocalizedText Call_FetchAnnouncements;
    public LocalizedText Call_CheckVersion;
    public LocalizedText Call_FetchServerInfo;
    public LocalizedText Call_SendReport;

    private GLRawGameApi rawApi;
    private Dictionary<string, GLApiError> errorCodeToEnum = new Dictionary<string, GLApiError>();

    protected override void OnSingletonInitialize()
    {
        rawApi = new GLRawGameApi(ApiPath);
        // rawApi.ExceptionFactory = null;
        var values = (GLApiError[])Enum.GetValues(typeof(GLApiError));
        foreach (var val in values)
        {
            errorCodeToEnum.Add(val.ToErrorCode(), val);
        }

        LoadCache();

        if (GLDebug.IsDebugMode && forceUseLive)
        {
            GLPopup.MakeWarningPopup("USING LIVE DATABASE!! WATCH OUT WITH YOUR ACTIVITIES. To disable, go to Api System object, and disable the 'forceLive' toggle.").Show();
        }
    }
    #endregion
    #region caching
    private class ApiCache
    {
        public Dictionary<string, string> displayNames = new Dictionary<string, string>();
        public Dictionary<string, Conversation> conversations = new Dictionary<string, Conversation>();
        public Dictionary<string, ConversationMessage> conversationMessages = new Dictionary<string, ConversationMessage>();

        public List<Announcement> announcements;
        public UserAgreement latestAgreement;
        public ClientVersion latestVersion;
        public ClientVersion currentVersion;
        public ClientVersionCheckResponseStatus versionStatus;
    }

    private void OnApplicationQuit()
    {
        if (useCache)
            SaveCache();
    }

    private void LoadCache()
    {
        apiCache.SetPreferenceType(typeof(ApiCache));
        if (!useCache) return;

        ApiCache cache = apiCache.Value as ApiCache;
        if (cache != null)
        {
            displayNames = cache.displayNames;
            conversations = cache.conversations;
            conversationMessages = cache.conversationMessages;
            announcements = cache.announcements;
            latestAgreement = cache.latestAgreement;
            latestVersion = cache.latestVersion;
            currentVersion = cache.currentVersion;
            versionStatus = cache.versionStatus;
        }
    }

    private void SaveCache()
    {
        ApiCache cache = new ApiCache();
        cache.displayNames = displayNames;
        cache.conversations = conversations;
        cache.conversationMessages = conversationMessages;
        cache.announcements = announcements;
        cache.latestAgreement = latestAgreement;
        cache.latestVersion = latestVersion;
        cache.currentVersion = currentVersion;
        cache.versionStatus = versionStatus;
        apiCache.Value = cache;
    }
    #endregion
    #region errors
    public static bool DiplayErrorPopupIfFailed(GLTaskResult result)
    {
        if (result.Failed)
        {
            var enumValue = GetEnumFromErrorString(result.ErrorMessage);
            var localizedText = GetLocalizedTextFromErrorCode(result.ErrorMessage);

            switch (enumValue)
            {
                case GLApiError.ApiUnknownError:
                case GLApiError.ApiClientError:
                case GLApiError.ApiServerError:
                    GLPopup.MakeErrorPopup(localizedText.Format((string)result.Result)).Show();
                    break;
                default:
                    GLPopup.MakeErrorPopup(localizedText).Show();
                    break;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static LocalizedText GetLocalizedTextFromErrorCode(string error)
    {
        var enumValue = GetEnumFromErrorString(error);
        return GetLocalizedTextFromErrorEnum(GetEnumFromErrorString(error));
    }

    public static LocalizedText GetLocalizedTextFromErrorEnum(GLApiError error)
    {
        switch (error)
        {
            case GLApiError.None:
                return GetInstance().Error_None;

            case GLApiError.EntityExists:
                return GetInstance().Error_EntityExists;
            case GLApiError.EntityNotFound:
                return GetInstance().Error_NotFound;
            case GLApiError.LoginInvalid:
                return GetInstance().Error_LoginInvalid;
            case GLApiError.ErrorAuthorization:
                return GetInstance().Error_Authorization;
            case GLApiError.ErrorUnknown:
                return GetInstance().Error_Unknown;
            case GLApiError.ErrorConstraintViolation:
                return GetInstance().Error_ConstrainViolation;

            case GLApiError.ErrorCodeUnknown:
                return GetInstance().Error_UnknownErrorCode;

            case GLApiError.TokenInvalid:
                return GetInstance().Error_TokenInvalid;

            case GLApiError.JsonParseException:
                return GetInstance().Error_JsonParseException;

            case GLApiError.ApiCanceled:
                return GetInstance().Error_ApiCanceled;
            case GLApiError.ApiUnknownError:
                return GetInstance().Error_ApiException;
            case GLApiError.ApiUnknownClientError:
                return GetInstance().Error_ApiUnknownClientError;
            case GLApiError.ApiClientError:
                return GetInstance().Error_ApiClientError;
            case GLApiError.ApiUnknownServerError:
                return GetInstance().Error_ApiUnknownServerError;
            case GLApiError.ApiServerError:
                return GetInstance().Error_ApiServerError;
            case GLApiError.ApiNotFound:
                return GetInstance().Error_ApiNotFound;
            case GLApiError.ServerNotFound:
                return GetInstance().Error_ServerNotFound;
            case GLApiError.EmptyResponse:
                return GetInstance().Error_EmptyResponse;
            default:
                return GetInstance().Error_UnknownErrorEnum;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="error"></param>
    /// <returns></returns>
    public static GLApiError GetEnumFromErrorString(string error)
    {
        var errorCodeToEnum = GetInstance().errorCodeToEnum;

        if (errorCodeToEnum.ContainsKey(error))
        {
            return errorCodeToEnum[error];
        } else
        {
            return GLApiError.ErrorCodeUnknown;
        }
    }
    #endregion
    #region thread tasks
    
    /// <summary>
    /// Make API task to manage the api thread call
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="threadTask"></param>
    /// <returns></returns>
    GLTask<T> MakeApiTask<T>(LocalizedText description, Func<System.Threading.Tasks.Task<ApiResponse<T>>> makeThreadTask, Dictionary<string, string> requestContext)
    {
        System.Threading.Tasks.Task<ApiResponse<T>> threadTask = null;

        ApiCall apiCall = new ApiCall()
        {
            doCall = () => {
                threadTask = makeThreadTask();
            }
        };

        AddCallToQueue(apiCall);

        return GLRun.AddTask(description, (GLTask<T> thisTask, int step) => 
        {
            if (threadTask == null) return;
            if (threadTask.IsCanceled)
            {
                OnCallFinished();

                thisTask.SetFailed(GLApiError.ApiCanceled.ToErrorCode());
                UnityEngine.Debug.Log("API Connection task canceled");
            }
            else if (threadTask.IsFaulted)
            {
                if (threadTask.Exception != null && threadTask.Exception.InnerException != null && threadTask.Exception.InnerException is ApiException)
                {
                    var apiException = threadTask.Exception.InnerException as ApiException;
                    var restResponse = apiException.ErrorContent as RestResponse;
                    UnityEngine.Debug.Log("Failed API call with error code: " + apiException.ErrorCode + "\n" + JsonConvert.SerializeObject(threadTask.Exception, Formatting.Indented));

                    if (restResponse == null)
                    {
                        thisTask.SetFailed(GLApiError.EmptyResponse.ToErrorCode());
                    }
                    else
                    {
                        UnityEngine.Debug.Log(MaskSecretProperties(JsonConvert.SerializeObject(restResponse, Formatting.Indented)));

                        if (restResponse.ErrorException != null)
                        {
                            if (restResponse.ErrorException is System.Net.WebException)
                            {
                                thisTask.SetFailed(GLApiError.ServerNotFound.ToErrorCode());
                            }
                            else
                            {
                                thisTask.SetFailed(GLApiError.ApiUnknownClientError.ToErrorCode());
                            }
                        }
                        else
                        {
                            if (restResponse.Content.IsValidJSON())
                            {
                                try
                                {
                                    ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(restResponse.Content);

                                    var errorEnum = GetEnumFromErrorString(error.Error.Code);
                                    if (errorEnum == GLApiError.LoginInvalid && IsLoggedIn())
                                    {
                                        GLGameFlow.I.Logout();
                                        errorEnum = GLApiError.TokenInvalid;
                                        error.Error.Code = GLApiError.TokenInvalid.ToErrorCode();
                                    }

                                    UnityEngine.Debug.Log(error.Error.Code);
                                    thisTask.SetFailed(error.Error.Code);
                                }
                                catch
                                {
                                    thisTask.SetFailed(GLApiError.JsonParseException.ToErrorCode());
                                }
                            }
                            else if (restResponse.StatusCode == System.Net.HttpStatusCode.NotFound)
                            {
                                thisTask.SetFailed(GLApiError.ApiNotFound.ToErrorCode());
                            }
                            else if ((int)restResponse.StatusCode < 500 && (int)restResponse.StatusCode >= 400)
                            {
                                thisTask.SetFailed(GLApiError.ApiClientError.ToErrorCode(), restResponse.StatusCode.ToString());
                            }
                            else if ((int)restResponse.StatusCode < 600 && (int)restResponse.StatusCode >= 500)
                            {
                                thisTask.SetFailed(GLApiError.ApiServerError.ToErrorCode(), restResponse.StatusCode.ToString());
                            }
                            else
                            {
                                thisTask.SetFailed(GLApiError.ApiUnknownError.ToErrorCode(), restResponse.StatusCode.ToString());
                            }
                        }
                    }
                }
                else
                {
                    thisTask.SetFailed(GLApiError.ApiUnknownClientError.ToErrorCode());
                    UnityEngine.Debug.Log("API Connection task failed with exception: " + threadTask.Exception);
                }

                OnCallFinished();
            } 
            else if (threadTask.IsCompleted)
            {
                OnCallFinished();

                var result = threadTask.Result;
                if (result.Data == null && result.StatusCode != 204)
                {
                    thisTask.SetFailed(GLApiError.ApiUnknownServerError.ToErrorCode());
                    UnityEngine.Debug.Log("Failed API call, status code: " + result.StatusCode + "; Headers: \n" + string.Join("\n", result.Headers.Select(x => x.Key + " = " + x.Value).ToArray()));
                } else
                {
                    thisTask.SetSucceeded(result.Data);
                }
            }
        }, requestContext, UpdateKnowledge);
    }
    #endregion
    #region logging

    private static readonly List<String> PROPERTIES_TO_MASK = new List<string> { "password", "licenceKey", "token", "sessionToken", "sessionToken", "X-Token" };
    public static string MaskSecretProperties(string body)
    {
        if (body == null) return "";
        var maskedBody = body;
        foreach (string propertyToMask in PROPERTIES_TO_MASK)
        {
            maskedBody = System.Text.RegularExpressions.Regex.Replace(maskedBody, "(\"" + propertyToMask + "\"\\s*:\\s*)\"(.*?)\"", "$1\"*****\"");
        }
        return maskedBody;
    }
    public static object MaskSecretPropertiesObject(object obj)
    {
        if (obj == null) return null;
        if (obj is System.String)
        {
            return MaskSecretProperties(obj as string);
        }
        return JsonConvert.DeserializeObject(MaskSecretProperties(JsonConvert.SerializeObject(obj)));
    }

    public static void LogCallRawApi(
        String path, RestSharp.Method method, List<KeyValuePair<String, String>> queryParams, Object postBody,
        Dictionary<String, String> headerParams, Dictionary<String, String> formParams,
        Dictionary<String, FileParameter> fileParams, Dictionary<String, String> pathParams,
        String contentType)
    {
        if (!I.logServerCommunication) return;
        if (path.Contains(I.bugReportPath))
        {
            UnityEngine.Debug.Log(I.communicationSentString + "Sent report to server");
            return;
        }

        var maskedBody = MaskSecretPropertiesObject(postBody);
        var bodyObject = maskedBody is string ? JsonConvert.DeserializeObject(maskedBody as string) : maskedBody;

        GLDebug.I.LogCommunicationSent(method.ToString(), path, bodyObject, queryParams, headerParams, formParams, fileParams, pathParams, contentType);

        UnityEngine.Debug.Log(I.communicationSentString + "\nSending " + method + " @ " + path
            + (maskedBody != null ? "\nPost body: " + maskedBody + "\n" : "")
            + (!queryParams.IsNullOrEmpty() ? "\nQuery params: " + queryParams.ToDebugString() + "\n" : "")
            + (!headerParams.IsNullOrEmpty() ? "\nHeader params: " + headerParams.ToDebugString(",", "X-Token") + "\n" : "")
            + (!formParams.IsNullOrEmpty() ? "\nForm params: " + formParams.ToDebugString() + "\n" : "")
            + (!pathParams.IsNullOrEmpty() ? "\nForm params: " + pathParams.ToDebugString() + "\n" : ""));
    }

    public static void LogResponseCallRawApi(IRestResponse response,
        String path, RestSharp.Method method, List<KeyValuePair<String, String>> queryParams, Object postBody,
        Dictionary<String, String> headerParams, Dictionary<String, String> formParams,
        Dictionary<String, FileParameter> fileParams, Dictionary<String, String> pathParams,
        String contentType)
    {
        if (!I.logServerCommunication) return;

        if (response is RestSharp.RestResponse)
        {

            var restResponse = (RestSharp.RestResponse)response;
            var maskedContent = MaskSecretProperties(restResponse.Content);

            GLDebug.I.LogCommunicationReceived(
                method.ToString(), 
                path, 
                restResponse.IsSuccessful, 
                restResponse.StatusCode.ToString(), 
                restResponse.ResponseStatus.ToString(), 
                restResponse.StatusDescription, 
                maskedContent.IsValidJSON() ? JsonConvert.DeserializeObject(maskedContent) : maskedContent, 
                restResponse.ErrorException, 
                restResponse.ErrorMessage, 
                restResponse.Headers);

            UnityEngine.Debug.Log(I.communicationReceivedString + "\nReceived reply for " + method + " @ " + path
                + "\nIsSuccessful: " + restResponse.IsSuccessful
                + "\nStatusCode: " + restResponse.StatusCode
                + "\nResponseStatus: " + restResponse.ResponseStatus
                + "\nStatusDescription: " + restResponse.StatusDescription
                + "\n\nContent: " + maskedContent
                + "\n\nException: " + restResponse.ErrorException
                + "\n\nErrorMessage: " + restResponse.ErrorMessage
                + "\n\nHeaders: " + restResponse.Headers.ToDebugString());
        }
        else
        {
            UnityEngine.Debug.Log("Could not log response of type " + response.GetType());
        }
    }


    #endregion
    #region api

    private class ApiCall
    {
        public Action doCall;
    }

    private static Queue<ApiCall> callQueue = new Queue<ApiCall>();

    private static void AddCallToQueue(ApiCall apiCall)
    {
        bool callRightAway = callQueue.Count == 0;
        callQueue.Enqueue(apiCall);
        if (callRightAway)
        {
            apiCall.doCall.Invoke();
        }
    }

    private static void OnCallFinished()
    {
        callQueue.Dequeue();
        if (callQueue.Count > 0)
        {
            callQueue.Peek().doCall();
        }
    }

    /// <summary>
    /// Postclientbugreport
    /// </summary>
    /// <remarks>
    /// Call to store bug reports (debug information), which will aid the developers to find, assist and fix problems
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="body"> (optional)</param>
    /// <returns>Task of ApiResponse</returns>
    public static GLTask<Object> SendBugReport(BugReport body = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_SendReport, () => GetInstance().rawApi.ClientBugReportPostAsyncWithHttpInfo(body), GetEmptyContext());
    }

    /// <summary>
    /// Getserverinfo
    /// </summary>
    /// <remarks>
    /// Get server info
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <returns>Task of ApiResponse (ServerInfo)</returns>
    public static GLTask<ServerInfo> FetchServerInfo()
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchServerInfo, () => GetInstance().rawApi.ServerInfoGetAsyncWithHttpInfo(), GetEmptyContext());
    }

    /// <summary>
    /// Get all current announcements
    /// </summary>
    /// <remarks>
    /// Get all current announcements
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <returns>Task of ApiResponse (Announcement)</returns>
    public static GLTask<List<Announcement>> FetchAnnouncements()
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchAnnouncements, () => GetInstance().rawApi.ClientAnnouncementGetAsyncWithHttpInfo(), GetEmptyContext());
    }

    /// <summary>
    /// Checks the given client version with the latest and returns the status
    /// </summary>
    /// <remarks>
    /// Checks the given client version with the latest and returns the status
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="build">The build number</param>
    /// <param name="platform">The build number</param>
    /// <returns>Task of ApiResponse (ClientVersionCheckResponse)</returns>
    public static GLTask<ClientVersionCheckResponse> CheckVersion(int build, Platform platform)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_CheckVersion, () => GetInstance().rawApi.ClientVersionCheckGetAsyncWithHttpInfo(build, platform.ToString()), GetEmptyContext());
    }

    /// <summary>
    /// Retrieve the latest user agreement
    /// </summary>
    /// <remarks>
    /// Retrieve the latest user agreement
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <returns>Task of ApiResponse (UserAgreement)</returns>
    public static GLTask<UserAgreement> FetchUserAgreement()
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchUserAgreement, () => GetInstance().rawApi.UserAgreementLatestGetAsyncWithHttpInfo(), GetEmptyContext());
    }

    /// <summary>
    /// Update users&#39; details in a specific course
    /// </summary>
    /// <remarks>
    /// Update users&#39; details in a specific course
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="userRef">The ref of the user</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (CourseUser)</returns>
    public static GLTask<CourseProgressSummary> UpdateCourseProgressSummary(string userRef, string courseRef, CourseProgressSummary body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_UpdateCourseProgressSummary, () => GetInstance().rawApi.CourseUserByCourseRefAndUserRefPutAsyncWithHttpInfo(loginToken, courseRef, userRef, body), GetContextForCourseProgress(userRef, courseRef));
    }


    /// <summary>
    /// Retrieve a users&#39; details in a specific course
    /// </summary>
    /// <remarks>
    /// Retrieve a users&#39; details in a specific course
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course of which the progress is to be retrieved.</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved.</param>
    /// <param name="property">Comma separated list of free-form user-course properties to retrieve. Leave blank for all. (optional)</param>
    /// <returns>Task of ApiResponse (CourseUser)</returns>
    public static GLTask<CourseProgressSummary> FetchCourseProgressSummary(string userRef, string courseRef, string property = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchCourseProgressSummary, () => GetInstance().rawApi.CourseUserByCourseRefAndUserRefGetAsyncWithHttpInfo(loginToken, courseRef, userRef, property), GetContextForCourseProgress(userRef, courseRef));
    }


    /// <summary>
    /// Retrieve a specific user course property
    /// </summary>
    /// <remarks>
    /// Retrieve a specific user course property
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="userRef">The ref of the user, who is enrolled in the course</param>
    /// <param name="propertyKey">The property key of the property to retrieve</param>
    /// <returns>Task of ApiResponse (Property)</returns>
    public static GLTask<Property> FetchPreference(string userRef, string courseRef, string propertyKey)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchUserCoursePreference, () => GetInstance().rawApi.CourseUserPropertyPropertyKeyByCourseRefAndUserRefGetAsyncWithHttpInfo(loginToken, courseRef, userRef, propertyKey), GetContextForCourseProgress(userRef, courseRef));
    }
     
    /// <summary>
    /// Update a specific user course property
    /// </summary>
    /// <remarks>
    /// Update a specific user course property
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="userRef">The ref of the user</param>
    /// <param name="propertyKey">The property key of the property to retrieve</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (Property)</returns>
    public static GLTask<Property> SendPreference(string userRef, string courseRef, string propertyKey, Property body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_SendUserCoursePreference, () => GetInstance().rawApi.CourseUserPropertyPropertyKeyByCourseRefAndUserRefPutAsyncWithHttpInfo(loginToken, courseRef, userRef, propertyKey, body), GetContextForCourseProgress(userRef, courseRef));
    }

    /// <summary>
    /// Retrieve a specific user property
    /// </summary>
    /// <remarks>
    /// Retrieve a specific user property
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user to retrieve. A special option is &#x60;logged-in&#x60;, which retrieves the currently logged in user.</param>
    /// <param name="propertyKey">The property key of the property to retrieve</param>
    /// <returns>Task of ApiResponse (Property)</returns>
    public static GLTask<Property> FetchPreference(string userRef, string propertyKey)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchUserPreference, () => GetInstance().rawApi.UserPropertyByUserRefAndPropertyKeyGetAsyncWithHttpInfo(loginToken, userRef, propertyKey), GetContextForUser(userRef));
    }

    /// <summary>
    /// Update (or add) a single user property
    /// </summary>
    /// <remarks>
    /// Note that the path parameter determines which property will be updated, and if the key in the given property is different, it will be overriden. 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user to retrieve. A special option is &#x60;logged-in&#x60;, which retrieves the currently logged in user.</param>
    /// <param name="propertyKey">The property key of the property to retrieve</param>
    /// <param name="body">The user property with updated value</param>
    /// <returns>Task of ApiResponse (Property)</returns>
    public static GLTask<Property> SendPreference(string userRef, string propertyKey, Property body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_SendUserPreference, () => GetInstance().rawApi.UserPropertyByUserRefAndPropertyKeyPutAsyncWithHttpInfo(loginToken, userRef, propertyKey, body), GetContextForUser(userRef));
    }

    /// <summary>
    /// Get the full information on a course 
    /// </summary>
    /// <remarks>
    /// Get&#39;s the course information from the perspective of the logged in user. Which means that user-properties on the course will be included. 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course to retrieve</param>
    /// <returns>Task of ApiResponse (Course)</returns>
    public static GLTask<Course> OpenCourse(string courseRef)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_OpenCourse, () => GetInstance().rawApi.CourseByRefGetAsyncWithHttpInfo(loginToken, courseRef), GetContextForCourse(courseRef));
    }

    /// <summary>
    /// Get a specific conversation
    /// </summary>
    /// <remarks>
    /// Retrieve a specific conversation. An error will be returned in case the logged in user does not have access to the conversation or it does not exist 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the user to retrieve</param>
    /// <param name="conversationRef">The ref of the user to retrieve</param>
    /// <param name="updated">Filter on messages that were updated after the given date-time (optional)</param>
    /// <param name="messageLimit">The maximum number of messages to show per conversation. The default is 10. Reducing this may improve performance. (optional)</param>
    /// <returns>Task of ApiResponse (Conversations)</returns>
    public static GLTask<Conversation> FetchConversation(string courseRef, string conversationRef, DateTime? updated = null, int? messageLimit = null, int? messageSkip = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchConversation, () => GetInstance().rawApi.CourseConversationByCourseRefAndConversationRefGetAsyncWithHttpInfo(loginToken,  courseRef,  conversationRef, updated, messageLimit, messageSkip), GetContextForConversation(courseRef, conversationRef));
    }

    /// <summary>
    /// Retrieve conversations
    /// </summary>
    /// <remarks>
    /// Retrieve all non-hidden conversations that the logged in user has access to 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="updated">Filter on conversations and messages that were updated after the given date-time (optional)</param>
    /// <param name="messageLimit">The maximum number of messages to show per conversation. The default is 10. Reducing this may improve performance. (optional)</param>
    /// <param name="conversations">A comma separated list of conversation refs to be included. Note that references of conversations for which the user has no permission will be ignored, but \&quot;hidden\&quot; conversations are included. (optional)</param>
    /// <returns>Task of ApiResponse (Conversations)</returns>
    public static GLTask<Conversations> FetchChatConversations(string courseRef, DateTime? updated = null, int? messageLimit = null, string conversations = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchChatConversations, () => GetInstance().rawApi.CourseConversationByCourseRefGetAsyncWithHttpInfo(loginToken, courseRef, updated, messageLimit, conversations), GetContextForCourse(courseRef));
    }

    /// <summary>
    /// Create conversation
    /// </summary>
    /// <remarks>
    /// Creates a new conversation with the given title, members and roles. Note that messages included in the conversation create request will be ignored 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (Conversation)</returns>
    public static GLTask<Conversation> CreateConversation(string courseRef, Conversation body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_CreateChatConversations, () => GetInstance().rawApi.CourseConversationByCourseRefPostAsyncWithHttpInfo(loginToken, courseRef, body), GetContextForCourse(courseRef));
    }

    /// <summary>
    /// Post/add a message to a conversation.
    /// </summary>
    /// <remarks>
    /// Adds a message to the conversation with the given ref. The sender will be the currently logged in user. 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the user to retrieve</param>
    /// <param name="conversationRef">The ref of the user to retrieve</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (ConversationMessage)</returns>
    public static GLTask<ConversationMessage> SendChatMessage(string courseRef, string conversationRef, PostConversationMessageRequest body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_SendChatMessage, () => GetInstance().rawApi.CourseConversationMessageByCourseRefAndConversationRefPostAsyncWithHttpInfo(loginToken, courseRef, conversationRef, body), GetContextForConversation(courseRef,conversationRef));
    }
    /// <summary>
    /// Markconversationasread Marks the conversation as read until the given message index 
    /// </summary>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="courseRef">The ref of the user to retrieve</param>
    /// <param name="conversationRef">The ref of the user to retrieve</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (Conversation)</returns>
    public static GLTask<Conversation> MarkConversationAsRead(string courseRef, string conversationRef, long untilIndex)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_MarkAsRead, () => GetInstance().rawApi.CourseConversationReadByCourseRefAndConversationRefPostAsyncWithHttpInfo(loginToken, courseRef, conversationRef, new PostConversationReadRequest(untilIndex)), GetContextForConversation(courseRef, conversationRef));
    }

    /// <summary>
    /// Retrieve a specific user
    /// </summary>
    /// <remarks>
    /// Retrieve a specific user
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="_ref">The ref of the user to retrieve. A special option is &#39;logged-in&#39;, which retrieves th currently logged in user.</param>
    /// <param name="property">Comma separated list of free-form user properties you want to retrieve. Leave blank for all. (optional)</param>
    /// <returns>Task of ApiResponse (User)</returns>
    public static GLTask<User> FetchUser(string _ref, string property = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchUser, () => GetInstance().rawApi.UserByRefGetAsyncWithHttpInfo(loginToken, _ref, property), GetContextForUser(_ref));
    }

    /// <summary>
    /// Update the user with the given ref
    /// </summary>
    /// <remarks>
    /// Update the user with the given ref
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="_ref">The ref of the user to retrieve</param>
    /// <param name="body">The user with it&#39;s updates. Note that only user-defined properties that are included in the request are updated.</param>
    /// <returns>Task of ApiResponse (User)</returns>
    public static GLTask<User> UpdateUser(User body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_UpdateUser, () => GetInstance().rawApi.UserByRefPutAsyncWithHttpInfo(loginToken, body.Ref, body), GetContextForUser(body.Ref));
    }

    /// <summary>
    /// Retrieve a users&#39; progress on a specific course
    /// </summary>
    /// <remarks>
    /// Retrieve a users&#39; progress on a specific course
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved. Special option &#x60;logged-in&#x60; may be used to retrieve the progress of the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course of which the progress is to be retrieved.</param>
    /// <param name="property">Comma separated list of free-form user-course properties to retrieve. Leave blank for all. (optional)</param>
    /// <returns>Task of ApiResponse (CourseProgress)</returns>
    public static GLTask<CourseProgress> FetchCourseProgress(string userRef, string courseRef, string property = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchCourseProgress, () => GetInstance().rawApi.UserCourseProgressByUserRefAndCourseRefGetAsyncWithHttpInfo(loginToken, userRef, courseRef, property), GetContextForCourseProgress(userRef,courseRef));
    }

    /// <summary>
    /// Retrieve a users&#39; progress on a specific chapter of a course
    /// </summary>
    /// <remarks>
    /// Retrieve a users&#39; progress on a specific chapter of a course
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved. Special options:  - &#x60;logged-in&#x60;: retrieve the progress of the currently logged in user.  - &#x60;reviewer-x&#x60;: retrieve the progress for the reviewer of the logged in user with index x, where x is replaced by a zero-based index.  - &#x60;reviewee-x&#x60;: retrieve the progress for the person that the logged in user should review (the reviewee), where x is replaced by a zero-based index.</param>
    /// <param name="courseRef">The ref of the course of which the progress is to be retrieved.</param>
    /// <param name="chapterRef">The ref of the chapter of which the progress is to be retrieved.</param>
    /// <returns>Task of ApiResponse (ChapterProgress)</returns>
    public static GLTask<ChapterProgress> FetchChapterProgress(string userRef, string courseRef, string chapterRef)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchChapterProgress, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefByUserRefAndCourseRefGetAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef), GetContextForChapterProgress(userRef, courseRef, chapterRef));
    }
    /// <summary>
    /// Post a resource quiz result
    /// </summary>
    /// <remarks>
    /// Post a resource quiz result
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="resourceRef">The ref of the course</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (CourseResourceResult)</returns>
    public static GLTask<CourseResourceResult> SendResourceQuizResult(string userRef, string courseRef, string resourceRef, QuizResult body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_FetchChapterProgress, () => GetInstance().rawApi.UserCourseProgressResourceResourceRefQuizByUserRefAndCourseRefPostAsyncWithHttpInfo(loginToken, userRef, courseRef, resourceRef, body), GetContextForResource(userRef, courseRef, resourceRef));
    }

    /// <summary>
    /// Add an argument Add an argument to a review step within a reviewer relation
    /// </summary>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewerRelationRef">The ref of the reviewer relation under which this result belongs</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (Argument)</returns>
    public static GLTask<ReviewStepResult> SendArgument(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef, Argument body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_UploadFileForReviewStep, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefArgumentByUserRefAndCourseRefPostAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, body), GetContextForReviewStepProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef));
    }

    /// <summary>
    /// Delete an argument
    /// </summary>
    /// <remarks>
    /// Delete an argument to a review step within a reviewer relation
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewerRelationRef">The ref of the reviewer relation under which this result belongs</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <param name="argumentRef">The ref of the reviewStep</param>
    /// <returns>Task of ApiResponse (ReviewStepResult)</returns>
    public static GLTask<ReviewStepResult> DeleteArgument(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef, string argumentRef)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_UploadFileForReviewStep, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefArgumentArgumentRefByUserRefAndCourseRefDeleteAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef), GetContextForArgumentProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef));
    }

    /// <summary>
    /// Update an argument
    /// </summary>
    /// <remarks>
    /// Add an argument to a review step within a reviewer relation
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewerRelationRef">The ref of the reviewer relation under which this result belongs</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <param name="argumentRef">The ref of the reviewStep</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (ReviewStepResult)</returns>
    public static GLTask<ReviewStepResult> UpdateArgument(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef, string argumentRef, Argument body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_UploadFileForReviewStep, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefArgumentArgumentRefByUserRefAndCourseRefPutAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, body), GetContextForArgumentProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef));
    }


    /// <summary>
    /// Upload a file for the review part
    /// </summary>
    /// <remarks>
    /// Upload a file for the review part
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <param name="file">The file to upload. (optional)</param>
    /// <returns>Task of ApiResponse (FileUploadMetaData)</returns>
    public static GLTask<ReviewStepResult> UploadFileForReviewStep(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef, System.IO.Stream file = null)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_UploadFileForReviewStep, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefFileUploadByUserRefAndCourseRefPostAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, new List<System.IO.Stream>(1) { file }), GetContextForReviewStepProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef));
    }

    /// <summary>
    /// Post the answers a user gave to a review step quiz/rubrics
    /// </summary>
    /// <remarks>
    /// Post the answers a user gave to a review step quiz/rubrics
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (QuizResult)</returns>
    public static GLTask<ReviewStepResult> SendReviewQuizResults(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef, QuizResult body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_SendReviewQuizResults, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefQuizByUserRefAndCourseRefPostAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, body), GetContextForReviewStepProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef));
    }

    /// <summary>
    /// Mark review step as complete
    /// </summary>
    /// <remarks>
    /// Marks the review step as completed, so that it closes and the other peers can view the result.
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewerRelationRef">The ref of the reviewer relation under which this result belongs</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <returns>Task of ApiResponse (ReviewStepResult)</returns>
    public static GLTask<ReviewStepResult> CloseReviewStep(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_CloseReviewStep, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefCompleteByUserRefAndCourseRefPostAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef), GetContextForReviewStepProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef));
    }

    /// <summary>
    /// Revokethereviewstep Revoke the review step, marking it as incomplete.
    /// </summary>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="courseRef">The ref of the course</param>
    /// <param name="chapterRef">The ref of the chapter</param>
    /// <param name="reviewerRelationRef">The ref of the reviewer relation under which this result belongs</param>
    /// <param name="reviewStepRef">The ref of the reviewStep</param>
    /// <returns>Task of ApiResponse (ReviewStepResult)</returns>
    public static GLTask<ReviewStepResult> OpenReviewStep(string userRef, string courseRef, string chapterRef, string reviewerRelationRef, string reviewStepRef)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_OpenReviewStep, () => GetInstance().rawApi.UserCourseProgressChapterChapterRefRelationReviewerRelationRefReviewStepReviewStepRefCompleteByUserRefAndCourseRefDeleteAsyncWithHttpInfo(loginToken, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef), GetContextForReviewStepProgress(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef));
    }
    
    /// <summary>
    /// Let&#39;s a user join a course using the course code
    /// </summary>
    /// <remarks>
    /// Let&#39;s a user join a course using the course code
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="xToken">The token received during login</param>
    /// <param name="userRef">The ref of the user for which the data is to be retrieved. A special option is &#39;logged-in&#39;, which causes the call to use the currently logged in user.</param>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse (CourseSummary)</returns>
    public static GLTask<CourseSummary> JoinCourse(string courseCode)
    {
        string userRef = GetActiveUser().Ref;
        return GetInstance().MakeApiTask(GetInstance().Call_JoinCourse, () => GetInstance().rawApi.UserCourseJoinByUserRefPostAsyncWithHttpInfo(loginToken, userRef, new UserCourseJoinRequest(courseCode)), GetContextForUser(userRef));
    }

    /// <summary>
    /// Call to make in case a user forgot his/her password.
    /// </summary>
    /// <remarks>
    /// Triggers an e-mail to be sent to the user with a password reset token. 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse</returns>
    public static GLTask<Object> RequestPasswordResetToken(UserForgotPasswordRequest body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_RequestPasswordResetToken, () => GetInstance().rawApi.UserForgotPasswordPostAsyncWithHttpInfo(body), GetEmptyContext());
    }

    /// <summary>
    /// Login any type of user
    /// </summary>
    /// <remarks>
    /// The role of the user may influence what operations the user is allowed te execute 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="body">The login request</param>
    /// <returns>Task of ApiResponse (UserLoginResponse)</returns>
    public static GLTask<UserLoginResponse> LoginUser(UserLoginRequest body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_LoginUser, () => GetInstance().rawApi.UserLoginPostAsyncWithHttpInfo(body), GetEmptyContext());
    }

    /// <summary>
    /// Registers a user.
    /// </summary>
    /// <remarks>
    /// Newly registered user always have the role of a \&quot;learner\&quot; 
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="body">User to register</param>
    /// <returns>Task of ApiResponse</returns>
    public static GLTask<Object> Register(UserRegistrationRequest body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_Register, () => GetInstance().rawApi.UserRegisterPostAsyncWithHttpInfo(body), GetEmptyContext());
    }

    /// <summary>
    /// Call to reset a user&#39;s password using a forgot password token
    /// </summary>
    /// <remarks>
    /// Call to reset a user&#39;s password using a forgot password token
    /// </remarks>
    /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="body"></param>
    /// <returns>Task of ApiResponse</returns>
    public static GLTask<Object> ResetPassword(UserResetPasswordRequest body)
    {
        return GetInstance().MakeApiTask(GetInstance().Call_ResetPassword, () => GetInstance().rawApi.UserResetPasswordPostAsyncWithHttpInfo(body), GetEmptyContext());
    }
    #endregion
}