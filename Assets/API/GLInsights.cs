﻿using IO.Swagger.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// a track is the relation between 2 users during a single chapter where one submits work and the other reviews
using ChapterTrack = IO.Swagger.Model.ReviewerRelation;

// to use maybe in future
public enum StepDeadlineStatus
{
    FUTURE,
    CLOSE,
    PASSED,
}

// to use maybe in future
public enum StepAvailabilityStatus
{
    UNAVAILABLE,
    AVAILABLE,
    HAS_DATA,
    READY_TO_CLOSE,
    CLOSED
}

public enum StepStatus
{
    UNAVAILABLE,
    AVAILABLE,
    DANGER,
    LATE,
    CLOSED
}

/// <summary>
/// Some precomputed knowledge on a course
/// </summary>
public class CourseInsight
{
    public Course course;
    public DateTime startTime;
    public DateTime endTime;
    public TimeSpan timeSpan;

    public List<TimelineInsight> timelineInsights;
    public List<SectionInsight> rootSectionInsights;
}

/// <summary>
/// Helper insight to manage and link deadlines
/// </summary>
public class TimelineInsight
{
    public DateTime startTime;
    public DateTime deadline;
    public ReviewStepInsight reviewStepInsight;
    public bool isPassed { get { return GLDate.Now > deadline; } }
    public bool isFuture { get { return !isPassed && GLDate.Now >= startTime; } }
    public bool isActive { get { return !isPassed && GLDate.Now < startTime; } }
}

/// <summary>
/// Some precomputed knowledge on a resource
/// </summary>
public class ResourceInsight
{
    public CourseResource resource;
    public bool isText;
    public List<SectionResourceRelationInsight> referencedBySectionInsights;
}

/// <summary>
/// helper class for the list of sections to remember the reverse knowledge on sections
/// </summary>
public class SectionResourceRelationInsight
{
    public ResourceRelation relation;
    public SectionInsight sectionInsight;
    public ResourceInsight resourceInsight;
}


/// <summary>
/// Some precomputed knowledge on a resource section
/// </summary>
public class SectionInsight
{
    public Section section;
    public string parentRef;
    public SectionInsight parentInsight;
    public List<SectionInsight> childrenInsights;
    
    public List<SectionResourceRelationInsight> referencingResources;
    public List<ChapterSectionRelationInsight> referencedByChapter;
}


/// <summary>
/// Some precomputed knowledge on a skill
/// </summary>
public class SkillInsight
{
    public Skill skill;
    public List<ResourceInsight> fromResourceInsights;
    public List<ReviewStepInsight> fromReviewStepInsights;
    public int maxLevel;
    public bool enabled;
}

/// <summary>
/// helper class for the list of sections to remember the reverse knowledge on sections
/// </summary>
public class ChapterSectionRelationInsight
{
    public SectionRelation relation;
    public SectionInsight sectionInsight;
    public ChapterInsight chapterInsight;
}

/// <summary>
/// Helper class to remember certain chapter knowledge, without recurring expensive searches
/// </summary>
public class ChapterInsight
{
    public Chapter chapter;
    public int chapterIndex;
    public string chapterNumber;
    
    public CourseInsight courseInsight;
    public ChapterInsight previousChapterInsight;
    public ChapterInsight nextChapterInsight;

    public Dictionary<string, ReviewStepInsight> stepInsightsMapping;

    public ReviewStepInsight firstSubmissionStepInsight;

    public List<ChapterSectionRelationInsight> referencedSectionInsights;

    public DateTime startTime;
    public DateTime endTime;
    public TimeSpan timeSpan;
}

/// <summary>
/// Helper class to remember certain review step knowledge, without recurring expensive searches
/// </summary>
public class ReviewStepInsight
{
    public ReviewStep step;
    public int stepIndex;
    public TimelineInsight timelineInsight;
    
    public ReviewStepInsight nextStepInsight;
    public ReviewStepInsight previousStepInsight;
    public ReviewStepInsight previousDiscussionStepInsight;

    public DateTime startTime;

    public ChapterInsight chapterInsight;

    public List<string> requiredArgumentTypeRefs;

    public bool requiresSubmission;
    public bool requiresDiscussion;
    public bool requiresQuiz;
    public bool requiresSubmissionOnly;
    
    public bool canReply;
    public bool canCreateThreads;
    public bool useArgumentTypes;

    public bool isDeadlinePassed;
    public bool isAvailable;
    public StepStatus generalStatus;

    public bool isReviewerStep;
    public bool isSubmitterStep;
}

/// <summary>
/// Helper class to remember quizes and their options
/// </summary>
public class QuizInsight
{
    public Quiz quiz;
    public ReviewStepInsight reviewStepInsight;

    public int maxScore;
    public int minScore;

    public Dictionary<string, QuizElementOption> optionMapper;
}

/// <summary>
/// Helper class to remember hierarchy of argument types
/// </summary>
public class ArgumentTypeInsight
{
    public ArgumentType argumentType;
    
    public ArgumentTypeInsight parentInsight;
    public List<ArgumentTypeInsight> childrenInsights;

    public int hierarchyLevel;
}

/// <summary>
/// Helper class to remember certain course knowledge, without recurring expensive searches
/// *** WARNING ***
/// can't contain direct reference to an object with course knowledge, because that my change and is maintained separately
/// </summary>
public class CourseProgressInsight
{
    public string userRef;
    public string courseRef;
    
    public List<ChapterProgressInsight> userChapterInsights;
    public List<StepProgressInsight> activeStepInsights;

    public List<StepProgressInsight> upcomingStepInsights;
    public DateTime upcomingStepsThreshold;

    public bool isLoggedInUserDone;
    public bool isLoggedInUser;
}

/// <summary>
/// Helper class to remember progress on a skill
/// *** WARNING ***
/// can't contain direct reference to an object with course knowledge, because that my change and is maintained separately
/// </summary>
public class SkillProgressInsight
{
    public string skillRef;
    public int level;
    public bool achieved;
    public HashSet<string> achievedUsingResources;
    public HashSet<string> achievedUsingReviewSteps;
}

/// <summary>
/// Helper class to remember certain chapter knowledge, without recurring expensive searches
/// *** WARNING ***
/// can't contain direct reference to an object with course knowledge, because that my change and is maintained separately
/// </summary>
public class ChapterProgressInsight
{
    public string userRef;
    public string chapterRef;
    public int chapterIndex;

    public CourseProgressInsight courseProgressInsight;
    public List<TrackProgressInsight> userTrackInsights;
    public List<TrackProgressInsight> userReviewerTrackInsights;
    public List<TrackProgressInsight> userSubmitterTrackInsights;
    public List<StepProgressInsight> activeStepInsights;

    public bool isAvailable;
    public bool isSubmissionStepAvailable;
    public bool isReviewStepAvailable;
    public bool isLoggedInUserDone;
    public bool isLoggedInUser;
}

/// <summary>
/// Helper class to remember certain track knowledge, without recurring expensive searches
/// *** WARNING ***
/// can't contain direct reference to an object with course knowledge, because that my change and is maintained separately
/// </summary>
public class TrackProgressInsight
{
    public ChapterProgressInsight reviewerChapterProgressInsight;
    public ChapterProgressInsight submitterChapterProgressInsight;
    public ChapterProgressInsight loggedInUserChapterProgressInsight;

    public ChapterTrack track;
    public int submitterTrackIndex;
    public int reviewerTrackIndex;

    public string chapterRef;
    public int chapterIndex;
    
    public Dictionary<string, StepProgressInsight> stepInsightsMapping;
    public List<StepProgressInsight> stepProgressInsights;
    public StepProgressInsight activeStepInsight;

    public bool hasClosedSubmission;
    public bool isSubmissionDeadlinePassed;
    public bool isStarted;
    public bool isCompleted;

    public PeerType userRole;
    public bool isLoggedInUserReviewer;
    public bool isLoggedInUserSubmitter;
    public bool isLoggedInUserDone;
    
    public List<ArgumentThreadInsight> discussionThreadInsights;
}

/// <summary>
/// Helper class to remember certain track knowledge, without recurring expensive searches
/// *** WARNING ***
/// can't contain direct reference to an object with course knowledge, because that my change and is maintained separately
/// </summary>
public class StepProgressInsight
{
    public TrackProgressInsight trackInsight;

    public string userRef;
    public int reviewStepIndex;
    public string reviewStepRef;

    public bool isDiscussionTodo;
    public bool isSubmissionTodo;
    public bool isQuizTodo;

    public ReviewStepResult result;
    
    public StepProgressInsight nextStepInsight;
    public StepProgressInsight previousStepInsight;

    public bool isAvailable;
    public bool isUpcoming;
    public bool isDeadlinePassed;
    public bool isReadyForClose;
    public bool isMarkedClosed;
    public bool isClosed;
    public bool isActiveStep;
    public bool isContentVisible;

    public bool hasSubmissionData;
    public bool hasDiscussionData;
    public bool hasCompletedDiscussion;
    public bool hasQuizData;
    public bool hasData;

    public List<string> requiredArgumentTypesTodo;

    public StepStatus status;

    public bool isLoggedInUserStep;
}

public class AllConversationsInsight
{
    public Dictionary<string, Vector3> userToColorMapping;

    public Dictionary<string, ConversationInsight> userToDirectConversationInsightMapping;
    public List<ConversationInsight> conversationInsights;
    public DateTime upToDateUntil;

    public bool loggedInUserHasDirectConversationsToAllOtherUsers;
    public List<string> missingDirectConversations;
    
    public List<ConversationInsight> unreadConversationInsights;
    public int unreadMessagesCount;
    public Dictionary<ForumTab,int> tabUnreadMessagesCount;
}

public class ConversationInsight
{
    public Conversation conversation;
    public List<string> otherMemberRefsThanLoggedInUser;
    public bool canLoggedInUserWriteMessages;
    public bool canLoggedInUserReadMessages;
    public bool canAllLearnersReadMessages;
    public bool canAllLearnersWriteMessages;
    public bool canAllTeachersWriteMessages;
    public bool canAllTeachersReadMessages;
    public bool canAllAdministratorsWriteMessages;
    public bool canAllAdministratorsReadMessages;

    public int rolesWithWritePermissionsCount;
    public int rolesWithReadPermissionsCount;

    public bool isEmptyConversation;
    
    public ForumTab forumTab;

    public string lastMessageLine;
    public string lastMessageUserRef;
    public bool lastMessageIsFromLoggedInUser;
    public int unreadMessagesCount;
    public DateTime lastMessageReceivedTime;
}

public class ConversationMessageInsight
{
    public ConversationInsight conversationInsight;
    public ConversationMessage message;
}

/// <summary>
/// Helper class to remember hierarchy of argument types
/// </summary>
public class ArgumentThreadInsight
{
    public string threadRef;
    public List<ArgumentInsight> argumentInsights;
    public Dictionary<string, ArgumentInsight> argumentPerStepInsights;
}

/// <summary>
/// Helper class to remember hierarchy of argument types
/// </summary>
public class ArgumentInsight
{
    public string stepRef;
    public Argument argument;
    public ArgumentTypeInsight argumentTypeInsight;
    public ArgumentThreadInsight threadInsight;
    public int argumentIndex;
}

/// <summary>
/// Helper class to remember resource results
/// </summary>
public class ResourceResultInsight
{
    public string resourceRef;
    public CourseResourceResult resourceResult;
    public bool isAchieved;
}

public static class GLInsights {

    public static readonly int CHAPTER_START_DAYS = 7;
    public static readonly int DEADLINE_DANGER_DAYS = 3;

    #region knowledge

    // courseRef
    private static Dictionary<string, CourseInsight> courseInsights = new Dictionary<string, CourseInsight>();
    // chapterRef
    private static Dictionary<string, ChapterInsight> chapterInsightss = new Dictionary<string, ChapterInsight>();
    // resourceRef
    private static Dictionary<string, ResourceInsight> resourceInsights = new Dictionary<string, ResourceInsight>();
    // sectionRef
    private static Dictionary<string, SectionInsight> sectionInsights = new Dictionary<string, SectionInsight>();
    // sectionRef
    private static Dictionary<string, SkillInsight> skillInsights = new Dictionary<string, SkillInsight>();
    // reviewStepRef
    private static Dictionary<string, ReviewStepInsight> reviewStepInsightss = new Dictionary<string, ReviewStepInsight>();
    // quizRef
    private static Dictionary<string, QuizInsight> quizInsights = new Dictionary<string, QuizInsight>();
    // resourceRef, userRef
    private static Dictionary<string, Dictionary<string, ResourceResultInsight>> resourceResultInsights = new Dictionary<string, Dictionary<string, ResourceResultInsight>>();
    // courseRef, userRef
    private static Dictionary<string, Dictionary<string, CourseProgressInsight>> courseProgressInsights = new Dictionary<string, Dictionary<string, CourseProgressInsight>>();
    // chapterRef, userRef
    private static Dictionary<string, Dictionary<string, ChapterProgressInsight>> chapterProgressInsights = new Dictionary<string, Dictionary<string, ChapterProgressInsight>>();
    // skillRef
    private static Dictionary<string, SkillProgressInsight> skillProgressInsights = new Dictionary<string, SkillProgressInsight>();
    // trackRef
    private static Dictionary<string, TrackProgressInsight> trackProgressInsightss = new Dictionary<string, TrackProgressInsight>();
    // argumentTypeRef
    private static Dictionary<string, ArgumentTypeInsight> argumentTypeInsights = new Dictionary<string, ArgumentTypeInsight>();
    // argumentRef
    private static Dictionary<string, ArgumentInsight> argumentInsights = new Dictionary<string, ArgumentInsight>();
    // argumentRef
    private static Dictionary<string, ArgumentThreadInsight> argumentThreadInsights = new Dictionary<string, ArgumentThreadInsight>();

    // the main conversation insight
    private static AllConversationsInsight allConversationsInsight;
    // conversationRef
    private static Dictionary<string, ConversationInsight> conversationInsights = new Dictionary<string, ConversationInsight>();

    public static void ClearInsights()
    {
        Debug.LogWarning("Clearing all insights");

        courseInsights.Clear();
        chapterInsightss.Clear();
        resourceInsights.Clear();
        sectionInsights.Clear();
        skillInsights.Clear();
        reviewStepInsightss.Clear();
        quizInsights.Clear();
        resourceResultInsights.Clear();
        courseProgressInsights.Clear();
        chapterProgressInsights.Clear();
        skillProgressInsights.Clear();
        trackProgressInsightss.Clear();
        argumentTypeInsights.Clear();
        argumentInsights.Clear();
        argumentThreadInsights.Clear();
        conversationInsights.Clear();
        allConversationsInsight = null;
    }

    private class JsonHelper
    {
        public Dictionary<string, CourseInsight> courseInsights;
        public Dictionary<string, ChapterInsight> chapterInsightss;
        public Dictionary<string, ResourceInsight> resourceInsights;
        public Dictionary<string, SectionInsight> sectionInsights;
        public Dictionary<string, SkillInsight> skillInsights;
        public Dictionary<string, ReviewStepInsight> reviewStepInsightss;
        public Dictionary<string, QuizInsight> quizInsights;
        public Dictionary<string, Dictionary<string, ResourceResultInsight>> resourceResultInsights;
        public Dictionary<string, Dictionary<string, CourseProgressInsight>> courseProgressInsights;
        public Dictionary<string, Dictionary<string, ChapterProgressInsight>> chapterProgressInsights;
        public Dictionary<string, SkillProgressInsight> skillProgressInsights;
        public Dictionary<string, TrackProgressInsight> trackProgressInsightss;
        public Dictionary<string, ArgumentTypeInsight> argumentTypeInsights;
        public Dictionary<string, ArgumentInsight> argumentInsights;
        public Dictionary<string, ArgumentThreadInsight> argumentThreadInsights;
        //public AllConversationsInsight allConversationsInsight;
        //public Dictionary<string, ConversationInsight> conversationInsights;
    }

    public static string ToJSONString()
    {
        var serializer = GLDebug.I.jsonSerializer;
        var jsonHelper = new JsonHelper()
        {
            courseInsights = courseInsights,
            chapterInsightss = chapterInsightss,
            resourceInsights = resourceInsights,
            sectionInsights = sectionInsights,
            skillInsights = skillInsights,
            reviewStepInsightss = reviewStepInsightss,
            quizInsights = quizInsights,
            resourceResultInsights = resourceResultInsights,
            courseProgressInsights = courseProgressInsights,
            chapterProgressInsights = chapterProgressInsights,
            skillProgressInsights = skillProgressInsights,
            trackProgressInsightss = trackProgressInsightss,
            argumentTypeInsights = argumentTypeInsights,
            argumentInsights = argumentInsights,
            argumentThreadInsights = argumentThreadInsights,
            //allConversationsInsight = allConversationsInsight,
            //conversationInsights = conversationInsights
        };
        return JObject.FromObject(jsonHelper, serializer).ToString();
    }


    //// chapter, submitter, reviewer
    //private static Dictionary<Tuple<string, string, string>, TrackProgressInsight> submitterReviewerTracks = new Dictionary<Tuple<string, string, string>, TrackProgressInsight>();
    //// chapter, submitter
    //private static Dictionary<string, Dictionary<string, List<TrackProgressInsight>>> submitterTracks = new Dictionary<string, Dictionary<string, List<TrackProgressInsight>>>();
    //// chapter, reviewer
    //private static Dictionary<string, Dictionary<string, List<TrackProgressInsight>>> reviewerTracks = new Dictionary<string, Dictionary<string, List<TrackProgressInsight>>>();

    /// <summary>
    /// Update all helper knowledge on the course
    /// </summary>
    /// <param name="courseProgress"></param>
    public static void UpdateCourseProgressInsights(CourseProgress courseProgress)
    {
        foreach (var chapterProgress in courseProgress.Chapters)
        {
            UpdateChapterProgressInsights(courseProgress.UserRef, chapterProgress, skipUpdatingInsights: true);
        }

        // update all the resource insights
        UpdateResourceProgressInsights(courseProgress);

        // we have gathered new information, so let's update all our insights
        UpdateProgressInsights();
    }

    /// <summary>
    /// Updates all inights on the results
    /// </summary>
    /// <param name="courseProgress"></param>
    public static void UpdateResourceProgressInsights(CourseProgress courseProgress)
    {
        foreach (var result in courseProgress.ResourceResults)
        {
            UpdateResourceProgressInsight(result, courseProgress.UserRef);
        }
    }

    /// <summary>
    /// Just update the knowledge on a single course resource result
    /// </summary>
    /// <param name="courseResourceResult"></param>
    /// <param name="userRef"></param>
    public static void UpdateResourceProgressInsight(CourseResourceResult courseResourceResult, string userRef)
    {
        var resourceRef = courseResourceResult.ResourceRef;
        var resourceInsight = resourceInsights[resourceRef];
        var newResult = new ResourceResultInsight
        {
            resourceRef = resourceRef,
            resourceResult = courseResourceResult,
            isAchieved = courseResourceResult.QuizResult != null && courseResourceResult.QuizResult.TotalScore >= resourceInsight.resource.QuizMinimalScore
        };
        resourceResultInsights.CreateSetOrAddToDictionary(resourceRef, userRef, newResult);
    }

    /// <summary>
    /// Updates the insights on all conversations.
    /// This may only be called when a course has already been loaded
    /// </summary>
    /// <param name="conversations"></param>
    public static void UpdateConversationsInsights(Conversations conversations)
    {
        if (!GLApi.IsLoggedInAndCourseSelected())
        {
            Debug.LogError("Can't update conversations insights when course is not loaded");
        }

        if (allConversationsInsight == null)
        {
            allConversationsInsight = new AllConversationsInsight
            {
                userToColorMapping = new Dictionary<string, Vector3>(),
                userToDirectConversationInsightMapping = new Dictionary<string, ConversationInsight>(),
                conversationInsights = new List<ConversationInsight>(),
                loggedInUserHasDirectConversationsToAllOtherUsers = true,
                missingDirectConversations = new List<string>(),
                upToDateUntil = GLApi.GetServerInfo().CurrentTime.Value,

                unreadConversationInsights = new List<ConversationInsight>(),
                unreadMessagesCount = 0,
                tabUnreadMessagesCount = new Dictionary<ForumTab, int>()
            };
        }

        // add all tabs to unreadcounter
        foreach (var tab in Enum.GetValues(typeof(ForumTab)))
        {
            allConversationsInsight.tabUnreadMessagesCount.SetOrAdd((ForumTab)tab, 0);
        }

        // create colors for each user
        var course = GLApi.GetActiveCourse();
        foreach (var courseMember in course.Users)
        {
            if (!allConversationsInsight.userToColorMapping.ContainsKey(courseMember.UserRef))
            {
                allConversationsInsight.userToColorMapping[courseMember.UserRef] = UnityEngine.Random.onUnitSphere;
            }
        }

        // update insights on each conversation
        foreach (var conversation in conversations._Conversations)
        {
            UpdateConversationInsight(conversation.Ref, skipFinalCollectiveUpdate: true);
        }

        // last time we received conversations content from the server
        allConversationsInsight.upToDateUntil = conversations.CurrentTime.Value;

        //
        //conversationsInsight.lastMessageReceivedTime = conversations.

        // update orderings
        UpdateAllDirectConversationsWithAllOtherUsers();
        UpdateUnreadConversationInsights();
        UpdateConversationOrderings();
    }

    /// <summary>
    /// Updates the insights of a specific conversation
    /// </summary>
    /// <param name="conversation"></param>
    public static void UpdateConversationInsight(string conversationRef, bool skipFinalCollectiveUpdate = false)
    {
        // initial values
        var user = GLApi.GetActiveUser();
        var userRole = user.Role.GetValueOrDefault(Role.LEARNER);
        var course = GLApi.GetActiveCourse();
        var conversation = GLApi.GetConversationKnowledge(conversationRef);

        // ensure the insight object exists
        if (!conversationInsights.ContainsKey(conversation.Ref))
        {
            conversationInsights[conversation.Ref] = new ConversationInsight
            {
                otherMemberRefsThanLoggedInUser = new List<string>(),
                lastMessageLine = "",
                unreadMessagesCount = 0
            };
            allConversationsInsight.conversationInsights.Add(conversationInsights[conversation.Ref]);
        }

        // get the insight object
        var insight = conversationInsights[conversation.Ref];
        insight.conversation = conversation;

        // update members
        var members = conversation.Members;
        insight.otherMemberRefsThanLoggedInUser.Clear();
        insight.otherMemberRefsThanLoggedInUser.AddRangeUnique(members);
        var isMember = insight.otherMemberRefsThanLoggedInUser.Contains(user.Ref);
        if (isMember) insight.otherMemberRefsThanLoggedInUser.Remove(user.Ref);

        // initialize role knowledge
        var roles = conversation.Roles;
        insight.canLoggedInUserWriteMessages = false;
        insight.canLoggedInUserReadMessages = false;
        insight.canAllLearnersReadMessages = false;
        insight.canAllLearnersWriteMessages = false;
        insight.canAllTeachersReadMessages = false;
        insight.canAllTeachersWriteMessages = false;
        insight.canAllAdministratorsReadMessages = false;
        insight.canAllAdministratorsWriteMessages = false;
        insight.rolesWithReadPermissionsCount = 0;
        insight.rolesWithWritePermissionsCount = 0;

        // obtain role knowledge from roles list
        foreach (var role in roles)
        {
            switch (role.Role)
            {
                case Role.LEARNER:
                    insight.canAllLearnersReadMessages = role.Permission != ConversationPermissionType.NONE;
                    insight.canAllLearnersWriteMessages = role.Permission == ConversationPermissionType.POST;
                    if (insight.canAllLearnersReadMessages) insight.rolesWithReadPermissionsCount++;
                    if (insight.canAllLearnersWriteMessages) insight.rolesWithWritePermissionsCount++;
                    break;
                case Role.TEACHER:
                    insight.canAllTeachersReadMessages = role.Permission != ConversationPermissionType.NONE;
                    insight.canAllTeachersWriteMessages = role.Permission == ConversationPermissionType.POST;
                    if (insight.canAllTeachersReadMessages) insight.rolesWithReadPermissionsCount++;
                    if (insight.canAllTeachersWriteMessages) insight.rolesWithWritePermissionsCount++;
                    break;
                case Role.ADMINISTRATOR:
                    insight.canAllAdministratorsReadMessages = role.Permission != ConversationPermissionType.NONE;
                    insight.canAllAdministratorsWriteMessages = role.Permission == ConversationPermissionType.POST;
                    if (insight.canAllAdministratorsReadMessages) insight.rolesWithReadPermissionsCount++;
                    if (insight.canAllAdministratorsWriteMessages) insight.rolesWithWritePermissionsCount++;
                    break;
                default:
                    break;
            }
        }

        // determine the permissions for current logged in user
        switch (userRole)
        {
            case Role.LEARNER:
                insight.canLoggedInUserReadMessages = isMember || insight.canAllLearnersReadMessages;
                insight.canLoggedInUserWriteMessages = isMember || insight.canAllLearnersWriteMessages;
                break;
            case Role.TEACHER:
                insight.canLoggedInUserReadMessages = isMember || insight.canAllTeachersReadMessages;
                insight.canLoggedInUserWriteMessages = isMember || insight.canAllTeachersWriteMessages;
                break;
            case Role.ADMINISTRATOR:
                insight.canLoggedInUserReadMessages = isMember || insight.canAllAdministratorsReadMessages;
                insight.canLoggedInUserWriteMessages = isMember || insight.canAllAdministratorsWriteMessages;
                break;
            default:
                break;
        }

        // gather all previously determined insights to determine role information further
        var otherUsersCount = insight.otherMemberRefsThanLoggedInUser.Count;

        // remember all direct conversations
        if (conversation.Type == ConversationType.DIRECT)
        {
            var otherUserRef = insight.otherMemberRefsThanLoggedInUser[0];
            allConversationsInsight.userToDirectConversationInsightMapping.SetOrAdd(otherUserRef, insight);
        }

        switch (insight.conversation.Type)
        {
            case ConversationType.ANNOUNCEMENTS:
            case ConversationType.EVERYONE:
            case ConversationType.LEARNERSONLY:
            case ConversationType.TEACHERSONLY:
            case ConversationType.ADMINSONLY:
            case ConversationType.TECHNICALSUPPORT:
            case ConversationType.EDUCATIVESUPPORT:
                insight.forumTab = ForumTab.Special;
                break;
            case ConversationType.TOPIC:
                insight.forumTab = ForumTab.Topics;
                break;
            case ConversationType.DIRECT:
                insight.forumTab = ForumTab.Direct;
                break;
            case ConversationType.GROUP:
                insight.forumTab = ForumTab.Group;
                break;
            default:
                break;
        }

        // determine last message line, and unread count
        insight.lastMessageLine = "";
        insight.lastMessageUserRef = null;
        insight.lastMessageReceivedTime = GLDate.Min;
        insight.isEmptyConversation = true;
        insight.unreadMessagesCount = 0;

        if (conversation.Messages.Count > 0)
        {
            insight.isEmptyConversation = false;

            var lastMessage = conversation.Messages.Last();
            var strippedMessage = lastMessage.Message.Trim();

            insight.unreadMessagesCount = (int)(lastMessage.Ind.Value - conversation.ReadUntil.Value);

            strippedMessage = strippedMessage.Substring(0, Mathf.Min(strippedMessage.Length, 100));
            strippedMessage = strippedMessage.RemoveNonSpaceWhitespace();
            insight.lastMessageLine = strippedMessage;
            insight.lastMessageUserRef = lastMessage.Sender;
            insight.lastMessageReceivedTime = lastMessage.Sent.Value;
            insight.lastMessageIsFromLoggedInUser = lastMessage.Sender == user.Ref;
        }

        if (!skipFinalCollectiveUpdate)
        {
            UpdateAllDirectConversationsWithAllOtherUsers();
            UpdateUnreadConversationInsights();
            UpdateConversationOrderings();
        }
    }

    /// <summary>
    /// Update the ordering of conversations based last message received
    /// </summary>
    private static void UpdateAllDirectConversationsWithAllOtherUsers()
    {
        var course = GLApi.GetActiveCourse();
        var activeUser = GLApi.GetActiveUser();
        var users = new HashSet<string>();

        if (activeUser.Role != Role.LEARNER)
        {
            // we only make conversations with learners if we ourselves are learners. 
            // Otherwise, we'll just have to do with the conversations that exist
            allConversationsInsight.loggedInUserHasDirectConversationsToAllOtherUsers = true;
            allConversationsInsight.missingDirectConversations.Clear();
            return;
        }

        // remember all users
        foreach (var user in course.Users)
        {
            if (user.Role == Role.LEARNER)
            {
                users.Add(user.UserRef);
            }
        }

        // remove all those that we already know
        foreach (var kvp in allConversationsInsight.userToDirectConversationInsightMapping)
        {
            users.RemoveIfContains(kvp.Value.otherMemberRefsThanLoggedInUser[0]);
        }

        // remove myself
        users.Remove(activeUser.Ref);

        // now gather all knowledge
        allConversationsInsight.loggedInUserHasDirectConversationsToAllOtherUsers = users.Count == 0;
        allConversationsInsight.missingDirectConversations.Clear();
        allConversationsInsight.missingDirectConversations.AddRange(users);
    }

    /// <summary>
    /// Update the ordering of conversations based last message received
    /// </summary>
    private static void UpdateConversationOrderings()
    {
        // sort unread messages
        allConversationsInsight.conversationInsights.Sort(
            (insight1, insight2) =>
            {
                var compare = insight2.lastMessageReceivedTime.CompareTo(insight1.lastMessageReceivedTime);
                if (compare == 0)
                {
                    var compareDeeper = insight2.conversation.Created.Value.CompareTo(insight1.conversation.Created.Value);
                    if (compareDeeper == 0)
                    {
                        return insight2.conversation.Ref.CompareTo(insight1.conversation.Ref);
                    }
                    else
                    {
                        return compareDeeper;
                    }
                }
                else
                {
                    return compare;
                }
            });

        // sort unread messages
        allConversationsInsight.unreadConversationInsights.Sort(
            (insight1, insight2) => {
                return insight2.lastMessageReceivedTime.CompareTo(insight1.lastMessageReceivedTime);
            }
        );
    }

    /// <summary>
    /// Update all the knowledge on the conversations that are unread
    /// </summary>
    public static void UpdateUnreadConversationInsights() {

        foreach (var tab in Enum.GetValues(typeof(ForumTab)))
        {
            allConversationsInsight.tabUnreadMessagesCount.SetOrAdd((ForumTab)tab, 0);
        }

        allConversationsInsight.unreadConversationInsights.Clear();
        allConversationsInsight.unreadMessagesCount = 0;
        foreach ( var conversationInsight in allConversationsInsight.conversationInsights)
        {
            if (conversationInsight.unreadMessagesCount > 0)
            {
                allConversationsInsight.unreadConversationInsights.Add(conversationInsight);
                allConversationsInsight.unreadMessagesCount += conversationInsight.unreadMessagesCount;
                allConversationsInsight.tabUnreadMessagesCount[conversationInsight.forumTab] += conversationInsight.unreadMessagesCount;
                allConversationsInsight.tabUnreadMessagesCount[ForumTab.All] += conversationInsight.unreadMessagesCount;
            }
        }
    }

    /// <summary>
    /// Update all helper knowledge on a chapter
    /// </summary>
    /// <param name="chapterProgress"></param>
    public static void UpdateChapterProgressInsights(string userRef, ChapterProgress chapterProgress, bool skipUpdatingInsights = false)
    {
        // get the chapter
        var chapterInsights = chapterInsightss[chapterProgress.ChapterRef];

        // base level knowledge of progress first
        foreach (var track in chapterProgress.ReviewerRelations)
        {
            // add the track to the ref mapping
            var trackProgressInsights = trackProgressInsightss.SetOrAdd(track.Ref, new TrackProgressInsight
            {
                track = track,
                chapterIndex = chapterInsights.chapterIndex,
                chapterRef = chapterInsights.chapter.Ref,
                stepInsightsMapping = new Dictionary<string, StepProgressInsight>(),
                stepProgressInsights = new List<StepProgressInsight>(),
                discussionThreadInsights = new List<ArgumentThreadInsight>()
            });

            // fill the dictionary that maps a reviewerStep to a step within this track
            var steps = chapterInsights.chapter.ReviewSteps;
            StepProgressInsight previousStepProgressInsight = null;
            for (int stepIndex = 0; stepIndex < steps.Count; stepIndex++)
            {
                var step = steps[stepIndex];
                var stepProgressInsight = new StepProgressInsight
                {
                    reviewStepRef = step.Ref,
                    reviewStepIndex = stepIndex,
                    trackInsight = trackProgressInsights,
                    userRef = step.PeerType == PeerType.REVIEWER ? track.Reviewer : track.Submitter,
                    previousStepInsight = previousStepProgressInsight,
                    nextStepInsight = null,

                    isDiscussionTodo = false,
                    isQuizTodo = false,
                    isSubmissionTodo = false,

                    requiredArgumentTypesTodo = new List<string>()
                };

                if (previousStepProgressInsight != null)
                {
                    previousStepProgressInsight.nextStepInsight = stepProgressInsight;
                }

                previousStepProgressInsight = stepProgressInsight;

                // find a matching result
                var index = track.ReviewStepResults.FindIndex(stepResult => stepResult.ReviewStepRef == step.Ref);
                if (index >= 0)
                {
                    var result = track.ReviewStepResults[index];
                    stepProgressInsight.result = track.ReviewStepResults[index];
                }

                // save the step insight
                trackProgressInsights.stepProgressInsights.Add(stepProgressInsight);
                trackProgressInsights.stepInsightsMapping.Add(step.Ref, stepProgressInsight);
            }
        }

        if (!skipUpdatingInsights)
        {
            // we have gathered new information, so let's update all our insights
            UpdateProgressInsights();
        }
    }

    /// <summary>
    /// precalculate a whole bunch of useful "metrics" on tracks and their steps
    /// </summary>
    public static void UpdateProgressInsights()
    {
        var user = GLApi.GetActiveUser();
        var userRef = user.Ref;
        var course = GLApi.GetActiveCourse();
        var courseRef = course.Ref;

        // clearing the chapter progresses to make room for the new 
        chapterProgressInsights.Clear();

        // create the bare skill progress insights
        foreach (var kvp in skillInsights)
        {
            var skillInsight = kvp.Value;
            var skillProgressInsight = skillProgressInsights.SetOrAdd(kvp.Key, new SkillProgressInsight
            {
                skillRef = kvp.Key,
                achieved = false,
                level = 0,
                achievedUsingResources = new HashSet<string>(),
                achievedUsingReviewSteps = new HashSet<string>()
            });
        }

        // update skill achieved status for each resource result
        foreach (var perResource in resourceResultInsights)
        {
            var resourceRef = perResource.Key;
            var resourceInsight = GetResourceInsight(resourceRef);
            foreach (var perUser in perResource.Value)
            {
                // process the achieved skills a resource has been completed
                if (perUser.Value.isAchieved)
                {
                    foreach (var skillRef in resourceInsight.resource.Skills)
                    {
                        var skillProgressInsight = GetSkillProgressInsight(skillRef);
                        if (!skillProgressInsight.achievedUsingResources.Contains(resourceRef))
                        {
                            skillProgressInsight.achieved = true;
                            skillProgressInsight.level++;
                            skillProgressInsight.achievedUsingResources.Add(resourceRef);
                        }
                    }
                }
            }
        }

        // reset lists that are going to be built up
        foreach (var perChapter in chapterProgressInsights)
        {
            foreach (var perUser in perChapter.Value)
            {
                var chapterProgressInsight = perUser.Value;
                chapterProgressInsight.activeStepInsights.Clear();
                chapterProgressInsight.userReviewerTrackInsights.Clear();
                chapterProgressInsight.userSubmitterTrackInsights.Clear();
                chapterProgressInsight.userTrackInsights.Clear();
            }
        }
        foreach (var perCourse in courseProgressInsights)
        {
            foreach (var perUser in perCourse.Value)
            {
                var courseProgressInsight = perUser.Value;
                courseProgressInsight.activeStepInsights.Clear();
                courseProgressInsight.upcomingStepInsights.Clear();
                courseProgressInsight.upcomingStepsThreshold = GLDate.Now.AddDays(12);
            }
        }

        // creating the chapter progress insights
        foreach (var kvp in trackProgressInsightss)
        {
            var trackInsight = kvp.Value;
            var chapterIndex = trackInsight.chapterIndex;
            var chapterRef = trackInsight.chapterRef;
            var submitterRef = trackInsight.track.Submitter;
            var reviewerRef = trackInsight.track.Reviewer;

            // add submitter track to tracks dictionary
            if (submitterRef != null)
            {
                CreateUserCourseChapterInsights(userRef, courseRef, chapterRef, chapterIndex, submitterRef);

                trackInsight.submitterTrackIndex = chapterProgressInsights[chapterRef][submitterRef].userSubmitterTrackInsights.Count;

                chapterProgressInsights[chapterRef][submitterRef].userTrackInsights.Add(trackInsight);
                chapterProgressInsights[chapterRef][submitterRef].userSubmitterTrackInsights.Add(trackInsight);
                trackInsight.submitterChapterProgressInsight = chapterProgressInsights[chapterRef][submitterRef];

                if (submitterRef == userRef)
                {
                    trackInsight.loggedInUserChapterProgressInsight = trackInsight.submitterChapterProgressInsight;
                }
            }

            // add submitter track to tracks dictionary
            if (reviewerRef != null)
            {
                CreateUserCourseChapterInsights(userRef, courseRef, chapterRef, chapterIndex, reviewerRef);

                trackInsight.reviewerTrackIndex = chapterProgressInsights[chapterRef][reviewerRef].userReviewerTrackInsights.Count;

                chapterProgressInsights[chapterRef][reviewerRef].userTrackInsights.Add(trackInsight);
                chapterProgressInsights[chapterRef][reviewerRef].userReviewerTrackInsights.Add(trackInsight);
                trackInsight.reviewerChapterProgressInsight = chapterProgressInsights[chapterRef][reviewerRef];

                if (reviewerRef == userRef)
                {
                    trackInsight.loggedInUserChapterProgressInsight = trackInsight.reviewerChapterProgressInsight;
                }
            }
        }

        // now create the insights for arguments in the reviewsteps
        foreach (var kvp in trackProgressInsightss)
        {
            var trackInsight = kvp.Value;
            // reset the list of threads because we are making a new one
            trackInsight.discussionThreadInsights.Clear();

            for (int stepIndex = 0; stepIndex < trackInsight.stepProgressInsights.Count; stepIndex++)
            {
                var stepInsight = trackInsight.stepProgressInsights[stepIndex];
                if (HasDiscussionData(stepInsight.result))
                {
                    foreach (var argument in stepInsight.result.Arguments)
                    {
                        // get or make the thread
                        ArgumentThreadInsight thread;
                        if (argument.ReplyToArgumentRef.IsNullOrEmpty())
                        {
                            thread = new ArgumentThreadInsight
                            {
                                threadRef = argument.Ref,
                                argumentInsights = new List<ArgumentInsight>(),
                                argumentPerStepInsights = new Dictionary<string, ArgumentInsight>()
                            };
                            // add the created thread to the tracks' list of threads
                            trackInsight.discussionThreadInsights.Add(thread);
                        }
                        else
                        {
                            thread = argumentThreadInsights.GetOrDefault(argument.ReplyToArgumentRef);
                        }

                        // set the thread
                        argumentThreadInsights.SetOrAdd(argument.Ref, thread);

                        // create the new argument insight
                        var argumentTypeInsight = argumentTypeInsights.GetOrDefault(argument.ArgumentTypeRef);
                        var newArgument = new ArgumentInsight
                        {
                            stepRef = stepInsight.reviewStepRef,
                            argument = argument,
                            argumentTypeInsight = argumentTypeInsight,
                            argumentIndex = thread.argumentInsights.Count,
                            threadInsight = thread
                        };

                        // add the new argument to the thread
                        thread.argumentInsights.Add(newArgument);
                        thread.argumentPerStepInsights.SetOrAdd(stepInsight.reviewStepRef, newArgument);
                        // set the new insight
                        argumentInsights.SetOrAdd(argument.Ref, newArgument);
                    }
                }
            }
        }

        // determine the general availability of steps
        foreach (var kvp in reviewStepInsightss)
        {
            var stepInsights = kvp.Value;
            stepInsights.isDeadlinePassed = stepInsights.step.Deadline < GLDate.Now;
            stepInsights.isAvailable = IsStepAvailable(null, stepInsights.step.Ref);
            stepInsights.generalStatus = stepInsights.isAvailable ? StepStatus.AVAILABLE : StepStatus.UNAVAILABLE;

            if (stepInsights.isAvailable)
            {
                var daysLeft = (stepInsights.step.Deadline.Value - GLDate.Now).TotalDays;
                if (daysLeft < 0)
                {
                    stepInsights.generalStatus = StepStatus.LATE;
                }
                else if (daysLeft < DEADLINE_DANGER_DAYS)
                {
                    stepInsights.generalStatus = StepStatus.DANGER;
                }
            }
        }

        // determine some internal knowledge about steps
        foreach (var kvp in trackProgressInsightss)
        {
            var trackProgressInsights = kvp.Value;

            var isReviewer = user.Ref == trackProgressInsights.track.Reviewer;
            var isSubmitter = user.Ref == trackProgressInsights.track.Submitter;
            trackProgressInsights.userRole = isReviewer ? PeerType.REVIEWER : PeerType.SUBMITTER;
            trackProgressInsights.isLoggedInUserReviewer = isReviewer;
            trackProgressInsights.isLoggedInUserSubmitter = isSubmitter;

            foreach (var stepProgressInsight in trackProgressInsights.stepProgressInsights)
            {
                var stepRef = stepProgressInsight.reviewStepRef;
                var stepInsight = reviewStepInsightss[stepRef];
                var reviewStep = stepInsight.step;

                stepProgressInsight.hasQuizData = HasQuizResult(stepProgressInsight);
                stepProgressInsight.hasSubmissionData = HasSubmissionData(stepProgressInsight);
                stepProgressInsight.hasDiscussionData = HasDiscussionData(stepProgressInsight);
                
                if (stepProgressInsight.hasDiscussionData)
                {
                    var argumentTypesTodo = new HashSet<string>(stepInsight.requiredArgumentTypeRefs);
                    stepProgressInsight.result.Arguments.ForEach(argument => argumentTypesTodo.RemoveIfContains(argument.ArgumentTypeRef));
                    stepProgressInsight.requiredArgumentTypesTodo = argumentTypesTodo.ToList();
                }
                else
                {
                    stepProgressInsight.requiredArgumentTypesTodo = stepInsight.requiredArgumentTypeRefs;
                }

                stepProgressInsight.hasCompletedDiscussion = HasCompletedDiscussion(stepProgressInsight);
                stepProgressInsight.hasData = stepProgressInsight.hasDiscussionData || stepProgressInsight.hasSubmissionData || stepProgressInsight.hasQuizData;

                stepProgressInsight.isDiscussionTodo = stepInsight.requiresDiscussion && !stepProgressInsight.hasCompletedDiscussion;
                stepProgressInsight.isSubmissionTodo = stepInsight.requiresSubmission && !stepProgressInsight.hasSubmissionData;
                stepProgressInsight.isQuizTodo = stepInsight.requiresQuiz && !stepProgressInsight.hasQuizData;

                stepProgressInsight.isDeadlinePassed = stepInsight.isDeadlinePassed;
                stepProgressInsight.isReadyForClose = !stepProgressInsight.isDiscussionTodo && !stepProgressInsight.isSubmissionTodo && !stepProgressInsight.isQuizTodo;
                stepProgressInsight.isMarkedClosed = stepProgressInsight.result != null && stepProgressInsight.result.MarkedComplete.HasValue;
                stepProgressInsight.isClosed = stepProgressInsight.isMarkedClosed && stepProgressInsight.isDeadlinePassed;

                // process the achieved skills when the progress is closed
                if (stepProgressInsight.isMarkedClosed)
                {
                    foreach (var skillRef in stepInsight.step.Skills)
                    {
                        var skillProgressInsight = GetSkillProgressInsight(skillRef);
                        if (!skillProgressInsight.achievedUsingReviewSteps.Contains(stepRef))
                        {
                            skillProgressInsight.achieved = true;
                            skillProgressInsight.level++;
                            skillProgressInsight.achievedUsingReviewSteps.Add(stepRef);
                        }
                    }
                }
            }
        }

        // determine track insights about submissions and total completion
        foreach (var kvp in trackProgressInsightss)
        {
            var trackInsights = kvp.Value;
            trackInsights.isCompleted = trackInsights.stepProgressInsights.TrueForAll(step => step.isClosed);
            trackInsights.isSubmissionDeadlinePassed = false;
            trackInsights.hasClosedSubmission = false;

            var chapterInsight = chapterInsightss.GetOrDefault(trackInsights.chapterRef);
            var submissionStepInsight = chapterInsight.firstSubmissionStepInsight;

            if (submissionStepInsight != null)
            {
                var submissionStepProgressInsight = trackInsights.stepInsightsMapping[submissionStepInsight.step.Ref];
                trackInsights.hasClosedSubmission = submissionStepProgressInsight.isClosed;
                trackInsights.isSubmissionDeadlinePassed = submissionStepProgressInsight.isDeadlinePassed;
            }
        }

        // determine step insights about step availabilities and status
        foreach (var kvp in trackProgressInsightss)
        {
            var trackProgressInsights = kvp.Value;
            var chapterProgressInsights = trackProgressInsights.loggedInUserChapterProgressInsight;
            var courseProgressInsights = chapterProgressInsights.courseProgressInsight;
            trackProgressInsights.activeStepInsight = null;

            foreach (var stepProgressInsight in trackProgressInsights.stepProgressInsights)
            {
                var stepInsights = reviewStepInsightss[stepProgressInsight.reviewStepRef];
                var reviewStep = stepInsights.step;
                var isReviewerStep = stepInsights.step.PeerType == PeerType.REVIEWER;

                stepProgressInsight.isAvailable = IsStepAvailable(trackProgressInsights.track.Ref, stepProgressInsight.reviewStepRef);
                stepProgressInsight.isActiveStep = stepProgressInsight.isAvailable && !stepProgressInsight.isClosed;
                stepProgressInsight.isLoggedInUserStep = (isReviewerStep && trackProgressInsights.isLoggedInUserReviewer) || (!isReviewerStep && trackProgressInsights.isLoggedInUserSubmitter);
                stepProgressInsight.isContentVisible = stepProgressInsight.isClosed || (stepProgressInsight.isActiveStep && stepProgressInsight.isLoggedInUserStep);
                stepProgressInsight.isUpcoming = stepProgressInsight.isActiveStep || (!stepProgressInsight.isClosed && courseProgressInsights != null && reviewStep.Deadline.Value < courseProgressInsights.upcomingStepsThreshold);

                if (stepProgressInsight.isAvailable && stepProgressInsight.userRef != null)
                {
                    var chapterProgresInsight = GetChapterProgressInsight(trackProgressInsights.chapterRef, stepProgressInsight.userRef);
                    chapterProgresInsight.isAvailable = true;
                    if (stepInsights.isReviewerStep) chapterProgresInsight.isReviewStepAvailable = true;
                    else chapterProgresInsight.isSubmissionStepAvailable = true;
                }

                if (stepProgressInsight.isActiveStep)
                {
                    trackProgressInsights.activeStepInsight = stepProgressInsight;

                    if (stepProgressInsight.isLoggedInUserStep && trackProgressInsights.loggedInUserChapterProgressInsight != null)
                    {
                        trackProgressInsights.loggedInUserChapterProgressInsight.activeStepInsights.Add(stepProgressInsight);
                        courseProgressInsights.activeStepInsights.Add(stepProgressInsight);
                    }
                }

                if (stepProgressInsight.isUpcoming)
                {
                    if (stepProgressInsight.isLoggedInUserStep)
                    {
                        courseProgressInsights.upcomingStepInsights.AddUnique(stepProgressInsight);
                    }
                }
                
                stepProgressInsight.status = stepProgressInsight.isAvailable ? StepStatus.AVAILABLE : StepStatus.UNAVAILABLE;

                if (stepProgressInsight.isAvailable)
                {

                    var daysLeft = (stepInsights.step.Deadline.Value - GLDate.Now).TotalDays;
                    if (daysLeft < 0)
                    {
                        stepProgressInsight.status = StepStatus.LATE;
                    }
                    else if (daysLeft < DEADLINE_DANGER_DAYS)
                    {
                        stepProgressInsight.status = StepStatus.DANGER;
                    }
                }

                if (stepProgressInsight.isClosed)
                {
                    stepProgressInsight.status = StepStatus.CLOSED;
                }

                if (stepProgressInsight.status != StepStatus.UNAVAILABLE)
                {
                    trackProgressInsights.isStarted = true;
                }
            }
        }

        // checking if active user is done for each track
        foreach (var kvp in trackProgressInsightss)
        {
            var trackInsights = kvp.Value;
            trackInsights.isLoggedInUserDone = trackInsights.stepProgressInsights.TrueForAll(step => (step.isLoggedInUserStep ? step.isClosed : true));
        }

        // checking if a chapter has been completed by the user
        // but since this is ONLY known for the active, as only complete information about that user is known
        // we only check completion for chapters for the active user
        foreach (var perChapter in chapterProgressInsights)
        {
            foreach (var perUser in perChapter.Value)
            {
                var userChapterProgressInsight = perUser.Value;
                if (userChapterProgressInsight.isLoggedInUser)
                {
                    userChapterProgressInsight.isLoggedInUserDone = userChapterProgressInsight.userTrackInsights.TrueForAll(i => i.isLoggedInUserDone);
                }
            }
        }

        // checking if a course has been completed by the user
        // but since this is ONLY known for the active, as only complete information about that user is known
        // we only check completion for chapters for the active user
        foreach (var perCourse in courseProgressInsights)
        {
            foreach (var perUser in perCourse.Value)
            {
                var userCourseProgressInsight = perUser.Value;
                if (userCourseProgressInsight.isLoggedInUser && userCourseProgressInsight.userChapterInsights.Count == course.Chapters.Count)
                {
                    userCourseProgressInsight.isLoggedInUserDone = userCourseProgressInsight.userChapterInsights.TrueForAll(i => i.isLoggedInUserDone);
                }
            }
        }
    }

    private static void CreateUserCourseChapterInsights(string activeUserRef, string courseRef, string chapterRef, int chapterIndex, string trackUserRef)
    {
        if (!chapterProgressInsights.Contains(chapterRef, trackUserRef))
        {
            var chapterInsight = chapterProgressInsights.CreateSetOrAddToDictionary(chapterRef, trackUserRef,
                new ChapterProgressInsight()
                {
                    userRef = trackUserRef,
                    chapterRef = chapterRef,
                    chapterIndex = chapterIndex,
                    isLoggedInUser = trackUserRef == activeUserRef,
                    userReviewerTrackInsights = new List<TrackProgressInsight>(),
                    userSubmitterTrackInsights = new List<TrackProgressInsight>(),
                    userTrackInsights = new List<TrackProgressInsight>(),
                    activeStepInsights = new List<StepProgressInsight>()
                });

            if (!courseProgressInsights.Contains(courseRef, trackUserRef))
            {
                courseProgressInsights.CreateSetOrAddToDictionary(courseRef, trackUserRef,
                    new CourseProgressInsight()
                    {
                        courseRef = courseRef,
                        userRef = trackUserRef,
                        isLoggedInUser = trackUserRef == activeUserRef,
                        userChapterInsights = new List<ChapterProgressInsight>(),
                        activeStepInsights = new List<StepProgressInsight>(),
                        upcomingStepInsights = new List<StepProgressInsight>(),
                        upcomingStepsThreshold = GLDate.Now
                    });
            }
            courseProgressInsights[courseRef][trackUserRef].userChapterInsights.Add(chapterInsight);
            chapterInsight.courseProgressInsight = courseProgressInsights[courseRef][trackUserRef];
        }
    }

    /// <summary>
    /// Update all helper knowledge on a course
    /// </summary>
    /// <param name="course"></param>
    public static void UpdateCourseInsights(Course course)
    {
        var courseInsight = courseInsights.SetOrAdd(course.Ref, new CourseInsight()
        {
            course = course,
            startTime = DateTime.MaxValue,
            endTime = DateTime.MinValue,
            rootSectionInsights = new List<SectionInsight>(),
            timelineInsights = new List<TimelineInsight>()
        });

        // remember all skills
        foreach (var skill in course.Skills)
        {
            var skillInsight = skillInsights.SetOrAdd(skill.Ref, new SkillInsight
            {
                skill = skill,
                fromResourceInsights = new List<ResourceInsight>(),
                fromReviewStepInsights = new List<ReviewStepInsight>(),
                maxLevel = 0,
                enabled = false
            });
        }

        // remember all resources and process the linked skills
        foreach (var resource in course.Resources)
        {
            var resourceInsight = resourceInsights.SetOrAdd(resource.Ref, new ResourceInsight
            {
                resource = resource,
                isText = resource.Quiz == null && resource.Url.IsNullOrEmpty() && resource.Skills.IsNullOrEmpty(),
                referencedBySectionInsights = new List<SectionResourceRelationInsight>()
            });

            foreach (var skillRef in resource.Skills)
            {
                var skillInsight = GetSkillInsight(skillRef);
                skillInsight.fromResourceInsights.Add(resourceInsight);
                skillInsight.maxLevel++;
                skillInsight.enabled = true;
            }
        }

        // remember library sections and process relations with resources
        foreach (var section in course.Sections)
        {
            var sectionInsight = sectionInsights.SetOrAdd(section.Ref, new SectionInsight
            {
                section = section,
                parentInsight = null,
                childrenInsights = new List<SectionInsight>(),
                referencingResources = new List<SectionResourceRelationInsight>(),
                referencedByChapter = new List<ChapterSectionRelationInsight>()
            });

            foreach (var resourceRelation in section.ResourceRelations)
            {
                var resourceInsight = GetResourceInsight(resourceRelation.ResourceRef);
                var relationInsight = new SectionResourceRelationInsight
                {
                    resourceInsight = resourceInsight,
                    sectionInsight = sectionInsight,
                    relation = resourceRelation
                };
                resourceInsight.referencedBySectionInsights.Add(relationInsight);
                sectionInsight.referencingResources.Add(relationInsight);
            }
        }

        // process parent/children relations for library sections
        foreach (var section in course.Sections)
        {
            var sectionInsight = sectionInsights[section.Ref];
            if (section.ParentRef != null)
            {
                var parentSection = GetSectionInsight(section.ParentRef);
                parentSection.childrenInsights.Add(sectionInsight);
                sectionInsight.parentInsight = parentSection;
            }
        }

        // remember root sections
        foreach (var section in course.Sections)
        {
            var insight = section.GetInsight();
            if (insight.parentInsight == null)
            {
                courseInsight.rootSectionInsights.Add(insight);
            }
        }

        // sort all sections
        SortSections(courseInsight.rootSectionInsights);

        // process all chapters
        ChapterInsight previousChapter = null;
        for (var chapterIndex = 0; chapterIndex < course.Chapters.Count; chapterIndex++)
        {
            var chapter = course.Chapters[chapterIndex];
            var chapterInsight = chapterInsightss.SetOrAdd(chapter.Ref, new ChapterInsight
            {
                courseInsight = courseInsight,
                chapter = chapter,

                previousChapterInsight = previousChapter,
                nextChapterInsight = null,
                chapterIndex = chapterIndex,
                chapterNumber = (chapterIndex + 1) + "",

                stepInsightsMapping = new Dictionary<string, ReviewStepInsight>(),
                referencedSectionInsights = new List<ChapterSectionRelationInsight>(),

                startTime = DateTime.MaxValue,
                endTime = DateTime.MinValue,

                firstSubmissionStepInsight = null
            });

            // previous step processing
            if (previousChapter != null) previousChapter.nextChapterInsight = chapterInsight;
            previousChapter = chapterInsight;

            // processing resources
            foreach (var relatedSection in chapter.SectionRelations)
            {
                var sectionInsight = sectionInsights[relatedSection.SectionRef];
                var relationInsight = new ChapterSectionRelationInsight
                {
                    chapterInsight = chapterInsight,
                    sectionInsight = sectionInsight,
                    relation = relatedSection
                };
                sectionInsight.referencedByChapter.Add(relationInsight);
                chapterInsight.referencedSectionInsights.Add(relationInsight);
            }
            
            // processing the review steps
            ReviewStepInsight previousInsight = null;
            ReviewStepInsight previousDiscussionInsight = null;
            for (var stepIndex = 0; stepIndex < chapter.ReviewSteps.Count; stepIndex++)
            {
                var step = chapter.ReviewSteps[stepIndex];

                // modify start time of the first step to create a spacing before first deadline has passed
                var startTime = stepIndex == 0
                    ? step.Deadline.Value - TimeSpan.FromDays(CHAPTER_START_DAYS)
                    : chapter.ReviewSteps[stepIndex - 1].Deadline.Value;

                if (startTime < chapterInsight.startTime) chapterInsight.startTime = startTime;
                if (step.Deadline > chapterInsight.endTime) chapterInsight.endTime = step.Deadline.Value;

                // create the new review step insight
                var newInsight = new ReviewStepInsight
                {
                    step = step,
                    previousStepInsight = previousInsight,
                    previousDiscussionStepInsight = previousDiscussionInsight,
                    nextStepInsight = null,

                    startTime = startTime,

                    chapterInsight = chapterInsight,
                    stepIndex = stepIndex,

                    requiredArgumentTypeRefs = new List<string>(),

                    isReviewerStep = step.PeerType == PeerType.REVIEWER,
                    isSubmitterStep = step.PeerType == PeerType.SUBMITTER,

                    requiresSubmission = RequiresSubmission(step),
                    requiresDiscussion = RequiresDiscussion(step),
                    requiresQuiz = RequiresQuiz(step),

                    canReply = step.DiscussionType != DiscussionType.NONE,
                    canCreateThreads = step.DiscussionType == DiscussionType.RESTRICTED_ARGUMENTS || step.DiscussionType == DiscussionType.FREE_FORM_ARGUMENTS,
                    useArgumentTypes = step.DiscussionType == DiscussionType.RESTRICTED_ARGUMENTS
                };

                // handle the deadline insights
                var timelineInsight = new TimelineInsight
                {
                    deadline = step.Deadline.Value,
                    startTime = startTime,
                    reviewStepInsight = newInsight
                };
                courseInsight.timelineInsights.Add(timelineInsight);
                newInsight.timelineInsight = timelineInsight;

                // determine the required argument type refs
                if (step.DiscussionType == DiscussionType.RESTRICTED_ARGUMENTS)
                {
                    foreach (var argumentType in step.AllowedArguments)
                    {
                        if (argumentType.Required.GetValueOrDefault(false))
                        {
                            newInsight.requiredArgumentTypeRefs.Add(argumentType.Ref);
                        }
                    }
                }

                // determine if step is a submission only
                newInsight.requiresSubmissionOnly = newInsight.requiresSubmission && !newInsight.requiresDiscussion && !newInsight.requiresQuiz;

                // handle previous' next step reference
                if (previousInsight != null)
                {
                    previousInsight.nextStepInsight = newInsight;
                }

                // remember the first submission step of this chapter
                if (chapterInsight.firstSubmissionStepInsight == null && newInsight.requiresSubmission)
                {
                    chapterInsight.firstSubmissionStepInsight = newInsight;
                }

                // remember the step of this chapter in the mapping
                chapterInsight.stepInsightsMapping.Add(step.Ref, newInsight);

                // remember the last known discussion step reference
                if (newInsight.requiresDiscussion)
                {
                    previousDiscussionInsight = newInsight;
                }

                // process the step's skills
                foreach (var skillRef in step.Skills)
                {
                    var skillInsight = GetSkillInsight(skillRef);
                    skillInsight.fromReviewStepInsights.Add(newInsight);
                    skillInsight.maxLevel++;
                    skillInsight.enabled = true;
                }

                // handle quiz insight
                if (newInsight.requiresQuiz)
                {
                    var quiz = step.Quiz;
                    var quizInsight = new QuizInsight
                    {
                        quiz = quiz,
                        reviewStepInsight = newInsight,
                        optionMapper = new Dictionary<string, QuizElementOption>()
                    };

                    // map each quiz option using its unique ref
                    foreach (var quizElement in quiz.Elements)
                    {
                        foreach (var option in quizElement.Options)
                        {
                            quizInsight.optionMapper.SetOrAdd(option.Ref, option);
                        }
                    }

                    quizInsights.SetOrAdd(quiz.Ref, quizInsight);
                }

                // remember the new step insight as the previous, as we move to the next step
                previousInsight = reviewStepInsightss.SetOrAdd(step.Ref, newInsight);
            }

            // determine chapter time span
            chapterInsight.timeSpan = chapterInsight.endTime - chapterInsight.startTime;

            // process end and start time to determine course time span
            if (chapterInsight.startTime < courseInsight.startTime) courseInsight.startTime = chapterInsight.startTime;
            if (chapterInsight.endTime > courseInsight.endTime) courseInsight.endTime = chapterInsight.endTime;
        }

        // let's sort the timeline insights in the course
        courseInsight.timelineInsights.Sort((t1, t2) => t1.deadline.CompareTo(t2.deadline));
        
        // determine course time span
        courseInsight.timeSpan = courseInsight.endTime - courseInsight.startTime;

        // now create all knowledge on argument types
        foreach (var chapter in course.Chapters)
        {
            foreach (var reviewStep in chapter.ReviewSteps)
            {
                foreach (var argumentType in reviewStep.AllowedArguments)
                {
                    argumentTypeInsights.SetOrAdd(argumentType.Ref, new ArgumentTypeInsight
                    {
                        argumentType = argumentType,
                        childrenInsights = new List<ArgumentTypeInsight>(),
                        hierarchyLevel = 0,
                        parentInsight = null
                    });
                }
            }
        }

        // then update all hierarchy knowledge or argument types
        foreach (var kvp in argumentTypeInsights)
        {
            var currentTypeInsight = kvp.Value;
            var currentType = currentTypeInsight.argumentType;

            if (currentType.ParentRef != null)
            {
                var parentInsight = argumentTypeInsights.GetOrDefault(currentType.ParentRef);

                currentTypeInsight.parentInsight = parentInsight;
                parentInsight.childrenInsights.Add(currentTypeInsight);
                currentTypeInsight.hierarchyLevel = parentInsight.hierarchyLevel + 1;
                // update children recurvsively on their hierarchylevel
                UpdateHierarchyTree(currentTypeInsight);
            }
        }

        // we have gathered new information, so let's update all our insights
        UpdateProgressInsights();
    }

    private static void SortSections(List<SectionInsight> sections)
    {
        if (sections.IsNullOrEmpty()) return;
        sections.Sort((insight1, insight2) => insight1.section.SortOrder.Value.CompareTo(insight2.section.SortOrder.Value));
        foreach (var section in sections)
        {
            SortSections(section.childrenInsights);
        }
    }

    private static void UpdateHierarchyTree(ArgumentTypeInsight insight)
    {
        foreach (var child in insight.childrenInsights)
        {
            child.hierarchyLevel = insight.hierarchyLevel + 1;
            UpdateHierarchyTree(child);
        }
    }

    #endregion

    #region helper extensions

    public static CourseInsight GetInsight(this Course course)
    {
        return courseInsights[course.Ref];
    }

    public static ChapterInsight GetInsight(this Chapter chapter)
    {
        return chapterInsightss[chapter.Ref];
    }

    public static ReviewStepInsight GetInsight(this ReviewStep step)
    {
        return reviewStepInsightss[step.Ref];
    }

    public static ResourceInsight GetInsight(this CourseResource resource)
    {
        return resourceInsights[resource.Ref];
    }

    public static SectionInsight GetInsight(this Section section)
    {
        return sectionInsights[section.Ref];
    }

    public static SkillInsight GetInsight(this Skill skill)
    {
        return skillInsights[skill.Ref];
    }

    public static SkillProgressInsight GetProgressInsight(this Skill skill)
    {
        return skillProgressInsights[skill.Ref];
    }

    public static SkillProgressInsight GetProgressInsight(this SkillInsight skillInsight)
    {
        return skillProgressInsights[skillInsight.skill.Ref];
    }

    public static ResourceResultInsight GetResultInsight(this CourseResource resource, string userRef)
    {
        return GetResourceResultInsight(resource.Ref, userRef);
    }

    public static ResourceResultInsight GetResourceResultInsight(string resourceRef, string userRef)
    {
        var resourceDict = resourceResultInsights.GetOrDefault(resourceRef);
        if (resourceDict == null) return null;
        return resourceDict.GetOrDefault(userRef);
    }

    public static QuizInsight GetInsight(this Quiz quiz)
    {
        return quizInsights[quiz.Ref];
    }

    public static StepProgressInsight GetProgressInsight(this ReviewStepInsight stepInsights, ChapterTrack track)
    {
        var trackProgressInsights = track.GetProgressInsight();
        return trackProgressInsights.stepInsightsMapping[stepInsights.step.Ref];
    }

    public static StepProgressInsight GetProgressInsight(this ReviewStepInsight stepInsights, TrackProgressInsight trackProgressInsight)
    {
        return trackProgressInsight.stepInsightsMapping[stepInsights.step.Ref];
    }

    public static StepProgressInsight GetProgressInsight(this ReviewStep step, string trackRef)
    {
        var trackInsight = GetTrackProgressInsight(trackRef);
        return trackInsight.stepInsightsMapping[step.Ref];
    }

    public static StepProgressInsight GetProgressInsight(this ReviewStepInsight stepInsight, string trackRef)
    {
        var trackInsight = GetTrackProgressInsight(trackRef);
        return trackInsight.stepInsightsMapping[stepInsight.step.Ref];
    }

    public static ArgumentTypeInsight GetInsight(this ArgumentType argumentType)
    {
        return argumentTypeInsights[argumentType.Ref];
    }

    public static string GetFullName(this ArgumentType argumentType)
    {
        return argumentType.GetInsight().GetFullName();
    }

    public static string GetFullName(this ArgumentTypeInsight argumentTypeInsight)
    {
        if (argumentTypeInsight == null) return "";
        List<string> names = new List<string>();
        var searchable = argumentTypeInsight;

        while (searchable != null)
        {
            names.Add(searchable.argumentType.Name);
            searchable = searchable.parentInsight;
        }

        names.Reverse();
        return string.Join(" > ", names);
    }

    public static ArgumentInsight GetInsight(this Argument argument)
    {
        return argumentInsights[argument.Ref];
    }

    public static ConversationInsight GetInsight(this Conversation conversation)
    {
        return conversationInsights[conversation.Ref];
    }

    public static ArgumentThreadInsight GetThreadInsight(this Argument argument)
    {
        return argumentThreadInsights[argument.Ref];
    }

    public static string GetUserRef(this ChapterTrack track, PeerType role)
    {
        return role == PeerType.REVIEWER ? track.Reviewer : track.Submitter;
    }

    public static string GetOpponentUserRef(this ChapterTrack track, PeerType friendRole)
    {
        return friendRole == PeerType.REVIEWER ? track.Submitter : track.Reviewer;
    }

    public static PeerType GetOpponentRole(this PeerType role)
    {
        return role == PeerType.REVIEWER ? PeerType.SUBMITTER : PeerType.REVIEWER;
    }

    public static TrackProgressInsight GetProgressInsight(this ChapterTrack track)
    {
        return trackProgressInsightss.GetOrDefault(track.Ref);
    }

    public static ChapterProgressInsight GetProgressInsight(this Chapter chapter, string userRef)
    {
        return chapterProgressInsights.GetOrDefault(chapter.Ref, userRef);
    }

    public static ChapterProgressInsight GetProgressInsight(this ChapterInsight chapterInsight, string userRef)
    {
        return chapterProgressInsights.GetOrDefault(chapterInsight.chapter.Ref, userRef);
    }

    public static CourseProgressInsight GetProgressInsight(this Course course, string userRef)
    {
        return courseProgressInsights.GetOrDefault(course.Ref, userRef);
    }

    public static CourseProgressInsight GetProgressInsight(this CourseInsight courseInsight, string userRef)
    {
        return courseProgressInsights.GetOrDefault(courseInsight.course.Ref, userRef);
    }

    public static List<TrackProgressInsight> GetReviewerTracks(this Chapter chapter, string userRef)
    {
        return GetReviewerTracks(chapter.Ref, userRef);
    }

    public static List<TrackProgressInsight> GetSubmitterTracks(this Chapter chapter, string userRef)
    {
        return GetSubmitterTracks(chapter.Ref, userRef);
    }

    #endregion

    private static bool RequiresQuiz(ReviewStep step)
    {
        return step.Quiz != null;
    }

    private static bool RequiresDiscussion(ReviewStep step)
    {
        return step.DiscussionType != DiscussionType.NONE;
    }

    private static bool RequiresSubmission(ReviewStep step)
    {
        return step.FileUpload.GetValueOrDefault(false);
    }

    private static bool HasQuizResult(ReviewStepResult stepResult)
    {
        return stepResult != null && stepResult.QuizResult != null;
    }

    private static bool HasQuizResult(StepProgressInsight stepProgressInsight)
    {
        return HasQuizResult(stepProgressInsight.result);
    }

    private static bool HasDiscussionData(ReviewStepResult stepResult)
    {
        return stepResult != null && stepResult.Arguments != null && stepResult.Arguments.Count > 0;
    }

    private static bool HasDiscussionData(StepProgressInsight stepProgressInsight)
    {
        return HasDiscussionData(stepProgressInsight.result);
    }

    private static bool HasCompletedDiscussion(ReviewStepResult stepResult, string trackRef)
    {
        return HasCompletedDiscussion(GetStepProgressInsight(trackRef, stepResult.ReviewStepRef));
    }

    private static bool HasCompletedDiscussion(StepProgressInsight stepProgressInsight)
    {
        var stepResult = stepProgressInsight.result;
        if (stepResult == null) return false;
        var track = stepProgressInsight.trackInsight;
        if (track.discussionThreadInsights == null) return false;
        if (stepResult.Arguments == null) return false;
        if (stepResult.Arguments.Count == 0) return false;
        if (stepProgressInsight.requiredArgumentTypesTodo.Count > 0) return false;
        if (track.discussionThreadInsights.Count != stepResult.Arguments.Count) return false;
        return true;
    }

    private static bool HasSubmissionData(ReviewStepResult stepResult)
    {
        return stepResult != null && stepResult.FileUploadMetadata != null;
    }

    private static bool HasSubmissionData(StepProgressInsight stepProgressInsight)
    {
        return HasSubmissionData(stepProgressInsight.result);
    }

    public static bool IsDeadlinePassed(ReviewStep reviewStep)
    {
        var step = reviewStepInsightss.GetOrDefault(reviewStep.Ref).step;
        return step.Deadline < GLDate.Now;
    }

    public static ReviewStepInsight GetReviewStepInsight(string stepRef)
    {
        return reviewStepInsightss[stepRef];
    }

    public static TrackProgressInsight GetTrackProgressInsight(string trackRef)
    {
        return trackProgressInsightss[trackRef];
    }

    public static ResourceInsight GetResourceInsight(string resourceRef)
    {
        return resourceInsights[resourceRef];
    }

    public static SectionInsight GetSectionInsight(string sectionRef)
    {
        return sectionInsights[sectionRef];
    }

    public static IEnumerable<SkillInsight> GetSkillInsights()
    {
        return skillInsights.Values;
    }

    public static SkillInsight GetSkillInsight(string skillRef)
    {
        return skillInsights[skillRef];
    }

    public static SkillProgressInsight GetSkillProgressInsight(string skillRef)
    {
        return skillProgressInsights[skillRef];
    }

    public static ChapterInsight GetChapterInsight(string chapterRef)
    {
        return chapterInsightss[chapterRef];
    }

    public static ChapterProgressInsight GetChapterProgressInsight(string chapterRef, string userRef)
    {
        return chapterProgressInsights.GetOrDefault(chapterRef, userRef);
    }

    public static CourseProgressInsight GetCourseProgressInsight(string courseRef, string userRef)
    {
        return courseProgressInsights.GetOrDefault(courseRef, userRef);
    }

    public static ArgumentTypeInsight GetArgumentTypeInsight(string argumentTypeRef)
    {
        return argumentTypeInsights[argumentTypeRef];
    }

    public static ArgumentThreadInsight GetArgumentThreadInsight(string argumentThreadRef)
    {
        return argumentThreadInsights[argumentThreadRef];
    }

    public static ArgumentInsight GetArgumentInsight(string argumentRef)
    {
        return argumentInsights[argumentRef];
    }

    public static ConversationInsight GetConversationInsight(string conversationRef)
    {
        return conversationInsights[conversationRef];
    }

    public static AllConversationsInsight GetAllConversationsInsight()
    {
        return allConversationsInsight;
    }

    public static List<TrackProgressInsight> GetTracks(string chapterRef, string userRef, PeerType role)
    {
        if (role == PeerType.REVIEWER)
        {
            return GetReviewerTracks(chapterRef, userRef);
        }
        else
        {
            return GetSubmitterTracks(chapterRef, userRef);
        }
    }

    public static List<TrackProgressInsight> GetTracks(string chapterRef, string userRef)
    {
        var chapterProgress = chapterProgressInsights.GetOrDefault(chapterRef, userRef);
        return chapterProgress?.userTrackInsights;
    }

    public static List<TrackProgressInsight> GetReviewerTracks(string chapterRef, string userRef)
    {
        var chapterProgress = chapterProgressInsights.GetOrDefault(chapterRef, userRef);
        return chapterProgress?.userReviewerTrackInsights;
    }

    public static List<TrackProgressInsight> GetSubmitterTracks(string chapterRef, string userRef)
    {
        var chapterProgress = chapterProgressInsights.GetOrDefault(chapterRef, userRef);
        return chapterProgress?.userSubmitterTrackInsights;
    }

    public static bool HasFakeIdentityDefined(string userRef, string courseRef)
    {
        return HasDisplayNameDefined(userRef, courseRef) && HasBusteDefined(userRef, courseRef);
    }

    public static bool HasDisplayNameDefined(string userRef, string courseRef)
    {
        var knowledge = GLApi.GetCourseProgressSummaryKnowledge(userRef, courseRef);
        return !knowledge.DisplayName.IsNullOrEmpty();
    }

    public static bool HasBusteDefined(string userRef, string courseRef)
    {
        return GLBuste.I.HasComposition(userRef, courseRef);
    }

    public static StepProgressInsight GetStepProgressInsight(string trackRef, string reviewStepRef)
    {
        var mapping = trackProgressInsightss.GetOrDefault(trackRef).stepInsightsMapping;
        return mapping.GetOrDefault(reviewStepRef);
    }

    public static bool HasSubmission(string chapterRef, string userRef)
    {
        var submissionTracks = GetSubmitterTracks(chapterRef, userRef);
        if (submissionTracks == null || submissionTracks.Count == 0) return false;
        var trackRef = submissionTracks[0].track.Ref;
        var trackProgressInsights = trackProgressInsightss[trackRef];
        return trackProgressInsights.hasClosedSubmission;
    }

    public static bool IsDeadlinePassed(string stepRef)
    {
        var step = reviewStepInsightss.GetOrDefault(stepRef).step;
        return step.Deadline < GLDate.Now;
    }

    public static bool IsStepAvailable(string trackRef, string reviewStepRef)
    {
        var reviewStepInsights = reviewStepInsightss[reviewStepRef];
        var trackInsight = trackRef != null ? trackProgressInsightss.GetOrDefault(trackRef) : null;

        if (reviewStepInsights.stepIndex == 0)
        {
            // first step is only available when first step of previous chapter is completed
            // or when it is simply the first chapter
            if (reviewStepInsights.chapterInsight.chapterIndex == 0)
            {
                // it is simply first chapter, so item must be available
                return true;
            }
            else
            {
                var previousChapter = reviewStepInsights.chapterInsight.previousChapterInsight;
                var previousChapterFirstSubmissionStep = previousChapter.firstSubmissionStepInsight;
                // if we have no previous first submission step, then we can't wait for it either, so let's just say we are available
                if (previousChapterFirstSubmissionStep == null)
                {
                    Debug.LogWarning("Chapter with index " + previousChapter.chapterIndex + " seems to have no first submission step");
                    return true;
                }
                // if submission deadline has not passed yet, then current submission is also not available
                if (!previousChapterFirstSubmissionStep.isDeadlinePassed)
                {
                    return false;
                }
                // if we have a track, we know the current user,
                // check if there is a submission done in the previous chapter
                // if so, this step will become available
                if (trackInsight != null)
                {
                    var stepProgressInsight = trackInsight.stepInsightsMapping[reviewStepRef];
                    // determine the user to which this step belongs
                    var userRef = stepProgressInsight.userRef;
                    // find all submission tracks for the user in the previous chapter
                    var tracks = GetSubmitterTracks(previousChapter.chapter, userRef);

                    // if we have no known tracks for the given user, his step can't be available now either
                    if (tracks.IsNullOrEmpty())
                    {
                        return false;
                    }

                    // if then one of those tracks has a submnission, then they all do, and this current step will be available
                    var aPreviousTrackInsight = tracks[0];
                    return aPreviousTrackInsight.hasClosedSubmission;
                }
                // if we have no tracks, then this step is simply not available
                return false;
            }
        }
        else
        {
            // if deadline of previous step has not yet passed, then the current step is not availabe
            if (!reviewStepInsights.previousStepInsight.isDeadlinePassed)
            {
                return false;
            }
            // if we have progress in this chapter, then determine if the previous step was closed
            if (trackInsight != null)
            {
                var stepProgressInsight = trackInsight.stepInsightsMapping[reviewStepRef];
                return stepProgressInsight.previousStepInsight.isClosed;
            }
            // if the given track is null, the current step is definitely not available
            // because no progress at all has been made
            return false;
        }
    }
}
