﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLQuiz : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Label_Next;
    public LocalizedText Label_Submit;
    public LocalizedText Label_Cancel;
    public LocalizedText Label_Reset;
    public LocalizedText Label_Back;
    public LocalizedText Title_Question;

    [Header("References")]
    public GLForm form;

    private Quiz quiz;
    private QuizResult result;
    private int totalScore;
    private int nextQuesiton;
    private List<QuizAnswer> answers = new List<QuizAnswer>();
    private Action<QuizResult> onSubmit;

    private bool isTakingQuiz = false;
    public bool IsTakingQuiz()
    {
        return isTakingQuiz;
    }

    public void SetupDisplayResults(Quiz quiz, QuizResult result)
    {
        SetupResults(quiz, result);
    }

    public void SetupModifyableResults(Quiz quiz, QuizResult result, LocalizedText retakeButtonLabel, Action<QuizResult> onSubmit)
    {
        SetupResults(quiz, result, retakeButtonLabel, onSubmit, () =>
        {
            // on cancel, just go back to showing results
            SetupModifyableResults(quiz, result, retakeButtonLabel, onSubmit);
        });
    }

    private void SetupResults(Quiz quiz, QuizResult result, LocalizedText retakeButtonLabel = null, Action<QuizResult> onSubmit = null, Action onCancel = null)
    {
        answers.Clear();
        this.quiz = quiz;
        this.result = result;
        isTakingQuiz = false;
        nextQuesiton = 0;

        var helper = GLForm.GenerateBasicForm(quiz.Name, quiz.Description, null, null);

        var quizInsight = quiz.GetInsight();

        var element = 0;
        foreach (var answer in result.Answers)
        {
            helper.AddHeader((quiz.Elements[element].Text));
            foreach (var optionRef in answer.Answers)
            {
                var option = quizInsight.optionMapper.GetOrDefault(optionRef);
                helper.AddPlainText((option.Text));
            }
            element++;
        }

        if (retakeButtonLabel != null)
        {
            helper.AddCustomButton(retakeButtonLabel, () =>
            {
                SetupTakeQuiz(quiz, result, onSubmit, onCancel);
            });
        }

        form.SetupForm(helper);
    }

    public void SetupTakeQuiz(Quiz quiz, Action<QuizResult> onSubmit, Action onCancel = null)
    {
        SetupTakeQuiz(quiz, null, onSubmit, onCancel);
    }

    public void SetupTakeQuiz(Quiz quiz, QuizResult result, Action<QuizResult> onSubmit, Action onCancel = null)
    {
        answers.Clear();
        this.quiz = quiz;
        this.onSubmit = onSubmit;
        this.result = result;
        isTakingQuiz = false;
        nextQuesiton = 0;
        totalScore = 0;

        var helper = GLForm.GenerateBasicForm(quiz.Name, quiz.Description, Label_Next, OnQuestionSubmit);
        if (onCancel != null)
        {
            helper.AddCustomButton(Label_Cancel, onCancel);
        }
        form.SetupForm(helper);
    }

    private int GetOptionScore(int questionIndex, string optionRef)
    {
        var element = quiz.Elements[questionIndex];
        var option = element.Options.Find(opt => opt.Ref == optionRef);
        return option != null ? option.Points.GetValueOrDefault(0) : 0;
    }

    private void OnQuestionSubmit(GLForm.Data data)
    {
        // just posted a quiz result?
        if (nextQuesiton > 0)
        {
            var questionIndex = nextQuesiton - 1;
            var element = quiz.Elements[questionIndex];
            QuizAnswer answer = new QuizAnswer(element.Ref, new List<string>());
            if (element.QuestionType == QuestionType.SELECT_ONE)
            {
                var optionRef = data["answer"];
                answer.Answers.Add(optionRef);
                totalScore += GetOptionScore(questionIndex, optionRef);
            }
            else
            {
                for (int optionIndex = 0; optionIndex < element.Options.Count; optionIndex++)
                {
                    if (data["answer" + optionIndex] == bool.TrueString)
                    {
                        var optionRef = element.Options[optionIndex].Ref;
                        answer.Answers.Add(optionRef);
                        totalScore += GetOptionScore(questionIndex, optionRef);
                    }
                }
            }
            answers.Add(answer);
        }
        
        // next question or send the results?
        if (quiz.Elements.Count <= nextQuesiton)
        {
            isTakingQuiz = false;
            var result = new QuizResult(quiz.Ref, totalScore, answers, GLDate.Now);
            onSubmit?.Invoke(result);
        }
        else
        {
            isTakingQuiz = true;
            var element = quiz.Elements[nextQuesiton];
            nextQuesiton++;

            var helper = GLForm.GenerateBasicForm(
                Title_Question.Format(nextQuesiton + "", quiz.Elements.Count.ToString()), 
                null,
                quiz.Elements.Count <= nextQuesiton ? Label_Submit : Label_Next,
                OnQuestionSubmit);

            if (element.QuestionType == QuestionType.SELECT_ONE)
            {
                helper.AddRadioButtons(element.Text,
                    "answer", element.Options, option => option.Ref, option => option.Text);
            }
            else
            {
                helper.AddCheckboxList(element.Text,
                    "answer", element.Options, option => option.Text);
            }

            helper.AddCustomButton(Label_Reset, () => { SetupTakeQuiz(quiz, onSubmit); });
            form.SetupForm(helper.formSetup);

            SetValuesFromPreviousResult(element);
        }
    }

    private void SetValuesFromPreviousResult(QuizElement element)
    {
        if (result != null)
        {
            // find the answers given to the current question
            var elementRef = element.Ref;
            var answeredQuestion = result.Answers.Find(ans => ans.QuizElementRef == elementRef);
            if (answeredQuestion != null)
            {
                // if it is a select_one type, just set the "answer" to the corresponding ref
                if (element.QuestionType == QuestionType.SELECT_ONE)
                {
                    form.SetValue("answer", answeredQuestion.Answers.GetOrDefault(0));
                }
                else
                {
                    // otherwise, we need to go through each selected option and find out the index
                    // and set that one
                    foreach (var answeredOptionRef in answeredQuestion.Answers)
                    {
                        var optionIndex = element.Options.FindIndex(o => o.Ref == answeredOptionRef);
                        if (optionIndex >= 0)
                        {
                            var option = element.Options.GetOrDefault(optionIndex);
                            form.SetValue("answer" + optionIndex, "1");
                        }
                    }
                }
            }
        }
    }
}
