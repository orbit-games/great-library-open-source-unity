﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GLFolder {
    SAVED_DOCUMENTS, 
    DEBUG_REPORT,
    MAIN
}
