﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Security.AccessControl;
using System.Security.Permissions;
using UnityEngine;
using static System.Environment;

public class GLFiles : GLSingletonBehaviour<GLFiles> {

    protected override void OnSingletonInitialize()
    {
        var userDocuments = GetUserDocumentsFolder();
        greatLibraryFolder = Path.Combine(userDocuments, GLDebug.IsDebugMode ? greatLibraryDevFolderName : greatLibraryFolderName);
        savedDocumentsFolder = Path.Combine(greatLibraryFolder, savedDocumentsFolderName);
        debugReportFolder = Path.Combine(greatLibraryFolder, debugReportFolderName);
        debugRecordingFolder = Path.Combine(debugReportFolder, debugRecordingFolderName);

        try
        {
            if (!Directory.Exists(greatLibraryFolder))
                Directory.CreateDirectory(greatLibraryFolder);
            if (!Directory.Exists(savedDocumentsFolder))
                Directory.CreateDirectory(savedDocumentsFolder);
            if (!Directory.Exists(debugReportFolder))
                Directory.CreateDirectory(debugReportFolder);
            if (!Directory.Exists(debugRecordingFolder))
                Directory.CreateDirectory(debugRecordingFolder);

            hasWritePermissions = Directory.Exists(greatLibraryFolder);
        }
        catch (Exception e)
        {
            hasWritePermissions = false;
            Debug.LogException(e);
        }
    }

    public static string GetUserDocumentsFolder()
    {
        return GetFolderPath(SpecialFolder.MyDocuments);
    }

    [Header("Settings")]
    public string greatLibraryFolderName = "Great Library";
    public string greatLibraryDevFolderName = "Great Library (DEVELOPMENT)";
    public string savedDocumentsFolderName = "Saved Documents";
    public string debugReportFolderName = "Debug Report";
    public string debugRecordingFolderName = "Low Quality Screenshots";

    [Header("Automatic")]
    [ReadOnly]
    [SerializeField]
    private string greatLibraryFolder;
    [ReadOnly]
    [SerializeField]
    private string savedDocumentsFolder;
    [ReadOnly]
    [SerializeField]
    private string debugReportFolder;
    [ReadOnly]
    [SerializeField]
    private string debugRecordingFolder;
    [ReadOnly]
    [SerializeField]
    private bool hasWritePermissions;

    [Obsolete("Doesnt work on mac, needs fixing. TODO")]
    public static bool HasWriteAccessToFolder(string path)
    {
        // doesnt work on mac apparently....
        try
        {
            var writeAllow = false;
            var writeDeny = false;
            var accessControlList = Directory.GetAccessControl(path);
            if (accessControlList == null)
                return false;
            var accessRules = accessControlList.GetAccessRules(true, true,
                                        typeof(System.Security.Principal.SecurityIdentifier));
            if (accessRules == null)
                return false;

            foreach (FileSystemAccessRule rule in accessRules)
            {
                if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write)
                    continue;

                if (rule.AccessControlType == AccessControlType.Allow)
                    writeAllow = true;
                else if (rule.AccessControlType == AccessControlType.Deny)
                    writeDeny = true;
            }

            return writeAllow && !writeDeny;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return false;
        }
    }

    public static string GreatLibraryFolder
    {
        get
        {
            return I.greatLibraryFolder;
        }
    }

    public static string SavedDocumentsFolder
    {
        get
        {
            return I.savedDocumentsFolder;
        }
    }

    public static string DebugReportFolder
    {
        get
        {
            return I.debugReportFolder;
        }
    }

    public static string DebugRecordingFolderName
    {
        get
        {
            return I.debugRecordingFolder;
        }
    }

    public static bool HasWritePermissions
    {
        get
        {
            return I.hasWritePermissions;
        }
    }
}

