﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GLNameComposition
{
    [System.Serializable]
    public class GenerationPart
    {
        public int list;
        public string word;
        public bool firstLetterUppercase;

        public override string ToString()
        {
            return firstLetterUppercase ? word.ToFirstUpper() : word;
        }
    }

    public List<GenerationPart> parts = new List<GenerationPart>();

    public void Clear()
    {
        parts.Clear();
        result = "";
    }

    public void AddPart(int list, string word, bool firstLetterUppercase)
    {
        parts.Add(new GenerationPart
        {
            list = list,
            word = word,
            firstLetterUppercase = firstLetterUppercase
        });
    }

    public void AddSpace()
    {
        parts.Add(new GenerationPart
        {
            list = -1,
            word = " ",
            firstLetterUppercase = false
        });
    }

    public void AddDash()
    {
        parts.Add(new GenerationPart
        {
            list = -2,
            word = "-",
            firstLetterUppercase = false
        });
    }

    private string result;
    public override string ToString()
    {
        if (result.IsNullOrEmpty())
        {
            result = "";
            foreach (var part in parts)
            {
                result += part.ToString();
            }
            result = result.Trim();
        }
        return result;
    }
}

public class GLName : GLSingletonBehaviour<GLName> {

    protected override void OnSingletonInitialize() { }


    [Header("Settings")]
    public float titleChance = 0.1f;
    public float extraNameChance = 0.1f;
    public float middleNameChance = 0.6f;
    public float middleNameAlternativeChance = 0.4f;
    public float middleNameLettersChance = 0.85f;
    public float middleNameLettersRepeatChance = 0.5f;
    public float nameTheNthChance = 0.05f;
    public float nameStartChance = 0.2f;
    public float nameEndChance = 0.2f;
    public float nameExtraPartChance = 0.2f;
    public float nameDashChance = 0.05f;
    public float firstRhymeChance = 0.2f;

    [Space(10f)]
    public int maxCharacters = 20;

    [Header("Definitions")]
    public List<string> titles; // 0
    public List<string> nameStartParts; // 10
    public List<string> nameParts; // 20
    public List<string> nameEndParts; // 30
    public List<string> middleNameAlternatives; // 40
    public List<string> middleNameLetters; // 50
    public List<string> theNthsParts; // 60

    [Header("Debug")]
    [Buttons("Generate", "Generate")]
    public ButtonsContainer generateDemo;
    public string finalName;

    public GLNameComposition Generate()
    {
        GLNameComposition result = new GLNameComposition();;

        do
        {
            result.Clear();

            // title
            if (Random.value < titleChance)
            {
                result.AddPart(0, titles.GetRandom(), true);
                result.AddSpace();
            }

            // first name
            string start = GenerateNamePart(result, null);
            result.AddSpace();

            if (Random.value > firstRhymeChance)
            {
                start = null;
            }

            // middle name
            if (Random.value < middleNameChance)
            {
                if (Random.value < middleNameAlternativeChance)
                {
                    if (Random.value < middleNameLettersChance)
                    {
                        result.AddPart(40, middleNameAlternatives.GetRandom(), false);
                        result.AddSpace();
                    }
                    else
                    {
                        do
                        {
                            result.AddPart(50, middleNameLetters.GetRandom(), true);
                            result.AddSpace();
                        } while (Random.value < middleNameLettersRepeatChance);
                    }
                }
                else
                {
                    GenerateNamePart(result, start);
                    result.AddSpace();
                }
            }

            // last name
            GenerateNamePart(result, start);

            // the nth
            if (Random.value < nameTheNthChance)
            {
                result.AddSpace();
                result.AddPart(60, theNthsParts.GetRandom(), false);
            }

            // finalize
            finalName = result.ToString();

        } while (finalName.Length > maxCharacters); // if too long, do it again
        
        return result;
    }

    private string GenerateNamePart(GLNameComposition result, string firstLetterRhyme)
    {
        string start = null;

        if (Random.value < nameStartChance)
        {
            start = nameStartParts.GetRandom();
            result.AddPart(10, start, true);
            result.AddPart(20, nameParts.GetRandom(), false);
        }
        else
        {
            start = nameParts.GetRandom();
            result.AddPart(20, start, true);
        }

        while (Random.value < nameExtraPartChance)
        {
            if (Random.value < nameDashChance)
            {
                if (Random.value < nameEndChance)
                {
                    result.AddPart(30, nameEndParts.GetRandom(), false);
                }

                result.AddDash();
                result.AddPart(20, nameParts.GetRandom(), true);
            }
            else
            {
                result.AddPart(20, nameParts.GetRandom(), false);
            }
        }


        if (Random.value < nameEndChance)
        {
            result.AddPart(30, nameEndParts.GetRandom(), false);
        }

        return start;
    }

}
