﻿using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLProgressLoader : GLSingletonBehaviour<GLProgressLoader> {

    protected override void OnSingletonInitialize() { }

    public float autoRefreshInterval = 10f;

    private Action onFinish = null;
    private bool loadingProgress = false;
    private float lastLoadTimeSinceStartup = 0f;

    public void RefreshIfNecessary(Action onFinish)
    {
        if (!GLApi.IsLoggedInAndCourseSelected())
        {
            onFinish?.Invoke();
        }
        else
        {
            if (Time.realtimeSinceStartup - lastLoadTimeSinceStartup > autoRefreshInterval)
            {
                Debug.Log("Refreshing course progress");
                LoadCourseProgress(onFinish);
            }
            else
            {
                onFinish?.Invoke();
            }
        }
    }

    public void Refresh()
    {
        LoadCourseProgress(null);
    }

    public void LoadCourseProgress(Action onFinish)
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        if (course == null)
        {
            onFinish?.Invoke();
            return;
        }

        this.onFinish += onFinish;
        if (loadingProgress) return;
        loadingProgress = true;

        lastLoadTimeSinceStartup = Time.realtimeSinceStartup;

        GLLoadingOverlay.ShowFullcoverLoading(GLApi.FetchCourseProgress(user.Ref, course.Ref), OnCourseProgressLoadResponse);
    }
    
    void OnCourseProgressLoadResponse(GLTaskResult fetchCourseProgressResult)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(fetchCourseProgressResult))
        {
            var user = GLApi.GetActiveUser();
            var course = GLApi.GetActiveCourse();
            GLLoadingOverlay.ShowLoading(GLApi.FetchChatConversations(course.Ref), OnGetConversationsReply);
        }
        else
        {
            onFinish = null;
            loadingProgress = false;
        }
    }

    void OnGetConversationsReply(GLTaskResult getConversationsResult)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(getConversationsResult))
        {
            var allConversationsInsight = GLInsights.GetAllConversationsInsight();
            if (allConversationsInsight.missingDirectConversations.Count > 0)
            {
                var user = GLApi.GetActiveUser();
                var course = GLApi.GetActiveCourse();
                var members = new List<string>();
                members.Add(user.Ref);
                members.Add(allConversationsInsight.missingDirectConversations[0]);

                GLLoadingOverlay.ShowLoading(GLApi.CreateConversation(course.Ref,
                    new Conversation(null, null, null, false, ConversationType.DIRECT, false, members, null, ConversationEmailNotificationType.DIRECT, 0, null, null)),
                    OnGetConversationsReply);
            }
            else
            {
                onFinish?.Invoke();
                onFinish = null;
                loadingProgress = false;
                GLGameFlow.I.Refresh();
            }
        }
        else
        {
            onFinish = null;
            loadingProgress = false;
        }
    }

    void OnCreateTemporaryConversation(GLTaskResult createConversationResult)
    {
        if (!GLApi.DiplayErrorPopupIfFailed(createConversationResult))
        {
            onFinish?.Invoke();
            onFinish = null;
            loadingProgress = false;
            GLGameFlow.I.Refresh();
        }
        else
        {
            onFinish = null;
            loadingProgress = false;
        }
    }
}
