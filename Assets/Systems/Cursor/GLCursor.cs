﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GLCursor : GLSingletonBehaviour<GLCursor> {

    [Header("State")]
    public float lastInteractionTime;
    public GameObject lastObjectInteracted;
    public GameObject currentObject;
    public bool isButton;
    public bool isInputField;
    public bool isFormElement;
    public bool isDraggable;
    public bool isPressing;
    public bool isTooltip;
    public bool isChat;
    public bool wasDraggingOnPress;
    public PointerEventData PointerEventData;
    public GameObject activeCursor;

    [Header("References")]
    public Transform cursorContainer;
    public RectTransform cursorRect;
    private RectTransform rectTransform;

    public GameObject horizontalIndicators;
    public GameObject verticalIndicators;

    public GameObject defaultCursor;
    public GameObject pointerCursor;
    public GameObject pressingCursor;
    public GameObject draggableCursor;
    public GameObject draggingCursor;
    public GameObject chatCursor;
    public GameObject chatClickCursor;

    public GraphicRaycaster Raycaster;
    public EventSystem EventSystem;

    protected override void OnSingletonInitialize()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public string GetCurrentObjectPath()
    {
        return currentObject.GetPath();
    }

    public string GetLastInteractedObjectPath()
    {
        return lastObjectInteracted.GetPath();
    }

    public float GetLastInteractionTime()
    {
        return lastInteractionTime;
    }

    private void LateUpdate()
    {
        // make sure cursor is not visible
        Cursor.visible = false;

        // setting the position of the cursor
        //Vector2 localPosition;
        //RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, Input.mousePosition, null, out localPosition);
        var camera = Camera.main;
        var newPos = camera.ScreenToWorldPoint(Input.mousePosition);
        cursorRect.position = new Vector3(newPos.x, newPos.y, cursorRect.position.z);

        //Set up the new Pointer Event
        PointerEventData = new PointerEventData(EventSystem);
        //Set the Pointer Event Position to that of the mouse position
        PointerEventData.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        EventSystem.RaycastAll(PointerEventData, results);
        
        if (results.Count == 0)
        {
            isButton = false;
            isDraggable = false;
            isPressing = false;
            isTooltip = false;
            isInputField = false;
            isFormElement = false;
            isChat = false;
        }
        else
        {
            currentObject = results[0].gameObject;

            var selectable = currentObject.GetComponentInParent<Selectable>();
            if (selectable != null)
            {
                isButton = selectable.IsInteractable();
                isInputField = selectable is TMP_InputField;
                isFormElement = isInputField
                    || selectable.GetComponentInParent<GLFormCheckboxOption>()
                    || selectable.GetComponentInParent<GLFormRadioButtonOption>()
                    || selectable.GetComponentInParent<GLFormAdvancedPickerField>();
            }
            else
            {
                isButton = false;
                isInputField = false;
                isFormElement = false;
            }

            var fileBrowserItem = currentObject.GetComponentInParent<SimpleFileBrowser.FileBrowserItem>();
            isButton = isButton || fileBrowserItem != null;

            var extra = currentObject.GetComponentInParent<GLCursorExtra>();
            isChat = extra?.isChat ?? false;

            var scroll = currentObject.GetComponentInParent<ScrollRect>();
            isDraggable = scroll != null;

            //var hasVerticalScrollbar = scroll != null && scroll.verticalScrollbar.gameObject.activeSelf;
            //var hasHorizontalScrollbar = scroll != null && scroll.horizontalScrollbar.gameObject.activeSelf;

            horizontalIndicators.SetActive(scroll != null && scroll.horizontal && !isInputField);
            verticalIndicators.SetActive(scroll != null && scroll.vertical && !isInputField);
        }

        isPressing = Input.GetMouseButton(0);

        if (Input.GetMouseButtonDown(0))
        {
            wasDraggingOnPress = isDraggable;

            if (isButton && !isFormElement)
            {
                GLScreenRecord.I.RecordThisFrame();
            }

            if (isDraggable)
            {
                //GLScreenRecord.I.RecordThisFrame();
            }
        }

        if (Input.GetMouseButtonUp(0) && wasDraggingOnPress)
        {
            // GLScreenRecord.I.RecordThisFrame();
        }

        if (isPressing)
        {
            lastObjectInteracted = currentObject;
            lastInteractionTime = Time.realtimeSinceStartup;
        }

        if (isButton)
        {
            if (isPressing)
            {
                SetCursor(isChat ? chatClickCursor : pressingCursor);
            }
            else
            {
                SetCursor(isChat ? chatCursor : pointerCursor);
            }
        }
        else if (isDraggable || (isPressing && wasDraggingOnPress))
        {
            if (isPressing)
            {
                SetCursor(draggingCursor);
            }
            else
            {
                SetCursor(draggableCursor);
            }
        }
        else
        {
            SetCursor(defaultCursor);
        }

    }

    private void SetCursor(GameObject cursor)
    {
        defaultCursor.SetActive(cursor == defaultCursor);
        pressingCursor.SetActive(cursor == pressingCursor);
        pointerCursor.SetActive(cursor == pointerCursor);
        draggableCursor.SetActive(cursor == draggableCursor);
        draggingCursor.SetActive(cursor == draggingCursor);
        chatClickCursor.SetActive(cursor == chatClickCursor);
        chatCursor.SetActive(cursor == chatCursor);
    }


    public void OnEnable()
    {
        Cursor.visible = false;
    }

    private void OnDisable()
    {
        Cursor.visible = true;
    }

    private void OnDestroy()
    {
        Cursor.visible = true;
    }

}
