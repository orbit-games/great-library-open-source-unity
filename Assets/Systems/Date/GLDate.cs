﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLDate : GLSingletonBehaviour<GLDate> {

    protected override void OnSingletonInitialize()
    {
        if (!GLDebug.IsDebugMode)
        {
            useDebugDateTime = false;
        }
    }

    [Header("Debug")]
    public bool useDebugDateTime = false;
    public int year = 2018;
    public int month = 06;
    public int day = 30;
    public int hour = 6;
    public int minute = 0;
    public int second = 33;

    [Header("Localization")]
    public LocalizedText Label_Second;
    public LocalizedText Label_Seconds;
    public LocalizedText Label_Minute;
    public LocalizedText Label_Minutes;
    public LocalizedText Label_Hour;
    public LocalizedText Label_Hours;
    public LocalizedText Label_Day;
    public LocalizedText Label_Days;
    public LocalizedText Label_Week;
    public LocalizedText Label_Weeks;
    public LocalizedText Label_JustNow;
    public LocalizedText Label_Yesterday;
    public LocalizedText Label_Tomorrow;

    [Space(10f)]
    public LocalizedText Label_TimeAgo;

    private DateTime GetDebugTime()
    {
        return new DateTime(year, month, day, hour, minute, second, DateTimeKind.Local);
    }

    public static DateTime Now
    {
        get
        {
            if (I.useDebugDateTime)
            {
                return I.GetDebugTime();
            }
            else
            {
                return DateTime.Now;
            }
        }
    }

    public static DateTime Min
    {
        get
        {
            return new DateTime(DateTime.MinValue.Ticks, DateTimeKind.Local);
        }
    }

    public static void NextDay()
    {
        var newDay = Now.AddDays(1);
        I.year = newDay.Year;
        I.month = newDay.Month;
        I.day = newDay.Day;
    }

    public static void PreviousDay()
    {
        var newDay = Now.AddDays(-1);
        I.year = newDay.Year;
        I.month = newDay.Month;
        I.day = newDay.Day;
    }

    public static GLPrettyDate GetPrettyDate(DateTime date)
    {
        return GetPrettyDate(date, Now);
    }

    public static GLPrettyDate GetTrueTimePrettyDate(DateTime date)
    {
        return GetPrettyDate(date, DateTime.Now);
    }

    public static GLPrettyDate GetPrettyDate(DateTime date, DateTime now)
    {
        var diff = date - now;

        if (Mathf.Abs((float)diff.TotalSeconds) < 60)
        {
            //return new GLPrettyDate(I.Label_JustNow.Value);
            return MakePretty((int)diff.TotalSeconds, I.Label_Second, I.Label_Seconds);
        }

        if (Mathf.Abs((float)diff.TotalMinutes) < 60)
        {
            return MakePretty((int)diff.TotalMinutes, I.Label_Minute, I.Label_Minutes);
        }

        if (Mathf.Abs((float)diff.TotalHours) < 24)
        {
            return MakePretty(Mathf.RoundToInt((float)diff.TotalHours), I.Label_Hour, I.Label_Hours);
        }

        if (Mathf.Abs((float)diff.TotalDays) < 7)
        {
            //var days = Mathf.RoundToInt((float)diff.TotalDays);

            //if (days == -1)
            //    return new GLPrettyDate(I.Label_Yesterday);
            //if (days == 1)
            //    return new GLPrettyDate(I.Label_Tomorrow);

            return MakePretty(Mathf.RoundToInt((float)diff.TotalDays), I.Label_Day, I.Label_Days);
        }

        if (Mathf.Abs((float)diff.TotalDays) < 30)
        {
            return MakePretty(Mathf.RoundToInt((float)diff.TotalDays / 7f), I.Label_Week, I.Label_Weeks);
        }

        if (date.Year == now.Year)
        {
            return new GLPrettyDate(date.Day, date.ToString("MMMM"));
        }
        else
        {
            return new GLPrettyDate(date.Day, date.ToString("MMMM yyyy"));
        }
    }

    public static string GetPrettyDateTime(DateTime date)
    {
        return GetPrettyDateTime(date, Now);
    }

    public static string GetPrettyTrueDateTime(DateTime date)
    {
        return GetPrettyDateTime(date, DateTime.Now);
    }

    private static string GetPrettyDateTime(DateTime date, DateTime now)
    {
        var isToday = date.Year == now.Year && date.Month == now.Month && date.Day == now.Day;
        if (isToday)
        {
            return date.ToShortTimeString();
        }

        var yesterday = date.AddDays(1);
        var isYesterday = yesterday.Year == now.Year && yesterday.Month == now.Month && yesterday.Day == now.Day;
        if (isYesterday)
        {
            return I.Label_Yesterday + " " + date.ToShortTimeString();
        }

        var tomorrow = date.AddDays(-1);
        var isTomorrow = tomorrow.Year == now.Year && tomorrow.Month == now.Month && tomorrow.Day == now.Day;
        if (isYesterday)
        {
            return I.Label_Tomorrow + " " + date.ToShortTimeString();
        }

        return date.ToShortDateString() + " " + date.ToShortTimeString();
    }

    private static GLPrettyDate MakePretty(int number, LocalizedText singleUnit, LocalizedText multiUnit)
    {
        string unit = Mathf.Abs(number) == 1 ? singleUnit.Value : multiUnit.Value;
        unit = number < 0 ? string.Format(I.Label_TimeAgo, unit) : unit;
        return new GLPrettyDate(Mathf.Abs(number), unit);
    }
}
