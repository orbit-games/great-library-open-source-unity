﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GLPrettyDate
{
    public int number;
    public string unit;
    public bool unitOnly;

    public GLPrettyDate(int number, string unit)
    {
        this.number = number;
        this.unit = unit;
        unitOnly = false;
    }

    public GLPrettyDate(string unit)
    {
        number = 0;
        this.unit = unit;
        unitOnly = true;
    }
    public override string ToString()
    {
        if (!unitOnly)
        {
            return number + " " + unit;
        }
        else
        {
            return unit;
        }
    }
}
