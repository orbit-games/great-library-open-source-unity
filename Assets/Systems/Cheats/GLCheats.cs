﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GLCheats : GLSingletonBehaviour<GLCheats>, ICheatListener
{
    [Serializable]
    public class GLCheat
    {
        public string uid;
        [TextArea(1, 2)]
        public string name;
        [TextArea(3,6)]
        public string description;
        public bool enabled = true;
        public bool shiftKey = false;
        public CheatKey primaryKey;
        public CheatKey alternativeKey;
        public List<ICheatListener> cheatListeners;
        public GameObject listeningObject;

        //public bool IsPressedDown()
        //{
        //    if (!enabled) return false;
        //    if (shiftKey != GLInput.IsHoldingShift()) return false;
        //    var primary = primaryKey != CheatKey.None && Input.GetKeyDown((KeyCode)primaryKey);
        //    var alternative = alternativeKey != CheatKey.None && Input.GetKeyDown((KeyCode)alternativeKey);
        //    return primary || alternative;
        //}

        //public bool IsHoldingDown()
        //{
        //    if (!enabled) return false;
        //    if (shiftKey != GLInput.IsHoldingShift()) return false;
        //    var primary = primaryKey != CheatKey.None && Input.GetKey((KeyCode)primaryKey);
        //    var alternative = alternativeKey != CheatKey.None && Input.GetKey((KeyCode)alternativeKey);
        //    return primary || alternative;
        //}

        public bool IsReleasedUp()
        {
            if (!enabled) return false;
            if (shiftKey != GLInput.IsHoldingShift()) return false;
            var primary = primaryKey != CheatKey.None && Input.GetKeyUp((KeyCode)primaryKey);
            var alternative = alternativeKey != CheatKey.None && Input.GetKeyUp((KeyCode)alternativeKey);
            return primary || alternative;
        }

        public override string ToString()
        {
            var output = "Cheat: " + name.ToUpper() + "\n" + description + "\n" + "\n";
            output += "Can be activated with key: " + primaryKey + "\n";
            if (alternativeKey != CheatKey.None)
            {
                output += "Can also be activated with key: " + alternativeKey + "\n";
            }
            return output;
        }
    }

    public List<GLCheat> cheats;
    private Dictionary<string, GLCheat> cheatsMapper = new Dictionary<string, GLCheat>();
    private Dictionary<string, bool> listeningCheats = new Dictionary<string, bool>();
    public static void RegisterCheat(GLCheat cheat)
    {
        var i = I;
        if (i.cheatsMapper.ContainsKey(cheat.uid))
        {
            Debug.LogError("Already have a cheat with given unique ID " + cheat.uid);
            return;
        }

        i.cheats.Add(cheat);
        i.cheatsMapper.Add(cheat.uid, cheat);
        i.listeningCheats.Add(cheat.uid, false);

        var listeners = cheat.listeningObject.GetComponents<ICheatListener>();
        cheat.cheatListeners = new List<ICheatListener>(listeners);

        if (I.cheatsEnabled)
        {
            Debug.Log("New cheat has been registered!");
            Debug.LogWarning(cheat);
        }
    }

    private bool cheatsEnabled;

    //public static bool IsCheatKeyPressedDown(string uid)
    //{
    //    var i = I;
    //    if (!i.cheatsEnabled) return false;
    //    if (!GLInput.IsRequestingCheat()) return false;
    //    var cheat = i.cheatsMapper.GetOrDefault(uid);
    //    if (cheat == null) return false;
    //    return cheat.IsPressedDown();
    //}

    //public static bool IsCheatKeyReleasedUp(string uid)
    //{
    //    var i = I;
    //    if (!i.cheatsEnabled) return false;
    //    if (!GLInput.IsRequestingCheat()) return false;
    //    var cheat = i.cheatsMapper.GetOrDefault(uid);
    //    if (cheat == null) return false;
    //    return cheat.IsReleasedUp();
    //}

    //public static bool IsHoldingDownCheatKey(string uid)
    //{
    //    var i = I;
    //    if (!i.cheatsEnabled) return false;
    //    if (!GLInput.IsRequestingCheat()) return false;
    //    var cheat = i.cheatsMapper.GetOrDefault(uid);
    //    if (cheat == null) return false;
    //    return cheat.IsHoldingDown();
    //}

    public void SetCheatEnabled(string uid, bool enabled)
    {
        var i = I;
        var cheat = i.cheatsMapper.GetOrDefault(uid);
        if (cheat == null) return;
        cheat.enabled = enabled;
    }

    protected override void OnSingletonInitialize()
    {
        foreach (var cheat in cheats)
        {
            if (cheatsMapper.ContainsKey(cheat.uid))
            {
                Debug.LogError("Already have a cheat with given unique ID " + cheat.uid);
                continue;
            }
            cheatsMapper.Add(cheat.uid, cheat);
            listeningCheats.Add(cheat.uid, false);

            var listeners = cheat.listeningObject.GetComponents<ICheatListener>();
            cheat.cheatListeners = new List<ICheatListener>(listeners);
        }
    }

    private void Update()
    {
        if (GLInput.IsEnablingCheats())
        {
            cheatsEnabled = !cheatsEnabled;
            if (cheatsEnabled)
            {
                Debug.LogWarning("Cheats enabled! All currently registered cheats:");
                foreach (var cheat in cheats)
                {
                    Debug.LogWarning(cheat);
                }
            }
            else
            {
                Debug.LogWarning("Cheats disabled...");
            }
        }

        if (cheatsEnabled && GLInput.IsRequestingCheat())
        {
            foreach (var cheat in cheats)
            {
                // pressing down
                //if (cheat.IsPressedDown())
                //{
                //    if (cheat.cheatListener.OnCheatDown(cheat.uid))
                //    {
                //        Debug.LogWarning("Cheat listened: " + cheat.name);
                //    }
                //}

                //// holding down
                //var wasListening = listeningCheats[cheat.uid];
                //var isListening = false;
                //if (cheat.IsHoldingDown())
                //{
                //    isListening = cheat.cheatListener.OnCheatHold(cheat.uid) || isListening;
                //}
                //if (!wasListening && isListening)
                //{
                //    Debug.LogWarning("Cheat is listening: " + cheat.name);
                //}
                //listeningCheats[cheat.uid] = isListening;

                // pressing up
                if (cheat.IsReleasedUp())
                {
                    foreach (var cheatListener in cheat.cheatListeners)
                    {
                        var msg = cheatListener.OnCheat(cheat.uid);
                        if (msg != null)
                        {
                            Debug.LogWarning(msg);
                        }
                    }
                }
            }
        }
    }

    public static bool fastForward;
    public string OnCheat(string cheatUID)
    {
        if (cheatUID == "ClearAllPreferences")
        {
            foreach (var pref in GLPrefs.I.preferences)
            {
                pref.ClearAll();
            }
            PlayerPrefs.DeleteAll();
            return "Clearing ALL preferences for this user. Including those in the cloud...";
        }
        else if (cheatUID == "ClearUnityPreferences")
        {
            PlayerPrefs.DeleteAll();
            return "Clearing preferences stored in Unity's PlayerPrefs system...";
        }
        else if (cheatUID == "ForceCrash")
        {
            Application.ForceCrash(0);
            return "Forcing a crash...";
        }
        else if (cheatUID == "FastForward")
        {
            fastForward = !fastForward;
            if (fastForward) return "Enabled fast forwarding, skipping certain animations";
            else return "Disabled fast forwarding, allowing certain animations to be displayed";
        }
        return null;
    }

    public static bool IsCheatsEnabled()
    {
        return I.cheatsEnabled;
    }
}
