﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICheatListener
{
    string OnCheat(string cheatUID);
}
