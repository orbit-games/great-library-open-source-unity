﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLPopup : GLSingletonBehaviour<GLPopup> {

    protected override void OnSingletonInitialize() { }
    
    private static Stack<Setup> popupsShowing =  new Stack<Setup>();

    public static bool IsPopupActive()
    {
        return popupsShowing.Count > 0;
    }

    private static void PushPopup(Setup popupSetup)
    {
        popupsShowing.Push(popupSetup);
    }

    private static void PopPopup()
    {
        popupsShowing.Pop();
    }

    [Header("References")]
    public Transform popupBase;
    public GLPopupWindow popupWindowPrefab;

    public Sprite notificationIcon;
    public Sprite confirmIcon;
    public Sprite promptIcon;
    public Sprite warningIcon;
    public Sprite errorIcon;

    [Header("Localization")]
    public LocalizedText cancelButtonText;
    public LocalizedText acceptButtonText;
    public LocalizedText submitButtonText;
    public LocalizedText closeButtonText;
    public LocalizedText errorTitle;
    public LocalizedText warningTitle;
    public LocalizedText notificationTitle;
    public LocalizedText confirmTitle;
    public LocalizedText confirmDeleteDescription;

    [Space(10f)]
    public LocalizedText titleUnsavedChanges;
    public LocalizedText descriptionUnsavedChanges;
    public LocalizedText labelKeepAndStay;
    public LocalizedText labelDiscardAndContinue;

    [Header("Test")]
    [Buttons("Test", "PopupTest")]
    public ButtonsContainer test;

    public enum PopupType
    {
        NOTIFY, CONFIRM, PROMPT, WARNING, ERROR
    }

    public Sprite GetIcon(PopupType type)
    {
        switch (type)
        {
            case PopupType.CONFIRM:
                return confirmIcon;
            case PopupType.PROMPT:
                return promptIcon;
            case PopupType.WARNING:
                return warningIcon;
            case PopupType.ERROR:
                return errorIcon;
            case PopupType.NOTIFY:
            default:
                return notificationIcon;
        }
    }

    public void PopupTest()
    {
        GLPopup.MakeNotificationPopup(("Bliep1"), ("bloep1"), () => Debug.Log("Closed notifiation")).Show();
        GLPopup.MakeWarningPopup(("Bliep2"), ("bloep2"), () => Debug.Log("Closed warning")).Show();
        GLPopup.MakeErrorPopup(("Bliep3"), ("bloep3"), () => Debug.Log("Closed error")).Show();
        GLPopup.MakeConfirmPopup(("Bliep4"), ("Would you bloep4?"), () => Debug.Log("Accepted!"), () => Debug.Log("Denied!")).Show();
        GLPopup.MakeConfirmPopup(("Bliep4"), ("Would you bloep4?"), () => Debug.Log("Accepted!"), () => Debug.Log("Denied!")).Show();

        var prompt = GLPopup.MakePromptPopup(("Bliep5"), ("Would you fill in the data below please"), (data) => Debug.Log(data), () => Debug.Log("Canceled"));
        prompt.formHelper.AddTextField(("Some bloop"), "a_variable");
        prompt.Show();

        var prompt2 = GLPopup.MakePromptPopup(("Bliep5"), ("Would you fill in the data below please"), (data) => Debug.Log(data), () => Debug.Log("Canceled"));
        prompt2.formHelper.AddTextField(("Some bloop"), "a_variable");
        prompt2.Show();
    }

    public static Setup MakeNotificationPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Action onClose = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetNotify(onClose);
    }

    public static Setup MakeNotificationPopup(GLFormattedLocalizedText message, Action onClose = null)
    {
        return ((TypeSetup)new Setup(I.notificationTitle, message)).SetNotify(onClose);
    }

    public static Setup MakeErrorPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Action onClose = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetError(onClose);
    }

    public static Setup MakeErrorPopup(GLFormattedLocalizedText message, Action onClose = null)
    {
        return ((TypeSetup)new Setup(I.errorTitle, message)).SetError(onClose);
    }

    public static Setup MakeWarningPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Action onClose = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetWarning(onClose);
    }

    public static Setup MakeWarningPopup(GLFormattedLocalizedText message, Action onClose = null)
    {
        return ((TypeSetup)new Setup(I.warningTitle, message)).SetWarning(onClose);
    }

    public static Setup MakeConfirmPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Action onPositive = null, Action onNegative = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetConfirm(onNegative, onPositive);
    }

    public static Setup MakeConfirmPopup(GLFormattedLocalizedText message, Action onPositive = null, Action onNegative = null)
    {
        return ((TypeSetup)new Setup(I.confirmTitle, message)).SetConfirm(onNegative, onPositive);
    }

    public static Setup MakeConfirmDeletePopup(Action onPositive = null, Action onNegative = null)
    {
        return ((TypeSetup)new Setup(I.confirmTitle, I.confirmDeleteDescription)).SetConfirm(onNegative, onPositive);
    }

    public static Setup MakeConfirmDiscardChangesPopup(Action onPositive = null, Action onNegative = null)
    {
        var popup = GLPopup.MakeConfirmPopup(I.titleUnsavedChanges, I.descriptionUnsavedChanges, onPositive, onNegative);
        popup.OverrideNegativeButtonText(I.labelKeepAndStay);
        popup.OverridePositiveButtonText(I.labelDiscardAndContinue);
        return popup;
    }

    public static Setup MakePromptPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Func<GLForm.Data, bool> verifier, Action<GLForm.Data> onSubmit = null, Action onCancel = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetPrompt(verifier, onSubmit, onCancel);
    }

    public static Setup MakePromptPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Action<GLForm.Data> onSubmit = null, Action onCancel = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetPrompt(SimplyAcceptTheData, onSubmit, onCancel);
    }

    public static Setup MakeDemandPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Func<GLForm.Data, bool> verifier, Action<GLForm.Data> onSubmit = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetDemand(verifier, onSubmit);
    }

    public static Setup MakeDemandPopup(GLFormattedLocalizedText title, GLFormattedLocalizedText message, Action<GLForm.Data> onSubmit = null)
    {
        return ((TypeSetup)new Setup(title, message)).SetDemand(SimplyAcceptTheData, onSubmit);
    }

    private static bool SimplyAcceptTheData(GLForm.Data data)
    {
        return true;
    }

    private interface TypeSetup
    {
        Setup SetError(Action onClose);
        Setup SetWarning(Action onClose);
        Setup SetNotify(Action onClose);
        Setup SetConfirm(Action onNegative, Action onPositive);
        Setup SetPrompt(Func<GLForm.Data, bool> dataVerifier, Action<GLForm.Data> onSubmit, Action onCancel);
        Setup SetDemand(Func<GLForm.Data, bool> dataVerifier, Action<GLForm.Data> onSubmit);
    }

    public class Setup : TypeSetup
    {
        public GLPopup popupSystem { get; private set; }
        public GLPopupWindow popupWindow { get; private set; }
        public GLForm.Setup formSetup { get; private set; }
        public GLForm.SetupHelper formHelper { get; private set; }

        // closing/canceling
        private Action onClose { get; set; }

        // submitting
        private Action onAccept { get; set; }
        private Action<GLForm.Data> onSubmit { get; set; }
        private Func<GLForm.Data, bool> dataVerifier { get; set; }

        public PopupType type { get; private set; }
        private bool shown = false;
        private bool closable = true;

        public Setup(GLFormattedLocalizedText title, GLFormattedLocalizedText message)
        {
            popupSystem = GetInstance();
            formHelper = GLForm.GenerateBasicForm(title, message, popupSystem.closeButtonText, (data) => {
                if (dataVerifier != null)
                {
                    if (!dataVerifier(data))
                    {
                        Debug.Log("data could not be verified");
                        return;
                    }
                }
                onSubmit?.Invoke(data);
                onAccept?.Invoke();
                Close();
            });
            formSetup = formHelper.formSetup;
            type = PopupType.NOTIFY;
        }

        public void Close()
        {
            if (!closable) return;
            if (!IsShowing()) return;
            popupWindow.CloseWindow();
            popupWindow = null;
            PopPopup();
        }

        public void SetClosable(bool closable)
        {
            this.closable = closable;
        }

        public Setup SetOnClose(Action onClose)
        {
            this.onClose = onClose;
            return this;
        }

        public Setup SetOnAccept(Action onAccept)
        {
            this.onAccept = onAccept;
            return this;
        }

        public Setup SetOnSubmit(Action<GLForm.Data> onSubmit)
        {
            this.onSubmit = onSubmit;
            return this;
        }

        public Setup SetDataVerifier(Func<GLForm.Data, bool> dataVerifier)
        {
            this.dataVerifier = dataVerifier;
            return this;
        }

        Setup TypeSetup.SetError(Action onClose)
        {
            type = PopupType.ERROR;
            return SetOnClose(onClose).SetOnAccept(onClose);
        }

        Setup TypeSetup.SetWarning(Action onClose)
        {
            type = PopupType.WARNING;
            return SetOnClose(onClose).SetOnAccept(onClose);
        }

        Setup TypeSetup.SetNotify(Action onClose)
        {
            type = PopupType.NOTIFY;
            return SetOnClose(onClose).SetOnAccept(onClose);
        }

        Setup TypeSetup.SetConfirm(Action onNegative, Action onPositive)
        {
            type = PopupType.CONFIRM;
            SetOnClose(onNegative);
            SetOnAccept(onPositive);

            // create the negative button
            formHelper.AddCustomButton(popupSystem.cancelButtonText, () =>
            {
                onClose?.Invoke();
                Close();
            });

            // swap buttons to set the positive button as our second button
            SwapButtons();

            // set the positive button text
            OverridePositiveButtonText(popupSystem.acceptButtonText);
            return this;
        }

        Setup TypeSetup.SetPrompt(Func<GLForm.Data, bool> dataVerifier, Action<GLForm.Data> onSubmit, Action onCancel)
        {
            type = PopupType.PROMPT;
            SetOnClose(onCancel);
            SetOnAccept(null);
            SetOnSubmit(onSubmit);
            SetDataVerifier(dataVerifier);

            // add the cancel button
            formHelper.AddCustomButton(popupSystem.cancelButtonText, () =>
            {
                onClose?.Invoke();
                Close();
            });

            // swap buttons to set the submit button as our positive second button
            SwapButtons();

            // set the submit button text
            OverrideSubmitButtonText(popupSystem.submitButtonText);
            return this;
        }

        Setup TypeSetup.SetDemand(Func<GLForm.Data, bool> dataVerifier, Action<GLForm.Data> onSubmit)
        {
            type = PopupType.PROMPT;
            SetOnClose(null);
            SetOnAccept(null);
            SetOnSubmit(onSubmit);
            SetDataVerifier(dataVerifier);

            // set the submit button text
            OverrideCloseButtonText(popupSystem.submitButtonText);
            return this;
        }

        public void RemoveButtons()
        {
            formHelper.buttons.subelements.Clear();
        }

        public void AddClosingButton(GLFormattedLocalizedText label, Action onClick)
        {
            formHelper.AddCustomButton(label, () =>
            {
                onClick?.Invoke();
                Close();
            });
        }

        public void AddSubmitButton(GLFormattedLocalizedText label, Action onClick)
        {
            formHelper.AddSubmitButton(label, () =>
            {
                onClick?.Invoke();
                Close();
            });
        }

        public void AddNonClosingButton(GLFormattedLocalizedText label, Action onClick)
        {
            formHelper.AddCustomButton(label, () =>
            {
                onClick?.Invoke();
            });
        }

        private void SwapButtons()
        {
            var temp = formHelper.buttons.subelements[0];
            formHelper.buttons.subelements[0] = formHelper.buttons.subelements[1];
            formHelper.buttons.subelements[1] = temp;
        }

        public void PressNegativeButton()
        {
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[0];
            button.onPress?.Invoke();
        }

        public void PressCloseButton()
        {
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[0];
            button.onPress?.Invoke();
        }

        public void PressAcceptButton()
        {
            // the last button is always the accept button
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[formHelper.buttons.subelements.Count - 1];
            button.onPress?.Invoke();
        }

        public void PressPositiveButton()
        {
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[1];
            button.onPress?.Invoke();
        }

        public void PressSubmitButton()
        {
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[1];
            button.onPress?.Invoke();
        }

        public Setup OverrideCloseButtonText(GLFormattedLocalizedText label)
        {
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[0];
            button.labelText = label;
            return this;
        }

        public Setup OverrideNegativeButtonText(GLFormattedLocalizedText label)
        {
            return OverrideCloseButtonText(label);
        }

        public Setup OverridePositiveButtonText(GLFormattedLocalizedText label)
        {
            GLForm.ButtonElement button = (GLForm.ButtonElement)formHelper.buttons.subelements[formHelper.buttons.subelements.Count - 1];
            button.labelText = label;
            return this;
        }

        public Setup OverrideSubmitButtonText(GLFormattedLocalizedText label)
        {
            return OverridePositiveButtonText(label);
        }

        public bool IsShowing()
        {
            return popupWindow != null;
        }

        public Setup Show()
        {
            if (IsShowing())
            {
                throw new Exception("A window of this setup is already being shown");
            }
            var instance = I;
            popupWindow = GLPool.placeCopy(instance.popupWindowPrefab, instance.popupBase);
            popupWindow.Setup(this);
            PushPopup(this);

            return this;
        }

        public void SetData(string variable, string value)
        {
            if (popupWindow == null)
            {
                throw new Exception("Popup was not yet shown, so no form was available to set data to");
            }
            popupWindow.form.SetValue(variable, value);
        }
    }
}