﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLPopupWindow : MonoBehaviour {

    [Header("References")]
    public GLForm form;
    public Image typeIcon;
    
    private GLPopup.Setup popupSetup;

    public void Setup(GLPopup.Setup popupSetup)
    {
        this.popupSetup = popupSetup;
        form.SetupForm(popupSetup.formSetup);
        typeIcon.sprite = popupSetup.popupSystem.GetIcon(popupSetup.type);

        var rect = GetComponent<RectTransform>();
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.anchoredPosition = Vector2.zero;
        rect.sizeDelta = Vector2.zero;
    }

    public void OnBackgroundPress()
    {
        popupSetup.PressCloseButton();
    }

    public void CloseWindow()
    {
        GLPool.removeCopy(this);
    }
}
