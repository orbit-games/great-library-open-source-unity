﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAnnouncementsButton : MonoBehaviour {

    [Header("Localization")]
    public LocalizedText Label_TotalAnnouncements;
    public LocalizedText Label_OneAnnouncement;

    [Header("References")]
    public LocalizedTextBehaviour buttonLabelField;
    public GameObject buttonObject;

    private void OnEnable()
    {
        var announcements = GLApi.GetAnnouncementsKnowledge();
        if (announcements.IsNullOrEmpty())
        {
            buttonObject.SetActive(false);
        }
        else
        {
            buttonObject.SetActive(true);
            buttonLabelField.FormattedAsset = 
                announcements.Count == 1 
                    ? Label_OneAnnouncement 
                    : Label_TotalAnnouncements.Format(announcements.Count.ToString());
        }
    }

    public void OnPress()
    {
        GLAnnouncementManager.I.ShowAllAnnouncements();
    }
}
