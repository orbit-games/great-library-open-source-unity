﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAnnouncementManager : GLSingletonBehaviour<GLAnnouncementManager> {

    [Header("Localization")]
    public LocalizedText Title_Announcement;
    public LocalizedText Label_DontShowAgain;
    public LocalizedText Label_OpenLink;
    public LocalizedText Label_Close;

    [Header("Preference")]
    public GLSerializablePreference shownAnnouncementsCache;
    private ShownAnnouncements shownAnnouncements;

    [System.Serializable]
    public class ShownAnnouncements
    {
        public List<string> refs = new List<string>();
    }

    protected override void OnSingletonInitialize()
    {
        shownAnnouncementsCache.SetPreferenceType(typeof(ShownAnnouncements));
        shownAnnouncements = (ShownAnnouncements)shownAnnouncementsCache.Value;
        if (shownAnnouncements == null)
        {
            shownAnnouncements = new ShownAnnouncements();
        }
    }
    
    public void CheckAnnouncements(Action onFinished)
    {
        GLLoadingOverlay.ShowFullcoverLoading(GLApi.FetchAnnouncements(), (result) =>
            {
                if (!GLApi.DiplayErrorPopupIfFailed(result))
                {
                    var announcements = GLApi.GetAnnouncementsKnowledge();
                    RemoveOutdatedRefs(announcements);
                    HandleAnnouncements(0, announcements, false, onFinished);
                }
            }
        );
    }

    public void ShowAllAnnouncements()
    {
        var announcements = GLApi.GetAnnouncementsKnowledge();
        HandleAnnouncements(0, announcements, true, null);
    }

    private void RemoveOutdatedRefs(List<Announcement> currentAnnouncements)
    {
        var shown = shownAnnouncements.refs;
        shown.RemoveAll(shownAnnouncementRef =>
            null == currentAnnouncements.Find(announcement =>
                shownAnnouncementRef == announcement.Ref)
        );
        shownAnnouncements.refs = shown;
        shownAnnouncementsCache.Value = shownAnnouncements;
    }

    private bool IsAlreadyShown(string announcementRef)
    {
        return shownAnnouncements.refs.Contains(announcementRef);
    }

    private void SetShown(string announcementRef, bool shown)
    {
        if (shown)
        {
            shownAnnouncements.refs.AddUnique(announcementRef);
        }
        else
        {
            shownAnnouncements.refs.RemoveIfContains(announcementRef);
        }

        shownAnnouncementsCache.Value = shownAnnouncements;
    }

    private void HandleAnnouncements(int index, List<Announcement> ofAnnouncements, bool showAll, Action onFinished)
    {
        if (ofAnnouncements.Count == 0 || ofAnnouncements.Count <= index)
        {
            onFinished?.Invoke();
            return;
        }

        var announcement = ofAnnouncements[index];
        if (showAll || GLInput.IsHoldingCtrl() || !IsAlreadyShown(announcement.Ref))
        {
            var popupSetup = GLPopup.MakeDemandPopup(announcement.Title, announcement.Text, null, data =>
                {
                    SetShown(announcement.Ref, data["hide"] == bool.TrueString);
                    HandleAnnouncements(++index, ofAnnouncements, showAll, onFinished);
                }
            );

            popupSetup.OverrideSubmitButtonText(Label_Close);
            popupSetup.formHelper.AddCheckbox(Label_DontShowAgain, "hide", false);
            
            if (!announcement.Url.IsNullOrEmpty())
            {
                popupSetup.AddNonClosingButton(Label_OpenLink, () =>
                {
                    GLSystem.OpenURL(announcement.Url);
                });
            }

            popupSetup.Show();
            popupSetup.SetData("hide", IsAlreadyShown(announcement.Ref).ToString());
        }
        else
        {
            HandleAnnouncements(++index, ofAnnouncements, showAll, onFinished);
        }

    }
}
