﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class GLAbstractPreference : ScriptableObject
{
    public abstract void Initialize();
    public abstract string GetKey();
    public abstract void Clear();
    public abstract void ClearAll();
    public abstract JObject ToJSONObject();
    public abstract void SetCloudValue(string userRef, string courseRef, string value);
}
