﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Preference", menuName = "Great Library/String Preference")]
public class GLStringPreference : GLBasePreference<string, string>
{
    protected override bool IsDifferent(string oldLiveValue, string newLiveValue)
    {
        if (oldLiveValue == null) return newLiveValue != null;
        else return oldLiveValue != newLiveValue;
    }
    
    protected override string LoadStoredValue(string storageKey, string defaultStoredValue)
    {
        return PlayerPrefs.GetString(storageKey, defaultStoredValue);
    }

    protected override void SaveStoredValue(string storageKey, string value)
    {
        PlayerPrefs.SetString(storageKey, value);
    }
    
    protected override string LiveToStoredValue(string liveValue)
    {
        return liveValue;
    }

    protected override string StoredToLiveValue(string value)
    {
        return value;
    }
    
    protected override string StoredToCloudValue(string storedValue)
    {
        return storedValue;
    }
    protected override string CloudToStoredValue(string cloudValue)
    {
        return cloudValue;
    }

    public static GLStringPreference Create(string key, string defaultValue, bool isUserDependent,bool isCourseDependent, bool isCloudSyncable)
    {
        var pref = CreateInstance<GLStringPreference>();
        pref.Setup(key, defaultValue, isUserDependent, isCourseDependent, isCloudSyncable);
        return pref;
    }
}