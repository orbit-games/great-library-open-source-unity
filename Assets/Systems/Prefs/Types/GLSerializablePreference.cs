﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Preference", menuName = "Great Library/Serializable Preference")]
public class GLSerializablePreference : GLBasePreference<object, string>
{
    [Header("Additional Settings")]
    public string type;
    public Type initializedType = null;
    private Type GetPreferenceType()
    {
        if (initializedType == null)
        {
            initializedType = Type.GetType(type, true);
            // throws exception if the type is not found
        }
        return initializedType;
    }

    public void SetPreferenceType(Type type)
    {
        initializedType = type;
        this.type = type.ToString();
    }
    
    protected override bool IsDifferent(object oldLiveValue, object newLiveValue)
    {
        if (oldLiveValue == null) return newLiveValue != null;
        else return true; // just always assume they are different, because it is probably easier to just set it, than making and comparing a large json string for instance
        // !oldLiveValue.Equals(newLiveValue);
    }

    protected override string LoadStoredValue(string storageKey, string defaultStoredValue)
    {
        return PlayerPrefs.GetString(storageKey, defaultStoredValue);
    }

    protected override void SaveStoredValue(string storageKey, string value)
    {
        PlayerPrefs.SetString(storageKey, value);
    }

    protected override string LiveToStoredValue(object liveValue)
    {
        return JsonConvert.SerializeObject(liveValue);
    }

    protected override object StoredToLiveValue(string storedValue)
    {
        return JsonConvert.DeserializeObject(storedValue, GetPreferenceType());
    }

    protected override string StoredToCloudValue(string storedValue)
    {
        return storedValue;
    }
    protected override string CloudToStoredValue(string cloudValue)
    {
        return cloudValue;
    }

    public static GLSerializablePreference Create(string key, object defaultValue, bool isUserDependent, bool isCourseDependent, bool isCloudSyncable)
    {
        var pref = CreateInstance<GLSerializablePreference>();
        pref.Setup(key, defaultValue, isUserDependent, isCourseDependent, isCloudSyncable);
        return pref;
    }
}