﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Preference", menuName = "Great Library/Bool Preference")]
public class GLBoolPreference : GLBasePreference<bool, int>
{
    protected override bool IsDifferent(bool oldLiveValue, bool newLiveValue)
    {
        return oldLiveValue != newLiveValue;
    }

    protected override int LoadStoredValue(string storageKey, int defaultStoredValue)
    {
        return PlayerPrefs.GetInt(storageKey, defaultStoredValue);
    }

    protected override void SaveStoredValue(string storageKey, int value)
    {
        PlayerPrefs.SetInt(storageKey, value);
    }

    protected override int LiveToStoredValue(bool liveValue)
    {
        return liveValue ? 1 : 0;
    }

    protected override bool StoredToLiveValue(int storedValue)
    {
        return storedValue == 1;
    }

    protected override string StoredToCloudValue(int storedValue)
    {
        return storedValue == 1 ? "1" : "0";
    }
    protected override int CloudToStoredValue(string cloudValue)
    {
        return cloudValue == "1" ? 1 : 0;
    }

    public static GLBoolPreference Create(string key, bool defaultValue, bool isUserDependent, bool isCourseDependent, bool isCloudSyncable)
    {
        var pref = CreateInstance<GLBoolPreference>();
        pref.Setup(key, defaultValue, isUserDependent, isCourseDependent, isCloudSyncable);
        return pref;
    }
}