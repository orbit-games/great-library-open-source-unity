﻿using System;
using System.Globalization;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Preference", menuName = "Great Library/Int Preference")]
public class GLIntPreference : GLBasePreference<int, int>
{
    protected override bool IsDifferent(int oldLiveValue, int newLiveValue)
    {
        return oldLiveValue != newLiveValue;
    }

    protected override int LoadStoredValue(string storageKey, int defaultStoredValue)
    {
        return PlayerPrefs.GetInt(storageKey, defaultStoredValue);
    }

    protected override void SaveStoredValue(string storageKey, int value)
    {
        PlayerPrefs.SetInt(storageKey, value);
    }

    protected override int LiveToStoredValue(int liveValue)
    {
        return liveValue;
    }

    protected override int StoredToLiveValue(int storedValue)
    {
        return storedValue;
    }

    protected override string StoredToCloudValue(int storedValue)
    {
        return storedValue.ToString();
    }
    protected override int CloudToStoredValue(string cloudValue)
    {
        return int.Parse(cloudValue);
    }

    public static GLIntPreference Create(string key, int defaultValue, bool isUserDependent, bool isCourseDependent, bool isCloudSyncable)
    {
        var pref = CreateInstance<GLIntPreference>();
        pref.Setup(key, defaultValue, isUserDependent, isCourseDependent, isCloudSyncable);
        return pref;
    }
}