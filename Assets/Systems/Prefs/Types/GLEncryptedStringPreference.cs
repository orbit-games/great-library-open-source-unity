﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Preference", menuName = "Great Library/Encrypted String Preference")]
public class GLEncryptedStringPreference : GLBasePreference<string, string>
{
    protected override bool IsDifferent(string oldLiveValue, string newLiveValue)
    {
        if (oldLiveValue == null) return newLiveValue != null;
        else return oldLiveValue != newLiveValue;
    }

    protected override string LoadStoredValue(string storageKey, string defaultStoredValue)
    {
        return PlayerPrefs.GetString(storageKey, defaultStoredValue);
    }

    protected override void SaveStoredValue(string storageKey, string value)
    {
        PlayerPrefs.SetString(storageKey, value);
    }

    protected override string LiveToStoredValue(string liveValue)
    {
        return GLCryptography.Encrypt(liveValue);
    }

    protected override string StoredToLiveValue(string storedValue)
    {
        return GLCryptography.Decrypt(storedValue);
    }

    protected override string StoredToCloudValue(string storedValue)
    {
        return storedValue;
    }
    protected override string CloudToStoredValue(string cloudValue)
    {
        return cloudValue;
    }

    public static GLEncryptedStringPreference Create(string key, string defaultValue, bool isUserDependent, bool isCourseDependent, bool isCloudSyncable)
    {
        var pref = CreateInstance<GLEncryptedStringPreference>();
        pref.Setup(key, defaultValue, isUserDependent, isCourseDependent, isCloudSyncable);
        return pref;
    }
}