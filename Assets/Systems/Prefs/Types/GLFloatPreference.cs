﻿using System;
using System.Globalization;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Preference", menuName = "Great Library/Float Preference")]
public class GLFloatPreference : GLBasePreference<float, float>
{
    protected override bool IsDifferent(float oldLiveValue, float newLiveValue)
    {
        return oldLiveValue != newLiveValue;
    }

    protected override float LoadStoredValue(string storageKey, float defaultStoredValue)
    {
        return PlayerPrefs.GetFloat(storageKey, defaultStoredValue);
    }

    protected override void SaveStoredValue(string storageKey, float value)
    {
        PlayerPrefs.SetFloat(storageKey, value);
    }

    protected override float LiveToStoredValue(float liveValue)
    {
        return liveValue;
    }

    protected override float StoredToLiveValue(float storedValue)
    {
        return storedValue;
    }

    protected override string StoredToCloudValue(float storedValue)
    {
        return storedValue.ToString("G9", CultureInfo.InvariantCulture);
    }
    protected override float CloudToStoredValue(string cloudValue)
    {
        return float.Parse(cloudValue, CultureInfo.InvariantCulture);
    }

    public static GLFloatPreference Create(string key, float defaultValue, bool isUserDependent, bool isCourseDependent, bool isCloudSyncable)
    {
        var pref = CreateInstance<GLFloatPreference>();
        pref.Setup(key, defaultValue, isUserDependent, isCourseDependent, isCloudSyncable);
        return pref;
    }
}