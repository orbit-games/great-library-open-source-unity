﻿using IO.Swagger.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class GLBasePreference<TLive, TStored> : GLAbstractPreference
{
    [Serializable]
    private class State
    {
        [ReadOnly]
        [JsonIgnore]
        public GLBasePreference<TLive, TStored> preference;
        [ReadOnly]
        public string userRef;
        [ReadOnly]
        public string courseRef;
        [ReadOnly]
        public string valueStorageKey;
        [ReadOnly]
        public string cloudStorageKey;
        [ReadOnly]
        [JsonIgnore]
        public TLive liveValue;
        [ReadOnly]
        public TStored storedValue;
        [ReadOnly]
        public bool isCloudDirty;
        [NonSerialized]
        [JsonIgnore]
        public List<GLTask<Property>> syncTasks = new List<GLTask<Property>>();
        [NonSerialized]
        [JsonIgnore]
        public Action onSyncedListeners;

        public State(GLBasePreference<TLive, TStored> preference, string userRef, string courseRef)
        {
            this.preference = preference;
            this.userRef = userRef;
            this.courseRef = courseRef;
        }

        private bool initialized = false;
        public void Initialize()
        {
            if (initialized) return;
            initialized = true;

            DetermineKeys();
            LoadValues();
            LoadCloudDirty();
        }

        [JsonIgnore]
        public TLive Value
        {
            get
            {
                Initialize();
                return liveValue;
            }
            set
            {
                Initialize();
                if (preference.IsDifferent(liveValue, value))
                {
                    liveValue = value;
                    storedValue = preference.LiveToStoredValue(liveValue);
                    preference.SaveStoredValue(valueStorageKey, storedValue);

                    if (preference.isCloudSyncable)
                    {
                        SetCloudDirty(true);
                    }
                }
            }
        }

        private void SetCloudDirty(bool dirty)
        {
            Initialize();
            isCloudDirty = dirty;
            PlayerPrefs.SetInt(cloudStorageKey, isCloudDirty ? 1 : 0);
            HandleDirtyValue();
        }

        private void HandleDirtyValue()
        {
            Initialize();
            if (isCloudDirty)
            {
                GLTask<Property> task = null;
                if (preference.isCourseDependent && preference.isUserDependent)
                {
                    task = GLApi.SendPreference(userRef, courseRef, preference.key, new Property(
                        preference.key, preference.StoredToCloudValue(storedValue)
                    ));
                }
                else if (preference.isUserDependent)
                {
                    task = GLApi.SendPreference(userRef, preference.key, new Property(
                        preference.key, preference.StoredToCloudValue(storedValue)
                    ));
                }
                if (task != null)
                {
                    syncTasks.Add(task);
                    task.AddOnCompletedListener(result =>
                    {
                        syncTasks.Remove(task);
                    });
                }
            }
        }

        public void SetCloudValue(string cloudValue)
        {
            Initialize();
            SetCloudDirty(false);
            storedValue = preference.CloudToStoredValue(cloudValue);
            liveValue = preference.StoredToLiveValue(storedValue);
            preference.SaveStoredValue(valueStorageKey, storedValue);
        }

        private void LoadCloudDirty()
        {
            isCloudDirty = PlayerPrefs.GetInt(cloudStorageKey, 0) == 1;
            HandleDirtyValue();
        }

        private void LoadValues()
        {
            storedValue = preference.LoadStoredValue(valueStorageKey, preference.LiveToStoredValue(preference.defaultValue));
            liveValue = preference.StoredToLiveValue(storedValue);
        }
        
        private void DetermineKeys()
        {
            valueStorageKey = GetPlatformPrefix();
            if (preference.isUserDependent)
            {
                valueStorageKey += userRef;
            }
            if (preference.isCourseDependent)
            {
                valueStorageKey += courseRef;
            }
            valueStorageKey += preference.key;
            cloudStorageKey = "CloudDirty_" + valueStorageKey;
        }
    }

    [Header("Debug")]
    [Multiline(15)]
    [JsonIgnore]
    public List<string> prints = new List<string>();
    [Buttons("Print knowledge", "PrintStates")]
    [JsonIgnore]
    public ButtonsContainer printButton;

    [NonSerialized]
    [JsonIgnore]
    private bool initialized = false;

    [Header("States")]
    [SerializeField]
    [JsonProperty]
    private GLUserCourseDictionary<State> dependencyToState = new GLUserCourseDictionary<State>();
    [JsonProperty]
    private State activeState;

    [Header("Settings")]
    [SerializeField]
    private string key;
    [SerializeField]
    private TLive defaultValue;
    [SerializeField]
    private bool isCloudSyncable = true;
    [SerializeField]
    private bool isUserDependent = true;
    [SerializeField]
    private bool isCourseDependent = true;
    [Buttons("Clear Value", "ClearValue")]
    [JsonIgnore]
    public ButtonsContainer clearButtons;

    protected void Setup(string key, TLive defaultValue, bool isUserDependent, bool isCourseDependent, bool isCloudSyncable)
    {
        if (this.key != null && this.key != "" && this.key != key)
        {
            throw new System.Exception("Can't change the key value for a preference during runtime");
        }
        if (key == null || key != "")
        {
            throw new System.Exception("Preference key is invalid");
        }

        this.key = key;
        this.defaultValue = defaultValue;
        this.isUserDependent = isUserDependent;
        this.isCourseDependent = isCourseDependent;
        this.isCloudSyncable = isCloudSyncable;

        Initialize();
    }

    public override void Clear()
    {
        Value = defaultValue;
    }

    public override void ClearAll()
    {
        foreach (var state in dependencyToState.Values)
        {
            state.Value = defaultValue;
        }
    }

    public override void Initialize()
    {
        if (initialized) return;
        initialized = true;
        GLPrefs.I.RegisterPreference(this);
    }

    public void PrintStates()
    {
        prints.Clear();
        foreach (var kvp in dependencyToState)
        {
            var state = kvp.Value;
            string printedState = "";
            if (state.userRef != null)
            {
                var user = GLApi.GetUserKnowledge(state.userRef);
                if (user != null) printedState += user.FullName + ", " + user.Email + "\n" + user.Ref + "\n\n";
                else printedState += "Unknown user with ref " + state.userRef + "\n\n";
            }
            if (state.courseRef != null)
            {
                var course = GLApi.GetCourseKnowledge(state.courseRef);
                if (course != null) printedState += course.Name + " " + course.Year + " " + course.Quarter + "\n" + course.Ref + "\n\n";
                else printedState += "Unknown course with ref " + state.courseRef + "\n\n";
            }

            printedState += "--------------------\n";
            printedState += "liveValue = " + state.liveValue + "\n";
            printedState += "storedValue = " + state.storedValue + "\n";
            printedState += "--------------------\n";
            printedState += "valueStorageKey = " + state.valueStorageKey + "\n";
            printedState += "cloudStorageKey = " + state.cloudStorageKey + "\n";
            printedState += "isCloudDirty = " + state.isCloudDirty + "\n";
            prints.Add(printedState);
        }
    }

    public override string GetKey()
    {
        return key;
    }

    private Tuple<string, string> GetStateTuple(string userRef, string courseRef)
    {
        if (isUserDependent && (userRef == null || userRef == ""))
        {
            throw new Exception("Received no USER for grabbing a preference state, but it depends on it!");
        }

        if (isCourseDependent && (courseRef == null || courseRef == ""))
        {
            throw new Exception("Received no COURSE for grabbing a preference state, but it depends on it!");
        }

        return Tuple.Create(userRef, courseRef);
    }

    private State GetState(string userRef, string courseRef)
    {
        Initialize();
        var stateTuple = GetStateTuple(userRef, courseRef);
        if (dependencyToState.ContainsKey(stateTuple))
        {
            return dependencyToState[stateTuple];
        }

        State state = new State(this, userRef, courseRef);
        dependencyToState.Add(stateTuple, state);
        return state;
    }

    private IReadOnlyCollection<GLTask<Property>> GetSyncTasks(string userRef, string courseRef)
    {
        Initialize();
        var stateTuple = GetStateTuple(userRef, courseRef);
        if (dependencyToState.ContainsKey(stateTuple))
        {
            return dependencyToState[stateTuple].syncTasks.AsReadOnly();
        }

        State state = new State(this, userRef, courseRef);
        dependencyToState.Add(stateTuple, state);
        return state.syncTasks.AsReadOnly();
    }

    [JsonIgnore]
    public TLive Value
    {
        get
        {
            return GetState(GLApi.GetActiveUser()?.Ref, GLApi.GetActiveCourse()?.Ref).Value;
        }
        set
        {
            GetState(GLApi.GetActiveUser()?.Ref, GLApi.GetActiveCourse()?.Ref).Value = value;
        }
    }

    [JsonIgnore]
    public IReadOnlyCollection<GLTask<Property>> SyncTasks
    {
        get
        {
            return GetSyncTasks(GLApi.GetActiveUser()?.Ref, GLApi.GetActiveCourse()?.Ref);
        }
    }

    public TLive this[User user, Course course]
    {
        get
        {
            return GetState(user?.Ref, course?.Ref).Value;
        }
        set
        {
            GetState(user?.Ref, course?.Ref).Value = value;
        }
    }

    public TLive this[User user]
    {
        get
        {
            return GetState(user?.Ref, GLApi.GetActiveCourse()?.Ref).Value;
        }
        set
        {
            GetState(user?.Ref, GLApi.GetActiveCourse()?.Ref).Value = value;
        }
    }

    public TLive this[string user, string course]
    {
        get
        {
            return GetState(user, course).Value;
        }
        set
        {
            GetState(user, course).Value = value;
        }
    }

    public TLive this[string user]
    {
        get
        {
            return GetState(user, GLApi.GetActiveCourse()?.Ref).Value;
        }
        set
        {
            GetState(user, GLApi.GetActiveCourse()?.Ref).Value = value;
        }
    }

    public override void SetCloudValue(string userRef, string courseRef, string value)
    {
        GetState(userRef, courseRef).SetCloudValue(value);
    }

    public override JObject ToJSONObject()
    {
        JObject pref = GLDebug.ObjectToJSONObject(this);
        return pref;
    }

    protected abstract bool IsDifferent(TLive oldLiveValue, TLive newLiveValue);

    protected abstract TStored LoadStoredValue(string storageKey, TStored defaultStoredValue);
    protected abstract void SaveStoredValue(string storageKey, TStored value);
    
    protected abstract TStored LiveToStoredValue(TLive liveValue);
    protected abstract TLive StoredToLiveValue(TStored value);

    protected abstract string StoredToCloudValue(TStored storedValue);
    protected abstract TStored CloudToStoredValue(string cloudValue); 

    private static string GetPlatformPrefix()
    {
#if UNITY_EDITOR
        return "EDIT_";
#elif DEVELOPMENT_BUILD
        return "DEV_";
#else
        return "";
#endif
    }

}
