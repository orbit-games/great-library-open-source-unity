﻿using IO.Swagger.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Internal;

public class GLPrefs : GLSingletonBehaviour<GLPrefs>
{
    public List<GLAbstractPreference> preferences = new List<GLAbstractPreference>();
    private Dictionary<string, GLAbstractPreference> keyToPreference = new Dictionary<string, GLAbstractPreference>();

    [Header("State")]
    [ReadOnly]
    public User currentUser;
    [ReadOnly]
    public Course currentCourse;

    protected override void OnSingletonInitialize()
    {
        GLAbstractPreference[] foundPreferences = GLResources.FindObjectsOfTypeAll<GLAbstractPreference>();
        foreach (var preference in foundPreferences)
        {
            preference.Initialize();
        }
    }

    public static bool Exists(string key)
    {
        return GetInstance().keyToPreference.ContainsKey(key);
    }

    public void RegisterPreference(GLAbstractPreference preference)
    {
        if (Exists(preference.GetKey()))
        {
            throw new Exception("Preference with given key " + preference.GetKey() + " was already registered");
        }

        preferences.Add(preference);
        keyToPreference.Add(preference.GetKey(), preference);
    }

    public static GLAbstractPreference Get(string key)
    {
        var keyToPreference = GetInstance().keyToPreference;
        if (keyToPreference.ContainsKey(key))
            return keyToPreference[key];
        else return null;
    }

    public static string ToJSONString()
    {
        var serializer = GLDebug.I.jsonSerializer;
        JObject insights = new JObject();
        var preferences = I.preferences;
        foreach (var preference in preferences)
        {
            insights[preference.GetKey()] = preference.ToJSONObject();
        }
        return insights.ToString();
    }
}