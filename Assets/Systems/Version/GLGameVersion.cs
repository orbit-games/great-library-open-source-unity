﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Version", menuName = "Great Library/Game Version", order = 1)]
public class GLGameVersion : ScriptableObject
{
    [Header("Build Number")]
    public int buildID = 0;

    [Header("Version")]
    public int majorVersion = 2;
    public int minorVersion = 1;
    public int buildVersion = 0;
    [ReadOnly]
    public string version;

    [Header("Actions")]
    [Buttons("+Build", "IncreaseBuild")]
    public ButtonsContainer increaseBuild;
    [Buttons("+Major", "IncreaseMajor", "+Minor", "IncreaseMinor")]
    public ButtonsContainer increaseOther;
    public void IncreaseMajor() { majorVersion++; buildVersion = 0; updateVersion(); Save(); }
    public void IncreaseMinor() { minorVersion++; buildVersion = 0; updateVersion(); Save(); }
    [Buttons("Update & Save!", "UpdateVersion")]
    public ButtonsContainer saveMe;
    public void UpdateVersion() { updateVersion(); Save(); }

    private void updateVersion()
    {
        version = GetFullVersionString();
    }

    public string GetFullVersionString()
    {
        return majorVersion + "." + minorVersion + "." + buildVersion;
    }

    public string GetCombinedVersionString()
    {
        return GetFullVersionString() + " (" + buildID + ")";
    }

    public void IncreaseBuild()
    {
        buildID++;
        buildVersion++;
        updateVersion();
        Save();
    }
    public void Save()
    {
#if UNITY_EDITOR
        GLVersionManager.SetupPlayerSettings();
        UnityEditor.EditorUtility.SetDirty(this);
        UnityEditor.AssetDatabase.SaveAssets();
#endif
    }
}
