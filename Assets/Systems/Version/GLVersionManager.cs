﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IO.Swagger.Model;
using System;
using GameToolkit.Localization;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GLVersionManager : GLSingletonBehaviour<GLVersionManager>
{
#if UNITY_EDITOR
    public static GLGameVersion GetVersionObject()
    {
        var versions = AssetDatabase.FindAssets("t:GLGameVersion");
        if (versions.Length != 1)
        {
            throw new System.Exception("Could not find game version asset...");
        }
        var path = AssetDatabase.GUIDToAssetPath(versions[0]);
        return AssetDatabase.LoadAssetAtPath<GLGameVersion>(path);
    }

    [MenuItem("Held/Set Build Settings")]
    static public void SetupPlayerSettings()
    {
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Standalone, "nl.orbitgames.greatlibrary");
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "nl.orbitgames.greatlibrary");
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "nl.orbitgames.greatlibrary");
        PlayerSettings.keyaliasPass = GLSecretKeys.KeyAliasPass;
        PlayerSettings.keystorePass = GLSecretKeys.KeyStorePass;
        PlayerSettings.Android.keyaliasPass = GLSecretKeys.KeyAliasPass;
        PlayerSettings.Android.keystorePass = GLSecretKeys.KeyStorePass;
        PlayerSettings.Android.keystoreName = "GL";
        PlayerSettings.Android.keyaliasName = "GL";
        PlayerSettings.productName = "The Great Library";
        SetupVersion();
    }

    static public void SetupVersion()
    {
        var buildVersion = GetVersionObject();
        PlayerSettings.iOS.buildNumber = buildVersion.buildID.ToString();
        PlayerSettings.bundleVersion = buildVersion.GetFullVersionString();
        PlayerSettings.Android.bundleVersionCode = buildVersion.buildID;
    }

    [MenuItem("Held/Manage Version")]
    public static void ManageVersion()
    {
        var buildVersion = GetVersionObject();
        Selection.activeObject = buildVersion;
    }
#endif

#if UNITY_IOS
    public readonly Platform platform = Platform.IOS;
#elif UNITY_ANDROID
    public readonly Platform platform = Platform.ANDROID;
#elif UNITY_WEBGL
    public readonly Platform platform = Platform.WEBGL;
#elif UNITY_STANDALONE_WIN
    public readonly Platform platform = Platform.WIN;
#elif UNITY_STANDALONE_LINUX
    public readonly Platform platform = Platform.LINUX;
#elif UNITY_STANDALONE_OSX
    public readonly Platform platform = Platform.MACOS;
#elif UNITY_EDITOR_WIN
    public readonly Platform platform = Platform.WIN;
#elif UNITY_EDITOR_LINUX
    public readonly Platform platform = Platform.LINUX;
#elif UNITY_EDITOR_OSX
    public readonly Platform platform = Platform.MACOS;
#endif

    [Header("References")]
    public GLGameVersion currentGameVersion;

    [Header("Localization")]
    public LocalizedText Label_UpdateAvailable;
    public LocalizedText Label_UpdateRequired;
    public LocalizedText Label_DowngradeRequired;

    public LocalizedText Description_UpdateAvailable;
    public LocalizedText Description_UpdateRequired;
    public LocalizedText Description_DowngradeRequired;

    public LocalizedText Label_Download;
    public LocalizedText Label_Continue;
    public LocalizedText Label_Exit;

    [Header("Preferences")]
    public GLSerializablePreference gameVersionLogPreference;
    public GLSerializablePreference cachedVersionCheck;
    public GLIntPreference latestNotifiedBuildNumber;

    private GameVersionLog gameVersionLog;
    public class GameVersionLog
    {
        public Dictionary<int, ClientVersion> usedVersions;
        public HashSet<string> usedVersionStrings;
        public ClientVersion lastLaunchVersion;
        public ClientVersion currentVersion;
    }

    [Header("Results")]
    [ReadOnly]
    public bool retrieved = false;
    [ReadOnly]
    private ClientVersionCheckResponseStatus gameVersionStatus;
    [ReadOnly]
    private ClientVersionCheckResponse gameVersionCheck;
    [ReadOnly]
    private bool userShouldBeNotified;
    [ReadOnly]
    public bool loading = false;
    
    protected override void OnSingletonInitialize()
    {
        cachedVersionCheck.SetPreferenceType(typeof(ClientVersionCheckResponse));
        gameVersionCheck = cachedVersionCheck.Value as ClientVersionCheckResponse;
        gameVersionLogPreference.SetPreferenceType(typeof(GameVersionLog));
        gameVersionLog = (GameVersionLog)gameVersionLogPreference.Value;

        if (gameVersionLog == null)
        {
            gameVersionLog = new GameVersionLog();
            gameVersionLog.usedVersions = new Dictionary<int, ClientVersion>();
            gameVersionLog.usedVersionStrings = new HashSet<string>();
        }
    }

    public ClientVersion GetPreviousLaunchVersion()
    {
        return gameVersionLog.lastLaunchVersion;
    }

    public bool IsBuildWithBuildNumberUsedOnDevice(int buildNumber)
    {
        return gameVersionLog.usedVersions.ContainsKey(buildNumber);
    }

    public bool IsBuildWithVersionUsedOnDevice(string version)
    {
        return gameVersionLog.usedVersionStrings.Contains(version);
    }

    public void CheckVersion(Action onFinished)
    {
        PerformCheck();

        loading = true;

        GLLoadingOverlay.ShowFullcoverLoading(
            GLApi.CheckVersion(currentGameVersion.buildID, platform),
            (result) =>
            {
                loading = false;

                if (!result.Failed)
                {
                    retrieved = true;
                    gameVersionCheck = (ClientVersionCheckResponse)result.Result;
                    cachedVersionCheck.Value = gameVersionCheck;

                    if (gameVersionCheck != null && gameVersionCheck.Current != null)
                    {
                        var currentVersion = gameVersionCheck.Current;
                        gameVersionLog.usedVersions.AddUnique(currentVersion.BuildNumber.Value, currentVersion);
                        gameVersionLog.usedVersionStrings.AddUnique(currentVersion.Version);
                        gameVersionLog.lastLaunchVersion = gameVersionLog.currentVersion;
                        gameVersionLog.currentVersion = currentVersion;
                        gameVersionLogPreference.Value = gameVersionLog;
                    }

                    PerformCheck();

                    if (ShouldUpdate())
                    {
                        Debug.Log("New version available! " + GetLatestVersion());
                        if (ShouldUserBeNotified())
                        {
                            HandleUpdateNotification(onFinished);
                        }
                        else
                        {
                            onFinished?.Invoke();
                        }
                    }
                    else
                    {
                        Debug.Log("Current version is up to date: " + GetLatestVersion());
                        onFinished?.Invoke();
                    }
                }
                else
                {
                    retrieved = false;
                    onFinished?.Invoke();
                    Debug.LogError(result.ErrorMessage);
                }
            }
        );
    }

    void HandleUpdateNotification(Action onFinished)
    {
        latestNotifiedBuildNumber.Value = gameVersionCheck.Latest.BuildNumber.Value;
        switch (gameVersionStatus)
        {
            case ClientVersionCheckResponseStatus.UPDATEAVAILABLE:
                NotifyUpdateAvailable(onFinished);
                break;
            case ClientVersionCheckResponseStatus.UPDATEREQUIRED:
                NotifyUpdateRequired(onFinished);
                break;
            case ClientVersionCheckResponseStatus.ROLLBACK:
                NotifyDowngradeRequired(onFinished);
                break;
        }
    }

    void NotifyUpdateAvailable(Action onFinished)
    {
        var popup = GLPopup.MakeNotificationPopup(Label_UpdateAvailable, Description_UpdateAvailable, onFinished);
        popup.formHelper.AddPlainText((gameVersionCheck.Latest.ReleaseNotes));
        popup.OverrideCloseButtonText(Label_Continue);
        popup.formHelper.AddCustomButton(Label_Download, () =>
        {
            GLSystem.OpenURL(gameVersionCheck.Latest.Url);
        });
        popup.formHelper.AddCustomButton(Label_Exit, () =>
        {
            Application.Quit();
        });
        popup.Show();
    }

    void NotifyUpdateRequired(Action onFinished)
    {
        var popup = GLPopup.MakeErrorPopup(Label_UpdateRequired, Description_UpdateRequired, () =>
        {
            GLSystem.OpenURL(gameVersionCheck.Latest.Url);
        });
        popup.formHelper.AddPlainText((gameVersionCheck.Latest.ReleaseNotes));
        popup.OverrideCloseButtonText(Label_Download);
        popup.formHelper.AddCustomButton(Label_Exit, () =>
        {
            Application.Quit();
        });
        popup.SetClosable(false);
        popup.Show();
    }

    void NotifyDowngradeRequired(Action onFinished)
    {
        var popup = GLPopup.MakeErrorPopup(Label_DowngradeRequired, Description_DowngradeRequired, () =>
        {
            GLSystem.OpenURL(gameVersionCheck.Latest.Url);
        });
        popup.formHelper.AddPlainText((gameVersionCheck.Latest.ReleaseNotes));
        popup.OverrideCloseButtonText(Label_Download);
        popup.formHelper.AddCustomButton(Label_Exit, () =>
        {
            Application.Quit();
        });
        popup.SetClosable(false);
        popup.Show();
    }

    void PerformCheck()
    {
        if (gameVersionCheck == null)
        {
            gameVersionStatus = ClientVersionCheckResponseStatus.OK;
            userShouldBeNotified = false;
        }
        else
        {
            gameVersionStatus = gameVersionCheck.Status.Value;
            switch (gameVersionStatus)
            {
                case ClientVersionCheckResponseStatus.OK:
                    userShouldBeNotified = false;
                    break;
                case ClientVersionCheckResponseStatus.UPDATEAVAILABLE:
                    userShouldBeNotified = latestNotifiedBuildNumber.Value != gameVersionCheck.Latest.BuildNumber.Value;
                    break;
                case ClientVersionCheckResponseStatus.UPDATEREQUIRED:
                    userShouldBeNotified = true;
                    break;
                case ClientVersionCheckResponseStatus.ROLLBACK:
                    userShouldBeNotified = true;
                    break;
                default:
                    userShouldBeNotified = false;
                    break;
            }
        }
    }
    
    public string GetCurrentVersion()
    {
        return currentGameVersion.GetFullVersionString();
    }

    public string GetCurrentCombinedVersion()
    {
        return currentGameVersion.GetCombinedVersionString();
    }

    public string GetLatestVersion()
    {
        return gameVersionCheck == null ? "Unknown" : gameVersionCheck.Latest.Version;
    }

    public bool ShouldUpdate()
    {
        return gameVersionStatus != ClientVersionCheckResponseStatus.OK;
    }

    public bool ShouldUserBeNotified()
    {
        return userShouldBeNotified;
    }

    public void SetUserNotified()
    {
        userShouldBeNotified = false;
        latestNotifiedBuildNumber.Value = gameVersionCheck.Latest.BuildNumber.Value;
    }
}
