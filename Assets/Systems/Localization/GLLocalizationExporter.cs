﻿using GameToolkit.Localization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class GLLocalizationExporter : MonoBehaviour
{
    public string localizationFolder = "Assets/Resources/Localization/";
    public string schemaPath = "Localization/schema.json";
    public string databasePath = "Localization/database.json";

    [Buttons("Export", "Export", "Import", "Import")]
    public ButtonsContainer buttons;

    public void Import()
    {
#if UNITY_EDITOR
        GLResources.LoadAll(force: true);

        StreamReader reader = new StreamReader(databasePath, false);
        string rawData = reader.ReadToEnd();
        reader.Close();

        var data = JObject.Parse(rawData);
        foreach (var table in data)
        {
            foreach (var dataEntry in (JObject)table.Value)
            {
                if (dataEntry.Key == "live" || dataEntry.Key == "drafts" || dataEntry.Key == "trash" || dataEntry.Key == "modifications")
                {
                    continue;
                }
                var dataObject = (JObject)dataEntry.Value;
                var fullPath = (string)dataObject["path"];
                var text = (string)dataObject["text_english"];
                var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<LocalizedText>(fullPath);

                if (asset == null)
                {
                    Debug.LogError("Could not find asset " + fullPath + ". You may import the defined text manually:\n\n" + text + "\n\n");
                }
                else
                {
                    asset.SetText(text);

                    // mark as dirty!
                    UnityEditor.EditorUtility.SetDirty(asset);
                }
            }
        }

        // save all changes
        UnityEditor.AssetDatabase.SaveAssets();
#endif
    }
    
    public void Export()
    {
#if UNITY_EDITOR
        GLResources.LoadAll(force: true);

        var helper = new ExporterHelper("Great Library Localization");

        var localizedTexts = Resources.FindObjectsOfTypeAll<LocalizedText>();
        localizationFolder = localizationFolder.Trim();

        foreach (var localizedText in localizedTexts)
        {
            var path = UnityEditor.AssetDatabase.GetAssetPath(localizedText).Trim();
            if (!path.StartsWith(localizationFolder))
            {
                Debug.LogError("Found localized text asset outside of " + localizationFolder + ". Asset path was: " + path);
            }
            else if (!path.EndsWith(".asset"))
            {
                Debug.LogError("Found localized text asset that did not end with .asset, namely: " + path);
            }
            else if (path.Contains(">"))
            {
                Debug.LogError("Path of localized text may not contain a '>' character. See: " + path);
            }
            else
            {
                var stripped = path.Substring(localizationFolder.Length, path.Length - localizationFolder.Length - (".asset").Length);
                var split = new List<string>(stripped.Split('/'));
                if (split.Count < 2)
                {
                    Debug.LogError("Found localized text asset that was not placed in a subfolder of the localization folder. Namely: " + path);
                }
                else
                {
                    var tableName = split[0];
                    var assetKey = split.Last();
                    
                    split.RemoveAt(0);
                    split.RemoveAt(split.Count - 1);

                    var assetSubfolder = string.Join("/", split);

                    helper.AddText(tableName, path, assetSubfolder, assetKey, localizedText.Value);
                }
            }
        }

        string schemaPath = "Localization/schema.json";
        string databasePath = "Localization/database.json";

        StreamWriter writer = new StreamWriter(schemaPath, false);
        writer.Write(helper.schema.ToString());
        writer.Close();

        writer = new StreamWriter(databasePath, false);
        writer.Write(helper.data.ToString());
        writer.Close();
#endif
    }

    private class ExporterHelper
    {
        private enum EditSetting
        {
            DisabledCompletely, DuringRowEditingOnly, AlwaysShowEditor
        }

        public JObject schema;
        public JObject data;

        private HashSet<string> tables = new HashSet<string>();

        public ExporterHelper(string databaseName)
        {
            schema = GenerateBaseSchemaObject(databaseName);
            data = GenerateBaseDataObject();
        }

        public void AddText(string tableName, string assetPath, string assetSubfolder, string localizationKey, string localizationText)
        {
            if (!tables.Contains(tableName))
            {
                CreateTable(tableName);
            }

            var tableData = (JObject)data[tableName];
            var tableLiveData = (JArray)tableData["live"];
            var id = assetSubfolder.Replace("/", "_") + localizationKey;
            var entry = GenerateBaseDataEntryObject(id, tableName);

            entry["path"] = assetPath;
            entry["subfolder"] = assetSubfolder;
            entry["key"] = localizationKey;
            entry["text_english"] = localizationText;

            tableData[id] = entry;
            tableLiveData.Add(id);
        }

        private void CreateTable(string tableName)
        {
            var tableSchema = GenerateBaseTableSchema(tableName, tableName, "subfolder", "key");
            var tableData = GenerateBaseTableDataObject();

            var columns = new JArray();
            columns.Add(GenerateTextSingleLineColumnSchema("subfolder", "Subfolder", EditSetting.DisabledCompletely));
            columns.Add(GenerateTextSingleLineColumnSchema("key", "Key", EditSetting.DisabledCompletely));
            columns.Add(GenerateTextSimpleColumnSchema("text_english", "English text", EditSetting.AlwaysShowEditor));
            tableSchema["columns"] = columns;
            tables.Add(tableName);

            var tablesArray = ((JArray)schema["tables"]);
            tablesArray.Add(tableSchema);
            data[tableName] = tableData;
        }

        private static JObject GenerateBaseDataEntryObject(string id, string tableID)
        {
            var dataEntry = new JObject();
            dataEntry["__id"] = id;

            var meta = new JObject();
            meta["tableID"] = tableID;
            meta["draft"] = false;
            meta["container"] = 1;
            meta["modification"] = 0;
            dataEntry["__meta"] = meta;
            
            return dataEntry;
        }

        private static JObject GenerateBaseTableDataObject()
        {
            var tableData = new JObject();
            tableData["live"] = new JArray();
            tableData["drafts"] = new JArray();
            tableData["trash"] = new JArray();
            return tableData;
        }

        private static JObject GenerateBaseSchemaObject(string name)
        {
            JObject schema = new JObject();
            JArray tables = new JArray();

            schema["name"] = name;
            schema["tables"] = tables;

            return schema;
        }

        private static JObject GenerateBaseDataObject()
        {
            JObject data = new JObject();
            return data;
        }

        private static JObject GenerateBaseTableSchema(string id, string name, string sortColumn1, string sortColumn2)
        {
            JObject table = new JObject();
            table["id"] = id;
            table["icon"] = "zmdi zmdi-file-text";
            table["name"] = name;
            table["hidden"] = false;
            table["viewPageDisabled"] = true;
            table["editPageDisabled"] = true;
            table["newPageDisabled"] = true;
            table["displayColumns"] = new JArray("key");
            table["sortColumn1"] = sortColumn1;
            table["sortColumn2"] = sortColumn2;

            return table;
        }

        private static JObject GenerateTextSimpleColumnSchema(string id, string name, EditSetting editSetting)
        {
            JObject column = GenerateBaseColumnSchema("TextSimpleColumn", id, name, editSetting);
            column["minimumLength"] = "0";
            column["maximumLength"] = "";
            column["lines"] = 3;
            column["maxListingCharacterCount"] = 255;
            return column;
        }

        private static JObject GenerateTextSingleLineColumnSchema(string id, string name, EditSetting editSetting)
        {
            JObject column = GenerateBaseColumnSchema("TextSingleLineColumn", id, name, editSetting);
            column["minimumLength"] = "0";
            column["maximumLength"] = "255";
            column["prefix"] = "";
            column["suffix"] = "";
            column["regex"] = ".*";
            column["maxListingCharacterCount"] = 255;
            return column;
        }

        private static JObject GenerateBaseColumnSchema(string type, string id, string name, EditSetting editSetting)
        {
            JObject column = new JObject();
            column["id"] = id;
            column["type"] = type;
            column["name"] = name;

            var editing = new JObject();
            editing["tooltip"] = "";
            editing["uniqueValue"] = false;
            editing["required"] = false;
            editing["disabled"] = editSetting == EditSetting.DisabledCompletely;
            column["editing"] = editing;

            var listing = new JObject();
            listing["show"] = true;
            listing["width"] = "150px";
            listing["groupable"] = false;
            listing["nowrap"] = false;
            listing["listEditor"] = editSetting == EditSetting.AlwaysShowEditor;
            column["listing"] = listing;

            var viewing = new JObject();
            editing["size"] = "normal";
            editing["header"] = "";
            column["viewing"] = viewing;

            return column;
        }
    }
}


