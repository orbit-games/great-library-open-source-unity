﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLFormattedLocalizedText {

    public LocalizedText LocalizedAsset;
    public string[] FormatArgs;
    
    public static implicit operator GLFormattedLocalizedText(LocalizedText d)
    {
        return new GLFormattedLocalizedText
        {
            LocalizedAsset = d,
            FormatArgs = new string[] { }
        };
    }
    
    public static implicit operator GLFormattedLocalizedText(string d)
    {
        return GLLocalization.Create(d);
    }

    public override string ToString()
    {
        return string.Format(LocalizedAsset.Value, FormatArgs);
    }
}
