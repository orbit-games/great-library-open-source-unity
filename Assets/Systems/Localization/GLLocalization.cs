﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLLocalization : GLSingletonBehaviour<GLLocalization> {

    protected override void OnSingletonInitialize()
    {

    }

    public LocalizedText emptyText;
    public LocalizedText variableText;
    public GLFormattedLocalizedText emptyFormattedText;

    public static LocalizedText EmptyText
    {
        get
        {
            return I.emptyText;
        }
    }

    public static GLFormattedLocalizedText EmptyFormattedText
    {
        get
        {
            if (I.emptyFormattedText == null)
            {
                I.emptyFormattedText = new GLFormattedLocalizedText
                {
                    LocalizedAsset = I.emptyText,
                    FormatArgs = new string[0] { }
                };
            }
            return I.emptyFormattedText;
        }
    }

    public static LocalizedText CreateLocalizedTextAsset(string text)
    {
        LocalizedText localized = ScriptableObject.CreateInstance<LocalizedText>();
        localized.SetText(text);
        return localized;
    }

    public static GLFormattedLocalizedText Create(string text)
    {
        if (text.IsNullOrEmpty())
        {
            return EmptyFormattedText;
        }
        else
        {
            return new GLFormattedLocalizedText
            {
                LocalizedAsset = I.variableText,
                FormatArgs = new string[1] { text }
            };
        }
    }
}

public static class LocalizedTextExtenstions
{
    public static GLFormattedLocalizedText Format(this LocalizedText asset, params string[] text)
    {
        string[] formatArgs = new string[text.Length];
        text.CopyTo(formatArgs, 0);
        return new GLFormattedLocalizedText
        {
            LocalizedAsset = asset,
            FormatArgs = formatArgs
        };
    }
}