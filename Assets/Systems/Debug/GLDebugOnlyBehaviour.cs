﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLDebugOnlyBehaviour : MonoBehaviour {

    private void OnEnable()
    {
        if (!GLDebug.IsDebugMode)
        {
            gameObject.SetActive(false);
        }
    }
}
