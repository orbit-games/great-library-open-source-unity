﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Analytics;

public class GLDebug : GLSingletonBehaviour<GLDebug> {

    public static bool IsDebugMode
    {
        get
        {
            return Application.isEditor || Debug.isDebugBuild;
        }
    }

    [Header("Preferences")]
    public GLStringPreference lastEmailUsed;

    [Header("Localization")]
    public LocalizedText Title_Feedback;
    public LocalizedText Description_Feedback;
    public LocalizedText Label_EmailField;
    public LocalizedText Label_FeedbackField;
    public LocalizedText Label_AgreeToSendAllData;
    public LocalizedText Label_SendFeedback;
    public LocalizedText Label_TotalReportSize;
    public LocalizedText Label_LookAtReportFiles;
    public LocalizedText Label_ChatWithDevelopers;
    [Space(10f)]
    public LocalizedText Description_FailedSending;
    [Space(10f)]
    public LocalizedText Description_AlreadySentReportBefore;
    [Space(10f)]
    public LocalizedText Description_CantSendReportWithoutData;
    [Space(10f)]
    public LocalizedText Title_ThanksForReporting;
    public LocalizedText Description_ThanksForReporting;

    [Header("Settings")]
    public float timePerScreenshot = 120f;
    public float timePerPing = 120f;
    public float timePerReport = 120f;
    public int totalScreenshots = 3;

    private float lastReportTime = float.MinValue;

    [Space(10f)]
    public string knowledgeFile = "knowledge.txt";
    public string preferencesFile = "preferences.txt";
    public string gameLogFile = "game-log.txt";
    public string completeReportFile = "complete-report.txt";
    public string screenshotFile = "history-screenshot-{0}.png";
    public string reportScreenshotFile = "report-screenshot.png";
    public string communicationLogFile = "communication-log.txt";
    public string deviceFile = "device.txt";

    [Header("Debug")]
    [Buttons("Dump", "Dump", "Report", "ShowProblemReporter")]
    public ButtonsContainer buttons;
    
    public JsonSerializerSettings jsonSerializerSettings;
    public JsonSerializer jsonSerializer;

    private string FullDebugFolderPath
    {
        get
        {
            return GLFiles.DebugReportFolder;
        }
    }
    
    /// <summary>
    /// Initialize Debug folder, and 
    /// </summary>
    protected override void OnSingletonInitialize()
    {
        jsonSerializerSettings = new JsonSerializerSettings
        {
            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            Formatting = Formatting.Indented
        };
        jsonSerializer = JsonSerializer.Create(jsonSerializerSettings);

        Application.logMessageReceived += HandleLog;
        StartPingReporter();
        StartScreenshotLogger();
        StartFPSLogger();
    }

    private bool aboutToShowReporter = false;
    private bool showingReporter = false;
    public void ShowProblemReporter()
    {
        if (aboutToShowReporter) return;

        if (lastReportTime + timePerReport > Time.realtimeSinceStartup)
        {
            var secondsLeft = Mathf.RoundToInt((lastReportTime + timePerReport) - Time.realtimeSinceStartup);
            GLPopup.MakeNotificationPopup(Description_AlreadySentReportBefore.Format(secondsLeft.ToString())).Show();
            return;
        }

        aboutToShowReporter = true;
        // log a report screenshot now
        LogReportScreenshot();
        GLRun.Delayed(0.5f, ShowProblemReporterAfterScreenshot);
    }
    
    private void ShowProblemReporterAfterScreenshot()
    {
        aboutToShowReporter = false;
        showingReporter = true;

        var popup = GLPopup.MakePromptPopup(Title_Feedback, Description_Feedback, null, SendReport, () =>
        {
            showingReporter = false;
        });
        var reportToSend = GenerateReport();

        var totalReport = reportToSend.ToJson();
        var byteCount = System.Text.Encoding.ASCII.GetByteCount(totalReport);
        var kb = (byteCount / 1024f).toNumberString(1);

        popup.formHelper.AddTextField(Label_EmailField, "email");
        popup.formHelper.AddTextArea(Label_FeedbackField, "feedback", null, 150, 500);
        popup.formHelper.AddCheckbox(Label_AgreeToSendAllData, "agree", false);
        popup.formHelper.AddPlainText(Label_TotalReportSize.Format(kb));
        popup.OverrideSubmitButtonText(Label_SendFeedback);
        popup.formHelper.AddCustomButton(Label_LookAtReportFiles, () => { ShowReportFiles(); });

        if (GLApi.IsLoggedInAndCourseSelected())
        {
            popup.formHelper.AddCustomButton(Label_ChatWithDevelopers, () => { ChatWithDeveloper(); popup.Close(); });
        }

        popup.Show();
        popup.SetData("email", lastEmailUsed.Value);
    }

    private void ShowReportFiles()
    {
        Dump();
        GLSystem.OpenURL(FullDebugFolderPath);
    }

    private void ChatWithDeveloper()
    {
        if (GLApi.IsLoggedInAndCourseSelected())
        {
            var allConversationInsights = GLInsights.GetAllConversationsInsight();
            foreach (var conversationInsight in allConversationInsights.conversationInsights)
            {
                if (conversationInsight.conversation.Type == ConversationType.TECHNICALSUPPORT)
                {
                    GLGameFlow.I.ShowChatWithConversation(conversationInsight.conversation.Ref);
                    return;
                }
            }
        }
    }

    private BugReport GenerateReport(string email = "", string feedback = "")
    {
        var device = GetAndWriteDeviceJSON();
        var gameLogs = GetAndWriteGameLogsJSON();
        var communicationLogs = GetAndWriteCommunicationLogsJSON();
        var knowledge = GetAndWriteKnowledgeJSON();
        var prefs = GetAndWritePreferencesJSON();
        var screenshots = new List<BugReportScreenshot>();

        var reportScreenshotBlob = GetScreenshotBlob(Path.Combine(FullDebugFolderPath, reportScreenshotFile));
        screenshots.Add(new BugReportScreenshot(DateTime.Now, reportScreenshotBlob));

        for (int i = 0; i < screenshotFiles.Count; i++)
        {
            var blob = GetScreenshotBlob(screenshotFiles[i]);
            screenshots.Add(new BugReportScreenshot(screenshotTimes[i], blob));
        }

        for (int i = 0; i < recordingScreens.Count; i++)
        {
            screenshots.Add(new BugReportScreenshot(recordingTimes[i], recordingScreens[i]));
        }

        var report = new BugReport(email, feedback, device, gameLogs, communicationLogs, screenshots, prefs, knowledge, DateTime.Now);

        if (IsDebugMode)
        {
            Write(completeReportFile, report.ToJson());
        }

        return report;
    }

    private void SendReport(GLForm.Data data)
    {
        showingReporter = false;
        if (data["agree"] == bool.FalseString)
        {
            GLPopup.MakeErrorPopup(Description_CantSendReportWithoutData).Show();
        }
        else
        {
            var report = GenerateReport(data["email"], data["feedback"]);
            GLLoadingOverlay.ShowFullcoverLoading(GLApi.SendBugReport(report), OnReportSendResponse);
        }
    }

    private void OnReportSendResponse(GLTaskResult result)
    {
        if (result.Failed)
        {
            GLPopup.MakeErrorPopup(Description_FailedSending.Format(FullDebugFolderPath)).Show();
        }
        else
        {
            lastReportTime = Time.realtimeSinceStartup;
            GLPopup.MakeNotificationPopup(Title_ThanksForReporting, Description_ThanksForReporting).Show();
        }
    }

    #region logs

    class BaseLog
    {
        public DateTime time;
        public float realtimeSinceStartup;
        public float runningAverageTime;
        public float runningAverageFPS;
        public string screenName;
        public string currentHoverObject;
        public string lastInteractedObject;
        public float lastInteractionSecondsAgo;
        public object logContents;
    }

    private BaseLog CreateLogEntry()
    {
        BaseLog log = new BaseLog();
        log.time = DateTime.Now;
        log.realtimeSinceStartup = Time.realtimeSinceStartup;
        log.currentHoverObject = GLCursor.I.GetCurrentObjectPath();
        log.lastInteractedObject = GLCursor.I.GetLastInteractedObjectPath();
        log.lastInteractionSecondsAgo = Time.realtimeSinceStartup - GLCursor.I.GetLastInteractionTime();
        log.runningAverageTime = runningAverageTime;
        log.runningAverageFPS = runningAverageFPS;
        log.screenName = GLGameFlow.I.GetScreenName();
        return log;
    }

    private List<BaseLog> gameLogs = new List<BaseLog>();
    private List<BaseLog> communicationLogs = new List<BaseLog>();
    private class LogMessage
    {
        public string logString;
        public string stackTrace;
        public string type;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        var logEntry = CreateLogEntry();
        logEntry.logContents = new LogMessage()
        {
            logString = logString,
            stackTrace = (type == LogType.Error || type == LogType.Exception) ? stackTrace : "",
            type = type.ToString()
        };

        if (logString.StartsWith(GLApi.I.communicationSentString) || logString.StartsWith(GLApi.I.communicationReceivedString))
        {
            // communicationLogs.Add(logEntry);
        }
        else
        {
            gameLogs.Add(logEntry);
        }
    }

    private class CommunicationSentLog
    {
        public string method;
        public string path;
        public object body;
        public object queryParams;
        public object headerParams;
        public object formParams;
        public object fileParams;
        public object pathParams;
        public object contentType;
    }

    private class CommunicationReceivedLog
    {
        public string method;
        public string path;
        public bool IsSuccessful;
        public string StatusCode;
        public string ResponseStatus;
        public string StatusDescription;
        public object Content;
        public object ErrorException;
        public string ErrorMessage;
        public object Headers;
    }

    public void LogCommunicationSent(string method, string path, object body, object queryParams, object headerParams, object formParams, object fileParams, object pathParams, object contentType)
    {
        var logEntry = CreateLogEntry();
        logEntry.logContents = new CommunicationSentLog()
        {
            method = method,
            path = path,
            body = body,
            queryParams = queryParams,
            headerParams = headerParams,
            formParams = formParams,
            fileParams = fileParams,
            pathParams = pathParams,
            contentType = contentType
        };
        communicationLogs.Add(logEntry);
    }

    public void LogCommunicationReceived(string method, string path, bool IsSuccessful, string StatusCode, string ResponseStatus, string StatusDescription, object Content, object ErrorException, string ErrorMessage, object Headers)
    {
        var logEntry = CreateLogEntry();
        logEntry.logContents = new CommunicationReceivedLog()
        {
            method = method,
            path = path,
            IsSuccessful = IsSuccessful,
            StatusCode = StatusCode,
            ResponseStatus = ResponseStatus,
            StatusDescription = StatusDescription,
            Content = Content,
            ErrorException = ErrorException,
            ErrorMessage = ErrorMessage,
            Headers = Headers
        };
        communicationLogs.Add(logEntry);
    }

    #endregion
    #region periodic loggers

    [Serializable]
    private class FPSLog
    {
        public float frameTimeNow;
        public string type;
    }

    private float lastLowFPSWarning;
    private float runningAverageTime;
    [ReadOnly]
    public float runningAverageFPS;

    private void StartFPSLogger()
    {
        StartCoroutine(FPSRunner());
    }

    private IEnumerator FPSRunner()
    {
        // don't need the first few seconds
        yield return new WaitForSeconds(2f);

        runningAverageTime = Time.unscaledDeltaTime;
        runningAverageFPS = 1f / Mathf.Max(0.00001f, runningAverageTime);
        lastLowFPSWarning = float.MinValue;

        while (true)
        {
            yield return null;

            var expectedAverageFPS = Application.targetFrameRate <= 0 ? 60f : Application.targetFrameRate;
            var expectedAverageTime = 1f / expectedAverageFPS;

            runningAverageTime = Mathf.Min(expectedAverageTime, runningAverageTime);
            runningAverageFPS = Mathf.Min(expectedAverageFPS, runningAverageFPS);

            var frameTime = Time.unscaledDeltaTime;
            if (frameTime > Mathf.Max(runningAverageTime * 1.75f, runningAverageTime + 0.02f))
            {
                LogFPS("FPSSpike", frameTime);
            }

            runningAverageTime = runningAverageTime * 0.95f + frameTime * 0.05f;
            runningAverageFPS = 1f / Mathf.Max(0.00001f, runningAverageTime);

            if (runningAverageFPS < expectedAverageFPS * 0.75f && runningAverageFPS < 30f)
            {
                if (lastLowFPSWarning < Time.realtimeSinceStartup - 30f)
                {
                    lastLowFPSWarning = Time.realtimeSinceStartup;
                    LogFPS("FPSLow", frameTime);
                }
            }
        }
    }

    private void LogFPS(string tag, float frameTime)
    {
        //AnalyticsEvent.Custom(tag, new Dictionary<string, object>
        //{
        //    { "frameTimeNow", frameTime },
        //    { "runningAverageTime", runningAverageTime },
        //    { "runningAverageFPS", runningAverageFPS },
        //    { "screenName", GLGameFlow.I.GetScreenName() },
        //    { "timeSinceStartup",  Time.realtimeSinceStartup }
        //});

        var logEntry = CreateLogEntry();
        logEntry.logContents = new FPSLog()
        {
            frameTimeNow = frameTime,
            type = tag
        };

        gameLogs.Add(logEntry);
    }

    public void StartScreenshotLogger()
    {
        Debug.LogWarning("Disabled screenshot logging, as we are using the recorder now");
        //StartCoroutine(screenshotLogger());
    }

    IEnumerator screenshotLogger()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(timePerScreenshot);
            if (!showingReporter)
            {
                LogScreenshot();
            }
        }
    }

    public void StartPingReporter()
    {
        StartCoroutine(pingReporter());
    }

    IEnumerator pingReporter()
    {
        while (true)
        {
#if !UNITY_WEBGL
            var ping = new Ping(GLApi.I.ApiIP);
            while (!ping.isDone)
            {
                yield return null;
            }
            Debug.Log("PING = " + ping.time + " ms");
#endif
            yield return new WaitForSecondsRealtime(timePerPing);
        }
    }
#endregion
#region JSON
    public void Dump()
    {
        GetAndWritePreferencesJSON();
        GetAndWriteKnowledgeJSON();
        GetAndWriteDeviceJSON();
        GetAndWriteCommunicationLogsJSON();
        GetAndWriteGameLogsJSON();
    }

    public string GetAndWritePreferencesJSON()
    {
        var json = GLPrefs.ToJSONString();
        Write(preferencesFile, json);
        return json.CompressGZip();
    }

    public string GetAndWriteKnowledgeJSON()
    {
        var json = GLInsights.ToJSONString();
        Write(knowledgeFile, json);
        return json.CompressGZip();
    }

    public string GetAndWriteDeviceJSON()
    {
        var json = GetDeviceJSONString();
        Write(deviceFile, json);
        return json.CompressGZip();
    }

    public string GetAndWriteCommunicationLogsJSON()
    {
        var json = GetCommunicationLogJSONString();
        Write(communicationLogFile, json); 
        return json.CompressGZip();
    }

    public string GetAndWriteGameLogsJSON()
    {
        var json = GetGameLogJSONString();
        Write(gameLogFile, json);
        return json.CompressGZip();
    }

    private string GetGameLogJSONString()
    {
        return GLApi.MaskSecretProperties(JsonConvert.SerializeObject(gameLogs, Newtonsoft.Json.Formatting.Indented));
    }

    private string GetCommunicationLogJSONString()
    {
        return GLApi.MaskSecretProperties(JsonConvert.SerializeObject(communicationLogs, Newtonsoft.Json.Formatting.Indented));
    }
#endregion
#region screenshots
    List<string> screenshotFiles = new List<string>();
    List<string> recordingScreens = new List<string>();
    List<DateTime> recordingTimes = new List<DateTime>();
    List<DateTime> screenshotTimes = new List<DateTime>();

    public void LogRecordedScreen(byte[] imageBytes)
    {
        var newScreenPath = Path.Combine(GLFiles.DebugRecordingFolderName, string.Format(screenshotFile, recordingScreens.Count));
        if (File.Exists(newScreenPath)) File.Delete(newScreenPath);
        File.WriteAllBytes(newScreenPath, imageBytes);

        recordingScreens.Add(Convert.ToBase64String(imageBytes));
        recordingTimes.Add(DateTime.Now);

        if (recordingScreens.Count > 1000)
        {
            recordingScreens.RemoveAt(0);
            recordingTimes.RemoveAt(0);
        }
    }

    public void LogScreenshot()
    {
        try
        {
            while (screenshotFiles.Count >= totalScreenshots)
            {
                File.Delete(Path.Combine(FullDebugFolderPath, string.Format(screenshotFile, 0)));
                
                // move those we have in file system according to their new ID
                for (int i = 0; i < screenshotFiles.Count - 1; i++)
                {
                    var newPath = Path.Combine(FullDebugFolderPath, string.Format(screenshotFile, i));
                    var nowPath = Path.Combine(FullDebugFolderPath, string.Format(screenshotFile, i + 1));
                    File.Move(nowPath, newPath);
                }

                // remove last screenshot
                screenshotFiles.RemoveAt(0);
                screenshotTimes.RemoveAt(0);
            }

            var newScreen = Path.Combine(FullDebugFolderPath, string.Format(screenshotFile, screenshotFiles.Count));

            CaptureScreenshotBlob(newScreen);

            screenshotFiles.Add(newScreen);
            screenshotTimes.Add(DateTime.Now);
            Debug.Log("Logged screenshot!");
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    public void LogReportScreenshot()
    {
        var file = Path.Combine(FullDebugFolderPath, reportScreenshotFile);
        CaptureScreenshotBlob(file);
    }

    private void CaptureScreenshotBlob(string file)
    {
        ScreenCapture.CaptureScreenshot(file);
    }

    private string GetScreenshotBlob(string file)
    {
        if (!File.Exists(file))
        {
            Debug.LogError("Could not find captured screenshot file... :(");
            return null;
        }
        else
        {
            var screenshotBytes = File.ReadAllBytes(file);
            //var screenshotBytes = screenshotTexture.EncodeToPNG();
            string screenshot64 = Convert.ToBase64String(screenshotBytes);
            // File.WriteAllBytes(debugFolder + string.Format(screenshotFile, screenshotBlobs.Count), screenshotBytes);
            return screenshot64;
        }
    }

#endregion
#region device
    private string GetDeviceJSONString()
    {
        JObject deviceInfo = new JObject();
        deviceInfo["supportsRawShadowDepthSampling"] = SystemInfo.supportsRawShadowDepthSampling;
        deviceInfo["supportsMotionVectors"] = SystemInfo.supportsMotionVectors;
        deviceInfo["supportsRenderToCubemap"] = SystemInfo.supportsRenderToCubemap;
        deviceInfo["supportsImageEffects"] = SystemInfo.supportsImageEffects;
        deviceInfo["supports3DTextures"] = SystemInfo.supports3DTextures;
        deviceInfo["supports2DArrayTextures"] = SystemInfo.supports2DArrayTextures;
        deviceInfo["supports3DRenderTextures"] = SystemInfo.supports3DRenderTextures;
        deviceInfo["supportsCubemapArrayTextures"] = SystemInfo.supportsCubemapArrayTextures;
        deviceInfo["copyTextureSupport"] = SystemInfo.copyTextureSupport.ToString();
        deviceInfo["supportsComputeShaders"] = SystemInfo.supportsComputeShaders;
        deviceInfo["supportsInstancing"] = SystemInfo.supportsInstancing;
        deviceInfo["supportsHardwareQuadTopology"] = SystemInfo.supportsHardwareQuadTopology;
        deviceInfo["supports32bitsIndexBuffer"] = SystemInfo.supports32bitsIndexBuffer;
        deviceInfo["supportsSparseTextures"] = SystemInfo.supportsSparseTextures;
        deviceInfo["supportedRenderTargetCount"] = SystemInfo.supportedRenderTargetCount;
        deviceInfo["supportsMultisampledTextures"] = SystemInfo.supportsMultisampledTextures;
        deviceInfo["supportsMultisampleAutoResolve"] = SystemInfo.supportsMultisampleAutoResolve;
        deviceInfo["supportsTextureWrapMirrorOnce"] = SystemInfo.supportsTextureWrapMirrorOnce;
        deviceInfo["usesReversedZBuffer"] = SystemInfo.usesReversedZBuffer;
        //deviceInfo["supportsStencil"] = SystemInfo.supportsStencil;
        deviceInfo["npotSupport"] = SystemInfo.npotSupport.ToString();
        deviceInfo["maxTextureSize"] = SystemInfo.maxTextureSize;
        deviceInfo["maxCubemapSize"] = SystemInfo.maxCubemapSize;
        deviceInfo["supportsAsyncCompute"] = SystemInfo.supportsAsyncCompute;
        deviceInfo["supportsAsyncGPUReadback"] = SystemInfo.supportsAsyncGPUReadback;
        deviceInfo["supportsMipStreaming"] = SystemInfo.supportsMipStreaming;
        deviceInfo["supportsShadows"] = SystemInfo.supportsShadows;
        deviceInfo["graphicsMultiThreaded"] = SystemInfo.graphicsMultiThreaded;
        deviceInfo["graphicsShaderLevel"] = SystemInfo.graphicsShaderLevel;
        deviceInfo["graphicsDeviceVersion"] = SystemInfo.graphicsDeviceVersion;
        deviceInfo["batteryLevel"] = SystemInfo.batteryLevel;
        deviceInfo["batteryStatus"] = SystemInfo.batteryStatus.ToString();
        deviceInfo["operatingSystem"] = SystemInfo.operatingSystem;
        deviceInfo["operatingSystemFamily"] = SystemInfo.operatingSystemFamily.ToString();
        deviceInfo["processorType"] = SystemInfo.processorType;
        deviceInfo["processorFrequency"] = SystemInfo.processorFrequency;
        deviceInfo["processorCount"] = SystemInfo.processorCount;
        deviceInfo["systemMemorySize"] = SystemInfo.systemMemorySize;
        deviceInfo["deviceUniqueIdentifier"] = SystemInfo.deviceUniqueIdentifier;
        //deviceInfo["graphicsPixelFillrate"] = SystemInfo.graphicsPixelFillrate;
        deviceInfo["deviceModel"] = SystemInfo.deviceModel;
        deviceInfo["supportsAccelerometer"] = SystemInfo.supportsAccelerometer;
        deviceInfo["deviceName"] = SystemInfo.deviceName;
        deviceInfo["supportsLocationService"] = SystemInfo.supportsLocationService;
        deviceInfo["graphicsUVStartsAtTop"] = SystemInfo.graphicsUVStartsAtTop;
        deviceInfo["graphicsDeviceType"] = SystemInfo.graphicsDeviceType.ToString();
        deviceInfo["supportsGyroscope"] = SystemInfo.supportsGyroscope;
        deviceInfo["graphicsDeviceID"] = SystemInfo.graphicsDeviceID;
        deviceInfo["graphicsDeviceVendor"] = SystemInfo.graphicsDeviceVendor;
        deviceInfo["graphicsDeviceVendorID"] = SystemInfo.graphicsDeviceVendorID;
        deviceInfo["graphicsMemorySize"] = SystemInfo.graphicsMemorySize;
        deviceInfo["deviceType"] = SystemInfo.deviceType.ToString();
        deviceInfo["supportsAudio"] = SystemInfo.supportsAudio;
        deviceInfo["supportsVibration"] = SystemInfo.supportsVibration;
        deviceInfo["graphicsDeviceName"] = SystemInfo.graphicsDeviceName;

        // display
        var displays = new JArray();
        for (int i = 0; i < Display.displays.Length; i++)
        {
            var display = new JObject();
            display["active"] = Display.displays[i].active;
            display["renderingHeight"] = Display.displays[i].renderingHeight;
            display["renderingWidth"] = Display.displays[i].renderingWidth;
            display["systemHeight"] = Display.displays[i].systemHeight;
            display["systemWidth"] = Display.displays[i].systemWidth;
            displays.Add(display);
        }
        deviceInfo["Display"] = displays;

        // resolutions
        var resolutions = new JArray();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            resolutions.Add(JObject.FromObject(Screen.resolutions[i]));
        }
        deviceInfo["Resolutions"] = resolutions;
        deviceInfo["currentResolution"] = JObject.FromObject(Screen.currentResolution);
        deviceInfo["gameWindowWidth"] = Screen.width;
        deviceInfo["gameWindowHeight"] = Screen.height;


        //deviceInfo["supportsVertexPrograms"] = SystemInfo.supportsVertexPrograms;
        return deviceInfo.ToString();
    }
#endregion
#region helpers
    private void Write(string file, string content)
    {
        try
        {
            var path = Path.Combine(FullDebugFolderPath, file);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            File.WriteAllText(path, content);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    public static JObject ObjectToJSONObject(object obj)
    {
        return JObject.FromObject(obj, I.jsonSerializer);
    }
#endregion
}
