﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class GLScreenRecord : GLSingletonBehaviour<GLScreenRecord> {

    [Header("Settings")]
    public bool isRecordingEnabled;
    public int screenshotHeight = 140;
    public int screenshotQuality = 30;
    public float additionalRecordingTimer = 30f;

    [Header("References")]
    public new Camera camera;
    public RenderTexture gameRender;

    private int lastFrameRecorded = -1;
    
    protected override void OnSingletonInitialize()
    {
        camera = GetComponent<Camera>();

        var ratio = (float)Screen.width / Screen.height;
        var targetWidth = (int)(screenshotHeight * ratio);

        gameRender.Release();
        gameRender = new RenderTexture(targetWidth, screenshotHeight, gameRender.depth, gameRender.format);
        gameRender.antiAliasing = 2;
    }

    public void RecordNextFrame()
    {
        GLRun.NextFrame(RecordThisFrame);
    }

    public void RecordThisFrame()
    {
        // was this frame already recorded?
        if (Time.frameCount == lastFrameRecorded)
        {
            // this frame was already recorded
            return;
        }

        // remember this frame was recorded
        lastFrameRecorded = Time.frameCount;
        
        var ratio = (float)Screen.width / Screen.height;
        var targetWidth = (int)(screenshotHeight * ratio);

        if (gameRender.width != targetWidth)
        {
            gameRender.Release();
            gameRender = new RenderTexture(targetWidth, screenshotHeight, gameRender.depth, gameRender.format);
            gameRender.antiAliasing = 2;
        }

        // set and render in the render texture
        camera.targetTexture = gameRender;
        camera.Render();
        RenderTexture.active = gameRender;

        // read results
        Texture2D virtualPhoto = new Texture2D(gameRender.width, gameRender.height, TextureFormat.RGBAHalf, false);
        virtualPhoto.ReadPixels(new Rect(0, 0, gameRender.width, gameRender.height), 0, 0);

        // remove render texture
        RenderTexture.active = null; //can help avoid errors 
        camera.targetTexture = null;

        // log the obtained bytes
        GLDebug.I.LogRecordedScreen(virtualPhoto.EncodeToJPG(screenshotQuality));

        // restart the additional recorder
        RestartAdditionalRecorder();
    }

    private void RestartAdditionalRecorder()
    {
        StopAllCoroutines();
        StartCoroutine(AdditionalRecording());
    }

    private IEnumerator AdditionalRecording()
    {
        while (true)
        {
            yield return new WaitForSeconds(additionalRecordingTimer);
            RecordThisFrame();
        }
    }
}
