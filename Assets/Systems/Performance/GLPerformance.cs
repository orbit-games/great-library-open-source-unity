﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GLRenderSpeed
{
    MAXIMUM = 0,
    LOW_ADAPTIVE = 1,
    TERRIBLE_ADAPTIVE = 2
}

public class GLPerformance : GLSingletonBehaviour<GLPerformance>
{
    protected override void OnSingletonInitialize()
    {
        // DetermineSuggestedResolutions();
    }
    
    [Header("Localization")]
    public LocalizedText Title_HighResolutionWarning;
    public LocalizedText Description_HighResolutionWarning;
    public LocalizedText Label_SetSuggestedResolution;
    public LocalizedText Label_KeepCurrentResolution;

    public LocalizedText Title_NonUniformResolutionWarning;
    public LocalizedText Description_NonUniformResolutionWarning;

    public LocalizedText Label_DontShowWarningAgain;
    public LocalizedText Label_OpenSettings;
    public LocalizedText Label_Close;

    [Header("Preferences")]
    public GLBoolPreference HasDismissedHighResolutionWarning;
    public GLBoolPreference HasDismissedNonUniformResolutionWarning;
    public GLIntPreference fullscreenWidth;
    public GLIntPreference fullscreenHeight;

    [Header("Settings")]
    public AnimationCurve timeScaleCurve;
    public AnimationCurve fpsModifierCurve;
    public float totalTransitionDuration = 20f;
    public float terribleSpeedMultiplier = 2f;
    public float terribleFPSMultiplier = 0.6f;

    [Header("State")]
    [ReadOnly]
    public float currentTimeScale;
    [ReadOnly]
    public float currentTargetFrameRate;
    [ReadOnly]
    public float renderSpeedMultiplier;
    [ReadOnly]
    public float renderFPSMultiplier;

    public int suggestedResolutionMaxWidth = 1920;
    public int suggestedResolutionMaxHeight = 1080;
    public int suggestedResolutionLargeWidth = 1920;
    public int suggestedResolutionLargeHeight = 1080;
    public int suggestedResolutionMediumWidth = 1280;
    public int suggestedResolutionMediumHeight = 720;
    public int suggestedResolutionSmallWidth = 1280;
    public int suggestedResolutionSmallHeight = 480;

    [Header("Debug")]
    [Buttons("Toggle", "ToggleLowQuality")]
    public ButtonsContainer toggle;

    private GLRenderSpeed renderSpeed = GLRenderSpeed.MAXIMUM;
    private float lastMoveTime = 0f;

    private bool suggestedResolutionsInitialized = false;
    public void InitializeSuggestedResolutions()
    {
        if (suggestedResolutionsInitialized) return;
        suggestedResolutionsInitialized = true;

        var maxResolution = GLSystem.GetMaxResolution();
        var suggestedRatio = (float)maxResolution.height / maxResolution.width;

        suggestedResolutionMaxWidth = maxResolution.width;
        suggestedResolutionMaxHeight = maxResolution.height;
        suggestedResolutionLargeWidth = Mathf.Min(maxResolution.width, 1920);
        suggestedResolutionLargeHeight = (int)(suggestedResolutionLargeWidth * suggestedRatio);
        suggestedResolutionMediumWidth = (int)(suggestedResolutionLargeWidth / 1.5f);
        suggestedResolutionMediumHeight = (int)(suggestedResolutionLargeHeight / 1.5f);
    }

    public bool LargeSuggestedResolutionIsMaxResolution()
    {
        return suggestedResolutionMaxWidth == suggestedResolutionLargeWidth
            && suggestedResolutionMaxHeight == suggestedResolutionLargeHeight;
    }

    public bool IsCurrentResolutionSetToCustom()
    {
        return !IsMaxResolutionCurrent() && !IsLargeResolutionCurrent() && !IsMediumResolutionCurrent();
    }

    public bool IsMaxResolutionCurrent()
    {
        InitializeSuggestedResolutions();
        return Screen.width == suggestedResolutionMaxWidth && Screen.height == suggestedResolutionMaxHeight;
    }

    public bool IsLargeResolutionCurrent()
    {
        InitializeSuggestedResolutions();
        return Screen.width == suggestedResolutionLargeWidth && Screen.height == suggestedResolutionLargeHeight;
    }

    public bool IsMediumResolutionCurrent()
    {
        InitializeSuggestedResolutions();
        return Screen.width == suggestedResolutionMediumWidth && Screen.height == suggestedResolutionMediumHeight;
    }

    public void SetMaximumResolution()
    {
        InitializeSuggestedResolutions();
        fullscreenWidth.Value = suggestedResolutionMaxWidth;
        fullscreenHeight.Value = suggestedResolutionMaxHeight;
        Screen.SetResolution(suggestedResolutionMaxWidth, suggestedResolutionMaxHeight, Screen.fullScreen);
    }

    public void SetLargeSuggestedResolution()
    {
        InitializeSuggestedResolutions();
        fullscreenWidth.Value = suggestedResolutionLargeWidth;
        fullscreenHeight.Value = suggestedResolutionLargeHeight;
        Screen.SetResolution(suggestedResolutionLargeWidth, suggestedResolutionLargeHeight, Screen.fullScreen);
    }

    public void SetMediumSuggestedResolution()
    {
        InitializeSuggestedResolutions();
        fullscreenWidth.Value = suggestedResolutionMediumWidth;
        fullscreenHeight.Value = suggestedResolutionMediumHeight;
        Screen.SetResolution(suggestedResolutionMediumWidth, suggestedResolutionMediumHeight, Screen.fullScreen);
    }

    public void DoResolutionCheck()
    {
        InitializeSuggestedResolutions();
        var maxResolution = GLSystem.GetMaxResolution();
        var currentResolution = GLSettings.CurrentWindowResolution;
        var currentRatio = (float)currentResolution.width / currentResolution.height;
        var suggestedRatio = (float)maxResolution.width / maxResolution.height;

        if (!HasDismissedHighResolutionWarning.Value 
            && (currentResolution.width > suggestedResolutionLargeWidth
            || currentResolution.height > suggestedResolutionLargeHeight))
        {
            var popupSetup = GLPopup.MakeWarningPopup(Title_HighResolutionWarning,
                Description_HighResolutionWarning.Format(
                    currentResolution.width.ToString(),
                    currentResolution.height.ToString(),
                    suggestedResolutionLargeWidth.ToString(),
                    suggestedResolutionLargeHeight.ToString(),
                    suggestedResolutionMediumWidth.ToString(),
                    suggestedResolutionMediumHeight.ToString()));

            // add the dont show again checkbox
            popupSetup.formHelper.AddCheckbox(Label_DontShowWarningAgain, "hide", false);

            // just reset the popup buttons completely
            popupSetup.RemoveButtons();

            // set the on submit call to remember the DontShowAgain state
            popupSetup.SetOnSubmit(data =>
            {
                HasDismissedHighResolutionWarning.Value = data["hide"] == bool.TrueString;
            });

            // set the close buttons to keep current resolution
            popupSetup.AddSubmitButton(Label_Close, null);

            // to open the settings menu
            popupSetup.AddSubmitButton(Label_OpenSettings, GLSettings.I.ShowSettings);

            // set the button to pick the larger resolution
            //popupSetup.AddSubmitButton(Label_SetSuggestedResolution.Format(
            //    suggestedResolutionLargeWidth.ToString(), suggestedResolutionLargeHeight.ToString()),
            //    SetLargeSuggestedResolution);

            //// set the button to pick the merdium resolution
            //popupSetup.AddSubmitButton(Label_SetSuggestedResolution.Format(
            //    suggestedResolutionMediumWidth.ToString(), suggestedResolutionMediumHeight.ToString()), 
            //    SetMediumSuggestedResolution);
            
            popupSetup.Show();

            return;
        }

        var isCorrectRatio = suggestedRatio - 0.05 < currentRatio && currentRatio < suggestedRatio + 0.05;

        if (!HasDismissedNonUniformResolutionWarning.Value && !isCorrectRatio)
        {
            var popupSetup = GLPopup.MakeWarningPopup(Title_NonUniformResolutionWarning,
                Description_NonUniformResolutionWarning.Format(
                    currentResolution.width.ToString(),
                    currentResolution.height.ToString(),
                    suggestedResolutionLargeWidth.ToString(),
                    suggestedResolutionLargeHeight.ToString(),
                    suggestedResolutionMediumWidth.ToString(),
                    suggestedResolutionMediumHeight.ToString()));

            // add the dont show again checkbox
            popupSetup.formHelper.AddCheckbox(Label_DontShowWarningAgain, "hide", false);

            // just reset the popup buttons completely
            popupSetup.RemoveButtons();

            // set the on submit call to remember the DontShowAgain state
            popupSetup.SetOnSubmit(data =>
            {
                HasDismissedNonUniformResolutionWarning.Value = data["hide"] == bool.TrueString;
            });

            // set the close buttons to keep current resolution
            popupSetup.AddSubmitButton(Label_Close, null);

            // to open the settings menu
            popupSetup.AddSubmitButton(Label_OpenSettings, GLSettings.I.ShowSettings);

            //// set the button to pick the larger resolution
            //popupSetup.AddSubmitButton(Label_SetSuggestedResolution.Format(
            //    suggestedResolutionLargeWidth.ToString(), suggestedResolutionLargeHeight.ToString()), () =>
            //         Screen.SetResolution(suggestedResolutionLargeWidth, suggestedResolutionLargeHeight, Screen.fullScreen));

            //// set the button to pick the medium resolution
            //popupSetup.AddSubmitButton(Label_SetSuggestedResolution.Format(
            //    suggestedResolutionMediumWidth.ToString(), suggestedResolutionMediumHeight.ToString()), () =>
            //         Screen.SetResolution(suggestedResolutionMediumWidth, suggestedResolutionMediumHeight, Screen.fullScreen));

            popupSetup.Show();

            return;
        }
    }


    public void SetRenderSpeed(GLRenderSpeed renderSpeed)
    {
        this.renderSpeed = renderSpeed;
        lastMoveTime = Time.realtimeSinceStartup;

        switch (renderSpeed)
        {
            case GLRenderSpeed.MAXIMUM:
                Application.targetFrameRate = 60;
                renderSpeedMultiplier = 1f;
                renderFPSMultiplier = 1f;
                break;
            case GLRenderSpeed.LOW_ADAPTIVE:
                Application.targetFrameRate = (int)fpsModifierCurve.Evaluate(0f);
                renderSpeedMultiplier = 1f;
                renderFPSMultiplier = 1f;
                break;
            case GLRenderSpeed.TERRIBLE_ADAPTIVE:
                Application.targetFrameRate = (int)fpsModifierCurve.Evaluate(0f);
                renderSpeedMultiplier = terribleSpeedMultiplier;
                renderFPSMultiplier = terribleFPSMultiplier;
                break;
            default:
                break;
        }
    }

    public GLRenderSpeed GetRenderSpeed()
    {
        return renderSpeed;
    }

    private Vector3 lastMousePosition;
    public void FixedUpdate()
    {
        if (renderSpeed > 0)
        {
            bool moving = lastMousePosition != Input.mousePosition;
            if (Input.anyKey) { lastMoveTime = Mathf.Max(lastMoveTime, Time.realtimeSinceStartup - 6f); }
            if (moving) { lastMoveTime = Time.realtimeSinceStartup; }

            var startTime = lastMoveTime;
            var duration = (Time.realtimeSinceStartup - startTime) * renderSpeedMultiplier;

            currentTimeScale = timeScaleCurve.Evaluate(duration);
            currentTargetFrameRate = fpsModifierCurve.Evaluate(duration) * renderFPSMultiplier;

            Time.timeScale = currentTimeScale;
            Application.targetFrameRate = Mathf.RoundToInt(currentTargetFrameRate);

            lastMousePosition = Input.mousePosition;
        }
    }
}
