﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GLCanvas : MonoBehaviour
{
    [Header("General Settings")]
    public bool showOnlyIfLoggedIn;
    public bool showOnlyIfCourseSelected;
    
    [Header("Overlay Settings")]
    public bool isOverlay;
    public bool alwaysOpen;

    [Header("Screen Settings")]
    public bool isScreen;
    public bool showScreenDetails;
    public LocalizedText screenTitle;
    public LocalizedText screenDescription;
    public GameMenu inGameMenu = GameMenu.BACK_MENU;


    private bool initialized = false;
    public void Initialize()
    {
        if (initialized) return;
        initialized = true;
        unsavedChanges = GetComponent<IUnsavedChanges>();
    }

    private Canvas canvas;
    /// <summary>
    /// Terrible hack to trigger setting proper position and scale of the canvas
    /// </summary>
    private void OnEnable()
    {
        if (canvas == null)
        {
            canvas = GetComponent<Canvas>();
        }

        var tempCamera = canvas.worldCamera;
        canvas.worldCamera = null; 
        canvas.worldCamera = tempCamera;
    }

    public IUnsavedChanges unsavedChanges;
    public bool HasUnsavedChanges()
    {
        if (unsavedChanges != null)
        {
            return unsavedChanges.HasUnsavedChanges();
        }
        return false;
    }
}
