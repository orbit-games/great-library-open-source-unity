﻿using GameToolkit.Localization;
using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class GLGameFlow : GLSingletonBehaviour<GLGameFlow>, ICheatListener {

    [Header("Localization")]
    public LocalizedText Title_ExitGame;
    public LocalizedText Description_ExitGame;
    public LocalizedText Label_KeepPlaying;
    public LocalizedText Label_ExitGame;

    public LocalizedText Error_GameNotGenuine;

    [Header("Game Canvases")]
    public GLCanvas screen_playerRoom;
    public GLCanvas screen_progress;
    public GLCanvas screen_library;
    public GLCanvas screen_defense;
    public GLCanvas screen_chat;
    public GLCanvas screen_tourOffice;
    public GLCanvas screen_monuments;
    public GLCanvas overlay_gameMenu;
    public GLCanvas overlay_achievement;

    [Header("Account Canvases")]
    public GLCanvas screen_administration;
    public GLCanvas screen_courseSelection;
    public GLCanvas screen_courseRegistration;
    public GLCanvas screen_login;

    [Header("General Canvases")]
    public GLCanvas overlay_loading;
    public GLCanvas overlay_popup;
    public GLCanvas overlay_agreement;
    public GLCanvas overlay_tooltip;

    [Header("Preferences")]
    public GLBoolPreference tourCompletion;
    public GLIntPreference fullscreenWidth;
    public GLIntPreference fullscreenHeight;

    [Header("State")]
    [ReadOnly]
    public GLCanvas activeScreen;
    public Stack<GLCanvas> backStack;
    public List<GLCanvas> canvases;

    protected override void OnSingletonInitialize()
    {
        // find all canvases
        canvases = new List<GLCanvas>(Resources.FindObjectsOfTypeAll<GLCanvas>());

        // initialize all canvases
        foreach (var canvas in canvases)
        {
            canvas.Initialize();
        }

        // initialize back stack
        backStack = new Stack<GLCanvas>();

        // disable all
        DisableAllCanvases();

        // then handle all canvas logic to enable those relevant
        HandleCanvases(false);

        // check if game is genuine
        if (Application.genuineCheckAvailable && !Application.genuine)
        {
            GLPopup.MakeErrorPopup(Error_GameNotGenuine).Show();
        }
        else
        {
            // bootup flow
            GLServerManager.I.FetchServerInfo(() =>
            {
                GLVersionManager.I.CheckVersion(() =>
                {
                    GLAnnouncementManager.I.CheckAnnouncements(() =>
                    {
                        GLAgreementManager.I.LoadAgreement(() =>
                        {
                            OnBootup();
                        });
                    });
                });
            });
        }
    }

    public void ToggleFullscreen()
    {
        if (Screen.fullScreen)
        {
            var currentResolution = GLSettings.CurrentWindowResolution;
            fullscreenWidth.Value = currentResolution.width;
            fullscreenHeight.Value = currentResolution.height;

            var quarterWidth = (int)(currentResolution.width * 0.75f);
            var quarterHeight = (int)(currentResolution.height * 0.75f);
            Screen.SetResolution(quarterWidth, quarterHeight, false);
        }
        else
        {
            int width = fullscreenWidth.Value;
            int height = fullscreenHeight.Value;

            if (width <= 400 || height <= 400)
            {
                var max = GLSystem.GetMaxResolution();
                width = max.width;
                height = max.height;
                fullscreenWidth.Value = max.width;
                fullscreenHeight.Value = max.height;
            }

            Screen.SetResolution(width, height, true);
        }
    }

    public void ExitGame()
    {
        OnScreenSwitchAllowed(() =>
        {
            var popup = GLPopup.MakeConfirmPopup(Title_ExitGame, Description_ExitGame, () => {
                    Debug.LogWarning("Closing application!");
                    GLSystem.Quit();
                });
            popup.OverrideNegativeButtonText(Label_KeepPlaying);
            popup.OverridePositiveButtonText(Label_ExitGame);
            popup.Show();
        });
    }

    public string GetScreenName()
    {
        return activeScreen == null ? "no active screen" : activeScreen.gameObject.GetPath();
    }

    public bool IsTourCompleted()
    {
        if (!GLApi.IsLoggedInAndCourseSelected()) return false;
        return tourCompletion.Value;
    }

    public bool IsFakeIdentityDefined()
    {
        if (!GLApi.IsLoggedInAndCourseSelected()) return false;
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        return GLInsights.HasFakeIdentityDefined(user?.Ref, course?.Ref);
    }

    public void OnBootup()
    {
        GLRun.Delayed(0.2f, () =>
        {
            GLPerformance.I.DoResolutionCheck();
            GLUpgrade.I.UpgradeOnBootup();
        });

        GLLoginScreen.I.AllowAutoLoginOnce();
        ShowLogin();
    }

    public void OnLogin()
    {
        GLCourseSelection.I.AllowAutoOpeningOnce();
        ShowCourseSelection();
    }

    private bool forciblyShowTour;
    private bool forciblyShowIdentityEditor;
    public string OnCheat(string cheatUID)
    {
        if (cheatUID == "forceShowTour")
        {
            forciblyShowTour = !forciblyShowTour;
            if (forciblyShowTour) return "From now on, tour will be shown upon chosing a course";
            else return "Tours will now only be shown when required";
        }
        else if (cheatUID == "forceShowIdentityEditor")
        {
            forciblyShowIdentityEditor = !forciblyShowIdentityEditor;
            if (forciblyShowIdentityEditor) return "From now on, identity editor will be shown upon chosing a course";
            else return "Identity editor will now only be shown when required";
        }
        return null;
    }

    public void TryEnteringPlayerRoom(bool skipRegistrationForNow = false)
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();

        if (user == null)
        {
            ShowLogin();
        }
        else if (course == null)
        {
            ShowCourseSelection();
        }
        //else if (forciblyShowTour || !IsTourCompleted())
        //{
        //    ShowTourOffice();
        //}
        else if (!skipRegistrationForNow 
            && (forciblyShowIdentityEditor || !IsFakeIdentityDefined()))
        {
            ShowCourseRegistration();
        }
        else
        {
            ShowPlayerRoom();
        }
    }

    public void ShowLogin()
    {
        CheckChangesBeforeSetScreen(screen_login);
    }

    public void ShowCourseSelection()
    {
        CheckChangesBeforeSetScreen(screen_courseSelection);
    }

    public void ShowCourseRegistration()
    {
        CheckChangesBeforeSetScreen(screen_courseRegistration);
    }

    public void ShowPlayerRoom()
    {
        CheckChangesBeforeSetScreen(screen_playerRoom);
    }

    public void ShowProgress()
    {
        CheckChangesBeforeSetScreen(screen_progress);
    }

    public void ShowLibrary()
    {
        CheckChangesBeforeSetScreen(screen_library);
    }

    public void ShowDebate()
    {
        CheckChangesBeforeSetScreen(screen_defense);
    }

    public void ShowDebate(string trackRef, string stepRef)
    {
        Debug.Log("showing debate with track " + trackRef + " and step " + stepRef);
        GLDebateHall.I.SetDebateTrack(trackRef, stepRef);
        CheckChangesBeforeSetScreen(screen_defense);
    }

    public void ShowChat()
    {
        CheckChangesBeforeSetScreen(screen_chat);
    }

    public void ShowChatWithConversation(string conversationRef)
    {
        Debug.Log("Opening conversation " + conversationRef);
        GLForum.I.OpenConversation(conversationRef);
        CheckChangesBeforeSetScreen(screen_chat);
    }

    public void ShowChatWithUser(string userRef)
    {
        Debug.Log("Opening chat with user " + userRef);
        GLForum.I.OpenConversationWithUser(userRef);
        CheckChangesBeforeSetScreen(screen_chat);
    }

    public void ShowMonuments()
    {
        CheckChangesBeforeSetScreen(screen_monuments);
    }

    public void ShowAdministration()
    {
        CheckChangesBeforeSetScreen(screen_administration);
    }

    public void ShowTourOffice()
    {
        CheckChangesBeforeSetScreen(screen_tourOffice);
    }

    private void Update()
    {
        if (!GLInput.IsNavigationBlocked() && GLInput.WantsToGoBack())
        {
            Back();
        }
    }

    public void Logout()
    {
        GLApi.Logout();
        SetScreen(screen_login);
        GLLoginScreen.I.OnLogout();
    }

    public void Refresh()
    {
        GLTransition.Out(activeScreen);
        GLTransition.In(activeScreen);
    }

    public void Back()
    {
        if (backStack.Count > 0)
        {
            OnScreenSwitchAllowed(() =>
            {
                Debug.Log("Going Back");
                var screen = backStack.Pop();
                MoveActiveScreenOut(); // so it won't end up on back stack
                SetScreen(screen);
            });
        }
        else
        {
            var user = GLApi.GetActiveUser();
            var course = GLApi.GetActiveCourse();

            if (user != null)
            {
                if (course != null)
                {
                    CheckChangesBeforeSetScreen(screen_playerRoom);
                }
                else
                {
                    CheckChangesBeforeSetScreen(screen_courseSelection);
                }
            }
            else
            {
                CheckChangesBeforeSetScreen(screen_login);
            }
        }
    }

    private void OnScreenSwitchAllowed(Action doSwitch)
    {
        if (activeScreen != null && activeScreen.HasUnsavedChanges())
        {
            GLPopup.MakeConfirmDiscardChangesPopup(doSwitch).Show();
        }
        else
        {
            doSwitch.Invoke();
        }
    }

    private void CheckChangesBeforeSetScreen(GLCanvas newScreen)
    {
        if (newScreen == activeScreen) return;

        OnScreenSwitchAllowed(() =>
        {
            SetScreen(newScreen);
        });
    }

    private void SetScreen(GLCanvas newScreen)
    {
        if (newScreen == activeScreen) return;

        GLProgressLoader.I.RefreshIfNecessary(() =>
        {
            if (activeScreen != null)
            {
                backStack.Push(activeScreen);
            }
            MoveActiveScreenOut();
            MoveNewScreenIntoActive(newScreen);
        });
    }

    private void MoveActiveScreenOut()
    {
        if (activeScreen != null)
        {
            GLTransition.Out(activeScreen);
            activeScreen = null;
        }
    }

    private void MoveNewScreenIntoActive(GLCanvas newScreen)
    {
        if (newScreen != null)
        {
            Debug.Log("Showing screen " + newScreen);
            activeScreen = newScreen;

            if (newScreen == screen_login)
            {
                GLApi.Logout();
            }

            GLTransition.In(newScreen);
            HandleCanvases(false);
        }
    }

    private void HandleCanvases(bool logoutWhenNoScreenActive = true)
    {
        var user = GLApi.GetActiveUser();
        var course = GLApi.GetActiveCourse();
        var loggedInAndCourseSelected = GLApi.IsLoggedInAndCourseSelected();
        var isTourCompleted = IsTourCompleted();
        var hasFakeIdentity = IsFakeIdentityDefined();

        // handle active screen
        if (activeScreen != null)
        {
            if (loggedInAndCourseSelected)// && isTourCompleted && hasFakeIdentity)
            {
                switch (activeScreen.inGameMenu)
                {
                    case GameMenu.MAIN_MENU:
                        GLGameMenu.I.ShowMainMenu();
                        break;
                    case GameMenu.BACK_MENU:
                        GLGameMenu.I.ShowBackMenu();
                        break;
                    case GameMenu.NO_MENU:
                        GLGameMenu.I.ShowNoMenu();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                GLGameMenu.I.ShowNoMenu();
            }
        }

        // handle always open overlays
        foreach (var canvas in canvases)
        {
            if (canvas.isOverlay && canvas.alwaysOpen)
            {
                GLTransition.In(canvas);
            }
        }
        
        // handle visibility
        foreach (var canvas in canvases)
        {
            if (canvas.showOnlyIfLoggedIn && user == null)
            {
                GLTransition.Out(canvas);
            }
            else if (canvas.showOnlyIfCourseSelected && course == null)
            {
                GLTransition.Out(canvas);
            }
        }

        if (logoutWhenNoScreenActive)
        {
            // is any screen still open yet?
            var anythingStillOpen = false;
            foreach (var canvas in canvases)
            {
                if (canvas.isScreen && canvas.gameObject.activeSelf)
                {
                    anythingStillOpen = true;
                    break;
                }
            }

            // if not, then well, fail
            if (!anythingStillOpen)
            {
                Debug.LogError("For some reason everything was closed due to visibility constraints probably. Logging out....");
                Logout();
            }
        }
    }

    private void DisableAllCanvases()
    {
        foreach (var canvas in canvases)
        {
            GLTransition.Disable(canvas);
        }
    }
}
