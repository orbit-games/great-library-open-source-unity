﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLFormAdvancedPickerCrumb: MonoBehaviour
{
    public LocalizedTextBehaviour title;
    private string itemValue;

    public void Setup(string itemValue, GLFormattedLocalizedText title)
    {
        this.title.FormattedAsset = title;
        this.itemValue = itemValue;
    }

    public void OnPress()
    {
        GLFormAdvancedPickerScreen.I.Open(itemValue);
    }
}
