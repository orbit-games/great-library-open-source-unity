﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLFormTitle : GLFormBaseElementGeneric<GLForm.TitleElement>
{
    [Header("References")]
    public LocalizedTextBehaviour titleField;

    public override void PutSelectablesInList(List<Selectable> selectables)
    {
        // nothing to do here
    }

    public override void ReadDataFrom(GLForm.Data data, bool resetOtherwise)
    {
        // nothing to do here
    }

    public override void WriteDataTo(GLForm.Data data)
    {
        // nothing to do here
    }

    public override void CalculatePoints(ref int points)
    {
        // nothing to do here
    }

    protected override void BuildSetup()
    {
        titleField.FormattedAsset = elementSetup.labelText;
    }

    public override void Remove()
    {
        GLPool.removeCopy(this);
    }
}
