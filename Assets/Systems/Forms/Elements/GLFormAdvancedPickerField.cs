﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLFormAdvancedPickerField : GLFormBaseElementGeneric<GLForm.AdvancedPickerElement>
{
    [Header("Localization")]
    public LocalizedText Label_PleaseSelect;

    [Header("References")]
    public LocalizedTextBehaviour labelField;
    public LocalizedTextBehaviour buttonField;
    public Button button;
    private string value;

    public override void PutSelectablesInList(List<Selectable> selectables)
    {
        selectables.Add(button);
    }

    public override void ReadDataFrom(GLForm.Data data, bool resetOtherwise)
    {
        if (data != null && data[elementSetup.variableName] != null)
        {
            SetValue(data[elementSetup.variableName]);
        }
        else if (resetOtherwise)
        {
            SetValue(null);
        }
    }

    public override void WriteDataTo(GLForm.Data data)
    {
        data[elementSetup.variableName] = value;
    }

    public override void CalculatePoints(ref int points)
    {
        // nothing to do here
    }

    protected override void BuildSetup()
    {
        labelField.gameObject.SetActive(elementSetup.labelText != null);
        labelField.FormattedAsset = elementSetup.labelText;
        buttonField.FormattedAsset = Label_PleaseSelect;

        button.transition = Selectable.Transition.ColorTint; // hack to reset tint transition animation
        button.interactable = !elementSetup.disabled;
    }

    public override void Remove()
    {
        GLPool.removeCopy(this);
    }

    private void SetValue(string value)
    {
        this.value = value;

        if (value != null)
        {
            var names = new List<string>();
            var item = elementSetup.optionElementMapper[value];
            names.Add(item.labelText.ToString());

            var parent = item.parentOption;
            while (parent != null)
            {
                names.Add(parent.labelText.ToString());
                parent = parent.parentOption;
            }

            names.Reverse();
            buttonField.FormattedAsset = string.Join(" > ", names);
        }
        else
        {
            buttonField.FormattedAsset = GLFormManager.I.Label_NothingSelected;
        }
    }

    public void OnPress()
    {
        GLFormAdvancedPickerScreen.ShowPicker(elementSetup, value, SetValue);
    }
}

