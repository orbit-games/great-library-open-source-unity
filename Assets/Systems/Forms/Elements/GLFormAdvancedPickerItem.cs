﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLFormAdvancedPickerItem : MonoBehaviour
{
    public LocalizedTextBehaviour title;
    public LocalizedTextBehaviour description;
    public LocalizedTextBehaviour subtext;

    public GameObject selectedIndicator;
    public GameObject openButton;
    public GameObject pickButton;
    
    private string itemValue;

    public void SetupPickable(string itemValue, GLFormattedLocalizedText title, GLFormattedLocalizedText description, GLFormattedLocalizedText subtext, bool isPicked, bool openable)
    {
        this.itemValue = itemValue;
        this.title.FormattedAsset = title;
        this.description.FormattedAsset = description;

        this.subtext.FormattedAsset = subtext;
        this.subtext.gameObject.SetActive(subtext != null);

        selectedIndicator.SetActive(isPicked);
        openButton.SetActive(openable);
        pickButton.SetActive(!openable);
    }

    public void SetupDisplayer(string itemValue, GLFormattedLocalizedText title, GLFormattedLocalizedText description)
    {
        this.itemValue = itemValue;
        this.title.FormattedAsset = title;
        this.description.FormattedAsset = description;

        selectedIndicator.SetActive(false);
        openButton.SetActive(false);
        pickButton.SetActive(false);
        subtext.gameObject.SetActive(false);
    }

    public void OnOpenPress()
    {
        GLFormAdvancedPickerScreen.I.Open(itemValue);
    }

    public void OnPickPress()
    {
        GLFormAdvancedPickerScreen.I.Pick(itemValue);
    }
}
