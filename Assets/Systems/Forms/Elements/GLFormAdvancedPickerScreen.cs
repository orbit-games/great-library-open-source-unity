﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

using PickerOption = GLForm.AdvancedPickerOptionElement;
using PickerSetup = GLForm.AdvancedPickerElement;


public class GLFormAdvancedPickerScreen : GLSingletonBehaviour<GLFormAdvancedPickerScreen>
{
    protected override void OnSingletonInitialize()
    {
        GLTransition.Disable(this);
    }

    [Header("Localization")]
    public LocalizedText Label_OptionsRoot;
    public LocalizedText Label_AvailableSuboptions;

    [Header("Settings")]
    public float doubleClickTime = 0.5f;

    [Header("References")]
    public GLPoolManagedList optionsList;
    public GLPoolManagedList pickedCrumbsList;
    public GLPoolManagedList browseCrumbsList;

    [Space(10f)]
    public GameObject openedItemContainer;
    public GLFormAdvancedPickerItem openedItemDisplayer;
    public GLFormAdvancedPickerItem pickedItemDisplayer;

    [Space(10f)]
    public GameObject acceptButton;
    public GameObject closeButton;
    public GameObject backButton;

    [Space(10f)]
    public GameObject nothingSelectedIndicator;

    private Action<string> onSelect;
    private PickerSetup setup;
    private string originalItemValue;
    private string pickedItemValue;
    private string openedItemValue;
    private float lastPickTime = 0f;

    private void Update()
    {
        if (!GLPopup.IsPopupActive() && GLInput.WantsToEscape())
        {
            Cancel();
        }
        else if (!GLPopup.IsPopupActive() && GLInput.WantsToSubmit())
        {
            Accept();
        }
    }

    public static bool IsShowingPicker()
    {
        return I.gameObject.activeSelf;
    }

    public static void ShowPicker(PickerSetup setup, string current, Action<string> onSelect)
    {
        I.SetupPicker(setup, current, onSelect);
    }

    private void SetupPicker(PickerSetup setup, string current, Action<string> onSelect)
    {
        GLTransition.In(this);

        this.onSelect = onSelect;
        this.setup = setup;
        originalItemValue = current;
        openedItemValue = current;
        lastPickTime = 0f;

        if (!current.IsNullOrEmpty())
        {
            var currentItem = setup.optionElementMapper[current];
            if (currentItem.suboptions.Count == 0)
            {
                openedItemValue = currentItem.parentOption?.optionValue;
            }
        }

        Pick(current);
    }

    private string GetParent(string ofItemWithValue)
    {
        return setup.optionElementMapper[ofItemWithValue].parentOption?.optionValue;
    }

    private GLFormattedLocalizedText GetDescription(string value)
    {
        return setup.optionElementMapper[value].descriptionText;
    }

    private GLFormattedLocalizedText GetTitle(string value)
    {
        if (value.IsNullOrEmpty())
        {
            return Label_OptionsRoot;
        }
        else
        {
            return setup.optionElementMapper[value].labelText;
        }
    }

    private List<string> GetParentCrumbs(string fromValue, bool includeRoot = true)
    {
        // get parent crumbs
        var parents = new List<string>();
        var parent = fromValue;
        while (!parent.IsNullOrEmpty())
        {
            parent = GetParent(parent);
            if (!includeRoot && parent.IsNullOrEmpty()) continue;
            if (parent == null) parent = "";
            parents.Add(parent);
        }
        parents.Reverse();
        return parents;
    }

    public void Pick(string pickedItemValue)
    {
        // check double click
        var timeSinceLastPick = Time.realtimeSinceStartup - lastPickTime;
        lastPickTime = Time.realtimeSinceStartup;
        if (pickedItemValue == this.pickedItemValue && timeSinceLastPick < doubleClickTime)
        {
            Accept();
            return;
        }

        // set picked
        this.pickedItemValue = pickedItemValue;

        if (pickedItemValue.IsNullOrEmpty())
        {
            nothingSelectedIndicator.SetActive(true);
            acceptButton.SetActive(false);
            pickedItemDisplayer.gameObject.SetActive(false);
        }
        else
        {
            nothingSelectedIndicator.SetActive(false);
            acceptButton.SetActive(true);
            pickedItemDisplayer.gameObject.SetActive(true);

            // get item
            var pickedItem = setup.optionElementMapper[pickedItemValue];

            // build crumbs
            var parents = GetParentCrumbs(pickedItemValue, includeRoot: false);
            pickedCrumbsList.SyncFully<string, GLFormAdvancedPickerCrumb>(
                parents, i => i, (value, crumb) => crumb.Setup(value, GetTitle(value)));

            // build the displayer for the picked item
            pickedItemDisplayer.SetupDisplayer(pickedItemValue, pickedItem.labelText, pickedItem.descriptionText);
        }

        Open(openedItemValue);
    }

    public void Open(string openedItemValue)
    {
        // set opened
        this.openedItemValue = openedItemValue;

        // get parent crumbs
        var parents = GetParentCrumbs(openedItemValue, includeRoot: true);

        // get all options to list, and build the crumbs list if necessary
        List<PickerOption> suboptions;
        if (parents.Count > 0)
        {
            // get item
            var openedItem = setup.optionElementMapper[openedItemValue];

            // show back button
            backButton.SetActive(true);

            // build the crumbs list
            browseCrumbsList.SyncFully<string, GLFormAdvancedPickerCrumb>(
                parents, i => i, (value, crumb) => crumb.Setup(value, GetTitle(value)));

            // we have a parent, lets display the opened item
            openedItemContainer.SetActive(true);
            openedItemDisplayer.SetupPickable(openedItemValue, openedItem.labelText, openedItem.descriptionText, null, pickedItemValue == openedItemValue, false);

            // get the suboptions
            suboptions = setup.optionElementMapper[openedItemValue].suboptions;
        }
        else
        {
            // hide back button
            backButton.SetActive(false);

            // clear crumbs
            browseCrumbsList.Clear();

            // no parent selected, we are in root
            openedItemContainer.SetActive(false);
            suboptions = setup.subelements;
        }

        // build options
        optionsList.SyncFully<PickerOption, GLFormAdvancedPickerItem>(
            suboptions, i => i.optionValue, (option, item) =>
            {
                GLFormattedLocalizedText subtext = null;
                int subOptionsCount = CalculateTotalSuboptionsCount(option);
                if (subOptionsCount > 0)
                {
                    subtext = Label_AvailableSuboptions.Format(subOptionsCount.ToString());
                }

                item.SetupPickable(option.optionValue, option.labelText, option.descriptionText, subtext, pickedItemValue == option.optionValue, subOptionsCount > 0);
            });
    }

    public void Back()
    {
        if (openedItemValue.IsNullOrEmpty()) return;
        var parent = GetParent(openedItemValue);
        Open(parent);
    }

    private int CalculateTotalSuboptionsCount(PickerOption option)
    {
        return option.suboptions.Count + option.suboptions.Sum(i => CalculateTotalSuboptionsCount(i));
    }

    public void Accept()
    {
        onSelect?.Invoke(pickedItemValue);
        GLTransition.Out(this);
    }

    public void Cancel()
    {
        GLTransition.Out(this);
    }
}
