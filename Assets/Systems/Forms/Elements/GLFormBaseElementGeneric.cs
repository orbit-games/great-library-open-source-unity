﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GLFormBaseElementGeneric<T> : GLFormBaseElementBehaviour where T : GLForm.BaseElement 
{
    /// <summary>
    /// The setup used to 
    /// </summary>
    public T elementSetup;
    public GLFormBaseElementBehaviour parentElement;
    [Buttons("Setup","BuildSetup")]
    public ButtonsContainer debug;
    
    /// <summary>
    /// Remembers the setup and initiates the construction of this element
    /// </summary>
    public override void Setup(GLForm parentForm, GLForm.BaseElement elementSetup, GLFormBaseElementBehaviour parentElement = null)
    {
        this.parentForm = parentForm;
        this.elementSetup = (T)elementSetup;
        this.parentElement = parentElement;
        BuildSetup();
    }

}