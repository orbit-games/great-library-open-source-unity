﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLFormButton : GLFormBaseElementGeneric<GLForm.ButtonElement>
{
    [Header("References")]
    public LocalizedTextBehaviour buttonField;
    public Button button;

    public override void PutSelectablesInList(List<Selectable> selectables)
    {
        selectables.Add(button);
    }

    public override void ReadDataFrom(GLForm.Data data, bool resetOtherwise)
    {
        // nothing to do here
    }

    public override void WriteDataTo(GLForm.Data data)
    {
        // nothing to do here
    }

    public override void CalculatePoints(ref int points)
    {
        // nothing to do here
    }

    protected override void BuildSetup()
    {
        buttonField.FormattedAsset = elementSetup.labelText;
        button.transition = Selectable.Transition.ColorTint; // hack to reset tint transition animation
        button.interactable = !elementSetup.disabled;
    }

    public override void Remove()
    {
        GLPool.removeCopy(this);
    }

    public void OnPress()
    {
        var setup = elementSetup;
        switch (setup.buttonType)
        {
            case GLForm.ButtonType.Submit:
                parentForm.SubmitForm();
                break;
            case GLForm.ButtonType.Reset:
                parentForm.ResetForm();
                break;
            default:
                break;
        }
        if (setup.onPress != null)
        {
            setup.onPress.Invoke();
        }
    }
}