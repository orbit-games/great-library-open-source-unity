﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class GLFormBaseGroup<T,W> : GLFormBaseElementGeneric<T> where T : GLForm.Setups.BaseGroupSetup<W> where W : GLForm.BaseElement
{
    public List<GLFormBaseElementBehaviour> subelements = new List<GLFormBaseElementBehaviour>();

    [Header("References")]
    public LocalizedTextBehaviour labelField;
    public Transform groupContainer;

    void Empty()
    {
        foreach (var e in subelements)
        {
            e.Remove();
        }
        subelements.Clear();
    }

    public override void PutSelectablesInList(List<Selectable> selectables)
    {
        foreach (var e in subelements)
        {
            e.PutSelectablesInList(selectables);
        }
    }

    public override void ReadDataFrom(GLForm.Data data, bool resetOtherwise)
    {
        foreach (var e in subelements)
        {
            e.ReadDataFrom(data, resetOtherwise);
        }
    }

    public override void WriteDataTo(GLForm.Data data)
    {
        foreach (var e in subelements)
        {
            e.WriteDataTo(data);
        }
    }

    public override void CalculatePoints(ref int points)
    {
        foreach (var e in subelements)
        {
            e.CalculatePoints(ref points);
        }
    }

    protected virtual GLFormBaseElementBehaviour GetPrefabFromElementSetup(GLForm.BaseElement elementSetup)
    {
        return GLFormManager.GetInstance().GetPrefabFromElementSetup(elementSetup);
    }

    protected override void BuildSetup()
    {
        Empty();

        if (elementSetup.labelText != null)
        {
            labelField.gameObject.SetActive(true);
            labelField.FormattedAsset = elementSetup.labelText;
        }
        else
        {
            labelField.LocalizedAsset = GLLocalization.EmptyText;
            labelField.gameObject.SetActive(false);
        }

        foreach (var subelementSetup in elementSetup.subelements)
        {
            var prefab = GetPrefabFromElementSetup(subelementSetup);
            var copy = GLPool.placeCopy(prefab, groupContainer);
            copy.Setup(parentForm, subelementSetup, this);
            subelements.Add(copy);
        }
    }

    public override void Remove()
    {
        Empty();
        GLPool.removeCopy(this);
    }
}
