﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLFormManager : GLSingletonBehaviour<GLFormManager> {

    [Header("Localization")]
    public LocalizedText Label_NothingSelected;

    [Header("References")]
    public GLFormTitle titlePrefab;
    public GLFormHeader headerPrefab;
    public GLFormPlainText plainTextPrefab;
    public GLFormTextField textFieldPrefab;
    public GLFormTextArea textAreaPrefab;
    public GLFormRichTextArea richTextAreaPrefab;
    public GLFormRadioButtons radioButtonsPrefab;
    public GLFormRadioButtonOption radioButtonOptionPrefab;
    public GLFormCheckboxes checkboxesPrefab;
    public GLFormCheckboxOption checkboxOptionPrefab;
    public GLFormDropdown dropdownPrefab;
    public GLFormDropdownOption dropdownOptionPrefab;
    public GLFormGroup groupPrefab;
    public GLFormButton buttonPrefab;
    public GLFormAdvancedPickerField advancedPickerPrefab;
    
    protected override void OnSingletonInitialize() { }

    public GLFormBaseElementBehaviour GetPrefabFromElementSetup(GLForm.BaseElement elementSetup)
    {
        if (elementSetup.GetType() == typeof(GLForm.TitleElement))
            return titlePrefab;
        if (elementSetup.GetType() == typeof(GLForm.HeaderElement))
            return headerPrefab;
        if (elementSetup.GetType() == typeof(GLForm.PlainTextElement))
            return plainTextPrefab;
        if (elementSetup.GetType() == typeof(GLForm.TextFieldElement))
            return textFieldPrefab;
        if (elementSetup.GetType() == typeof(GLForm.TextAreaElement))
            return textAreaPrefab;
        if (elementSetup.GetType() == typeof(GLForm.RichTextAreaElement))
            return richTextAreaPrefab;
        if (elementSetup.GetType() == typeof(GLForm.RadioButtonsElement))
            return radioButtonsPrefab;
        if (elementSetup.GetType() == typeof(GLForm.RadioButtonOptionElement))
            return radioButtonOptionPrefab;
        if (elementSetup.GetType() == typeof(GLForm.CheckboxesElement))
            return checkboxesPrefab;
        if (elementSetup.GetType() == typeof(GLForm.CheckboxOptionElement))
            return checkboxOptionPrefab;
        if (elementSetup.GetType() == typeof(GLForm.DropdownElement))
            return dropdownPrefab;
        if (elementSetup.GetType() == typeof(GLForm.DropdownOptionElement))
            return dropdownOptionPrefab;
        if (elementSetup.GetType() == typeof(GLForm.ElementsGroup))
            return groupPrefab;
        if (elementSetup.GetType() == typeof(GLForm.ButtonElement))
            return buttonPrefab;
        if (elementSetup.GetType() == typeof(GLForm.AdvancedPickerElement))
            return advancedPickerPrefab;
        return null;
    }
}
