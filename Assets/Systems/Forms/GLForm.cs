﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GLForm : MonoBehaviour, IPoolEventsListener
{
    #region setups
    // ###################################
    // ------------ SETUPS -------------
    // ###################################

    public enum ButtonType
    {
        Submit, Reset, Custom
    }

    public static class Setups
    {

        [Serializable]
        public class BaseElementSetup
        {
            public GLFormattedLocalizedText labelText;
            public GLFormattedLocalizedText descriptionText;
            public bool disabled;
        }

        [Serializable]
        public class ButtonSetup : BaseElementSetup
        {
            public ButtonType buttonType;
            public Action onPress;
        }

        [Serializable]
        public class BaseFieldSetup : BaseElementSetup
        {
            public string variableName;
            public string defaultValue = "";
        }

        [Serializable]
        public class TextFieldSetup : BaseFieldSetup
        {
            public TMPro.TMP_InputField.ContentType contentType = TMPro.TMP_InputField.ContentType.Standard;
            public GLFormattedLocalizedText hintText;
            public int characterLimit = 1024 * 32;
            public int preferredWidth = 160;
        }

        [Serializable]
        public class TextAreaSetup : TextFieldSetup
        {
            public int preferredHeight = 160;
            public TextAreaSetup()
            {
                preferredWidth = 260;
            }
        }

        [Serializable]
        public class OptionSetup : BaseElementSetup
        {
            public string optionValue;
            public int optionPoints;
        }

        [Serializable]
        public class BooleanSetup : BaseFieldSetup
        {
            public int optionPoints;
        }

        [Serializable]
        public class AdvancedPickerOptionSetup<T> : OptionSetup where T : AdvancedPickerOptionSetup<T>
        {
            public T parentOption;
            public List<T> suboptions = new List<T>();
            public void AddSubOption(T suboption)
            {
                suboptions.Add(suboption);
            }
        }

        [Serializable]
        public class BaseGroupSetup<T> : BaseElementSetup, BaseElement // design flaw cheat, needed due to monobehaviour hierarchy for groups to avoid duplicate code
        {
            public List<T> subelements = new List<T>();
            public W AddElement<W>(W element) where W : T
            {
                subelements.Add(element);
                return element;
            }
        }

        [Serializable]
        public class BaseFieldGroupSetup<T> : BaseGroupSetup<T>
        {
            // design flaw cheat, needed because I can't override both the basegroupsetup AND the basefieldsetup
            public string variableName;
            public string defaultValue = "";
        }
    }
    #endregion
    #region elements
    // ###################################
    // ------------ ELEMENTS -------------
    // ###################################

    public interface BaseElement { }
    
    public class TitleElement : Setups.BaseElementSetup, BaseElement { }
    
    public class HeaderElement : Setups.BaseElementSetup, BaseElement { }
    
    public class PlainTextElement : Setups.BaseElementSetup, BaseElement
    {
        public int preferredWidth = 160;
    }
    
    public class TextFieldElement : Setups.TextFieldSetup, BaseElement { }
    
    public class TextAreaElement : Setups.TextAreaSetup, BaseElement { }
    
    public class RichTextAreaElement : Setups.TextAreaSetup, BaseElement { }
    
    public class RadioButtonsElement : Setups.BaseFieldGroupSetup<RadioButtonOptionElement>, BaseElement { }
    
    public class RadioButtonOptionElement : Setups.OptionSetup, BaseElement { }
    
    public class CheckboxesElement : Setups.BaseGroupSetup<CheckboxOptionElement>, BaseElement { }
    
    public class CheckboxOptionElement : Setups.BooleanSetup, BaseElement { }
    
    public class DropdownElement : Setups.BaseFieldGroupSetup<DropdownOptionElement>, BaseElement { }
    
    public class DropdownOptionElement : Setups.OptionSetup, BaseElement { }
    
    public class AdvancedPickerElement : Setups.BaseFieldGroupSetup<AdvancedPickerOptionElement>, BaseElement {
        public Dictionary<string, AdvancedPickerOptionElement>  optionElementMapper;
    }
    
    public class AdvancedPickerOptionElement : Setups.AdvancedPickerOptionSetup<AdvancedPickerOptionElement>, BaseElement { }
    
    public class ElementsGroup : Setups.BaseGroupSetup<BaseElement>
    {
        public enum Direction { Horizontal, Vertical }
        public Direction direction = Direction.Horizontal;
    }
    
    public class ButtonElement : Setups.ButtonSetup, BaseElement { }


    #endregion
    #region data
    // #############################
    // ------------ DATA -----------
    // #############################

    [Serializable]
    public struct VarValuePair
    {
        public string name;
        public string value;

        public override string ToString()
        {
            return "" + name + " = " + value + "";
        }
    }

    [Serializable]
    public class Data
    {
        public List<VarValuePair> data = new List<VarValuePair>();
        Dictionary<string, int> varToIndex = new Dictionary<string, int>();

        public int Count
        {
            get
            {
                return data.Count;
            }
        }

        public VarValuePair this[int index]
        {
            get
            {
                return data.GetOrDefault(index);
            }
        }

        public void SetData(string name, string value)
        {
            if (!varToIndex.ContainsKey(name))
            {
                if (name == null || name.Length < 0)
                {
                    Debug.LogError("Failed to set data because variable name was empty or null.");
                }
                else
                {
                    varToIndex.Add(name, data.Count);
                    data.Add(new VarValuePair() { name = name, value = value });
                }
            }
            else
            {
                int index = varToIndex[name];
                data[index] = new VarValuePair() {
                    name = data[index].name,
                    value = value
                };
            }
        }

        public string this[string name]
        {
            get
            {
                return GetData(name);
            }
            set
            {
                SetData(name, value);
            }
        }

        public string GetData(string name)
        {
            if (varToIndex.ContainsKey(name))
            {
                return data[varToIndex[name]].value;
            }
            return null;
        }

        public void Clear()
        {
            data.Clear();
            varToIndex.Clear();
        }

        public override string ToString()
        {
            string result = "";
            foreach (var pair in data)
            {
                result += pair.ToString() + "\n";
            }
            return result;
        }
    }
    #endregion
    #region form setup
    // ##################################
    // ------------ FORM SETUP ----------
    // ##################################

    [Serializable]
    public class Setup
    {
        public List<BaseElement> elements = new List<BaseElement>();
        public Data data = new Data();

        public Action<Data> onSubmit;
        public Action onReset;

        public T AddElement<T>(T element) where T : BaseElement
        {
            elements.Add(element);
            return element;
        }

        public T AddElement<T>(T element, string value) where T : BaseElement
        {
            elements.Add(element);
            if (element is Setups.BaseFieldSetup)
            {
                var variableName = (element as Setups.BaseFieldSetup).variableName;
                if (variableName != null && variableName.Length > 0) {
                    data[variableName] = value;
                }
            }
            return element;
        }

        public void AddData(string name, string value)
        {
            data[name] = value;
        }
    }
    #endregion
    #region helpers
    // ##################################
    // ------------ HELPERS -----------
    // ##################################

    public class SetupHelper
    {
        public Setup formSetup;
        public ElementsGroup fields;
        public ElementsGroup buttons;

        public void AddResetButton(GLFormattedLocalizedText resetLabel)
        {
            buttons.AddElement(new ButtonElement()
            {
                buttonType = ButtonType.Reset,
                labelText = resetLabel
            });
        }

        public void AddSubmitButton(GLFormattedLocalizedText resetLabel, Action onPress)
        {
            buttons.AddElement(new ButtonElement()
            {
                buttonType = ButtonType.Submit,
                labelText = resetLabel,
                onPress = () =>
                {
                    onPress?.Invoke();
                }
            });
        }

        public void AddCustomButton(GLFormattedLocalizedText label, Action onPress)
        {
            buttons.AddElement(new ButtonElement()
            {
                buttonType = ButtonType.Custom,
                labelText = label,
                onPress = () =>
                {
                    onPress?.Invoke();
                }
            });
        }

        public TextFieldElement AddTextField(GLFormattedLocalizedText label, string variableName, GLFormattedLocalizedText hint = null, int width = 200)
        {
            return fields.AddElement(new TextFieldElement()
            {
                labelText = label,
                hintText = hint,
                variableName = variableName,
                preferredWidth = width
            });
        }

        public AdvancedPickerElement AddAdvancedPicker<T>(GLFormattedLocalizedText label, string variableName, IList<T> items, Func<T, string> toParentValue, Func<T, string> toValue, Func<T, string> toName, Func<T, string> toDescription, string defaultValue = null)
        {
            var advancedPickerElement = fields.AddElement(new AdvancedPickerElement()
            {
                labelText = label,
                variableName = variableName,
                defaultValue = defaultValue,
                optionElementMapper = new Dictionary<string, AdvancedPickerOptionElement>()
            });
            
            // creating the options
            foreach (var item in items)
            {
                var value = toValue?.Invoke(item);
                var element = new AdvancedPickerOptionElement
                {
                    labelText = toName?.Invoke(item),
                    descriptionText = toDescription?.Invoke(item),
                    optionValue = value
                };
                advancedPickerElement.optionElementMapper.Add(value, element);
            }

            // managing hierarchy
            foreach (var item in items)
            {
                var parent = toParentValue?.Invoke(item);
                var value = toValue?.Invoke(item);
                var element = advancedPickerElement.optionElementMapper[value];

                if (parent == null)
                {
                    advancedPickerElement.AddElement(element);
                }
                else
                {
                    var parentOption = advancedPickerElement.optionElementMapper[parent];
                    parentOption.AddSubOption(element);
                    element.parentOption = parentOption;
                }

            }

            return advancedPickerElement;
        }

        public DropdownElement AddDropdown<T>(GLFormattedLocalizedText label, string variableName, IList<T> items, Func<T, string> toValue, Func<T, string> toName, string defaultValue = null)
        {
            var dropdownElement = fields.AddElement(new DropdownElement()
            {
                labelText = label,
                variableName = variableName,
                defaultValue = defaultValue,
            });

            foreach (var item in items)
            {
                dropdownElement.AddElement(new DropdownOptionElement
                {
                    labelText = (toName(item)),
                    optionValue = toValue(item),
                });
            }

            return dropdownElement;
        }

        public RadioButtonsElement AddRadioButtons<T>(GLFormattedLocalizedText label, string variableName, IList<T> items, Func<T, string> toValue, Func<T, string> toName, string defaultValue = null)
        {
            var radioElement = fields.AddElement(new RadioButtonsElement()
            {
                labelText = label,
                variableName = variableName,
                defaultValue = defaultValue,
            });

            foreach (var item in items)
            {
                radioElement.AddElement(new RadioButtonOptionElement
                {
                    labelText = (toName(item)),
                    optionValue = toValue(item),
                });
            }

            return radioElement;
        }

        public CheckboxesElement AddCheckboxList<T>(GLFormattedLocalizedText label, string variableName, IList<T> items, Func<T, string> toName)
        {
            var radioElement = fields.AddElement(new CheckboxesElement()
            {
                labelText = label
            });

            var index = 0;
            foreach (var item in items)
            {
                radioElement.AddElement(new CheckboxOptionElement
                {
                    labelText = (toName(item)),
                    variableName = variableName + index
                });
                index++;
            }

            return radioElement;
        }

        public TextAreaElement AddTextArea(GLFormattedLocalizedText label, string variableName, GLFormattedLocalizedText hint = null, int height = 200, int width = 200)
        {
            return fields.AddElement(new TextAreaElement()
            {
                labelText = label,
                hintText = hint,
                variableName = variableName,
                preferredWidth = width,
                preferredHeight = height
            });
        }

        public RichTextAreaElement AddRichTextArea(GLFormattedLocalizedText label, string variableName, GLFormattedLocalizedText hint = null, int height = 200, int width = 200)
        {
            return fields.AddElement(new RichTextAreaElement()
            {
                labelText = label,
                hintText = hint,
                variableName = variableName,
                preferredWidth = width,
                preferredHeight = height
            });
        }

        public HeaderElement AddHeader(GLFormattedLocalizedText label)
        {
            return fields.AddElement(new HeaderElement()
            {
                labelText = label
            });
        }

        public PlainTextElement AddPlainText(GLFormattedLocalizedText text)
        {
            return fields.AddElement(new PlainTextElement()
            {
                labelText = text
            });
        }

        public TextFieldElement AddPasswordField(GLFormattedLocalizedText label, string variableName, int width = 200)
        {
            return fields.AddElement(new TextFieldElement()
            {
                labelText = label,
                hintText = GLLocalization.EmptyText,
                contentType = TMPro.TMP_InputField.ContentType.Password,
                variableName = variableName,
                preferredWidth = width
            });
        }

        public CheckboxOptionElement AddCheckbox(GLFormattedLocalizedText label, string variableName, bool defaultValue = false)
        {
            return fields.AddElement(new CheckboxOptionElement()
            {
                labelText = label,
                variableName = variableName,
                defaultValue = defaultValue.ToString()
            });
        }
    }

    public static SetupHelper GenerateBasicForm(GLFormattedLocalizedText title, GLFormattedLocalizedText description, GLFormattedLocalizedText submitText, Action<Data> onSubmit)
    {
        SetupHelper helper = new SetupHelper();
        helper.formSetup = new Setup();
        helper.formSetup.onSubmit = onSubmit;
        if (title != null)
        {
            helper.formSetup.AddElement(new TitleElement()
            {
                labelText = title
            });
        }
        if (description != null)
        {
            helper.formSetup.AddElement(new PlainTextElement()
            {
                labelText = description
            });
        }

        helper.fields = helper.formSetup.AddElement(new ElementsGroup() { direction = ElementsGroup.Direction.Vertical });
        
        helper.buttons = helper.formSetup.AddElement(new ElementsGroup() { });
        if (submitText != null)
        {
            helper.buttons.AddElement(new ButtonElement()
            {
                buttonType = ButtonType.Submit,
                labelText = submitText
            });
        }

        return helper;
    }

    #endregion

    // ##################################
    // ------------ BEHAVIOUR -----------
    // ##################################

    private static int updateFrame;
    private static GLForm activeForm;
    private static int selectedIndex;
    private static Selectable selectAtEndOfFrame;
    private static bool nextSelectedChanged = false;
    private static List<GLForm> builtForms = new List<GLForm>();

    public static void SetNextSelected(Selectable selectAtEndOfFrame)
    {
        nextSelectedChanged = true;
        GLForm.selectAtEndOfFrame = selectAtEndOfFrame;
    }

    private static void DoSelectNextObject()
    {
        if (!nextSelectedChanged) return;
        nextSelectedChanged = false;

        selectAtEndOfFrame?.Select();
        EventSystem.current.SetSelectedGameObject(selectAtEndOfFrame?.gameObject);
    }

    public static GLForm GetActiveForm()
    {
        return activeForm;
    }

    public static GLForm IsFormActive()
    {
        return activeForm;
    }

    public static Selectable GetActiveSelectable()
    {
        DetectActiveForm();
        if (activeForm != null)
        {
            if (selectedIndex >= 0)
            {
                return activeForm.selectables[selectedIndex];
            }
        }
        return null;
    }

    public static bool IsInputActive()
    {
        var activeSelectable = GetActiveSelectable();
        if (activeSelectable != null)
        {
            if (activeSelectable is TMPro.TMP_InputField) return true;
            if (activeSelectable is InputField) return true;
        }
        if (GLFormAdvancedPickerScreen.IsShowingPicker())
        {
            return true;
        }
        return false;
    }

    private static void DetectActiveForm()
    {
        if (updateFrame != Time.frameCount)
        {
            updateFrame = Time.frameCount;
            activeForm = null;
            selectedIndex = 0;

            var currentActiveObject = EventSystem.current.currentSelectedGameObject;
            foreach (var form in builtForms)
            {
                var index = form.selectables.FindIndex(selectable => selectable.gameObject == currentActiveObject);
                if (index >= 0)
                {
                    selectedIndex = index;
                    activeForm = form;
                    break;
                }
            }
        }
    }

    private static bool IsFormActive(GLForm active)
    {
        DetectActiveForm();
        return activeForm == active;
    }

    private List<GLFormBaseElementBehaviour> elements = new List<GLFormBaseElementBehaviour>();
    private List<Selectable> selectables = new List<Selectable>();
    private Setup formSetup = new Setup();

    void Empty()
    {
        foreach (var e in elements)
        {
            e.Remove();
        }
        elements.Clear();
        selectables.Clear();
        builtForms.RemoveAll(form => form == this);
    }

    public void OnDestroy() { Empty(); }
    public void OnRemovedToPool() { Empty(); }
    public void OnPlacedFromPool() { Empty(); }
    public void OnCreatedForPool() { Empty(); }

    public void SetupForm(SetupHelper helper, bool autoSelect = true) { SetupForm(helper.formSetup, autoSelect); }
    public void SetupForm(Setup formSetup, bool autoSelect = true)
    {
        this.formSetup = formSetup;

        Empty();

        foreach (var elementSetup in formSetup.elements)
        {
            var prefab = GLFormManager.GetInstance().GetPrefabFromElementSetup(elementSetup);
            var copy = GLPool.placeCopy(prefab, this);
            copy.Setup(this, elementSetup);
            copy.PutSelectablesInList(selectables);
            elements.Add(copy);
        }

        builtForms.Add(this);

        foreach (var element in elements)
        {
            element.ReadDataFrom(formSetup.data, true);
        }

        if (!selectables.IsNullOrEmpty() && autoSelect)
        {
            SetNextSelected(selectables[0]);
        }
    }

    public void SetValue(string variableName, string value)
    {
        Data data = new Data();
        data[variableName] = value;
        SetValues(data);
    }

    public void SetValues(Data data)
    {
        foreach (var e in elements)
        {
            e.ReadDataFrom(data, resetOtherwise: false);
        }
    }

    public void ResetForm()
    {
        foreach (var e in elements)
        {
            e.ReadDataFrom(formSetup.data, resetOtherwise: true);
        }
        formSetup.onReset?.Invoke();
    }

    public Data GetData()
    {
        Data data = new Data();
        foreach (var e in elements)
        {
            e.WriteDataTo(data);
        }
        return data;
    }

    public Data SubmitForm()
    {
        var data = GetData();
        formSetup?.onSubmit(data);
        EventSystem.current.SetSelectedGameObject(null);
        GLScreenRecord.I.RecordThisFrame();
        return data;
    }

    private void PressButton(Selectable selectable)
    {
        if (selectable is Button)
        {
            Button btn = (Button)selectable;
            (selectable as Button).onClick?.Invoke();

            var meta = btn.GetComponent<GLFormMetaSettings>();

            if (meta == null || !meta.keepSelectedOnPress)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }

    // logic to cycle through selectables using the tab key
    // and submitting or pressing buttons
    void Update ()
    {
        if (selectables.Count > 0 && IsFormActive(this))
        {
            bool updateSelectable = false;

            if (GLInput.IsForwardCyclePressed())
            {
                updateSelectable = true;
                selectedIndex = (selectedIndex + 1) % selectables.Count;
            } else if (GLInput.IsBackwardCyclePressed())
            {
                updateSelectable = true;
                selectedIndex = (selectedIndex + selectables.Count - 1) % selectables.Count;
            }

            if (updateSelectable)
            {
                SetNextSelected(selectables[selectedIndex]);
            }

            var selectable = selectables.GetOrDefault(selectedIndex);
            if (selectable != null)
            {
                if (selectable is Button && GLInput.WantsToPressButton())
                {
                    PressButton(selectable);
                    GLScreenRecord.I.RecordThisFrame();
                    return;
                }

                var preventSubmit = 
                    (selectable is TMP_InputField) 
                    && (selectable as TMP_InputField).multiLine 
                    && (selectable as TMP_InputField).lineType == TMP_InputField.LineType.MultiLineNewline;

                if ((GLInput.WantsToSubmit() && !preventSubmit)
                    || GLInput.WantsToReallySubmit())
                {
                    SubmitForm();
                    return;
                }
            }
        }
    }

    private void LateUpdate()
    {
        DoSelectNextObject();
    }
}
