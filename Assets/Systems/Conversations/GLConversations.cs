﻿using IO.Swagger.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLConversations : GLSingletonBehaviour<GLConversations>
{
    protected override void OnSingletonInitialize() 
    {
        StartCoroutine(refreshConversations());
    }

    public float conversationShownReloadSpeed = 2.5f;
    public float conversationNotShownReloadSpeed = 15f;
    private int conversationsShown = 0;

    public void OnConversationShown()
    {
        conversationsShown++;
    }

    public void OnConversationHidden()
    {
        conversationsShown--;
    }

    public void EnsureConversationsWithAllStudents()
    {
        var course = GLApi.GetActiveCourse();
        var students = course.Users;

        var conversations = GLApi.GetAllConversationKnowledge();
        var conversationsWithStudents = new HashSet<string>();

        foreach (var conversation in conversations)
        {
            if (conversation.Members?.Count == 2)
            {

            }
        }

        foreach (var courseProgressSummaries in students)
        {

        }
    }

    IEnumerator refreshConversations()
    {
        while (true)
        {
            float reloadWaitTime = 0f;

            while (true)
            {
                yield return null;
                reloadWaitTime += Time.deltaTime;

                // just restart refreshing when no course or user is active
                if (!GLApi.IsLoggedInAndCourseSelected())
                {
                    reloadWaitTime = 0f;
                }

                // if a conversation is shown, we should surely reload the conversation
                if (conversationsShown > 0 && reloadWaitTime > conversationShownReloadSpeed)
                {
                    break;
                }
                // otherwise we do a slow reload time
                else if (reloadWaitTime > conversationNotShownReloadSpeed)
                {
                    break;
                }
            }

            var course = GLApi.GetActiveCourse();
            var allConversationsInsight = GLInsights.GetAllConversationsInsight();
            var task = GLApi.FetchChatConversations(course.Ref, allConversationsInsight.upToDateUntil);

            // wait untill task is completed, then restart the counter!
            while (!task.IsCompleted())
            {
                yield return null;
            }
        }
    }
}
