﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnlockable
{
    Rarity GetRarity();
}
