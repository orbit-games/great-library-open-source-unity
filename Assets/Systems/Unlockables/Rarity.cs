﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Rarity
{
    AVAILABLE,
    COMMON,
    RARE,
    EPIC,
    LEGENDARY,
    QUEST_ONLY
}
