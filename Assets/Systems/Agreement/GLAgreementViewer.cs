﻿using GameToolkit.Localization;
using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLAgreementViewer : GLSingletonBehaviour<GLAgreementViewer>
{
    protected override void OnSingletonInitialize()
    {
        GLTransition.Out(agreementContainer);
    }

    [Header("References")]
    public PDFViewer agreementViewer;
    public GameObject agreementContainer;

    private Action onDecline;
    private Action onAccept;

    public void ShowAgreement(string url, Action onAccept, Action onDecline)
    {
        GLTransition.In(agreementContainer);
        agreementViewer.LoadDocumentFromWeb(url, null);
        this.onAccept = onAccept;
        this.onDecline = onDecline;
    }

    public void OnAcceptPress()
    {
        GLTransition.Out(agreementContainer);
        onAccept?.Invoke();
        onAccept = null;
    }

    public void OnDeclinePress()
    {
        GLTransition.Out(agreementContainer);
        onDecline?.Invoke();
        onDecline = null;
    }
}
