﻿using GameToolkit.Localization;
using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAgreementManager : GLSingletonBehaviour<GLAgreementManager> {

    protected override void OnSingletonInitialize() { }

    [Header("Localization")]
    public LocalizedText Label_AgreementUpdated;
    public LocalizedText Label_AgreementOnRegistration;
    public LocalizedText Label_AgreementDeclined;
    public LocalizedText Description_AgreementUpdated;
    public LocalizedText Description_AgreementOnRegistration;
    public LocalizedText Description_AgreementDeclined;
    public LocalizedText Label_ShowAgreement;
    public LocalizedText Label_Back;
    public LocalizedText Label_Exit;
    
    public void LoadAgreement(Action onFinished)
    {
        GLLoadingOverlay.ShowFullcoverLoading(GLApi.FetchUserAgreement(), (result) =>
        {
            if (!GLApi.DiplayErrorPopupIfFailed(result))
            {
                onFinished?.Invoke();
            }
            else
            {
                GLPopup.MakeErrorPopup(("Failed getting agreement from server"));
                Debug.LogError(result.ErrorMessage);
            }
        }
        );
    }
    
    public void HandleLogin(Action onLoginHandled, Action onLoginFailed)
    {
        var user = GLApi.GetActiveUser();
        if (user == null)
        {
            onLoginFailed?.Invoke();
            return;
        }
        var agreement = GLApi.GetLatestAgreementKnowledge();
        if (user.SignedUserAgreementRef != agreement.Ref)
        {
            GLPopup.MakeNotificationPopup(Label_AgreementUpdated, Description_AgreementUpdated, () =>
            {
                ShowAgreement(() =>
                {
                    SignAndSendAgreementForActiveUser(onLoginHandled, onLoginFailed);
                },
                    () =>
                    {
                        ShowAgreementDeclinedPopup(onLoginFailed);
                    }
                );
            }).OverrideCloseButtonText(Label_ShowAgreement).Show();
        }
        else
        {
            onLoginHandled?.Invoke();
        }
    }
    
    public void ShowRegistrationAgreement(Action onRegistrationHandled, Action onRegistrationFailed)
    {
        GLPopup.MakeNotificationPopup(Label_AgreementOnRegistration, Description_AgreementOnRegistration, () =>
        {
            GetInstance().ShowAgreement(onRegistrationHandled, onRegistrationFailed);
        }).OverrideCloseButtonText(Label_ShowAgreement).Show();
    }
    
    private void SignAndSendAgreementForActiveUser(Action onSigned, Action onFailed)
    {
        var user = GLApi.GetActiveUser();
        var agreement = GLApi.GetLatestAgreementKnowledge();
        user.SignedUserAgreementRef = agreement.Ref;
        GLLoadingOverlay.ShowLoading(GLApi.UpdateUser(user), (result) =>
        {
            if (!result.Failed)
            {
                onSigned?.Invoke();
            }
            else
            {
                onFailed?.Invoke();
                Debug.LogError("Api problems, could not sign");
            }
        });
    }

    private void ShowAgreementDeclinedPopup(Action onAccept)
    {
        GLPopup.MakeErrorPopup(Label_AgreementDeclined, Description_AgreementDeclined, onAccept).Show();
    }

    private void ShowAgreement(Action onAccept, Action onDecline)
    {
        var agreement = GLApi.GetLatestAgreementKnowledge();
        GLAgreementViewer.GetInstance().ShowAgreement(agreement.PdfUrl, onAccept, onDecline);
    }
}