﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLUtility : MonoBehaviour
{

    public Text textField;

    public void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        textField.text = "Locally stored preferences have been cleared!";
        textField.color = new Color(0f, 0.5f, 0f);
    }

    public void Exit()
    {
        GLSystem.Quit();
    }
}
