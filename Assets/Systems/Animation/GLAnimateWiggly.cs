﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAnimateWiggly : MonoBehaviour {

    [Header("Time")]
    public float generalSpeed = 1f;

    [Header("Scale")]
    public bool useScale;
    public bool separateAxis = true;
    public Vector2 scaleEffect = new Vector2(0.05f, 0.05f);
    public Vector2 scaleSpeed = new Vector2(0.8f, 1.1f);
    private Vector2 scaleTime;
    private Vector2 scaleStart;

    [Header("Rotation")]
    public bool useRotation;
    public float rotationEffect = 5f;
    public float rotationSpeed = 1f;
    private float rotationTime;
    private float rotationStart;

    [Header("Position")]
    public bool usePosition = false;
    public Vector2 positionEffect = new Vector2(10, 10);
    public Vector2 positionSpeed = new Vector2(0.8f, 1.1f);
    private Vector2 positionTime;
    private Vector2 positionStart;

    [Header("Generator Settings")]
    public int timestamps = 15;
    public float minValueDifference = 1f;
    public float timeJiggle = 0.5f;

    [Header("Generated")]
    public AnimationCurve randomCurve;
    private bool startInitialized = false;
    private RectTransform rectTransform;

    private void Awake()
    {
        StartInitialize();

        randomCurve = new AnimationCurve();

        var up = true;
        var lastValue = -1f;
        var time = 0f;

        for (int i = 0; i < timestamps; i++)
        {
            var newValue = 0f;
            if (up)
            {
                newValue = Random.Range(lastValue + minValueDifference, 1f);
            }
            else
            {
                newValue = Random.Range(-1f, lastValue - minValueDifference);
            }

            randomCurve.AddKey(new Keyframe(time, newValue, 0, 0));

            var jiggle = Random.Range(-timeJiggle * 0.5f, -timeJiggle * 0.5f);
            time = time + 1f + jiggle;

            lastValue = newValue;
            up = !up;
        }

        randomCurve.postWrapMode = WrapMode.PingPong;
        randomCurve.preWrapMode = WrapMode.PingPong;
    }

    private void StartInitialize()
    {
        if (startInitialized) return;
        startInitialized = true;

        scaleStart = transform.localScale;
        scaleTime = new Vector3(Random.value * timestamps, Random.value * timestamps, Random.value * timestamps);

        rotationStart = transform.localEulerAngles.z;
        rotationTime = Random.value * timestamps;

        rectTransform = GetComponent<RectTransform>();
        positionStart = rectTransform != null ? rectTransform.anchoredPosition : (Vector2)transform.localPosition;
        positionTime = new Vector3(Random.value * timestamps, Random.value * timestamps, Random.value * timestamps);
    }

    private void OnDisable()
    {
        if (useRotation)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotationStart);
        }

        if (useScale)
        {
            transform.localScale = scaleStart;
        }

        if (usePosition)
        {
            if (rectTransform != null)
            {
                rectTransform.anchoredPosition = positionStart;
            }
            else
            {
                transform.localPosition = positionStart;
            }
        }
    }

    public void Reinitialize()
    {
        startInitialized = false;
        StartInitialize();
    }

    public void ResetStartPosition()
    {
        positionStart = transform.localPosition;
    }

    public void ResetStartRotation()
    {
        rotationStart = transform.localEulerAngles.z;
    }

    public void ResetStartScale()
    {
        scaleStart = transform.localScale;
    }

    void LateUpdate ()
    {
        var delateTime = Time.deltaTime * generalSpeed;

        if (useScale)
        {
            scaleTime.x = scaleTime.x + delateTime * scaleSpeed.x;
            float scaleX = scaleStart.x + scaleEffect.x * randomCurve.Evaluate(scaleTime.x);
            float scaleY = scaleX;

            if (separateAxis)
            {
                scaleTime.y = scaleTime.y + delateTime * scaleSpeed.y;
                scaleY = scaleStart.y + scaleEffect.y * randomCurve.Evaluate(scaleTime.y);
            }

            transform.localScale = new Vector2(scaleX, scaleY);
        }

        if (useRotation)
        {
            rotationTime = rotationTime + delateTime * rotationSpeed;
            float rotationZ = rotationStart + rotationEffect * randomCurve.Evaluate(rotationTime);

            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotationZ);
        }
        
        if (usePosition)
        {
            positionTime.x = positionTime.x + delateTime * positionSpeed.x;
            float positionX = positionStart.x + positionEffect.x * randomCurve.Evaluate(positionTime.x);
            float positionY = positionX;
            
            positionTime.y = positionTime.y + delateTime * positionSpeed.y;
            positionY = positionStart.y + positionEffect.y * randomCurve.Evaluate(positionTime.y);

            if (rectTransform != null)
            {
                rectTransform.anchoredPosition = new Vector2(positionX, positionY);
            }
            else
            {
                transform.localPosition = new Vector2(positionX, positionY);
            }
        }
    }


}
