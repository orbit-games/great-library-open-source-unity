﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLAnimateLooped : MonoBehaviour {

    [Header("Time")]
    public float generalSpeed = 1f;

    [Header("Scale")]
    public bool useScale;
    public bool separateAxis = true;
    public Vector2 scaleEffect = new Vector2(0.05f, 0.05f);
    public Vector2 scaleSpeed = new Vector2(0.8f, 1.1f);
    public Vector2 scaleStartTime;
    public bool randomizeScaleStartTimes;
    private Vector2 scaleTime;
    private Vector2 scaleStart;
    public AnimationCurve scaleCurve;

    [Header("Rotation")]
    public bool useRotation;
    public float rotationEffect = 5f;
    public float rotationSpeed = 1f;
    public float rotationStartTime;
    public bool randomizeRotationeStartTimes;
    private float rotationTime;
    private float rotationStart;
    public AnimationCurve rotationCurve;

    [Header("Position")]
    public bool usePosition = false;
    public Vector2 positionEffect = new Vector2(10, 10);
    public Vector2 positionSpeed = new Vector2(0.8f, 1.1f);
    public Vector2 positionStartTime;
    public bool randomizePositionStartTimes;
    private Vector2 positionTime;
    private Vector2 positionStart;
    public AnimationCurve positionCurve;

    private bool startInitialized = false;
    private RectTransform rectTransform;

    private void Awake()
    {
        StartInitialize();
    }

    private void StartInitialize()
    {
        if (startInitialized) return;
        startInitialized = true;

        scaleStart = transform.localScale;
        scaleTime = randomizeScaleStartTimes ? new Vector2(Random.value, Random.value) : scaleStartTime;

        rotationStart = transform.localEulerAngles.z;
        rotationTime = randomizeRotationeStartTimes ? Random.value : rotationStartTime;

        rectTransform = GetComponent<RectTransform>();
        positionStart = rectTransform != null ? rectTransform.anchoredPosition : (Vector2)transform.localPosition;
        positionTime = randomizePositionStartTimes ? new Vector2(Random.value, Random.value) : positionStartTime;
    }

    private void OnDisable()
    {
        if (useRotation)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotationStart);
        }

        if (useScale)
        {
            transform.localScale = scaleStart;
        }

        if (usePosition)
        {
            if (rectTransform != null)
            {
                rectTransform.anchoredPosition = positionStart;
            }
            else
            {
                transform.localPosition = positionStart;
            }
        }
    }

    public void Reinitialize()
    {
        startInitialized = false;
        StartInitialize();
    }

    public void ResetStartPosition()
    {
        positionStart = transform.localPosition;
    }

    public void ResetStartRotation()
    {
        rotationStart = transform.localEulerAngles.z;
    }

    public void ResetStartScale()
    {
        scaleStart = transform.localScale;
    }

    void LateUpdate ()
    {
        var delateTime = Time.deltaTime * generalSpeed;

        if (useScale)
        {
            scaleTime.x = scaleTime.x + delateTime * scaleSpeed.x;
            float scaleX = scaleStart.x + scaleEffect.x * scaleCurve.Evaluate(scaleTime.x);
            float scaleY = scaleX;

            if (separateAxis)
            {
                scaleTime.y = scaleTime.y + delateTime * scaleSpeed.y;
                scaleY = scaleStart.y + scaleEffect.y * scaleCurve.Evaluate(scaleTime.y);
            }

            transform.localScale = new Vector2(scaleX, scaleY);
        }

        if (useRotation)
        {
            rotationTime = rotationTime + delateTime * rotationSpeed;
            float rotationZ = rotationStart + rotationEffect * rotationCurve.Evaluate(rotationTime);

            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotationZ);
        }
        
        if (usePosition)
        {
            positionTime.x = positionTime.x + delateTime * positionSpeed.x;
            float positionX = positionStart.x + positionEffect.x * positionCurve.Evaluate(positionTime.x);
            float positionY = positionX;
            
            positionTime.y = positionTime.y + delateTime * positionSpeed.y;
            positionY = positionStart.y + positionEffect.y * positionCurve.Evaluate(positionTime.y);

            if (rectTransform != null)
            {
                rectTransform.anchoredPosition = new Vector2(positionX, positionY);
            }
            else
            {
                transform.localPosition = new Vector2(positionX, positionY);
            }
        }
    }


}
