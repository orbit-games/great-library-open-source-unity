﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLLoadingOverlay : GLSingletonBehaviour<GLLoadingOverlay>
{
    protected override void OnSingletonInitialize()
    {
        loader.onTasksCompleted = OnLoadingCompleted;
        GLTransition.Disable(fullcoverBackground);
        GLTransition.Disable(loadingContainer);
    }

    [Header("References")]
    public GameObject fullcoverBackground;
    public GameObject loadingContainer;
    public GLLoading loader;

    private List<GLTaskResult> loadResults = new List<GLTaskResult>();
    private Action<List<GLTaskResult>> onLoadingCompleted;

    public static void ShowLoading(IGLBaseTask task, Action<GLTaskResult> onLoaded = null)
    {
        GetInstance()._addTask(task, onLoaded, false);
    }

    public static void ShowLoading(IEnumerable<IGLBaseTask> tasks, Action<GLTaskResult> onLoaded = null)
    {
        foreach (var task in tasks)
        {
            GetInstance()._addTask(task, onLoaded, false);
        }
    }

    public static void ShowFullcoverLoading(IGLBaseTask task, Action<GLTaskResult> onLoaded = null)
    {
        GetInstance()._addTask(task, onLoaded, true);
    }

    public static void ShowFullcoverLoading(IEnumerable<IGLBaseTask> tasks, Action<GLTaskResult> onLoaded = null)
    {
        foreach (var task in tasks)
        {
            GetInstance()._addTask(task, onLoaded, true);
        }
    }

    public static void AddOnLoadingCompletedListener(Action<List<GLTaskResult>> onLoadingCompleted)
    {
        I.onLoadingCompleted += onLoadingCompleted;
    }

    private void _addTask(IGLBaseTask task, Action<GLTaskResult> onLoaded, bool fullcoverLoading = false)
    {
        GLTransition.In(loadingContainer);

        loader.AddTask(task, (result) => {
            loadResults.Add(result);
            onLoaded?.Invoke(result);
        });

        if (fullcoverLoading)
        {
            fullcoverBackground.SetActive(true);
            GLTransition.In(fullcoverBackground);
        }

        GLScreenRecord.I.RecordNextFrame();
    }

    private void OnLoadingCompleted()
    {
        onLoadingCompleted?.Invoke(loadResults);
        onLoadingCompleted = null;
        loadResults.Clear();

        GLTransition.Out(fullcoverBackground);
        GLTransition.Out(loadingContainer);
    }
}
