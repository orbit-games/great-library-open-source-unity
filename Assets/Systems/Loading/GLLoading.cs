﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GLLoading : MonoBehaviour {

    [Header("Settings")]
    public float minLoadTime;

    [Header("References")]
    public TMP_Text taskDescription;

    [Header("Dynamic State")]
    [ReadOnly]
    public bool loading;
    [ReadOnly]
    public float loadTime;
    [ReadOnly]
    public int todo;
    [ReadOnly]
    public int completed;

    public Action onTasksCompleted;

    private List<LoadingTask> loadingTasks = new List<LoadingTask>();

    private class LoadingTask
    {
        public Action<GLTaskResult> onLoaded;
        public IGLBaseTask task;
        public bool completed;

        public LoadingTask(IGLBaseTask task, Action<GLTaskResult> onLoaded)
        {
            this.onLoaded = onLoaded;
            this.task = task;
            completed = false;
        }
    }

    private void Awake()
    {
        ResetLoading();
    }

    public void AddTask(IGLBaseTask task, Action<GLTaskResult> onLoaded)
    {
        todo++;

        loading = true;
        loadTime = 0f;
        gameObject.SetActive(true);
        loadingTasks.Add(new LoadingTask(task, onLoaded));
    }

    public void Update()
    {
        if (!loading || todo == 0)
        {
            gameObject.SetActive(false);
            return;
        }

        loadTime += Time.deltaTime;

        if (completed == todo)
        {
            if (loadTime > minLoadTime)
            {
                // make a copy of the loaded tasks so we can reset, and then do there callbacks
                var completedTasks = loadingTasks.ToArray();

                // reset
                ResetLoading();
                
                // it is possible that from calling these callbacks new tasks
                // are added to be loaded, so we need to check this afterwards
                foreach (var completed in completedTasks)
                {
                    completed.onLoaded?.Invoke(completed.task.Result);
                }

                // are we done now? or have tasks been added?
                if (completed == todo)
                {
                    // we are done
                    onTasksCompleted?.Invoke();
                }
                else
                {
                    // we are not done, let's do another update to update this object immediately
                    Update();
                }
            }

            return;
        }

        var someTaskDescription = loadingTasks[0].task.GetDescription();
        foreach (var loading in loadingTasks)
        {
            if (someTaskDescription == null && !loading.completed)
                someTaskDescription = loading.task.GetDescription();

            if (loading.task.IsCompleted())
            {
                loading.completed = true;
                completed++;
            }
        }

        SetDescription(someTaskDescription);
    }

    private void SetDescription(LocalizedText text)
    {
        if (taskDescription == null) return;
        if (text == null) return;
        taskDescription.text = text.Value;
    }

    private void ResetLoading()
    {
        gameObject.SetActive(false);

        todo = 0;
        completed = 0;
        loadingTasks.Clear();

        loadTime = 0f;
        loading = false;

        if (taskDescription != null) 
            taskDescription.text = "";
    }

}
