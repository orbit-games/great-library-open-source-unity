﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLUpgrade : GLSingletonBehaviour<GLUpgrade>
{
    protected override void OnSingletonInitialize() { }

    [Header("Upgrade to 1.0.5")]
    public LocalizedText Title_UpgradeFixTo_1_0_5;
    public LocalizedText Description_UpgradeFixTo_1_0_5;
    public GLBoolPreference HasHadUpgradeFixTo_1_0_5;
    public LocalizedText Label_OpenSettings;

    public void UpgradeOnBootup()
    {
        if (!HasHadUpgradeFixTo_1_0_5.Value)
        {
            if (GLPerformance.I.GetRenderSpeed() == GLRenderSpeed.MAXIMUM)
            {
                var popup = GLPopup.MakeNotificationPopup(Title_UpgradeFixTo_1_0_5, Description_UpgradeFixTo_1_0_5);
                popup.AddClosingButton(Label_OpenSettings, GLSettings.I.ShowSettings);
                popup.Show();

                GLSettings.I.SetRenderSpeed(GLRenderSpeed.LOW_ADAPTIVE);
            }

            HasHadUpgradeFixTo_1_0_5.Value = true;
        }
    }
}
