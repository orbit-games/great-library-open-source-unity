﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLServerManager : GLSingletonBehaviour<GLServerManager> {
    
    public void FetchServerInfo(Action onFinished)
    {
        GLLoadingOverlay.ShowFullcoverLoading(GLApi.FetchServerInfo(), (result) =>
        {
            if (!GLApi.DiplayErrorPopupIfFailed(result))
            {
                onFinished?.Invoke();
            }
        }
        );
    }

    protected override void OnSingletonInitialize() { }
}
