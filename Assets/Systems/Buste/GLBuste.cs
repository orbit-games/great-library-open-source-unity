﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLBuste : GLSingletonBehaviour<GLBuste> {

    public Dictionary<string, GLSocketDefinition> idToSocketDefinition = new Dictionary<string, GLSocketDefinition>();
    public Dictionary<string, GLFeatureDefinition> idToFeatureDefinition = new Dictionary<string, GLFeatureDefinition>();
    public Dictionary<string, GLPlugDefinition> idToPlugDefinition = new Dictionary<string, GLPlugDefinition>();
    public Dictionary<string, GLAdjustableDefinition> idToAdjustableDefinition = new Dictionary<string, GLAdjustableDefinition>();

    public Dictionary<string, GLFeatureDesign> idToFeatureDesign = new Dictionary<string, GLFeatureDesign>();
    public Dictionary<string, GLPlug> idToPlugPrefab = new Dictionary<string, GLPlug>();
    public Dictionary<GLFeatureDefinition, List<GLFeatureDesign>> featureToDesigns = new Dictionary<GLFeatureDefinition, List<GLFeatureDesign>>();
    public Dictionary<GLSocketDefinition, List<GLPlug>> socketToPlugs = new Dictionary<GLSocketDefinition, List<GLPlug>>();
    public Dictionary<string, GLBaseDefinition> allDefinitionIDs = new Dictionary<string, GLBaseDefinition>();

    [ReadOnly]
    public string busteSocketID = "buste";
    public GLSocketDefinition mainBusteSocket;
    public GLSerializablePreference busteSettings;
    [TextArea(3,6)]
    [SerializeField]
    private string defaultBusteComposition;
    private GLBusteComposition defaultBusteCompositionDeserialized;

    public GLBusteComposition DefaultBusteComposition
    {
        get
        {
            if (defaultBusteCompositionDeserialized == null)
            {
                defaultBusteCompositionDeserialized = GLBusteComposition.FromJson(defaultBusteComposition);
            }
            return defaultBusteCompositionDeserialized;
        }
    }

    public GLBusteComposition GetComposition(string userRef, string courseRef)
    {
        if (HasComposition(userRef, courseRef))
        {
            var comp = (GLBusteComposition)busteSettings[userRef, courseRef];
            return comp;
        }
        else
        {
            return DefaultBusteComposition;
        }
    }

    public bool HasComposition(string userRef, string courseRef)
    {
        var comp = (GLBusteComposition)busteSettings[userRef, courseRef];
        return comp != null;
    }

    protected override void OnSingletonInitialize()
    {
        FindAndAddDefinitions(idToSocketDefinition);
        FindAndAddDefinitions(idToFeatureDefinition);
        FindAndAddDefinitions(idToPlugDefinition);
        FindAndAddDefinitions(idToAdjustableDefinition);

        FindAndAddDefinitions(idToFeatureDesign);
        FindAndAddDefined(idToPlugPrefab);

        foreach (var item in idToPlugPrefab)
        {
            var socketDefinition = item.Value.definition.forSocket;
            if (!socketToPlugs.ContainsKey(socketDefinition))
            {
                socketToPlugs.Add(socketDefinition, new List<GLPlug>());
            }
            socketToPlugs[socketDefinition].Add(item.Value);
        }

        foreach (var item in idToFeatureDesign)
        {
            var featureDefinition = item.Value.forFeature;
            if (!featureToDesigns.ContainsKey(featureDefinition))
            {
                featureToDesigns.Add(featureDefinition, new List<GLFeatureDesign>());
            }
            featureToDesigns[featureDefinition].Add(item.Value);
        }

        if (mainBusteSocket == null)
        {
            mainBusteSocket = idToSocketDefinition[busteSocketID];
        }
    }

    private void FindAndAddDefinitions<T>(Dictionary<string, T> dictionary) where T : GLBaseDefinition
    {
        var objects = GLResources.FindObjectsOfTypeAll<T>();
        foreach (var def in objects)
        {
            if (!dictionary.ContainsKey(def))
            {
                var id = def.GetUniqueID();
                if (!allDefinitionIDs.ContainsKey(id))
                {
                    dictionary.Add(id, def);
                    allDefinitionIDs.Add(id, def);
                }
                else
                {
                    throw new System.Exception("The following objects have the same unique ID\n" + def + "\n" + allDefinitionIDs[id]);
                }
            }
            else
            {
                throw new System.Exception("Duplicate entry for definition " + def);
            }
        }
    }
    private void FindAndAddDefined<T>(Dictionary<string, T> dictionary) where T : MonoBehaviour, IGLDefined
    {
        var objects = GLResources.FindObjectsOfTypeAll<T>();
        foreach (var obj in objects)
        {
            if (GLEditorExtensions.IsInScene(obj))
            {
                // objects in scene are not supported, we only work with objects found in the resources folder
                continue;
            }

            var def = obj.GetDefinition();
            var id = def.GetUniqueID();
            if (!dictionary.ContainsKey(id))
            {
                dictionary.Add(id, obj);
            }
            else
            {
                throw new System.Exception("Duplicate entry for defined object " + obj.gameObject.name + " with definition " + obj.GetDefinition());
            }
        }
    }

    public Dictionary<string, GLSocketDefinition>.ValueCollection GetSocketDefinitions()
    {
        return idToSocketDefinition.Values;
    }

    public GLSocketDefinition GetSocketDefinition(string id)
    {
        return idToSocketDefinition.GetOrDefault(id);
    }

    public Dictionary<string, GLFeatureDefinition>.ValueCollection GetFeatureDefinitions()
    {
        return idToFeatureDefinition.Values;
    }

    public GLFeatureDefinition GetFeatureDefinition(string id)
    {
        return idToFeatureDefinition.GetOrDefault(id);
    }

    public Dictionary<string, GLPlugDefinition>.ValueCollection GetPlugDefinitions()
    {
        return idToPlugDefinition.Values;
    }

    public GLPlugDefinition GetPlugDefinition(string id)
    {
        return idToPlugDefinition.GetOrDefault(id);
    }

    public Dictionary<string, GLAdjustableDefinition>.ValueCollection GetAdjustableDefinitions()
    {
        return idToAdjustableDefinition.Values;
    }

    public GLAdjustableDefinition GetAdjustableDefinition(string id)
    {
        return idToAdjustableDefinition.GetOrDefault(id);
    }

    public Dictionary<string, GLFeatureDesign>.ValueCollection GetFeatureDesigns()
    {
        return idToFeatureDesign.Values;
    }

    public List<GLFeatureDesign> GetDesigns(GLFeatureDefinition feature)
    {
        return featureToDesigns.GetOrDefault(feature);
    }

    public GLFeatureDesign GetFeatureDesign(string id)
    {
        return idToFeatureDesign.GetOrDefault(id);
    }

    public Dictionary<string, GLPlug>.ValueCollection GetPlugs()
    {
        return idToPlugPrefab.Values;
    }

    public List<GLPlug> GetPlugs(GLSocketDefinition socket)
    {
        return socketToPlugs.GetOrDefault(socket);
    }

    public GLPlug GetPlug(string id)
    {
        return idToPlugPrefab.GetOrDefault(id);
    }
}
