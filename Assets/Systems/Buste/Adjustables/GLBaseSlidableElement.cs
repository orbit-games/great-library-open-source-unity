﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GLBaseSlidableElement : GLBaseAdjustableElement
{
    public abstract string GetMinDisplayValue();
    public abstract string GetMaxDisplayValue();
}
