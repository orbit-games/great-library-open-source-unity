﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLBusteBuilder : GLBaseBusteObject
{
    protected override void InitializeObject() { }
    protected override void ResetObject() { }
    
    public GLBusteComposition GetCurrentComposition()
    {
        var busteComp = new GLBusteComposition();

        HashSet<object> uniqueCheck = new HashSet<object>();
        var sockets = GetComponentsInChildren<GLSocket>(true);
        foreach (var socket in sockets)
        {
            if (uniqueCheck.Contains(socket.definition)) continue;
            var plug = socket.GetCurrentPlug();
            if (plug != null)
            {
                busteComp.plugs.Add(plug.definition.GetUniqueID());
                uniqueCheck.Add(socket.definition);
            }
        }
        uniqueCheck.Clear();
        var features = GetComponentsInChildren<GLFeature>(true);
        foreach (var feature in features)
        {
            if (uniqueCheck.Contains(feature.definition)) continue;
            var design = feature.GetCurrentDesign();
            if (design != null)
            {
                busteComp.features.Add(design.GetUniqueID());
                uniqueCheck.Add(feature.definition);
            }
        }
        uniqueCheck.Clear();
        var adjustables = GetComponentsInChildren<GLBaseAdjustableElement>(true);
        foreach (var adjustable in adjustables)
        {
            if (uniqueCheck.Contains(adjustable.definition)) continue;
            busteComp.adjustables.Add(adjustable.definition.GetUniqueID());
            busteComp.settings.Add(adjustable.ToStoredValue());
            uniqueCheck.Add(adjustable.definition);
        }

        return busteComp;
    }
    [TextArea(3, 10)]
    public string fromJson;
    [Buttons("FromJSON", "FromJSON", "ToJSON", "ToJSON", "Random", "GenerateRandomComposition")]
    public ButtonsContainer buttons;
    [TextArea(3, 10)]
    public string toJson;
    [TextArea(3, 10)]
    public string randomJson;

    public void ToJSON()
    {
        toJson = GetCurrentComposition().ToJson();
    }

    public void FromJSON()
    {
        BuildComposition(GLBusteComposition.FromJson(fromJson));
    }

    public GLBusteComposition GenerateRandomComposition()
    {
        var busteComp = new GLBusteComposition();
        var sockets = GLBuste.I.GetSocketDefinitions();
        HashSet<GLAdjustableDefinition> adjustables = new HashSet<GLAdjustableDefinition>();
        foreach (var socket in sockets)
        {
            var plugs = GLBuste.I.GetPlugs(socket);
            if (plugs.IsNullOrEmpty()) continue;
            var plug = plugs[Random.Range(0, plugs.Count)];
            adjustables.AddRangeUnique(plug.GetAdjustableDefinitions());
            busteComp.plugs.Add(plug.definition.GetUniqueID());
        }
        var features = GLBuste.I.GetFeatureDefinitions();
        foreach (var feat in features)
        {
            var designs = GLBuste.I.GetDesigns(feat);
            if (designs.IsNullOrEmpty()) continue;
            var design = designs[Random.Range(0, designs.Count)];
            busteComp.features.Add(design.GetUniqueID());
        }
        foreach (var adjustable in adjustables)
        {
            var random = (Random.Range(0f, 1f) + Random.Range(0f, 1f) + Random.Range(0f, 1f) + Random.Range(0f, 1f)) / 4f;
            busteComp.adjustables.Add(adjustable.GetUniqueID());
            busteComp.settings.Add(random.toRoundTripString());
        }
        randomJson = Newtonsoft.Json.JsonConvert.SerializeObject(busteComp);
        BuildComposition(busteComp);
        return busteComp;
    }
}
