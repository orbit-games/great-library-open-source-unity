﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GLBusteComposition
{
    public List<string> plugs = new List<string>();
    public List<string> features = new List<string>();

    public List<string> adjustables = new List<string>();
    public List<string> settings = new List<string>();

    [NonSerialized]
    private Dictionary<GLSocketDefinition, GLPlug> socketToPlug = new Dictionary<GLSocketDefinition, GLPlug>();
    [NonSerialized]
    private Dictionary<GLFeatureDefinition, GLFeatureDesign> featureToDesign = new Dictionary<GLFeatureDefinition, GLFeatureDesign>();
    [NonSerialized]
    private Dictionary<GLAdjustableDefinition, string> adjustableToSetting = new Dictionary<GLAdjustableDefinition, string>();

    private bool initialized = false;
    private void Initialize()
    {
        if (initialized) return;
        initialized = true;

        foreach (var plugID in plugs)
        {
            var plug = GLBuste.I.GetPlug(plugID);
            if (plug != null)
            {
                socketToPlug.SetOrAdd(plug.definition.forSocket, plug);
            }
        }

        foreach (var featureID in features)
        {
            var feature = GLBuste.I.GetFeatureDesign(featureID);
            if (feature != null)
            {
                featureToDesign.SetOrAdd(feature.forFeature, feature);
            }
        }

        for (int i = 0; i < adjustables.Count; i++)
        {
            var adjustable = GLBuste.I.GetAdjustableDefinition(adjustables[i]);
            if (adjustable != null)
            {
                adjustableToSetting.SetOrAdd(adjustable, settings[i]);
            }
        }
    }

    public string ToJson()
    {
        return Newtonsoft.Json.JsonConvert.SerializeObject(this);
    }

    public static GLBusteComposition FromJson(string busteCompositionJson)
    {
        return Newtonsoft.Json.JsonConvert.DeserializeObject<GLBusteComposition>(busteCompositionJson);
    }

    public GLPlug GetPlug(GLSocketDefinition socket)
    {
        Initialize();
        return socketToPlug.GetOrDefault(socket);
    }

    public GLFeatureDesign GetFeatureDesign(GLFeatureDefinition feature)
    {
        Initialize();
        return featureToDesign.GetOrDefault(feature);
    }

    public string GetSetting(GLAdjustableDefinition adjustable)
    {
        Initialize();
        return adjustableToSetting.GetOrDefault(adjustable);
    }
}
