﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Feature_Design_", menuName = "Great Library/Buste/Feature Design")]
public class GLFeatureDesign : GLBaseDefinition
{
    [Header("Settings")]
    public GLFeatureDefinition forFeature;
    public bool hidden = false;

    [Header("Design")]
    public List<FeatureDetail> details;

    private Dictionary<GLFeatureDetailDefinition, Sprite> definitionToSprite;
    public Sprite GetSprite(GLFeatureDetailDefinition forDetail)
    {
        if (definitionToSprite == null)
        {
            definitionToSprite = new Dictionary<GLFeatureDetailDefinition, Sprite>();
            foreach (var detail in details)
            {
                definitionToSprite.Add(detail.definition, detail.sprite);
            }
        }
        if (definitionToSprite.ContainsKey(forDetail))
        {
            return definitionToSprite[forDetail];
        }
        else
        {
            return null;
        }
    }

    protected override string GetFilePrefix()
    {
        return "Feature_Design";
    }

    [Serializable]
    public class FeatureDetail
    {
        public GLFeatureDetailDefinition definition;
        public Sprite sprite;
    }
}