﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GLBaseDefinition : LocalizedAsset<string>
{
    [Header("Internal ID")]
    [SerializeField]
    private string uniqueID;
    [Buttons("Generate", "Generate", "From Title", "FromTitle", "To Name", "ToName")]
    public ButtonsContainer generate;

    public void Generate()
    {
        uniqueID = System.Guid.NewGuid().ToString().Substring(0, 6);
        GLEditorExtensions.SaveAsset(this);
    }

    public void FromTitle()
    {
        uniqueID = GetTitle().Replace(' ', '_').azAZ09_().ToLower();
        GLEditorExtensions.SaveAsset(this);
    }

    public void ToTitle()
    {
        m_LocaleItems[0].Value = uniqueID.Replace('_',' ').ToTitleCase();
        GLEditorExtensions.SaveAsset(this);
    }

    public void ToName()
    {
#if UNITY_EDITOR
        var prefix = GetFilePrefix();
        var newName = GetUniqueID().Replace('.', ' ').Replace('_', ' ').ToTitleCase().Replace(' ', '_');
        if (!prefix.IsNullOrEmpty())
        {
            newName = prefix + "_" + newName;
        }
        var path = UnityEditor.AssetDatabase.GetAssetPath(this);
        UnityEditor.AssetDatabase.RenameAsset(path, newName + ".asset");

        GLEditorExtensions.SaveAsset(this);
#endif
    }

    protected abstract string GetFilePrefix();

    public virtual string GetUniqueID()
    {
        return uniqueID;
    }

    protected void SetUniqueID(string uniqueID)
    {
        this.uniqueID = uniqueID;
    }

    [Header("Title")]
    [SerializeField]
    private TextLocaleItem[] m_LocaleItems = new TextLocaleItem[1];
    public override LocaleItemBase[] LocaleItems { get { return m_LocaleItems; } }
    [System.Serializable]
    private class TextLocaleItem : LocaleItem<string> { };

    public override string ToString()
    {
        return name + " (" + GetType() + ")";
    }

    public string GetTitle()
    {
        return m_LocaleItems[0].Value;
    }

}
