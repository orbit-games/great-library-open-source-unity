﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Adjustable_", menuName = "Great Library/Buste/Adjustable Definition")]
public class GLAdjustableDefinition : GLBaseDefinition
{
    protected override string GetFilePrefix()
    {
        return "Adjustable";
    }
}
