﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Socket__Hole_", menuName = "Great Library/Buste/Socket Hole Definition")]
public class GLSocketHoleDefinition : ScriptableObject
{
    public string extraID = "";

    public override string ToString()
    {
        return "GLSocketHoleDefinition (Name = " + name + ")";
    }
}
