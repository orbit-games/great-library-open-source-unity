﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Plug_", menuName = "Great Library/Buste/Plug Definition")]
public class GLPlugDefinition : GLBaseDefinition, IUnlockable
{
    [Header("Settings")]
    public GLSocketDefinition forSocket;
    public Rarity rarity;

    public Rarity GetRarity()
    {
        return rarity;
    }

    public override string GetUniqueID()
    {
        return forSocket.GetUniqueID() + "." + base.GetUniqueID();
    }

    protected override string GetFilePrefix()
    {
        return "Plug";
    }
}
