﻿using GameToolkit.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Feature_Detail_", menuName = "Great Library/Buste/Feature Detail Definition")]
public class GLFeatureDetailDefinition : ScriptableObject
{
    public string extraID = "";

    [Header("Dummy")]
    public Sprite dummySprite;
}
