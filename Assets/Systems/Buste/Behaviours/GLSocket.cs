﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLSocket : MonoBehaviour {

    [Header("Definition")]
    public GLSocketDefinition definition;
    public List<GLSocketHole> holes;
    [Buttons("Generate Holes", "Generate", "Move to Plug", "MoveToPlug", "Find Holes", "FindHoles")]
    public ButtonsContainer generate;
    public void Generate()
    {
        GLBusteMenuCommands.GenerateSocketHoles(gameObject);
    }
    public void MoveToPlug()
    {
        var parentPlug = GetComponentInParent<GLPlug>();
        if (parentPlug != null)
        {
            if (parentPlug.gameObject == this.gameObject)
            {
                throw new System.Exception("Socket is already on a plug");
            }
            var socket = parentPlug.gameObject.AddComponent<GLSocket>();
            socket.definition = definition;
        }
        else
        {
            throw new System.Exception("No parent plug found");
        }

        var index = this.transform.GetSiblingIndex();
        while (transform.childCount > 0)
        {
            var child = transform.GetChild(transform.childCount - 1);
            child.SetParent(transform.parent);
            child.SetSiblingIndex(index);
        }

        var components = GetComponents<Component>();
        Debug.Log(components.ToDebugString());
        if (components.Length > 2)
        {
            gameObject.name = "GameObject";
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
    public void FindHoles()
    {
        this.holes = new List<GLSocketHole>();
        definitionToObject = new Dictionary<GLSocketHoleDefinition, GLSocketHole>();
        var foundHoles = GetComponentsInChildren<GLSocketHole>(true);
        foreach (var hole in foundHoles)
        {
            if (definition.holes.Contains(hole.definition))
            {
                this.holes.AddUnique(hole);
            }
        }
        GLEditorExtensions.SaveAsset(this);
        if (holes.Count != definition.holes.Count)
        {
            throw new System.Exception("It seems like not all defined holes have been accounted for socket " + definition);
        }
    }

    private Dictionary<GLSocketHoleDefinition, GLSocketHole> definitionToObject;

    private void Initialize()
    {
        if (definitionToObject == null)
        {
            if (holes.Count != definition.holes.Count)
            {
                throw new System.Exception("It seems like not all defined holes have been accounted for socket " + definition);
            }

            definitionToObject = new Dictionary<GLSocketHoleDefinition, GLSocketHole>();
            foreach (var hole in holes)
            {
                if (definition.holes.Contains(hole.definition))
                {
                    definitionToObject.Add(hole.definition, hole);
                }
            }
        }
    }

    public GLSocketHole GetSocketHole(GLSocketHoleDefinition fromDefinition)
    {
        Initialize();
        if (definitionToObject.ContainsKey(fromDefinition))
        {
            return definitionToObject[fromDefinition];
        }
        else
        {
            return null;
        }
    }

    public void Unplug()
    {
        if (currentPlug != null)
        {
            GLPool.removeCopy(currentPlug);
            currentPlug = null;
        }
    }

    GLPlug currentPlug;

    public GLPlug Plugin(GLPlug plug, GLBusteComposition composition = null)
    {
        Initialize();

        Unplug();

        if (plug != null)
        {
            if (plug.definition.forSocket != definition)
            {
                throw new System.Exception("Can't plug for socket " + plug.definition.forSocket + " on this socket " + definition);
            }

            currentPlug = GLPool.placeCopy(plug, transform);
            currentPlug.PlugPins(this);
            currentPlug.BuildComposition(composition);
        }
        return currentPlug;
    }

    public GLPlug GetCurrentPlug()
    {
        return currentPlug;
    }
}
