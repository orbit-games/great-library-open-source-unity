﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GLPlugPin : GLConnectable, IPoolEventsListener
{
    [Header("Definition")]
    public GLSocketHoleDefinition forSocketHole;

    [Header("Connection")]
    public GLPlug parentPlug;
    public GLSocketHole currentSocketHole;
    [Buttons("Update parent", "UpdateParent")]
    public ButtonsContainer updateButton;
    
    public void Reset()
    {
        if (currentSocketHole != null)
        {
            currentSocketHole = null;
            SetParent(parentPlug.transform);
        }
    }
    
    public void ConnectToSocketHole(GLSocketHole hole)
    {
        if (hole == null)
        {
            gameObject.SetActive(false);
            Reset();
            return;
        }
        else
        {
            if (hole.definition != forSocketHole)
            {
                throw new System.Exception("Can't connect to different socket hole " + hole + " than designed for. " +
                    "Was expecting " + forSocketHole);
            }
            else
            {
                gameObject.SetActive(true);
                currentSocketHole = hole;
                SetParent(currentSocketHole.transform);
            }
        }
    }

    private void SetParent(Transform newParent)
    {
        transform.SetParent(newParent);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public void OnCreatedForPool() { Reset(); }
    public void OnPlacedFromPool() { Reset(); }
    public void OnRemovedToPool() { Reset(); }

    [Header("Debug")]
    public GLSocketHole hole;
    [Buttons("Connect", "Connect", "Reset", "Reset")]
    public ButtonsContainer debug;

    public void Connect()
    {
        ConnectToSocketHole(hole);
    }
}
