﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLSocketHole : GLConnectable
{
    [Header("Definition")]
    public GLSocketHoleDefinition definition;
}
