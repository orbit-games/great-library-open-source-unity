﻿using Fenderrio.ImageWarp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLFeatureDetail : MonoBehaviour {

    [Header("Definition")]
    public GLFeatureDetailDefinition definition;

    [NonSerialized]
    private bool initialized = false;
    private Image image;
    private RawImageWarp warpableImage;

    private void Initialized()
    {
        if (initialized) return;
        initialized = true;

        image = GetComponent<Image>();
        warpableImage = GetComponent<RawImageWarp>();
    }

    public void SetFeatureDetail(Sprite sprite)
    {
        Initialized();
        if (image != null) image.sprite = sprite;
        else if (warpableImage != null) warpableImage.texture = sprite != null ? sprite.texture : null;

        if (sprite != null)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
