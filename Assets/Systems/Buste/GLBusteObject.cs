﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GLBaseBusteObject : MonoBehaviour, IPoolEventsListener {
    
    private Dictionary<GLSocketDefinition, List<GLSocket>> sockets;
    private Dictionary<GLFeatureDefinition, List<GLFeature>> features;
    private Dictionary<GLAdjustableDefinition, List<GLBaseAdjustableElement>> adjustables;
    [NonSerialized]
    private bool initialized = false;

    [Buttons("Log")]
    public ButtonsContainer triggerbutton;

    protected abstract void ResetObject();
    protected abstract void InitializeObject();

    private void ResetContents()
    {
        foreach (var kvp in sockets)
        {
            foreach (var component in kvp.Value)
            {
                component.Unplug();
            }
        }

        foreach (var kvp in features)
        {
            foreach (var component in kvp.Value)
            {
                component.EmptyDesign();
            }
        }
    }

    //public void Log()
    //{
    //    var foundSockets = GetComponentsInChildren<GLSocket>(true);
    //    var foudnFeatures = GetComponentsInChildren<GLFeature>(true);
    //    var foundAdjustables = GetComponentsInChildren<GLBaseAdjustableElement>(true);
    //    var logString = "---------- " + this + " ------------- ";
    //    logString += "\n\nFound Sockets: " + foundSockets.ToDebugString();
    //    logString += "\n\nSockets: " + sockets.ToDebugString();
    //    logString += "\n\nFound Features: " + foudnFeatures.ToDebugString();
    //    logString += "\n\nFeatures: " + features.ToDebugString();
    //    logString += "\n\nFound Adjustables: " + foundAdjustables.ToDebugString();
    //    logString += "\n\nAdjustables: " + adjustables.ToDebugString();
    //    logString += "\n\n";
    //    Debug.Log(logString);
    //}

    protected void Initialize()
    {
        if (!initialized)
        {

            initialized = true;

            sockets = new Dictionary<GLSocketDefinition, List<GLSocket>>();
            var foundSockets = GetComponentsInChildren<GLSocket>(true);
            foreach (var socket in foundSockets)
            {
                sockets.CreateOrAddToList(socket.definition, socket);
            }

            features = new Dictionary<GLFeatureDefinition, List<GLFeature>>();
            var foudnFeatures = GetComponentsInChildren<GLFeature>(true);
            foreach (var feature in foudnFeatures)
            {
                features.CreateOrAddToList(feature.definition, feature);
            }

            adjustables = new Dictionary<GLAdjustableDefinition, List<GLBaseAdjustableElement>>();
            var foundAdjustables = GetComponentsInChildren<GLBaseAdjustableElement>(true);
            foreach (var adjustable in foundAdjustables)
            {
                adjustables.CreateOrAddToList(adjustable.definition, adjustable);
            }

            //Log();

            InitializeObject();
        }
    }

    public ICollection<GLAdjustableDefinition> GetAdjustableDefinitions()
    {
        Initialize();
        return adjustables.Keys;
    }

    public ICollection<GLFeatureDefinition> GetFeatureDefinitions()
    {
        Initialize();
        return features.Keys;
    }

    public ICollection<GLSocketDefinition> GetSocketDefinitions()
    {
        Initialize();
        return sockets.Keys;
    }

    public void BuildComposition(GLBusteComposition composition)
    {
        Initialize();

        if (composition == null || composition.plugs.Count == 0)
        {
            composition = GLBuste.I.DefaultBusteComposition;
        }
        if (composition != null && composition.plugs.Count > 0)
        {
            foreach (var kvp in features)
            {
                var design = composition.GetFeatureDesign(kvp.Key);
                foreach (var component in kvp.Value)
                {
                    component.SetFeatureDesign(design);
                }
            }

            foreach (var kvp in adjustables)
            {
                var setting = composition.GetSetting(kvp.Key);
                foreach (var component in kvp.Value)
                {
                    component.FromStoredValue(setting);
                }
            }

            foreach (var kvp in sockets)
            {
                var plug = composition.GetPlug(kvp.Key);
                foreach (var component in kvp.Value)
                {
                     component.Plugin(plug, composition);
                }
            }
        }
    }
    
    public void OnRemovedToPool() { ResetContents(); ResetObject(); }
    public void OnPlacedFromPool() { ResetContents(); ResetObject(); }
    public void OnCreatedForPool() { Initialize(); }
}
