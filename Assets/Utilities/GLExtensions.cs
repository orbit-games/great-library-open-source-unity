﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using UnityEngine;

public static class GLExtensions {

    private static CultureInfo cult;
    private static CultureInfo GetCulture()
    {
        if (cult == null) 
        {
            try {

                cult = new CultureInfo("en-EN", false);
            }
            catch { cult = null; }
            if (cult == null)
            {
                cult = CultureInfo.InvariantCulture;
            }
        }
        return cult;
    }

    public static bool IsNullOrEmpty(this string item)
    {
        return item == null || item.Length == 0;
    }

    public static string ToTitleCase(this string item)
    {
        return GetCulture().TextInfo.ToTitleCase(item);
    }

    public static string ToFirstUpper(this string str)
    {
        if (str == null)
            return null;

        if (str.Length > 1)
            return char.ToUpper(str[0]) + str.Substring(1);

        return str.ToUpper();
    }

    public static bool IsWhiteSpace(this string input)
    {
        if (input.IsNullOrEmpty()) return false;
        return System.Text.RegularExpressions.Regex.IsMatch(input, @"^\s+$");
    }

    public static bool IsValidJSON(this string strInput)
    {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
    }

    /// <summary>
    /// Compresses the string.
    /// https://stackoverflow.com/questions/7343465/compression-decompression-string-with-c-sharp
    /// http://gigi.nullneuron.net/gigilabs/compressing-strings-using-gzip-in-c/
    /// </summary>
    /// <param name="text">The text.</param>
    /// <returns></returns>
    public static string CompressGZip(this string text)
    {
        byte[] buffer = Encoding.UTF8.GetBytes(text);
        return buffer.CompressGZip();
    }
    public static string CompressGZip(this byte[] buffer)
    {
        var memoryStream = new MemoryStream();
        using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
        {
            gZipStream.Write(buffer, 0, buffer.Length);
        }

        var outputBytes = memoryStream.ToArray();
        var outputbase64 = Convert.ToBase64String(outputBytes);
        return outputbase64;
    }

    public static string azAZ09_(this string item, string replaceWith = "")
    {
        return System.Text.RegularExpressions.Regex.Replace(item, @"[^\w]", replaceWith);
    }

    public static string azAZ09(this string item, string replaceWith = "")
    {
        return System.Text.RegularExpressions.Regex.Replace(item, @"[^a-zA-Z0-9]", replaceWith);
    }

    public static string azAZ(this string item, string replaceWith = "")
    {
        return System.Text.RegularExpressions.Regex.Replace(item, @"[^a-zA-Z]", replaceWith);
    }

    public static string RemoveNonSpaceWhitespace(this string input)
    {
        return new string(input.Where(c => !Char.IsWhiteSpace(c) || c == ' ').ToArray());
    }

    public static bool IsNullOrEmpty<TValue>(this ICollection<TValue> collection)
    {
        return collection == null || collection.Count == 0;
    }

    public static string ToDebugString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string separator = ", ", params TKey[] skipKeys)
    {
        if (dictionary == null) return "null";
        if (dictionary.Count == 0) return "empty";
        return "{" + string.Join(separator, dictionary.Where(kv => !skipKeys.Contains(kv.Key)).Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
    }

    public static string ToDebugString<TValue>(this IList<TValue> dictionary, string separator = ", ")
    {
        if (dictionary == null) return "null";
        if (dictionary.Count == 0) return "empty";
        return "{" + string.Join(separator, dictionary.Select(kv => kv).ToArray()) + "}";
    }

    public static void SerializeToDebugLog<TValue>(this TValue anything, string prefix = "")
    {
        Newtonsoft.Json.JsonSerializerSettings settings = new Newtonsoft.Json.JsonSerializerSettings
        {
            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
            Formatting = Newtonsoft.Json.Formatting.Indented,
        };
        Debug.Log(prefix + Newtonsoft.Json.JsonConvert.SerializeObject(anything, settings));
    }

    public static TValue CreateOrAddToList<TKey, TValue>(this IDictionary<TKey, List<TValue>> dictionary, TKey key, TValue value)
    {
        if (dictionary.ContainsKey(key))
        {
            dictionary[key].Add(value);
        }
        else
        {
            var list = new List<TValue>();
            list.Add(value);
            dictionary.Add(key, list);
        }
        return value;
    }

    public static TValue CreateSetOrAddToDictionary<TKey, TSubKey, TValue>(this IDictionary<TKey, Dictionary<TSubKey, TValue>> dictionary, TKey key, TSubKey subkey, TValue subValue)
    {
        if (!dictionary.ContainsKey(key))
        {
            dictionary.Add(key, new Dictionary<TSubKey, TValue>());
        }
        dictionary[key].SetOrAdd(subkey, subValue);
        return subValue;
    }

    public static TValue GetOrDefault<TKey, TSubKey, TValue>(this IDictionary<TKey, Dictionary<TSubKey, TValue>> dictionary, TKey key, TSubKey subkey)
    {
        if (!dictionary.ContainsKey(key))
        {
            dictionary.Add(key, new Dictionary<TSubKey, TValue>());
        }
        return dictionary[key].GetOrDefault(subkey);
    }

    public static bool Contains<TKey, TSubKey, TValue>(this IDictionary<TKey, Dictionary<TSubKey, TValue>> dictionary, TKey key, TSubKey subkey)
    {
        if (!dictionary.ContainsKey(key))
        {
            dictionary.Add(key, new Dictionary<TSubKey, TValue>());
        }
        return dictionary.GetOrDefault(key, subkey) != null;
    }

    public static TValue SetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
    {
        if (dictionary.ContainsKey(key))
        {
            dictionary[key] = value;
        }
        else
        {
            dictionary.Add(key, value);
        }
        return value;
    }

    public static void RemoveIfContainsKey<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key)
    {
        if (dict.ContainsKey(key))
        {
            dict.Remove(key);
        }
    }

    public static void RemoveIfContains<TValue>(this ICollection<TValue> list, TValue value)
    {
        if (list.Contains(value))
        {
            list.Remove(value);
        }
    }

    public static TValue AddUnique<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
    {
        if (!dictionary.ContainsKey(key))
        {
            dictionary[key] = value;
        }
        return value;
    }

    public static TValue AddUnique<TValue>(this ICollection<TValue> list, TValue value)
    {
        if (!list.Contains(value))
        {
            list.Add(value);
        }
        return value;
    }

    public static void AddRangeUnique<TValue>(this ICollection<TValue> list, IEnumerable<TValue> values)
    {
        foreach (var value in values)
        {
            list.AddUnique(value);
        }
    }

    public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue = default(TValue))
    {
        if (key == null) return defaultValue;
        if (dictionary.ContainsKey(key))
        {
            return dictionary[key];
        }
        else
        {
            return defaultValue;
        }
    }

    public static TValue GetOrDefault<TValue>(this IList<TValue> list, int index, TValue defaultValue = default(TValue))
    {
        if (0 <= index && index < list.Count)
        {
            return list[index];
        }
        else
        {
            return defaultValue;
        }
    }

    public static TValue GetClamped<TValue>(this IList<TValue> list, int index)
    {
        if (list.Count == 0) return default(TValue);
        return list[Mathf.Clamp(index, 0, list.Count - 1)];
    }

    public static TValue GetLooped<TValue>(this IList<TValue> list, int index)
    {
        if (list.Count == 0) return default(TValue);
        // note that dividing integers results in floored int values, thus (x / y) * y is NOT the same as x
        if (index < 0) index -= (index / list.Count) * list.Count - list.Count; // funky code to get index above 0 with multiples of list.Count
        return list[index % list.Count];
    }

    public static TValue GetRandom<TValue>(this IList<TValue> list)
    {
        if (list.Count == 0) return default(TValue);
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static int GetRandomIndex<TValue>(this IList<TValue> list)
    {
        if (list.Count == 0) return -1;
        return UnityEngine.Random.Range(0, list.Count);
    }

    public static string toNumberString(this float number, int decimals = 0)
    {
        if (decimals == 0) return (Mathf.RoundToInt(number)).ToString("d", GetCulture());
        return number.ToString("n" + decimals, GetCulture());
    }

    public static string toRoundTripString(this float number)
    {
        return number.ToString("G9", CultureInfo.InvariantCulture);
    }

    public static string GetPath(this GameObject gameObject)
    {
        List<string> path = new List<string>();
        while (gameObject != null)
        {
            path.Add(gameObject.name);
            gameObject = gameObject.transform.parent?.gameObject;
        }
        path.Reverse();
        return string.Join(" > ", path);
    }

    public static void ResetTransform(this Component component)
    {
        component.transform.localPosition = Vector3.zero;
        component.transform.localScale = Vector3.one;
        component.transform.localRotation = Quaternion.identity;
    }

    public static void ResetTransform(this GameObject component)
    {
        component.transform.ResetTransform();
    }

    public static Vector3 LocalPositionWithin(this GameObject thisObject, Component otherObject)
    {
        return otherObject.transform.InverseTransformPoint(thisObject.transform.position);
    }

    public static Vector3 LocalPositionWithin(this Component thisObject, Component otherObject)
    {
        return otherObject.transform.InverseTransformPoint(thisObject.transform.position);
    }

    public static Vector3 LocalPositionWithin(this GameObject thisObject, GameObject otherObject)
    {
        return otherObject.transform.InverseTransformPoint(thisObject.transform.position);
    }

    public static Vector3 LocalPositionWithin(this Component thisObject, GameObject otherObject)
    {
        return otherObject.transform.InverseTransformPoint(thisObject.transform.position);
    }

    public static Vector3 LocalPointWithin(this Component source, Vector3 point, Component destination)
    {
        return destination.transform.InverseTransformPoint(source.transform.TransformPoint(point));
    }

    public static Vector3 LocalPointWithin(this Component source, Vector3 point, GameObject destination)
    {
        return destination.transform.InverseTransformPoint(source.transform.TransformPoint(point));
    }

    public static Vector3 LocalPointWithin(this GameObject source, Vector3 point, GameObject destination)
    {
        return destination.transform.InverseTransformPoint(source.transform.TransformPoint(point));
    }

    public static Vector3 LocalPointWithin(this GameObject source, Vector3 point, Component destination)
    {
        return destination.transform.InverseTransformPoint(source.transform.TransformPoint(point));
    }

    public static void SmoothCurve(this AnimationCurve animationCurve)
    {
        for (int k = 0; k < animationCurve.keys.Length; k++)
        {
            animationCurve.SmoothTangents(k, 0);
        }
    }

    /// <summary>
    /// Tries to match the transform of an object with that of another in the scene. Note that this fails when objects have complex
    /// transformations, because Unity does not support proper shearing
    /// </summary>
    /// <param name="matcher"></param>
    /// <param name="with"></param>
    public static void Match(this Transform matcher, Transform with)
    {

        // hacky way to grab same transform as another object
        // but fails because unity doesnt support shearing
        //var rememberParent = transform.parent;
        //transform.SetParent(toMatch, false);
        //transform.localPosition = Vector3.zero;
        //transform.localEulerAngles = Vector3.zero;
        //transform.localScale = Vector3.one;
        //transform.SetParent(rememberParent, true);

        // correct way to grab same transform like another object
        // but also fails because unity doesnt support shearing
        matcher.position = with.transform.position;
        matcher.rotation = with.transform.rotation;
        matcher.localScale = matcher.parent.InverseTransformVector(with.lossyScale);
    }

    /// <summary>
    /// Creates a curve that approximates the XY flip of a curve. Unfortunately
    /// </summary>
    /// <param name="animationCurve"></param>
    /// <returns></returns>
    public static AnimationCurve FlipXY(this AnimationCurve animationCurve)
    {
        if (animationCurve.keys.Length < 2) {
            return new AnimationCurve(animationCurve.keys);
        }

        // attempt at doing it by calculations with curves, but the weights for a problem
        // haven't found a solution to give proper weights

        //foreach (var key in valueMapping.keys)
        //{

        //    var outAngle = Mathf.Atan2(key.outTangent, 1f) * Mathf.Rad2Deg;
        //    var newOutAngle = Mathf.Clamp(-outAngle + 90f, -85f, 85f);
        //    var newOutTangent = Mathf.Tan(newOutAngle * Mathf.Deg2Rad);

        //    var inAngle = Mathf.Atan2(key.inTangent, 1f) * Mathf.Rad2Deg;
        //    var newinAngle = Mathf.Clamp(-inAngle + 90f, -85f, 85f);
        //    var newinTangent = Mathf.Tan(newinAngle * Mathf.Deg2Rad);

        //    var newKey = new Keyframe()
        //    {
        //        time = key.value,
        //        value = key.time,
        //        inTangent = newinTangent,
        //        outTangent = newOutTangent,
        //        inWeight = key.inWeight,
        //        outWeight = key.outWeight,
        //        weightedMode = WeightedMode.Both
        //    };

        //    newKeyframes.Add(newKey);
        //}

        var approximatingFrames = new List<Keyframe>();
        var previousValue = animationCurve.keys[0].value;
        for (int t = 0; t < 51; t++)
        {
            var time = t / 50f;
            var value = animationCurve.Evaluate(time);
            var key = new Keyframe(value, time);
            approximatingFrames.Add(key);

            if (value < previousValue)
            {
                throw new System.Exception("This method does not support curves with decreasing values");
            }

            previousValue = value;
        }

        //for (int k = 1; k < approximatingFrames.Count - 1; k++)
        //{
        //    var key1 = approximatingFrames[k - 1];
        //    var key2 = approximatingFrames[k];
        //    var key3 = approximatingFrames[k + 1];
        //    var slope1 = (key2.value - key1.value) / Mathf.Max(0.0001f, key2.time - key1.time);
        //    var slope2 = (key3.value - key2.value) / Mathf.Max(0.0001f, key3.time - key2.time);

        //    if (slope2 == 0 && slope1 == 0
        //        || (slope2 != 0 && Mathf.Abs((slope1 / slope2) - 1f) < 0.2f)
        //        //|| (Mathf.Abs(slope2 - slope1) <= 0.05f)
        //        )
        //    {
        //        approximatingFrames.Remove(key2);
        //        k--;
        //    }
        //}

        AnimationCurve backupCurve = new AnimationCurve(approximatingFrames.ToArray());
        AnimationCurve newCurve = new AnimationCurve(approximatingFrames.ToArray());
        backupCurve.SmoothCurve();
        newCurve.SmoothCurve();

        // approximate the curve by removing and checking how strongly the curve has changed
        bool removedFrame = false;
        int maxIterations = 500;
        do
        {
            removedFrame = false;
            for (int existingFrameIndex = 1; existingFrameIndex < newCurve.keys.Length - 1; existingFrameIndex++)
            {
                var testingFrame = newCurve.keys[existingFrameIndex];
                newCurve.RemoveKey(existingFrameIndex);
                
                var variance = newCurve.CalculateMaxVariance(backupCurve);

                if (variance >= 0f && variance <= 0.05f)
                {
                    // removal was ok!
                    removedFrame = true;
                    existingFrameIndex--;
                }
                else
                {
                    // removal was NOT ok..
                    newCurve.AddKey(testingFrame);
                }
            }
        } while (removedFrame && maxIterations-- > 0);

        if (maxIterations <= 0)
        {
            Debug.LogError("Curve cleaning took way too long, canceled further execution");
        }

        //AnimationCurve newCurve = new AnimationCurve(newKeyframes.ToArray());
        //for (int k = 0; k < newCurve.keys.Length; k++)
        //{
        //    UnityEditor.AnimationUtility.SetKeyLeftTangentMode(newCurve, k, UnityEditor.AnimationUtility.TangentMode.Linear);
        //    UnityEditor.AnimationUtility.SetKeyRightTangentMode(newCurve, k, UnityEditor.AnimationUtility.TangentMode.Linear);
        //}
        return newCurve;
    }

    public static float CalculateMaxVariance(this AnimationCurve fromCurve, AnimationCurve trueCurve)
    {
        List<float> times = new List<float>();
        float percentBetweenKeys = 1f / 0.25f;
        for (int k = 0; k < trueCurve.keys.Length - 1; k++)
        {
            var key1 = trueCurve.keys[k];
            var key2 = trueCurve.keys[k + 1];
            var timeDifference = key2.time - key1.time;
            for (float t = key1.time; t < key2.time; t += timeDifference * percentBetweenKeys)
            {
                times.Add(t);
            }
        }
        times.Add(trueCurve.keys[trueCurve.keys.Length - 1].time);

        var maxVariance = 0f;
        foreach (var time in times)
        {
            var eval1 = fromCurve.Evaluate(time) * 100f;
            var eval2 = trueCurve.Evaluate(time) * 100f;
            maxVariance = Mathf.Max(maxVariance, (eval1 - eval2) * (eval1 - eval2));
        }
        return maxVariance;
    }
}
