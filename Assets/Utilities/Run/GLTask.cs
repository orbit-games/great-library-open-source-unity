﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameToolkit.Localization;
using UnityEngine;

public struct GLTaskResult
{
    public GLTaskResult(dynamic result, Dictionary<string, string> context, string errorMessage = null)
    {
        Result = result;
        Failed = errorMessage != null;
        ErrorMessage = errorMessage;
        RequestContext = context;
    }

    public bool Failed { get; private set; }
    public dynamic Result { get; private set; }
    public string ErrorMessage { get; private set; }
    public Dictionary<string, string> RequestContext { get; private set; }

    public override string ToString()
    {
        if (Failed)
        {
            return ErrorMessage;
        }
        else
        {
            return Result;
        }
    }
}

public class GLTask<T> : IGLBaseTask
{
    public enum State
    {
        Waiting, Running, Succeeded, Failed
    }
    
    public delegate void DoTaskStep(GLTask<T> thisTask, int step);

    private Action<GLTaskResult> onCompletedListeners;
    private DoTaskStep stepper;

    public LocalizedText Description { get; private set; }
    public State CurrentState { get; private set; }
    public GLTaskResult Result { get; private set; }
    public Dictionary<string, string> requestContext;
    public int Step { get; private set; }

    //public GLTask(DoTaskStep stepper, Dictionary<string, string> requestContext = null, Action<GLTaskResult> onCompleted = null)
    //    : this(null, stepper, requestContext, onCompleted) { }

    public GLTask(LocalizedText description, DoTaskStep stepper, Dictionary<string, string> requestContext = null, Action<GLTaskResult> onCompleted = null)
    {
        Description = description;
        CurrentState = State.Waiting;
        this.stepper = stepper;
        this.requestContext = requestContext;
        if (onCompleted != null)
        {
            AddOnCompletedListener(onCompleted);
        }
    }

    public LocalizedText GetDescription()
    {
        return Description;
    }

    public void Update()
    {
        if (IsCompleted()) return;
        CurrentState = State.Running;
        stepper.Invoke(this, Step++);
    }

    public GLTask<T> AddOnCompletedListener(Action<GLTaskResult> onCompleted)
    {
        this.onCompletedListeners += onCompleted;
        if (IsCompleted())
        {
            onCompleted(Result);
        }
        return this;
    }

    public void SetFailed(string message, string extraDetails = "")
    {
        CurrentState = State.Failed;
        Result = new GLTaskResult(extraDetails, requestContext, message);
        onCompletedListeners?.Invoke(Result);
    }

    public void SetSucceeded(T resultingData)
    {
        CurrentState = State.Succeeded;
        Result = new GLTaskResult(resultingData, requestContext);
        onCompletedListeners?.Invoke(Result);
    }

    public bool IsCompleted()
    {
        return CurrentState == State.Failed || CurrentState == State.Succeeded;
    }

    public bool IsFailed()
    {
        return CurrentState == State.Failed;
    }

    public bool IsSucceeded()
    {
        return CurrentState == State.Succeeded;
    }
}