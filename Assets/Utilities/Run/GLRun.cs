﻿using GameToolkit.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLRun : GLSingletonBehaviour<GLRun> {

    protected override void OnSingletonInitialize() { }

    private LinkedList<IGLBaseTask> tasks = new LinkedList<IGLBaseTask>();
    private Queue<Action> lateUpdateTodo = new Queue<Action>();

    public static IGLBaseTask AddTask(IGLBaseTask task)
    {
        GetInstance().tasks.AddLast(task);
        return task;
    }

    public static GLTask<T> AddTask<T>(LocalizedText description, Action<GLTask<T>, int> stepper, Dictionary<string, string> requestContext, Action<GLTaskResult> onCompleted = null)
    {
        GLTask<T> task = new GLTask<T>(description, (GLTask<T> thisTask, int step) =>
        {
            stepper(thisTask, step);
        }, requestContext, onCompleted);
        GetInstance().tasks.AddLast(task);
        return task;
    }

    private void Update()
    {
        if (tasks.Count > 0)
        {
            LinkedListNode<IGLBaseTask> currentNode = tasks.First;
            while (currentNode != null)
            {
                IGLBaseTask task = currentNode.Value;
                if (task.IsCompleted())
                {
                    var temp = currentNode;
                    tasks.Remove(temp);
                } else
                {
                    task.Update();
                }
                currentNode = currentNode.Next;
            }
        }
    }

    public static void LateUpdate(Action method)
    {
        I.lateUpdateTodo.Enqueue(method);
    }

    public static void Coroutine(IEnumerator routine)
    {
        GetInstance().StartCoroutine(routine);
    }

    public static void NextFrame(Action method)
    {
        GetInstance().StartCoroutine(GetInstance().NextFrameCoroutine(method));
    }

    private IEnumerator NextFrameCoroutine(Action method)
    {
        yield return null;
        method();
    }

    public static void Delayed(float seconds, Action method)
    {
        GetInstance().StartCoroutine(GetInstance().DelayedCoroutine(seconds, method));
    }

    private IEnumerator DelayedCoroutine(float seconds, Action method)
    {
        yield return new WaitForSeconds(seconds);
        method();
    }

    private void LateUpdate()
    {
        while (lateUpdateTodo.Count > 0)
        {
            lateUpdateTodo.Dequeue()?.Invoke();
        }
    }
}
