﻿using GameToolkit.Localization;
using System;

public interface IGLBaseTask
{
    void Update();
    bool IsCompleted();
    bool IsFailed();
    bool IsSucceeded();
    LocalizedText GetDescription();
    GLTaskResult Result { get; }
}