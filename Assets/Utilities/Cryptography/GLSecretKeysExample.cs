﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GLSecretKeysExample
{
    public static string PasswordHash = "random key";
    public static string SaltKey;
    public static string VIKey = "16 characters long random key";
    public static string KeyStorePass = "";
    public static string KeyAliasPass = "";

    static GLSecretKeysExample()
    {
        SaltKey = "yet another random key";
    }
}
