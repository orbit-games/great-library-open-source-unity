﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class GLCryptography : GLSingletonBehaviour<GLCryptography> {
    
    public static Encoding DefaultEncoding = Encoding.UTF8;
    public const HashType DefaultHashType = HashType.SHA256;

    private ICryptoTransform encryptor;
    private ICryptoTransform decryptor;

    protected override void OnSingletonInitialize()
    {
        {
            byte[] keyBytes = new Rfc2898DeriveBytes(GLSecretKeys.PasswordHash, Encoding.ASCII.GetBytes(GLSecretKeys.SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(GLSecretKeys.VIKey));
        }

        {
            byte[] keyBytes = new Rfc2898DeriveBytes(GLSecretKeys.PasswordHash, Encoding.ASCII.GetBytes(GLSecretKeys.SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };
            decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(GLSecretKeys.VIKey));
        }
    }

    public enum HashType
    {
        MD5,
        SHA1,
        SHA256,
        SHA512
    }

    public static string Hash(string text, HashType hashType = DefaultHashType, Encoding encoding = null)
    {
        switch (hashType)
        {
            case HashType.MD5:
                return Hash(text, new MD5CryptoServiceProvider(), encoding);
            case HashType.SHA1:
                return Hash(text, new SHA1Managed(), encoding);
            case HashType.SHA256:
                return Hash(text, new SHA256Managed(), encoding);
            case HashType.SHA512:
                return Hash(text, new SHA512Managed(), encoding);
            default:
                throw new CryptographicException("Invalid hash alrgorithm.");
        }
    }

    public static string Hash(string text, string salt, HashType hashType = DefaultHashType, Encoding encoding = null)
    {
        return Hash(text + salt, hashType, encoding);
    }

    public static string Hash(string text, HashAlgorithm algorithm, Encoding encoding = null)
    {
        byte[] message = (encoding == null) ? DefaultEncoding.GetBytes(text) : encoding.GetBytes(text);
        byte[] hashValue = algorithm.ComputeHash(message);
        return Convert.ToBase64String(hashValue);
    }

    public static bool Compare(string original, string hashString, HashType hashType = DefaultHashType, Encoding encoding = null)
    {
        string originalHash = Hash(original, hashType, encoding);
        return (originalHash == hashString);
    }

    public static bool Compare(string original, string salt, string hashString, HashType hashType = DefaultHashType, Encoding encoding = null)
    {
        return Compare(original + salt, hashString, hashType, encoding);
    }

    public static string Encrypt(string plainText)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var cryptoStream = new CryptoStream(memoryStream, GetInstance().encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();
                cryptoStream.Close();
            }
            memoryStream.Close();
        }
        return Convert.ToBase64String(cipherTextBytes);
    }

    public static string Decrypt(string encryptedText)
    {
        byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
        var memoryStream = new MemoryStream(cipherTextBytes);
        var cryptoStream = new CryptoStream(memoryStream, GetInstance().decryptor, CryptoStreamMode.Read);
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        memoryStream.Close();
        cryptoStream.Close();
        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
    }
}
