﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLTransition : GLSingletonBehaviour<GLTransition> {

    //public enum Curves
    //{
    //    Linear,

    //    EasedStartMild,
    //    EasedStartStrong,
    //    EasedStartBack,

    //    EasedEndMild,
    //    EasedEndStrong,
    //    EasedEndBack,

    //    EasedFullyMild,
    //    EasedFullyStrong,
    //    EasedFullyBack,


    //}

    //[Header("Basic Curves")]
    //public AnimationCurve Linear;

    //public AnimationCurve EasedStartMild;
    //public AnimationCurve EasedStartStrong;
    //public AnimationCurve EasedStartAnticipate;

    //public AnimationCurve EasedEndMild;
    //public AnimationCurve EasedEndStrong;
    //public AnimationCurve EasedEndAnticipate;

    //public AnimationCurve EasedFullyMild;
    //public AnimationCurve EasedFullyStrong;
    //public AnimationCurve EasedFullyAnticipate;

    protected override void OnSingletonInitialize()
    {

    }

    public static void Disable(Component component)
    {
        Disable(component.gameObject);
    }

    public static void Disable(GameObject gameObject)
    {
        // temporarily just disable without any animations
        gameObject.SetActive(false);
    }

    public static void Out(Component component)
    {
        Out(component.gameObject);
    }

    public static void Out(GameObject gameObject)
    {
        // temporarily just disable without any animations
        gameObject.SetActive(false);
    }

    public static void In(Component component)
    {
        In(component.gameObject);
    }

    public static void In(GameObject gameObject)
    {
        // temporarily just enable without any animations
        gameObject.SetActive(true);
    }


}
