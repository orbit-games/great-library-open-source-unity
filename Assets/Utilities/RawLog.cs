﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RawLog : MonoBehaviour
{
    public Text textField;
    public List<Color> logtypeToColor;

    // Start is called before the first frame update
    void Start()
    {
        Application.logMessageReceived += HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        var newText = GLPool.placeCopy(textField, textField.transform.parent);
        newText.color = logtypeToColor[(int)type];
        newText.text = type + " ~ " + (logString.Length > 1000 ? logString.Substring(0, 1000) : logString) + "\n";
        newText.text += (type == LogType.Error || type == LogType.Exception) ? stackTrace + "\n" : "";
    }
}
