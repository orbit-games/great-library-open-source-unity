﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLPoolDynamicList : MonoBehaviour
{
    public enum PoolPlacement
    {
        Start, End, Specific
    }

    [Header("Settings")]
    public PoolPlacement childPlacement = PoolPlacement.End;
    public int specificChildPlacementIndex;
    public bool reverseOrder;

    private class PrefabSetup
    {
        public MonoBehaviour prefab;
        public Action<object> placeMethod;
        public Func<object, object> objectToUID;
        public Action<object, object> setupMethod;
        public Action<object, object> updateMethod;
    }

    private Dictionary<string, PrefabContext> prefabs = new Dictionary<string, PrefabContext>();
    private Action<object> placementLogic;

    public void SetPrefabPlacer<ListObject>(Action<Builder<ListObject>> placementLogic)
    {
        this.placementLogic = builder =>
        {
            placementLogic(builder as Builder<ListObject>);
        };
    }

    public void SetMainPrefab<ListObject, PrefabComponent>(
        PrefabComponent prefab,
        Func<AlgorithmState<ListObject>, object> objectToUID,
        Action<PrefabComponent, AlgorithmState<ListObject>> setupMethod) where PrefabComponent : MonoBehaviour
    {
        AddPrefab("mainPrefab", prefab, objectToUID, setupMethod);
    }

    public void AddPrefab<ListObject, PrefabComponent>(
        string key,
        PrefabComponent prefab,
        Func<AlgorithmState<ListObject>, object> objectToUID,
        Action<PrefabComponent, AlgorithmState<ListObject>> setupMethod) where PrefabComponent : MonoBehaviour
    {
        AddPrefab(key, prefab, objectToUID, setupMethod, setupMethod);
    }

    public void AddPrefab<ListObject, PrefabComponent>(
        string key,
        PrefabComponent prefab,
        Func<AlgorithmState<ListObject>, object> objectToUID,
        Action<PrefabComponent, AlgorithmState<ListObject>> setupMethod,
        Action<PrefabComponent, AlgorithmState<ListObject>> updateMethod) where PrefabComponent : MonoBehaviour
    {
        var newPrefabContext = prefabs.AddUnique(key, new PrefabContext
        {
            listContainer = transform
        });
        newPrefabContext.prefabSetup = new PrefabSetup
        {
            prefab = prefab,
            placeMethod = (state) =>
            {
                newPrefabContext.PlacePrefab<ListObject, PrefabComponent>(state as AlgorithmState<ListObject>);
            },
            objectToUID = state =>
            {
                return objectToUID((AlgorithmState<ListObject>)state);
            },
            setupMethod = (prefabInstance, algorithmState) =>
            {
                setupMethod((PrefabComponent)prefabInstance, (AlgorithmState<ListObject>)algorithmState);
            },
            updateMethod = (prefabInstance, algorithmState) =>
            {
                updateMethod((PrefabComponent)prefabInstance, (AlgorithmState<ListObject>)algorithmState);
            }
        };
    }

    private interface IHelperLooper<ListObject>
    {
        void Setup(GLPoolDynamicList dynamicList, List<ListObject> items);
        void Next();
        bool HasNext();
    }

    public class AlgorithmState<ListObject> : IHelperLooper<ListObject>
    {
        public GLPoolDynamicList DynamicList { get; protected set; }
        public List<ListObject> Items { get; protected set; }
        public int CurrentIndex { get; protected set; }
        public ListObject PreviousItem { get; protected set; }
        public ListObject CurrentItem { get; protected set; }
        public ListObject NextItem { get; protected set; }

        void IHelperLooper<ListObject>.Setup(GLPoolDynamicList dynamicList, List<ListObject> items)
        {
            DynamicList = dynamicList;
            Items = items;
            CurrentIndex = -1;
            PreviousItem = default(ListObject);
            CurrentItem = default(ListObject);
            NextItem = Items.GetOrDefault(CurrentIndex + 1);
        }

        void IHelperLooper<ListObject>.Next()
        {
            CurrentIndex++;
            PreviousItem = CurrentItem;
            CurrentItem = NextItem;
            NextItem = Items.GetOrDefault(CurrentIndex + 1);
        }

        bool IHelperLooper<ListObject>.HasNext()
        {
            return Items.Count > CurrentIndex + 1;
        }
    }

    public class Builder<ListObject> : AlgorithmState<ListObject>
    {
        public void PlaceMainPrefab()
        {
            PlacePrefab("mainPrefab");
        }
        public void PlacePrefab(string key)
        {
            var prefabContext = DynamicList.prefabs[key];
            prefabContext.prefabSetup.placeMethod(this);
        }
    }

    private class PrefabContext
    {
        public Transform listContainer;
        public PrefabSetup prefabSetup;
        public Dictionary<object, MonoBehaviour> instanceMapping = new Dictionary<object, MonoBehaviour>();
        public List<MonoBehaviour> currentInstances = new List<MonoBehaviour>();
        public HashSet<object> existingIDsNotCovered = new HashSet<object>();

        private T CreateInstance<T>(object id) where T : MonoBehaviour
        {
            var newInstance = GLPool.placeCopy(prefabSetup.prefab as T, listContainer.gameObject);
            instanceMapping.Add(id, newInstance);
            currentInstances.Add(newInstance);
            return newInstance;
        }

        private void RemoveInstance(object id)
        {
            var instance = instanceMapping.GetOrDefault(id);
            GLPool.removeCopy(instance);
            instanceMapping.Remove(id);
            currentInstances.Remove(instance);
        }

        public T GetInstance<T>(object id) where T : MonoBehaviour
        {
            return instanceMapping.GetOrDefault(id) as T;
        }

        public void Clear()
        {
            foreach (var instance in currentInstances)
            {
                GLPool.removeCopy(instance);
            }

            currentInstances.Clear();
            instanceMapping.Clear();
        }

        public void OnAlgorithmStart()
        {
            // remember all IDs we already have
            existingIDsNotCovered.Clear();
            existingIDsNotCovered.AddRangeUnique(instanceMapping.Keys);
        }

        public void OnAlgorithmEnd()
        {
            // remove those that we did not see in the given list
            foreach (var id in existingIDsNotCovered)
            {
                RemoveInstance(id);
            }
        }

        public void PlacePrefab<ListObject, PrefabComponent>(AlgorithmState<ListObject> state) where PrefabComponent : MonoBehaviour
        {
            var reverseOrder = state.DynamicList.reverseOrder;
            var id = prefabSetup.objectToUID(state);
            existingIDsNotCovered.Remove(id);

            // create objects that we did not know about yet
            if (!instanceMapping.ContainsKey(id))
            {
                var component = CreateInstance<PrefabComponent>(id);
                prefabSetup.setupMethod?.Invoke(component, state);
                state.DynamicList.SetSiblingIndex(component);
            }
            // update objects we already had
            else
            {
                var component = instanceMapping[id];
                prefabSetup.updateMethod?.Invoke(component as PrefabComponent, state);
                state.DynamicList.SetSiblingIndex(component);
            }
        }
    }

    public System.Collections.ObjectModel.ReadOnlyCollection<MonoBehaviour> GetReadOnlyInstancesCollection(string key)
    {
        return new System.Collections.ObjectModel.ReadOnlyCollection<MonoBehaviour>(prefabs[key].currentInstances);
    }
    
    private int currentInstancesCount;
    private int currentSiblingIndex;

    private void AlgorithmStart()
    {
        foreach (var prefabContext in prefabs.Values)
        {
            prefabContext.OnAlgorithmStart();
        }
    }

    private void DetermineTotalInstancesCount()
    {
        currentInstancesCount = 0;
        foreach (var prefabContext in prefabs.Values)
        {
            currentInstancesCount += prefabContext.currentInstances.Count;
        }
    }

    private void DetermineSiblingStartIndex()
    {
        DetermineTotalInstancesCount();

        currentSiblingIndex = 0;
        switch (childPlacement)
        {
            case PoolPlacement.End:
                currentSiblingIndex = transform.childCount - currentInstancesCount;
                break;
            case PoolPlacement.Specific:
                currentSiblingIndex = specificChildPlacementIndex;
                break;
            case PoolPlacement.Start:
                break;
        }
    }

    private void SetSiblingIndex(Component component)
    {
        component.transform.SetSiblingIndex(reverseOrder ? currentSiblingIndex : currentSiblingIndex++);
    }

    private void AlgorithmEnd()
    {
        foreach (var prefabContext in prefabs.Values)
        {
            prefabContext.OnAlgorithmEnd();
        }
    }

    public void Clear()
    {
        foreach (var prefabContext in prefabs.Values)
        {
            prefabContext.Clear();
        }
    }

    public void Sync<ListObject, PrefabComponent>(IEnumerable<ListObject> itemList) where PrefabComponent : MonoBehaviour
    {
        // start
        AlgorithmStart();

        // determine sibling start index
        DetermineSiblingStartIndex();

        // make list from enumerable
        var list = (itemList is List<ListObject>) ? (itemList as List<ListObject>) : new List<ListObject>(itemList);

        // create the builder
        IHelperLooper<ListObject> builder = new Builder<ListObject>();
        builder.Setup(this, list);

        // go through obtained list
        while (builder.HasNext())
        {
            builder.Next();
            placementLogic(builder);
        }

        // finalize
        AlgorithmEnd();
    }
}