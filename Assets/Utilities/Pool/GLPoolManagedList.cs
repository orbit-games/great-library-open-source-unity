﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLPoolManagedList : MonoBehaviour {

    public enum PoolPlacement
    {
        Start, End, Specific
    }

    private Dictionary<object, MonoBehaviour> instanceMapping = new Dictionary<object, MonoBehaviour>();
    private List<MonoBehaviour> currentInstances = new List<MonoBehaviour>();

    public Transform prefab;
    public PoolPlacement childPlacement = PoolPlacement.End;
    public int specificChildPlacementIndex;
    public bool reverseOrder;

    public void Clear()
    {
        foreach (var instance in currentInstances)
        {
            GLPool.removeCopy(instance);
        }

        currentInstances.Clear();
        instanceMapping.Clear();
    }

    public void SyncNewAndRemovedOnly<W, T>(IEnumerable<W> list, Func<W, object> idGetter, Action<W, T> setupMethod) where T : MonoBehaviour
    {
        Sync(list, idGetter, setupMethod, null);
    }

    public void SyncFully<W, T>(IEnumerable<W> list, Func<W, object> idGetter, Action<W, T> setupMethod) where T : MonoBehaviour
    {
        Sync(list, idGetter, setupMethod, setupMethod);
    }

    private void Sync<W, T>(IEnumerable<W> list, Func<W, object> idGetter, Action<W, T> setupMethod, Action<W, T> updateMethod) where T : MonoBehaviour
    {
        // remember all IDs we already have
        var existingIDsNotCovered = new HashSet<object>(instanceMapping.Keys);

        // determine the start sibling index
        var siblingIndex = 0;

        switch (childPlacement)
        {
            case PoolPlacement.End:
                siblingIndex = transform.childCount - currentInstances.Count;
                break;
            case PoolPlacement.Specific:
                siblingIndex = specificChildPlacementIndex;
                break;
            case PoolPlacement.Start:
                break;
        }

        // go through obtained list
        foreach (var item in list)
        {
            var id = idGetter(item);
            existingIDsNotCovered.Remove(id);

            // create objects that we did not know about yet
            if (!instanceMapping.ContainsKey(id))
            {
                var component = CreateInstance<T>(id);
                setupMethod?.Invoke(item, component as T);

                component.transform.SetSiblingIndex(reverseOrder ? siblingIndex : siblingIndex++);
            }
            // update objects we already had
            else
            {
                var component = instanceMapping[id];
                updateMethod?.Invoke(item, component as T);

                component.transform.SetSiblingIndex(reverseOrder ? siblingIndex : siblingIndex++);
            }
        }

        // remove those that we did not see in the given list
        foreach (var id in existingIDsNotCovered)
        {
            RemoveInstance(id);
        }
    }

    private T CreateInstance<T>(object id) where T : MonoBehaviour
    {
        var newInstance = GLPool.placeCopy(prefab.GetComponent<T>(), this);
        instanceMapping.Add(id, newInstance);
        currentInstances.Add(newInstance);
        return newInstance;
    }

    private void RemoveInstance(object id)
    {
        var instance = instanceMapping[id];
        GLPool.removeCopy(instance);
        instanceMapping.Remove(id);
        currentInstances.Remove(instance);
    }

    public T GetInstance<T>(object id) where T : MonoBehaviour
    {
        return instanceMapping[id] as T;
    }
}
