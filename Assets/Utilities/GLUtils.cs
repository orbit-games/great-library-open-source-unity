﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GLUtils {
    
    public static void Swap<T>(ref T swap1, ref T swap2)
    {
        T temp = swap1;
        swap1 = swap2;
        swap2 = temp;
    }
}
