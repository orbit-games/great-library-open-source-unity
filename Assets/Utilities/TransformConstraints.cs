﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TransformConstraints  {

    [Serializable]
    public class Constraints
    {
        public bool enabled;
        [ConditionalField("enabled")]
        public bool separateAxis;
        [ConditionalField("enabled")]
        public Vector3 min;
        [ConditionalField("enabled")]
        public Vector3 max;
    }
    
    public Constraints position;
    public Constraints rotation;
    public Constraints scale;
}
