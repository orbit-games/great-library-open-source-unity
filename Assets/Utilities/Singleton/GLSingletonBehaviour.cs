﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GLAbstractSingleton : MonoBehaviour {
    public abstract void _Initialize();
}

public abstract class GLSingletonBehaviour<T> : GLAbstractSingleton where T : GLSingletonBehaviour<T>
{
    private static bool allInstancesGathered = false;
    private static bool quiting = false;
    private static List<T> instances = new List<T>();
    [NonSerialized]
    private bool _initialized = false;
    private void Awake()
    {
        _Initialize();
    }

    /// <summary>
    /// Shortcut for GetInstance()
    /// </summary>
    public static T I
    {
        get
        {
            return GetInstance();
        }
    }

    private void OnApplicationQuit()
    {
        quiting = true;
    }

    /// <summary>
    /// Get the one and only instance of the desired class. Creates the object if it doesn't exist yet or can't find it.
    /// </summary>
    /// <returns></returns>
    public static T GetInstance(bool thisTypeOnly = false)
    {
        if (!thisTypeOnly && Application.isPlaying && !allInstancesGathered)
        {
            GLAbstractSingleton[] objects = Resources.FindObjectsOfTypeAll<GLAbstractSingleton>();
            foreach (var obj in objects)
            {
                obj._Initialize();
            }
            allInstancesGathered = true;
        }
        if (instances.Count == 0) {
            // then try to find by searching scene
            T[] objects = Resources.FindObjectsOfTypeAll<T>();
            foreach (var obj in objects)
            {
                Debug.LogWarning("Instance of " + typeof(T).ToString() + " was not yet set. Found it, though!");
                obj._Initialize();
            }
        }
        if (instances.Count == 1)
        {
            // get the only instance known to us
            return ReturnUndestroyed(instances[0], thisTypeOnly);
        }
        if (instances.Count > 1) 
        {
            // get the one that is currently active
            // this singleton script is assuming there is always only 1 ever active
            for (int i = 0; i < instances.Count; i++)
            {
                if (instances[i] == null)
                {
                    // an object was destroyed, let's handle it and try getting a new one
                    return ReturnUndestroyed(instances[i], thisTypeOnly);
                }
                if (instances[i].gameObject.activeInHierarchy)
                {
                    return instances[i];
                }
            }
            // nothing was active, so let's just get the first
            return ReturnUndestroyed(instances[0], thisTypeOnly);
        }

        if (!quiting)
        {
            Debug.LogWarning("Instance of " + typeof(T).ToString() + " does not exist!");
        }
        return null;
    }

    private static T ReturnUndestroyed(T instanceToCheck, bool thisTypeOnly = false)
    {
        if (instanceToCheck != null) return instanceToCheck;
        instances.RemoveAll(instance => instance == null);

        if (!quiting)
        {
            Debug.LogWarning("Instance of " + typeof(T).ToString() + " was destroyed! Trying to find a new one");
            return GetInstance(thisTypeOnly);
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Helper method to initialize only once
    /// </summary>
    public override void _Initialize()
    {
        if (!_initialized)
        {
            _initialized = true;
            instances.Add(this as T);
            OnSingletonInitialize();
        }
    }

    /// <summary>
    /// Called only once, whenever this behaviour is awoken or created.
    /// </summary>
    abstract protected void OnSingletonInitialize();
}
