﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLResources {

    private static bool loaded = false;
    public static void LoadAll(bool force = false)
    {
        if (loaded && !force) return;
        loaded = true;
        Resources.LoadAll("");
    }
    public static T[] FindObjectsOfTypeAll<T>() where T : Object
    {
        LoadAll();
        return Resources.FindObjectsOfTypeAll<T>();
    }
    public static Object[] FindObjectsOfTypeAll(System.Type type)
    {
        LoadAll();
        return Resources.FindObjectsOfTypeAll(type);
    }
}
