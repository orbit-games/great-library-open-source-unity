﻿using IO.Swagger.Model;
using System;

public class GLUserCourseDictionary<T> : TwoKeysDictionary<string, string, T>{

    public T this[User key1, Course key2]
    {
        get { return base[Tuple.Create(key1.Ref, key2.Ref)]; }
        set { base[Tuple.Create(key1.Ref, key2.Ref)] = value; }
    }

    public void Add(User key1, Course key2, T value)
    {
        base.Add(Tuple.Create(key1.Ref, key2.Ref), value);
    }

    public bool ContainsKey(User key1, Course key2)
    {
        return base.ContainsKey(Tuple.Create(key1.Ref, key2.Ref));
    }
}
