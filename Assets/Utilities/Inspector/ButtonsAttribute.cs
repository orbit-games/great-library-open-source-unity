﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ButtonsAttribute : PropertyAttribute
{
    public List<Btn> buttons = new List<Btn>();

    public ButtonsAttribute(string method)
    {
        buttons.Add(new Btn(method, method));
    }

    public ButtonsAttribute(string[] labels, string[] methods)
    {
        for (int i = 0; i < labels.Length; i++)
        {
            buttons.Add(new Btn(labels[i], methods[i]));
        }
    }

    public ButtonsAttribute(params string[] labelMethodPairs)
    {
        for (int i = 0; i < labelMethodPairs.Length; i += 2)
        {
            buttons.Add(new Btn(labelMethodPairs[i], labelMethodPairs[i + 1]));
        }
    }

    public ButtonsAttribute(Btn btn)
    {
        buttons.Add(btn);
    }

    public ButtonsAttribute(params Btn[] btns)
    {
        foreach (var btn in btns)
        {
            buttons.Add(btn);
        }
    }
}

public class Btn
{
    public string method;
    public string label;

    public Btn(string method)
    {
        this.label = method;
        this.method = method;
    }

    public Btn(string label, string method)
    {
        this.label = label;
        this.method = method;
    }
}