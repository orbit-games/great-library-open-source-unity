﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection;

// built along with http://www.sharkbombs.com/2015/02/17/unity-editor-enum-flags-as-toggle-buttons/

[CustomPropertyDrawer(typeof(ButtonsAttribute))]
public class ButtonDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + 10f;
    }
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUI.LabelField(rect, label);
        rect.x += EditorGUIUtility.labelWidth;
        rect.width -= EditorGUIUtility.labelWidth;

        List<Btn> buttons = (attribute as ButtonsAttribute).buttons;
        rect.width /= buttons.Count;

        foreach (Btn btn in buttons)
        {
            bool clicked = GUI.Toggle(rect, false, btn.label, "Button");
            if (clicked)
            {
                System.Object obj = GetParent(property);
                MethodInfo[] methods = obj.GetType().GetMethods();
                foreach (var method in methods)
                {
                    if (method.Name.Equals(btn.method))
                    {
                        var parameters = method.GetParameters();
                        var allOptional = true;
                        foreach (var parameter in parameters)
                        {
                            allOptional = allOptional && parameter.IsOptional;
                        }
                        if (allOptional)
                        {
                            method.Invoke(obj, new object[0]);
                            break;
                        }
                    }
                }
            }
            rect.x += rect.width;
        }
    }

    // source: http://answers.unity3d.com/questions/425012/get-the-instance-the-serializedproperty-belongs-to.html

    public object GetParent(SerializedProperty prop)
    {
        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements.Take(elements.Length - 1))
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("["));
                var index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = GetValue(obj, elementName, index);
            }
            else
            {
                obj = GetValue(obj, element);
            }
        }
        return obj;
    }

    public object GetValue(object source, string name)
    {
        if (source == null)
            return null;
        var type = source.GetType();
        var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
        {
            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p == null)
                return null;
            return p.GetValue(source, null);
        }
        return f.GetValue(source);
    }

    public object GetValue(object source, string name, int index)
    {
        var enumerable = GetValue(source, name) as IEnumerable;
        var enm = enumerable.GetEnumerator();
        while (index-- >= 0)
            enm.MoveNext();
        return enm.Current;
    }
}