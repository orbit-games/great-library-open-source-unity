﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GLEditorExtensions {

    public static void SaveAsset(Object asset)
    {
#if UNITY_EDITOR
        //if (!Application.isPlaying)
        {
            if (asset is Component && IsInScene((Component)asset))
                return;
            if (asset is GameObject && IsInScene((GameObject)asset))
                return;
            if (asset is Component && IsPrefabInstance(((Component)asset).gameObject))
                return;
            if (asset is GameObject && IsPrefabInstance((GameObject)asset))
                return;

            Debug.Log("Saving asset: " + asset);
            UnityEditor.EditorUtility.SetDirty(asset);
            UnityEditor.AssetDatabase.SaveAssets();
        }
#else
#endif
    }

    public static bool IsInScene(GameObject gameObject)
    {
        return !gameObject.scene.name.IsNullOrEmpty();
    }

    public static bool IsInScene(Component obj)
    {
        return IsInScene(obj.gameObject);
    }

    public static bool IsPrefabInstance(GameObject gameObject)
    {
#if UNITY_EDITOR
        return UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(gameObject) != null && UnityEditor.PrefabUtility.GetPrefabObject(gameObject.transform) != null;
#else
        return false;
#endif
    }

    public static bool IsDisconnectedPrefabInstance(GameObject gameObject)
    {
#if UNITY_EDITOR
        return UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(gameObject) != null && UnityEditor.PrefabUtility.GetPrefabObject(gameObject.transform) == null;
#else
        return false;
#endif
    }

    public static bool IsPrefabOriginal(GameObject gameObject)
    {
#if UNITY_EDITOR
        return UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(gameObject) == null && UnityEditor.PrefabUtility.GetPrefabObject(gameObject.transform) != null;
#else
        return false;
#endif
    }

    public static Object GetPrefabOriginal(GameObject gameObject)
    {
#if UNITY_EDITOR
        return UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(gameObject);
#else
        return null;
#endif
    }
}
