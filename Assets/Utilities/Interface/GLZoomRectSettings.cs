﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GLZoomRect))]
public class GLZoomRectSettings : MonoBehaviour
{
    public float minZoom = .3f;
    public float maxZoom = 3f;
    public float zoomLerpSpeed = 10f;
    public float mouseWheelSensitivity = 1f;
    public Slider slider;
}
