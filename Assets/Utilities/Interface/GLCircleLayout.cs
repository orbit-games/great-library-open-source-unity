﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GLCircleLayout : MonoBehaviour {

    public List<RectTransform> items;
    public float diameter;
    public float midScale;
    public float startPhase;

    public void Update()
    {
        var activeCount = items.Count(i => i.gameObject.activeSelf);

        Vector2 currentAngle = new Vector2(diameter, 0f);
        currentAngle = Quaternion.AngleAxis(startPhase, Vector3.forward) * currentAngle;

        if (activeCount == 0) return;

        var scale = Vector3.one * (1f + (midScale - 1f) / activeCount);

        if (activeCount == 1) currentAngle = Vector2.zero;

        foreach (var item in items)
        {
            if (item.gameObject.activeSelf)
            {
                item.anchoredPosition = currentAngle;
                item.localScale = scale;

                currentAngle = Quaternion.AngleAxis(360f / activeCount, Vector3.forward) * currentAngle;
            }
        }

    }
}
