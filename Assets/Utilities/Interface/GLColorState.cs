﻿using Fenderrio.ImageWarp;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GLColorState : MonoBehaviour {
    
    [Header("References")]
    public Image image;
    public TMP_Text textField;
    public RawImageWarp warpImage;

    [Header("Settings")]
    public Color state1;
    public Color state2;

    [Header("Helpers")]
    [Buttons("c = State 1", "ReadState1", "c = State 2", "ReadState2")]
    public ButtonsContainer read;
    [Buttons("State 1 = c", "RememberToState1", "State 2 = c", "RememberToState2")]
    public ButtonsContainer remember;
    [Buttons("Swap")]
    public ButtonsContainer swap;
    [Buttons("ResetReferences")]
    public ButtonsContainer references;

    public void RememberToState1() { state1 = GetColor(); }
    public void RememberToState2() { state2 = GetColor(); }
    public void ReadState1() { SetColor(state1); }
    public void ReadState2() { SetColor(state2); }
    public void Swap() { var temp = state1; state1 = state2; state2 = temp; }
    public void ResetReferences()
    {
        image = GetComponent<Image>();
        warpImage = GetComponent<RawImageWarp>();
        textField = GetComponent<TMP_Text>();
    }

    void Reset ()
    {
        ResetReferences();
        RememberToState1();
    }

    public static void BroadcastColorState(Component component, bool firstState)
    {
        BroadcastColorState(component.gameObject, firstState);
    }

    public static void BroadcastColorState(GameObject gameObject, bool firstState)
    {
        if (firstState)
        {
            gameObject.BroadcastMessage("SetFirstColorState", SendMessageOptions.DontRequireReceiver);
        }
        else
        {
            gameObject.BroadcastMessage("SetSecondColorState", SendMessageOptions.DontRequireReceiver);
        }
    }

    public void SetColorState(bool firstState)
    {
        if (firstState) SetFirstColorState();
        else SetSecondColorState();
    }

    public void SetFirstColorState()
    {
        SetColor(state1);
    }

    public void SetSecondColorState()
    {
        SetColor(state2);
    }

    private Color GetColor()
    {
        if (image != null)
        {
            return image.color;
        }
        if (textField != null)
        {
            return textField.color;
        }
        if (warpImage != null)
        {
            return warpImage.color;
        }
        return default(Color);
    }

    private void SetColor(Color color)
    {
        if (image != null)
        {
            image.color = color;
        }
        if (textField != null)
        {
            textField.color = color;
        }
        if (warpImage != null)
        {
            warpImage.color = color;
        }
    }
}
