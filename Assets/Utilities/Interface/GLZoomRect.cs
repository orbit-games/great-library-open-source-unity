﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Source:
/// https://answers.unity.com/questions/1280592/pinch-and-zoom-functionality-on-canvas-ui-images.html
/// </summary>
[RequireComponent(typeof(GLZoomRectSettings))]
public class GLZoomRect : ScrollRect
{
    float _currentZoom = 1;
    Vector2 _startPinchCenterPosition;
    Vector2 _startPinchScreenPosition;
    RectTransform myRect;
    Camera mainCamera;

    GLZoomRectSettings settings;

    protected override void Awake()
    {
        base.Awake();
        scrollSensitivity = 0;
        myRect = GetComponent<RectTransform>();
        settings = GetComponent<GLZoomRectSettings>();
        mainCamera = Camera.main;
    }

    public void SetZoom(float zoom)
    {
        _currentZoom = settings.minZoom + zoom * (settings.maxZoom - settings.minZoom);
        DeterminePinchCenterPosition(new Vector2(Screen.width / 2, Screen.height / 2));
    }

    private void DeterminePinchCenterPosition(Vector2 zoomScreenPosition)
    {
        _startPinchScreenPosition = zoomScreenPosition;
        Vector2 tempStartPinchCenterPosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(myRect, _startPinchScreenPosition, mainCamera, out tempStartPinchCenterPosition);
        _startPinchCenterPosition = content.InverseTransformPoint(myRect.TransformPoint(tempStartPinchCenterPosition));
    }

    private void Update()
    {
        float scrollWheelInput = Input.GetAxis("Mouse ScrollWheel");
        if (Mathf.Abs(scrollWheelInput) > float.Epsilon)
        {
            _currentZoom *= 1 + scrollWheelInput * settings.mouseWheelSensitivity;
            _currentZoom = Mathf.Clamp(_currentZoom, settings.minZoom, settings.maxZoom);
            settings.slider.value = (_currentZoom - settings.minZoom) / (settings.maxZoom - settings.minZoom);

            DeterminePinchCenterPosition(Input.mousePosition);
        }

        if (Mathf.Abs(content.localScale.x - _currentZoom) > 0.001f)
        {
            var oldPos = content.LocalPointWithin(_startPinchCenterPosition, myRect);
            content.localScale = Vector3.Lerp(content.localScale, Vector3.one * _currentZoom, settings.zoomLerpSpeed * Time.deltaTime);
            var newPos = content.LocalPointWithin(_startPinchCenterPosition, myRect);
            content.localPosition += oldPos - newPos;
        }
    }

    static void SetPivot(RectTransform rectTransform, Vector2 pivot)
    {
        if (rectTransform == null) return;

        Vector2 size = rectTransform.rect.size;
        Vector2 deltaPivot = rectTransform.pivot - pivot;
        Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y) * rectTransform.localScale.x;
        rectTransform.pivot = pivot;
        rectTransform.localPosition -= deltaPosition;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(content.TransformPoint(_startPinchCenterPosition), 0.5f);
    }
}