﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLImageAnimation : MonoBehaviour
{
    public enum StartBehaviour {
        Restart, Random, Continue
    }

    [Header("References")]
    public Image image;
    public List<Sprite> sprites = new List<Sprite>();
    
    [Header("Settings")]
    public float secondsPerSprite = 1/15f;
    public StartBehaviour startBehaviour;

    private float currentTime = 0f;
    private int currentSprite = 0;

    private void OnEnable()
    {
        switch (startBehaviour)
        {
            case StartBehaviour.Restart:
                currentTime = 0f;
                currentSprite = 0;
                break;
            case StartBehaviour.Random:
                currentTime = 0f;
                currentSprite = Random.Range(0, sprites.Count - 1);
                break;
            case StartBehaviour.Continue:
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        while (currentTime > secondsPerSprite)
        {
            currentTime -= secondsPerSprite;
            currentSprite = (currentSprite + 1) % sprites.Count;
        }
        image.sprite = sprites[currentSprite];
    }


}
