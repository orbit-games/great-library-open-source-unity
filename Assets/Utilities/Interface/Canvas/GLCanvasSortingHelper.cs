﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLCanvasSortingHelper : MonoBehaviour {

    public static readonly int stepsize = 1;
    public static readonly int startValue = 0;
    public static readonly int maxValue = 30;

    [Buttons("Apply sorting", "ApplySorting")]
    public ButtonsContainer button;
    public GLCanvasSortingHelper previousHelper;
    public GLCanvasSortingHelper nextHelper;

    public void ApplySorting()
    {
        if (nextHelper != null)
        {
            nextHelper.previousHelper = this;
            nextHelper.ApplySorting();
        }
        else
        {
            ApplySortingNow();
        }
    }

    private int ApplySortingNow()
    {
        int currentValue = startValue;
        if (previousHelper != null)
        {
            currentValue = previousHelper.ApplySortingNow();
        }

        for (var i = 0; i < transform.childCount; i++)
        {
            var canvas = transform.GetChild(i).GetComponent<Canvas>();
            if (canvas != null)
            {
                canvas.sortingOrder = currentValue;
                //canvas.planeDistance = 10;
                canvas.planeDistance = maxValue - currentValue;
                currentValue += stepsize;
            }
        }

        return currentValue;
    }
}
